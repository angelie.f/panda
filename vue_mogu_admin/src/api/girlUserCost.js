import request from '@/utils/request'

export function getUserCostList(params) {
  return request({
    url: process.env.ADMIN_API + '/girlUserCost/getList',
    method: 'post',
    data: params
  })
}

export function addUserCost(params) {
  return request({
    url: process.env.ADMIN_API + '/girlUserCost/add',
    method: 'post',
    data: params
  })
}

export function editUserCost(params) {
  return request({
    url: process.env.ADMIN_API + '/girlUserCost/edit',
    method: 'post',
    data: params
  })
}

export function deleteUserCost(params) {
  console.log("删除="+JSON.stringify(params))
  return request({
    url: process.env.ADMIN_API + '/girlUserCost/delete',
    method: 'post',
    data: params
  })
}
