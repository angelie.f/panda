import request from '@/utils/request'

export function getPictureAtlasList(params) {
  return request({
    url: process.env.ADMIN_API + '/pictureAtlas/getList',
    method: 'post',
    data: params
  })
}

export function getAtlasListBySort(params) {
  return request({
    url: process.env.ADMIN_API + '/pictureAtlas/getAtlasListBySort',
    method: 'post',
    data: params
  })
}

export function getResourceList() {
  return request({
    url: process.env.ADMIN_API + '/pictureAtlas/getResourceList',
    method: 'post'
  })
}

export function addPictureAtlas(params) {
  return request({
    url: process.env.ADMIN_API + '/pictureAtlas/add',
    method: 'post',
    data: params
  })
}

export function editPictureAtlas(params) {
  // console.log("图集编辑params="+params)
  return request({
    url: process.env.ADMIN_API + '/pictureAtlas/edit',
    method: 'post',
    data: params
  })
}

export function deletePictureAtlas(params) {
  return request({
    url: process.env.ADMIN_API + '/pictureAtlas/delete',
    method: 'post',
    data: params
  })
}

export function stickPictureAtlas(params) {
  return request({
    url: process.env.ADMIN_API + '/pictureAtlas/stick',
    method: 'post',
    data: params
  })
}

export function getPictureAtlasByUid(params) {
  return request({
    url: process.env.ADMIN_API + '/pictureAtlas/getPictureAtlasByUid',
    method: 'post',
    data: params
  })
}

export function updateTag(params) {
  return request({
    url: process.env.ADMIN_API + '/pictureAtlas/updateTag',
    method: 'post',
    data: params
  })
}
export function upOrOut(params) {
  return request({
    url: process.env.ADMIN_API + '/pictureAtlas/upOrOut',
    method: 'post',
    data: params
  })
}
