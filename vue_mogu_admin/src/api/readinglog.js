import request from '@/utils/request'

export function getReadingLogList(params) {
  return request({
    url: process.env.ADMIN_API + '/readinglog/getList',
    method: 'post',
    data: params
  })
}
export function getSourceList(params) {
  return request({
    url: process.env.ADMIN_API + '/booksProject/sources',
    method: 'post',
    data: params
  })
}
