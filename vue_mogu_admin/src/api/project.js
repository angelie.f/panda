import request from '@/utils/request'

export function getProjectList(params) {
  return request({
    url: process.env.ADMIN_API + '/project/getList',
    method: 'post',
    data: params
  })
}

export function addProject(params) {
  return request({
    url: process.env.ADMIN_API + '/project/add',
    method: 'post',
    data: params
  })
}

export function editProject(params) {
  return request({
    url: process.env.ADMIN_API + '/project/edit',
    method: 'post',
    data: params
  })
}

export function deleteBatchProject(params) {
  return request({
    url: process.env.ADMIN_API + '/project/deleteBatch',
    method: 'post',
    data: params
  })
}
