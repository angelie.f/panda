import request from '@/utils/request'

export function getPicList(params) {
  return request({
    url: process.env.ADMIN_API + '/girlPicture/getList',
    method: 'post',
    data: params
  })
}
export function editGirlPicList(params) {
  return request({
    url: process.env.ADMIN_API + '/girlPicture/editGirlPicList',
    method: 'post',
    data: params
  })
}
