import request from '@/utils/request'

export function getBooksList(params) {
  return request({
    url: process.env.ADMIN_API + '/booksAdmin/getList',
    method: 'post',
    data: params
  })
}



