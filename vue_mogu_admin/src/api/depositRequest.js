import request from '@/utils/request'

export function getOrderRequestList(params) {
  return request({
    url: process.env.ADMIN_API + '/pandaUser/getDepositList',
    method: 'post',
    data: params
  })
}

export function getDepositTotal(params) {
  return request({
    url: process.env.ADMIN_API + '/pandaUser/getDepositTotal',
    method: 'post',
    data: params
  })
}
export function getSourceList(params) {
  return request({
    url: process.env.ADMIN_API + '/pandaProject/sources',
    method: 'post',
    data: params
  })
}
