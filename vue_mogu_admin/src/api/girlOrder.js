import request from '@/utils/request'

export function getOrderList(params) {
  // console.log("params="+JSON.stringify(params))
  return request({
    url: process.env.ADMIN_API + '/girlOrder/getList',
    method: 'post',
    data: params
  })
}
export function getAmountList(params) {
  return request({
    url: process.env.ADMIN_API + '/girlOrder/amounts',
    method: 'post',
    data: params
  })
}
export function getVipLevelList(params) {
  return request({
    url: process.env.ADMIN_API + '/girlOrder/vipLevels',
    method: 'post',
    data: params
  })
}

export function getSourceList(params) {
  return request({
    url: process.env.ADMIN_API + '/girlOrder/sources',
    method: 'post',
    data: params
  })
}


export function editOrder(params) {
  return request({
    url: process.env.ADMIN_API + '/girlOrder/edit',
    method: 'post',
    data: params
  })
}
export function sumAndCount(params) {
  return request({
    url: process.env.ADMIN_API + '/girlOrder/sumAndCount',
    method: 'post',
    data: params
  })
}
