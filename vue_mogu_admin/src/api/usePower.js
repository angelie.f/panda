import request from '@/utils/request'

export function getUsePowerList(params) {
  // console.log("params="+JSON.stringify(params))
  return request({
    url: process.env.ADMIN_API + '/usePower/getList',
    method: 'post',
    data: params
  })
}

export function editUsePower(params) {
  return request({
    url: process.env.ADMIN_API + '/usePower/edit',
    method: 'post',
    data: params
  })
}

export function addUsePower(params) {
  return request({
    url: process.env.ADMIN_API + '/usePower/add',
    method: 'post',
    data: params
  })
}

