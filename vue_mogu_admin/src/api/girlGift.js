import request from '@/utils/request'

export function getGirlGiftList(params) {
  return request({
    url: process.env.ADMIN_API + '/girlGift/getList',
    method: 'post',
    data: params
  })
}



