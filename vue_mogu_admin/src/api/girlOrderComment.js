import request from '@/utils/request'

export function getGirlOrderCommentList(params) {
  return request({
    url: process.env.ADMIN_API + '/girlOrderComment/getList',
    method: 'post',
    data: params
  })
}

export function editGirlOrderComment(params) {
  return request({
    url: process.env.ADMIN_API + '/girlOrderComment/edit',
    method: 'post',
    data: params
  })
}


export function addGirlOrderComment(params) {
  return request({
    url: process.env.ADMIN_API + '/girlOrderComment/create',
    method: 'post',
    data: params
  })
}
export function addCommentGirlOrderComment(params) {
  return request({
    url: process.env.ADMIN_API + '/girlOrderComment/createComment',
    method: 'post',
    data: params
  })
}



