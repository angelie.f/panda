import request from '@/utils/request'

export function getSettingList(params) {
  // console.log("params="+JSON.stringify(params))
  return request({
    url: process.env.ADMIN_API + '/booksSetting/getList',
    method: 'post',
    data: params
  })
}


export function editSetting(params) {
  return request({
    url: process.env.ADMIN_API + '/booksSetting/edit',
    method: 'post',
    data: params
  })
}


