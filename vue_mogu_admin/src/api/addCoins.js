import request from '@/utils/request'

export function getAddCoinList(params) {
  return request({
    url: process.env.ADMIN_API + '/gameAddUserCoins/getCoinsList',
    method: 'post',
    data: params
  })
}

export function addCoins(params) {
  return request({
    url: process.env.ADMIN_API + '/gameAddUserCoins/add',
    method: 'post',
    data: params
  })
}
