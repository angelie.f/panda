import request from '@/utils/request'

export function getChapterList(params) {
  return request({
    url: process.env.ADMIN_API + '/booksChapter/getList',
    method: 'post',
    data: params
  })
}



