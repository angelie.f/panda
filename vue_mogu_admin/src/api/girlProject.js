import request from '@/utils/request'

export function getProjectList(params) {
  return request({
    url: process.env.ADMIN_API + '/girlProject/getList',
    method: 'post',
    data: params
  })
}

export function addProject(params) {
  return request({
    url: process.env.ADMIN_API + '/girlProject/add',
    method: 'post',
    data: params
  })
}

export function editProject(params) {
  return request({
    url: process.env.ADMIN_API + '/girlProject/edit',
    method: 'post',
    data: params
  })
}
export function initPorject(params) {
  return request({
    url: process.env.ADMIN_API + '/girlProject/initPorject',
    method: 'post',
    data: params
  })
}
