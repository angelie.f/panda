import request from '@/utils/request'

export function getTagList(params) {
  // console.log("params="+JSON.stringify(params))
  return request({
    url: process.env.ADMIN_API + '/girlTag/getList',
    method: 'post',
    data: params
  })
}

export function addTag(params) {
  return request({
    url: process.env.ADMIN_API + '/girlTag/add',
    method: 'post',
    data: params
  })
}

export function editTag(params) {
  return request({
    url: process.env.ADMIN_API + '/girlTag/edit',
    method: 'post',
    data: params
  })
}

export function deleteTag(params) {
  return request({
    url: process.env.ADMIN_API + '/girlTag/delete',
    method: 'post',
    data: params
  })
}
