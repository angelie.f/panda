import request from '@/utils/request'

export function getGirlSortList(params) {
  return request({
    url: process.env.ADMIN_API + '/girlSort/getList',
    method: 'post',
    data: params
  })
}

export function getResourceList() {
  return request({
    url: process.env.ADMIN_API + '/girlSort/getResourceList',
    method: 'post'
  })
}

export function addGirlSort(params) {
  return request({
    url: process.env.ADMIN_API + '/girlSort/add',
    method: 'post',
    data: params
  })
}

export function editGirlSort(params) {
  return request({
    url: process.env.ADMIN_API + '/girlSort/edit',
    method: 'post',
    data: params
  })
}

export function deleteSort(params) {
  return request({
    url: process.env.ADMIN_API + '/girlSort/delete',
    method: 'post',
    data: params
  })
}

