import request from '@/utils/request'

export function getGameConfigList(params) {
  return request({
    url: process.env.ADMIN_API + '/gameConfig/getGameConfigList',
    method: 'post',
    data: params
  })
}
export function editGameConfig(params) {
  return request({
    url: process.env.ADMIN_API + '/gameConfig/edit',
    method: 'post',
    data: params
  })
}
export function changeAvailable(params) {
  return request({
    url: process.env.ADMIN_API + '/gameConfig/changeAvailable',
    method: 'post',
    data: params
  })
}
export function getGameTrial(params) {
  return request({
    url: process.env.ADMIN_API + '/gameConfig/getGameTrial',
    method: 'post',
    data: params
  })
}
export function getGameTypeList(params) {
  return request({
    url: process.env.ADMIN_API + '/gameType/gameTypes',
    method: 'post',
    data: params
  })
}

