import request from '@/utils/request'

export function getTagList(params) {
  // console.log("params="+JSON.stringify(params))
  return request({
    url: process.env.ADMIN_API + '/pictureTag/getList',
    method: 'post',
    data: params
  })
}

export function addTag(params) {
  return request({
    url: process.env.ADMIN_API + '/pictureTag/add',
    method: 'post',
    data: params
  })
}

export function editTag(params) {
  return request({
    url: process.env.ADMIN_API + '/pictureTag/edit',
    method: 'post',
    data: params
  })
}

export function deleteBatchTag(params) {
  return request({
    url: process.env.ADMIN_API + '/pictureTag/deleteBatch',
    method: 'post',
    data: params
  })
}

export function stickTag(params) {
  return request({
    url: process.env.ADMIN_API + '/pictureTag/stick',
    method: 'post',
    data: params
  })
}

export function tagSortByClickCount(params) {
  return request({
    url: process.env.ADMIN_API + '/pictureTag/tagSortByClickCount',
    method: 'post',
    params
  })
}

export function tagSortByCite(params) {
  return request({
    url: process.env.ADMIN_API + '/pictureTag/tagSortByCite',
    method: 'post',
    params
  })
}
