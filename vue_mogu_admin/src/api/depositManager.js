import request from '@/utils/request'

export function getDepositConfigList(params) {
  return request({
    url: process.env.ADMIN_API + '/depositConfig/getDCList',
    method: 'post',
    data: params
  })
}

export function addDepositConfig(params) {
  return request({
    url: process.env.ADMIN_API + '/depositConfig/add',
    method: 'post',
    data: params
  })
}

export function editDepositConfig(params) {
  return request({
    url: process.env.ADMIN_API + '/depositConfig/edit',
    method: 'post',
    data: params
  })
}

export function deleteDepConfig(params) {
  return request({
    url: process.env.ADMIN_API + '/depositConfig/delete',
    method: 'post',
    data: params
  })
}
