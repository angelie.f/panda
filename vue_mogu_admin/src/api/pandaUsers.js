import request from '@/utils/request'

export function getPandaUserList(params) {
  return request({
    url: process.env.ADMIN_API + '/pandaUser/getPandaUserList',
    method: 'post',
    data: params
  })
}
export function getSourceList(params) {
  return request({
    url: process.env.ADMIN_API + '/pandaProject/sources',
    method: 'post',
    data: params
  })
}
export function getBlacklist(params) {
  return request({
    url: process.env.ADMIN_API + '/pandaUser/getBlacklist',
    method: 'post',
    data: params
  })
}
export function editBlacklist(params) {
  return request({
    url: process.env.ADMIN_API + '/pandaUser/editBlacklist',
    method: 'post',
    data: params
  })
}
export function editOne(params) {
  return request({
    url: process.env.ADMIN_API + '/pandaUser/editOne',
    method: 'post',
    data: params
  })
}

export function getLogList(params) {
  return request({
    url: process.env.ADMIN_API + '/pandaUser/getLogList',
    method: 'post',
    data: params
  })
}
