import request from '@/utils/request'

export function getBooksUserCostList(params) {
  return request({
    url: process.env.ADMIN_API + '/booksUserCost/getList',
    method: 'post',
    data: params
  })
}

export function addBooksUserCost(params) {
  return request({
    url: process.env.ADMIN_API + '/booksUserCost/add',
    method: 'post',
    data: params
  })
}

export function editBooksUserCost(params) {
  return request({
    url: process.env.ADMIN_API + '/booksUserCost/edit',
    method: 'post',
    data: params
  })
}
export function getLevelList(params) {
  return request({
    url: process.env.ADMIN_API + '/booksUserCost/levels',
    method: 'post',
    data: params
  })
}
