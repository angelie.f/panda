import request from '@/utils/request'

export function getProjectList(params) {
  return request({
    url: process.env.ADMIN_API + '/pandaProject/getList',
    method: 'post',
    data: params
  })
}

export function addProject(params) {
  return request({
    url: process.env.ADMIN_API + '/pandaProject/add',
    method: 'post',
    data: params
  })
}

export function editProject(params) {
  return request({
    url: process.env.ADMIN_API + '/pandaProject/edit',
    method: 'post',
    data: params
  })
}

export function deleteBatchProject(params) {
  return request({
    url: process.env.ADMIN_API + '/pandaProject/deleteBatch',
    method: 'post',
    data: params
  })
}
