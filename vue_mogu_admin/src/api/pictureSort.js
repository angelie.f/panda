import request from '@/utils/request'

export function getPictureSortList(params) {
  return request({
    url: process.env.ADMIN_API + '/pictureSort/getList',
    method: 'post',
    data: params
  })
}

export function getResourceList() {
  return request({
    url: process.env.ADMIN_API + '/pictureSort/getResourceList',
    method: 'post'
  })
}

export function addPictureSort(params) {
  return request({
    url: process.env.ADMIN_API + '/pictureSort/add',
    method: 'post',
    data: params
  })
}

export function editPictureSort(params) {
  return request({
    url: process.env.ADMIN_API + '/pictureSort/edit',
    method: 'post',
    data: params
  })
}
export function initPorject(params) {
  return request({
    url: process.env.ADMIN_API + '/pictureSort/initPorject',
    method: 'post',
    data: params
  })
}
