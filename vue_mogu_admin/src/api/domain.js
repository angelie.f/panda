import request from '@/utils/request'

export function getDomainList(params) {
  return request({
    url: process.env.ADMIN_API + '/domain/getList',
    method: 'post',
    data: params
  })
}

export function addDomain(params) {
  return request({
    url: process.env.ADMIN_API + '/domain/add',
    method: 'post',
    data: params
  })
}

export function editDomain(params) {
  return request({
    url: process.env.ADMIN_API + '/domain/edit',
    method: 'post',
    data: params
  })
}
export function deleteDomain(params) {
  return request({
    url: process.env.ADMIN_API + '/domain/delete',
    method: 'post',
    data: params
  })
}

