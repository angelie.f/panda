import request from '@/utils/request'

export function getAdvConfig(params) {
  return request({
    url: process.env.ADMIN_API + '/advContentConfig/getAdvContentConfig',
    method: 'get',
    params
  })
}

export function editAdvConfig(params) {
  return request({
    url: process.env.ADMIN_API + '/advContentConfig/editAdvConfig',
    method: 'post',
    data: params
  })
}
