import request from '@/utils/request'

export function getCityList(params) {
  // console.log("params="+JSON.stringify(params))
  return request({
    url: process.env.ADMIN_API + '/girlCity/getList',
    method: 'post',
    data: params
  })
}

export function addCity(params) {
  return request({
    url: process.env.ADMIN_API + '/girlCity/add',
    method: 'post',
    data: params
  })
}

export function editCity(params) {
  return request({
    url: process.env.ADMIN_API + '/girlCity/edit',
    method: 'post',
    data: params
  })
}

export function deleteCity(params) {
  return request({
    url: process.env.ADMIN_API + '/girlCity/delete',
    method: 'post',
    data: params
  })
}
