import request from '@/utils/request'

export function getBannerList(params) {
  return request({
    url: process.env.ADMIN_API + '/bannnerManager/getbannnerList',
    method: 'post',
    data: params
  })
}

export function addBanner(params) {
  return request({
    url: process.env.ADMIN_API + '/bannnerManager/add',
    method: 'post',
    data: params
  })
}

export function editBanner(params) {
  return request({
    url: process.env.ADMIN_API + '/bannnerManager/edit',
    method: 'post',
    data: params
  })
}
