import request from '@/utils/request'

export function getWithdrawalList(params) {
  return request({
    url: process.env.ADMIN_API + '/pandaUser/getWithdrawalList',
    method: 'post',
    data: params
  })
}

export function getWithdrawalTotal(params) {
  return request({
    url: process.env.ADMIN_API + '/pandaUser/getWithdrawalTotal',
    method: 'post',
    data: params
  })
}

export function editWithdrawalInfo(params) {
  return request({
    url: process.env.ADMIN_API + '/pandaUser/editWithdrawalInfo',
    method: 'post',
    data: params
  })
}

export function getSourceList(params) {
  return request({
    url: process.env.ADMIN_API + '/pandaProject/sources',
    method: 'post',
    data: params
  })
}
