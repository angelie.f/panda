import request from '@/utils/request'

export function getBooksUserLevelList(params) {
  return request({
    url: process.env.ADMIN_API + '/booksUserLevel/getList',
    method: 'post',
    data: params
  })
}

export function addBooksUserLevel(params) {
  return request({
    url: process.env.ADMIN_API + '/booksUserLevel/add',
    method: 'post',
    data: params
  })
}

export function editBooksUserLevel(params) {
  return request({
    url: process.env.ADMIN_API + '/booksUserLevel/edit',
    method: 'post',
    data: params
  })
}
export function deleteBooksUserLevel(params) {
  return request({
    url: process.env.ADMIN_API + '/booksUserLevel/delete',
    method: 'post',
    data: params
  })
}
