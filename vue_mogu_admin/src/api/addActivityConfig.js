import request from '@/utils/request'

export function getActConfigList(params) {
  return request({
    url: process.env.ADMIN_API + '/gameActConfig/getActConfigList',
    method: 'post',
    data: params
  })
}

export function addActConfig(params) {
  return request({
    url: process.env.ADMIN_API + '/gameActConfig/add',
    method: 'post',
    data: params
  })
}

export function editActConfig(params) {
  return request({
    url: process.env.ADMIN_API + '/gameActConfig/edit',
    method: 'post',
    data: params
  })
}
