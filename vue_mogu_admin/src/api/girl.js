import request from '@/utils/request'

export function getGirlList(params) {
  // console.log("params="+JSON.stringify(params))
  return request({
    url: process.env.ADMIN_API + '/girl/getList',
    method: 'post',
    data: params
  })
}

export function addGirl(params) {
  return request({
    url: process.env.ADMIN_API + '/girl/add',
    method: 'post',
    data: params
  })
}

export function editGirl(params) {
  return request({
    url: process.env.ADMIN_API + '/girl/edit',
    method: 'post',
    data: params
  })
}

export function deleteGirl(params) {
  return request({
    url: process.env.ADMIN_API + '/girl/delete',
    method: 'post',
    data: params
  })
}

export function changeAvailable(params) {
  return request({
    url: process.env.ADMIN_API + '/girl/changeAvailable',
    method: 'post',
    data: params
  })
}

export function getGirlListBySort(params) {
  return request({
    url: process.env.ADMIN_API + '/girl/getGirlListBySort',
    method: 'post',
    data: params
  })
}
export function upOrOut(params) {
  return request({
    url: process.env.ADMIN_API + '/girl/upOrOut',
    method: 'post',
    data: params
  })
}
