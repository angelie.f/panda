import request from '@/utils/request'

export function getGameTypeList(params) {
  return request({
    url: process.env.ADMIN_API + '/gameType/getList',
    method: 'post',
    data: params
  })
}

export function addGameType(params) {
  return request({
    url: process.env.ADMIN_API + '/gameType/add',
    method: 'post',
    data: params
  })
}

export function editGameType(params) {
  return request({
    url: process.env.ADMIN_API + '/gameType/edit',
    method: 'post',
    data: params
  })
}
export function deleteGameType(params) {
  return request({
    url: process.env.ADMIN_API + '/gameType/delete',
    method: 'post',
    data: params
  })
}

