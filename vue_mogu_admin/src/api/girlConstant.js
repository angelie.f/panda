import request from '@/utils/request'


export function getGirlConstant(params) {
  return request({
    url: process.env.ADMIN_API + '/girlConstant/getOne',
    method: 'post',
    data: params
  })
}


export function editGirlConstant(params) {
  return request({
    url: process.env.ADMIN_API + '/girlConstant/edit',
    method: 'post',
    data: params
  })
}


