import request from '@/utils/request'

export function getFirstDepReportList(params) {
  return request({
    url: process.env.ADMIN_API + '/pandaUser/getFirstDepReport',
    method: 'post',
    data: params
  })
}
export function getSourceList(params) {
  return request({
    url: process.env.ADMIN_API + '/pandaProject/sources',
    method: 'post',
    data: params
  })
}
