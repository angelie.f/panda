import request from '@/utils/request'

export function getActReportList(params) {
  return request({
    url: process.env.ADMIN_API + '/pandaUser/getActReport',
    method: 'post',
    data: params
  })
}
export function getSourceList(params) {
  return request({
    url: process.env.ADMIN_API + '/pandaProject/sources',
    method: 'post',
    data: params
  })
}
