import request from '@/utils/request'

export function getBooksProjectList(params) {
  return request({
    url: process.env.ADMIN_API + '/booksProject/getList',
    method: 'post',
    data: params
  })
}

export function addBooksProject(params) {
  return request({
    url: process.env.ADMIN_API + '/booksProject/add',
    method: 'post',
    data: params
  })
}

export function editBooksProject(params) {
  return request({
    url: process.env.ADMIN_API + '/booksProject/edit',
    method: 'post',
    data: params
  })
}
export function getSourceList(params) {
  return request({
    url: process.env.ADMIN_API + '/booksProject/sources',
    method: 'post',
    data: params
  })
}
