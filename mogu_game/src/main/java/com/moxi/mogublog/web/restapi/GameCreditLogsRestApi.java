package com.moxi.mogublog.web.restapi;

import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.service.GameConfigService;
import com.moxi.mogublog.xo.service.GameCreditLogsService;
import com.moxi.mogublog.xo.vo.GameCreditLogsVO;
import com.moxi.mougblog.base.global.BaseMessageConf;
import com.moxi.mougblog.base.validator.group.GetList;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/game/creditLogs")
@Api(value = "账变记录接口", tags = {"账变记录接口"})
@Slf4j
public class GameCreditLogsRestApi {

    @Autowired
    private GameCreditLogsService gameCreditLogsService;
    @Autowired
    private GameConfigService gameConfigService;
    @ApiOperation(value = "用户账变记录列表", notes = "用户账变记录列表", response = String.class)
    @PostMapping(value = "/getList")
    public String createDepositOrder(@Validated({GetList.class}) @RequestBody GameCreditLogsVO gameCreditLogsVO) {
        if (StringUtils.isEmpty(gameCreditLogsVO.getCustomerId())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.CUSTOMER_ID_NULL);
        }
        return ResultUtil.result(SysConf.SUCCESS, gameCreditLogsService.getPageList(gameCreditLogsVO));
    }

//    @ApiOperation(value = "游戏上分", notes = "游戏上分", response = String.class)
//    @PostMapping(value = "/transferIn")
//    public String transferIn(@RequestBody GameConfigVO gameConfigVO) {
//        if (StringUtils.isEmpty(gameConfigVO.getThirdNo())){
//            return ResultUtil.result(com.moxi.mogublog.xo.global.SysConf.ERROR, BaseMessageConf.CUSTOMER_ID_NULL);
//        }
//        String key = "enterGame_"+gameConfigVO.getCustomerId();
//        boolean isLock = RedissonDistributedLocker.tryLock(key, TimeUnit.MILLISECONDS, 30*1000,30*1000);
//        if (isLock) {
//            try {
//                return gameConfigService.enterGame(gameConfigVO);
//            } finally {
//                if (RedissonDistributedLocker.isLock(key))
//                    RedissonDistributedLocker.unlock(key);
//            }
//        }
//        return ResultUtil.result(SysConf.ERROR, BaseMessageConf.SYSTEM_BUSY);
//    }


}

