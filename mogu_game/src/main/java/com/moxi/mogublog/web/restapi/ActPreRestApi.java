package com.moxi.mogublog.web.restapi;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.moxi.mogublog.commons.entity.GameActConfig;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.service.BlacklistService;
import com.moxi.mogublog.xo.service.GameActConfigService;
import com.moxi.mogublog.xo.service.GameActRecordService;
import com.moxi.mogublog.xo.vo.ActRecordRequest;
import com.moxi.mougblog.base.global.BaseMessageConf;
import com.moxi.mougblog.base.validator.group.GetList;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("/game/actPre")
@Api(value = "欢乐送彩金接口", tags = {"欢乐送彩金接口"})
@Slf4j
public class ActPreRestApi {
    @Autowired
    private GameActRecordService gameActRecordService;
    @Autowired
    private GameActConfigService gameActConfigService;
    @Autowired
    private BlacklistService blacklistService;

    @ApiOperation(value = "欢乐送彩金会员记录", notes = "欢乐送彩金会员记录", response = String.class)
    @PostMapping(value = "/getPreInfo")
    public String getPreInfo(@Validated({GetList.class}) @RequestBody ActRecordRequest actRecordRequest) {
        if (StringUtils.isEmpty(actRecordRequest.getCustomerId())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.CUSTOMER_ID_NULL);
        }
        if(blacklistService.checkInBlackByCustomerId(actRecordRequest.getCustomerId())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.USER_IS_BLACK);
        }
        return ResultUtil.result(SysConf.SUCCESS, gameActRecordService.searchActUserInfo(actRecordRequest));
    }



    @ApiOperation(value = "活动设置列表", notes = "活动设置列表", response = String.class)
    @PostMapping(value = "/getActConfigList")
    public String getActConfigList() {
        QueryWrapper<GameActConfig> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status",1);
        queryWrapper.le("act_begin_time",new Date());
        queryWrapper.ge("act_end_time",new Date());
        return ResultUtil.result(SysConf.SUCCESS, gameActConfigService.list(queryWrapper));
    }
}
