package com.moxi.mogublog.web.restapi;

import com.moxi.mogublog.utils.RedisUtil;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.service.*;
import com.moxi.mogublog.xo.vo.*;
import com.moxi.mougblog.base.global.BaseMessageConf;
import com.moxi.mougblog.base.validator.group.GetList;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.math.BigDecimal;
import java.util.Map;

@RestController
@RequestMapping("/game/deposit")
@Api(value = "充值相关接口", tags = {"充值相关接口"})
@Slf4j
public class DepositRestApi {

    @Value(value = "${T08.url}")
    private String t08Url;

    @Autowired
    private GameDepositConfigService gameDepositConfigService;

    @Autowired
    private GameDepositRequestService gameDepositRequestService;

    @Autowired
    private GameDepositAwardService gameDepositAwardService;

    @Autowired
    private BlacklistService blacklistService;

    @Autowired
    RedisUtil redisUtil;

    @ApiOperation(value = "获取充值配置列表", notes = "获取充值配置列表", response = String.class)
    @PostMapping(value = "/getConfig")
    public String getConfig(@Validated({GetList.class}) @RequestBody GameDepositVO gameDepositVO) {
        log.info("获取充值配置列表: {}", gameDepositVO);

        return ResultUtil.result(com.moxi.mogublog.xo.global.SysConf.SUCCESS, gameDepositConfigService.getDepositConfigList(gameDepositVO));
    }

    @ApiOperation(value = "创建存款订单", notes = "创建存款订单", response = String.class)
    @PostMapping(value = "/createDepositOrder")
    public String createDepositOrder(@RequestBody GameDepositVO gameDepositVO) {
        if (StringUtils.isEmpty(gameDepositVO.getCustomerId())){
            return ResultUtil.result(com.moxi.mogublog.xo.global.SysConf.ERROR, BaseMessageConf.CUSTOMER_ID_NULL);
        }
        if(blacklistService.checkInBlackByCustomerId(gameDepositVO.getCustomerId())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.USER_IS_BLACK);
        }
        if (gameDepositVO.getPrice()==null){
            return ResultUtil.result(com.moxi.mogublog.xo.global.SysConf.ERROR, BaseMessageConf.AMOUNT_NULL);
        }
        if (gameDepositVO.getCoins()==null){
            return ResultUtil.result(com.moxi.mogublog.xo.global.SysConf.ERROR, BaseMessageConf.COINS_NULL);
        }
        if (gameDepositVO.getPrice().compareTo(BigDecimal.ZERO)<=0){
            return ResultUtil.result(com.moxi.mogublog.xo.global.SysConf.ERROR, BaseMessageConf.AMOUNT_LE_ZERO);
        }
        if (gameDepositVO.getCoins()<=0){
            return ResultUtil.result(com.moxi.mogublog.xo.global.SysConf.ERROR, BaseMessageConf.COINS_LE_ZERO);
        }

        return ResultUtil.result(SysConf.SUCCESS, t08Url+gameDepositRequestService.createOrder(gameDepositVO));
    }

    @ApiOperation(value = "支付回调", notes = "支付回调", response = String.class)
    @PostMapping(value = "/callbackOrder")
    public Map<String,Object> callbackOrder(@RequestBody CallbackOrderRequest callbackOrderRequest) {
        log.info("回调参数: {}",callbackOrderRequest);
        return gameDepositRequestService.callbackOrder(callbackOrderRequest.getParam());
    }
    @ApiOperation(value = "获取充值列表", notes = "获取充值列表", response = String.class)
    @PostMapping(value = "/getList")
    public String getList(@Validated({GetList.class}) @RequestBody GameDepositRequestVo gameDepositRequestVo) {
        if (StringUtils.isEmpty(gameDepositRequestVo.getCustomerId())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.CUSTOMER_ID_NULL);
        }
        if(blacklistService.checkInBlackByCustomerId(gameDepositRequestVo.getCustomerId())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.USER_IS_BLACK);
        }
        log.info("获取充值列表: {}", gameDepositRequestVo);
        return ResultUtil.result(com.moxi.mogublog.xo.global.SysConf.SUCCESS, gameDepositRequestService.getPageList(gameDepositRequestVo));
    }

    @ApiOperation(value = "读取当日后端实时成功首存人数", notes = "读取当日后端实时成功首存人数", response = String.class)
    @PostMapping(value = "/getFirstDepNum")
    public String getFirstDepNum() {
        return ResultUtil.result(SysConf.SUCCESS, gameDepositRequestService.getFirstDepositNum());
    }

    @ApiOperation(value = "读取当日后端实时成功首存獎金", notes = "读取当日后端实时成功首存獎金", response = String.class)
    @PostMapping(value = "/getFirstDepAward")
    public String getFirstDepAward() {
        return ResultUtil.result(SysConf.SUCCESS, gameDepositRequestService.getFirstDepositNum()*2);
    }
    @ApiOperation(value = "读取用户首存狀態", notes = "读取用户首存狀態", response = String.class)
    @PostMapping(value = "/getFirstDepositStats")
    public String getFirstDepositStats(@Validated({GetList.class}) @RequestBody GameDepositVO gameDepositVO) {
        if (StringUtils.isEmpty(gameDepositVO.getCustomerId())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.CUSTOMER_ID_NULL);
        }
        log.info("读取用户首存狀態: {}", gameDepositVO);
        return ResultUtil.result(SysConf.SUCCESS, gameDepositRequestService.getUserFirstDepositStatus(gameDepositVO.getCustomerId()));
    }

    @ApiOperation(value = "读取后端得奖资讯", notes = "读取后端得奖资讯", response = String.class)
    @PostMapping(value = "/getAwardUserInfo")
    public String getAwardUserInfo(@Validated({GetList.class}) @RequestBody GameDepositAwardVo gameDepositAwardVo) {
        if (StringUtils.isEmpty(gameDepositAwardVo.getAwardDate())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.AWARD_DATE_NULL);
        }
        log.info("读取后端得奖资讯: {}", gameDepositAwardVo);
        return ResultUtil.result(SysConf.SUCCESS, gameDepositAwardService.queryAward(gameDepositAwardVo));
    }
    @ApiOperation(value = "读取最近得奖列表(7天)", notes = "读取最近得奖列表(7天)", response = String.class)
    @PostMapping(value = "/getRecentAwards")
    public String getRecentAwards() {

        log.info("取最近得奖列表(7天)");
        return ResultUtil.result(SysConf.SUCCESS, gameDepositAwardService.queryRecentAwards());
    }
}

