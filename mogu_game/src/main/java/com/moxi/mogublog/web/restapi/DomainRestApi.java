package com.moxi.mogublog.web.restapi;

import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.service.DomainService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/game/domain")
@Api(value = "域名相关接口", tags = {"域名相关接口"})
@Slf4j
public class DomainRestApi {

    @Autowired
    private DomainService domainService;

    @ApiOperation(value = "域名列表", notes = "domainType:1-启动页,2-收银台;不传或为空则获取所有域名", response = String.class)
    @PostMapping(value = "/getList")
    public String getConfig(@RequestParam(value = "domainType", required = false ,defaultValue = "1") Integer domainType) {
        return ResultUtil.result(SysConf.SUCCESS, domainService.getList(domainType));
    }

}

