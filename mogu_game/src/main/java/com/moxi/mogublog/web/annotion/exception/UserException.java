package com.moxi.mogublog.web.annotion.exception;

public class UserException extends RuntimeException {
    private int isOneMember;
    private String userId;
    /**
     * 异常构造方法 在使用时方便传入错误码和信息
     */
    public UserException(String msg, int isOneMember,String userId) {
        super(msg);
        this.isOneMember = isOneMember;
        this.userId=userId;
    }
}
