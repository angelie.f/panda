package com.moxi.mogublog.web.restapi;

import com.moxi.mogublog.commons.entity.GameConfigTrial;
import com.moxi.mogublog.utils.RedisUtil;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.service.GameConfigService;
import com.moxi.mogublog.xo.vo.GameConfigTrialVO;
import com.moxi.mogublog.xo.vo.GameConfigVO;
import com.moxi.mougblog.base.global.BaseMessageConf;
import com.moxi.mougblog.base.validator.group.GetList;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;


@RestController
@RequestMapping("/game/config")
@Api(value = "游戏配置接口", tags = {"游戏配置接口"})
@Slf4j
public class GameConfigRestApi {

    @Autowired
    private GameConfigService gameConfigService;
    @Autowired
    RedisUtil redisUtil;

    @ApiOperation(value = "游戏列表接口", notes = "游戏列表接口", response = String.class)
    @PostMapping(value = "/getList")
    public String getList(@Validated({GetList.class}) @RequestBody GameConfigVO gameConfigVO) {
        return ResultUtil.result(SysConf.SUCCESS, gameConfigService.getConfigAndType());
    }

    @ApiOperation(value = "游戏試玩接口", notes = "游戏試玩接口", response = String.class)
    @PostMapping(value = "/getGameTrial")
    public String getGameTrial(@Validated({GetList.class}) @RequestBody GameConfigTrialVO gameConfigTrialVO) {
        return gameConfigService.getGameTrail(gameConfigTrialVO);
    }

    @ApiOperation(value = "进入游戏", notes = "进入游戏", response = String.class)
    @PostMapping(value = "/enterGame")
    public String createDepositOrder(@RequestBody GameConfigVO gameConfigVO) {
        if (StringUtils.isEmpty(gameConfigVO.getThirdNo())){
            return ResultUtil.result(com.moxi.mogublog.xo.global.SysConf.ERROR, BaseMessageConf.CUSTOMER_ID_NULL);
        }
        String key = "enterGame_"+gameConfigVO.getCustomerId();
        //boolean isLock = RedissonDistributedLocker.tryLock(key, TimeUnit.MILLISECONDS, 30*1000,30*1000);
        boolean isLock = redisUtil.setIfAbsent(key,"",30*1000,TimeUnit.MILLISECONDS);
        if (isLock) {
            try {
                //由于调用了远程又没有中间态 扩大锁的范围放到事务之外 避免进入游戏扣钱 事务没提交  退出游戏又拿到了原来的余额 导致金额异常
                String keyAmount = "gameBalance_"+gameConfigVO.getCustomerId();
                boolean isLockAmount = redisUtil.setIfAbsent(keyAmount,"",30*1000, TimeUnit.MILLISECONDS);
                if (isLockAmount) {
                    try {
                        return gameConfigService.enterGame(gameConfigVO);
                    } finally {
                        if (redisUtil.hasKey(keyAmount))
                            redisUtil.delete(keyAmount);
                    }
                }else {
                    return ResultUtil.result(SysConf.ERROR,BaseMessageConf.TRANSFER_GAME);
                }

            } finally {
                if (redisUtil.hasKey(key))
                    redisUtil.delete(key);
            }
        }
        return ResultUtil.result(SysConf.ERROR, BaseMessageConf.SYSTEM_BUSY);
    }

    @ApiOperation(value = "游戏下分", notes = "游戏下分", response = String.class)
    @PostMapping(value = "/transferOut")
    public String transferOut(@RequestBody GameConfigVO gameConfigVO) {
        if (StringUtils.isEmpty(gameConfigVO.getCustomerId())){
            return ResultUtil.result(com.moxi.mogublog.xo.global.SysConf.ERROR, BaseMessageConf.CUSTOMER_ID_NULL);
        }
        String key = "transferOut_"+gameConfigVO.getCustomerId();
        boolean isLock = redisUtil.setIfAbsent(key,"",30*1000,TimeUnit.MILLISECONDS);
        if (isLock) {
            try {
                return gameConfigService.transferOut(gameConfigVO);
            } finally {
                if (redisUtil.hasKey(key))
                    redisUtil.delete(key);
            }
        }
        return ResultUtil.result(SysConf.ERROR, BaseMessageConf.SYSTEM_BUSY);
    }

}

