package com.moxi.mogublog.web.restapi;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.moxi.mogublog.commons.entity.Announcement;
import com.moxi.mogublog.commons.entity.Customers;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.service.AnnouncementService;
import com.moxi.mogublog.xo.service.GameDepositConfigService;
import com.moxi.mogublog.xo.service.GameDepositRequestService;
import com.moxi.mogublog.xo.vo.CallbackOrderRequest;
import com.moxi.mogublog.xo.vo.GameDepositVO;
import com.moxi.mougblog.base.global.BaseMessageConf;
import com.moxi.mougblog.base.validator.group.GetList;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Wrapper;
import java.util.Map;


@RestController
@RequestMapping("/game/announcement")
@Api(value = "公告接口", tags = {"公告相关接口"})
@Slf4j
public class AnnouncementRestApi {

    @Autowired
    private AnnouncementService announcementService;


    @ApiOperation(value = "公告列表", notes = "公告列表", response = String.class)
    @PostMapping(value = "/getList")
    public String getConfig() {
        QueryWrapper<Announcement> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status",1);
        return ResultUtil.result(SysConf.SUCCESS, announcementService.list(queryWrapper));
    }

}

