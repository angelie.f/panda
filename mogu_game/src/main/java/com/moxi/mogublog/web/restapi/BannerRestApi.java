package com.moxi.mogublog.web.restapi;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.moxi.mogublog.commons.entity.Banner;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.service.BannerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/game/banner")
@Api(value = "轮播图相关接口", tags = {"轮播图相关接口"})
@Slf4j
public class BannerRestApi {

    @Autowired
    private BannerService bannerService;


    @ApiOperation(value = "轮播图列表", notes = "轮播图列表", response = String.class)
    @PostMapping(value = "/getList")
    public String getConfig() {
        QueryWrapper<Banner> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("banner_type","1");
        queryWrapper.eq("status",1);
        queryWrapper.orderByDesc("sort");
        return ResultUtil.result(SysConf.SUCCESS, bannerService.list(queryWrapper));
    }

    @ApiOperation(value = "活动列表", notes = "活动列表", response = String.class)
    @PostMapping(value = "/getActivityList")
    public String getAactivity() {
        QueryWrapper<Banner> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("banner_type","2");
        queryWrapper.eq("status",1);
        queryWrapper.orderByDesc("sort");
        return ResultUtil.result(SysConf.SUCCESS, bannerService.list(queryWrapper));
    }

}

