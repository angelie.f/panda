package com.moxi.mogublog.web.controller;

import com.moxi.mogublog.web.annotion.exception.UserException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class ExceptionController {
    /**
     * 异常处理 controller request url
     */
    public static final String ERROR_CONTROLLER_PATH = "/error/throw";

    @RequestMapping(ERROR_CONTROLLER_PATH)
    public void handleException(HttpServletRequest request){
        throw (UserException) request.getAttribute("filterError");
    }

}
