package com.moxi.mogublog.web.annotion.exception;

import com.moxi.mogublog.utils.ResultUtil;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@Component
@ControllerAdvice
public class GlobalException {
    @ResponseBody
    @ExceptionHandler(UserException.class)
    public String HandleException(UserException e) {
        return ResultUtil.errorWithData(e);
    }

}
