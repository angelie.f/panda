package com.moxi.mogublog.web.restapi;


import com.alibaba.fastjson.JSONObject;
import com.moxi.mogublog.commons.entity.*;
import com.moxi.mogublog.utils.*;
import com.moxi.mogublog.web.global.MessageConf;
import com.moxi.mogublog.web.global.SysConf;
import com.moxi.mogublog.xo.service.*;
import com.moxi.mogublog.xo.vo.*;
import com.moxi.mougblog.base.global.BaseMessageConf;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/game/customers")
@Api(value = "用户相关接口", tags = {"用户相关接口"})
@Slf4j
public class IndexRestApi {

    @Autowired
    private CustomersService customersService;
    @Autowired
    private RedisUtil redisUtil;


    @ApiOperation(value = "游戏用户注册", notes = "游戏用户注册")
    @PostMapping("/register")
    public String register(@Valid @RequestBody QQCLoginRequest request, HttpServletRequest httpServletRequest) {
        Map<String,String> map = new HashMap<>();

        log.info("加密串:{}",request.getUserInfo());
        String params = URLDecoder.decode(AESUtils.decryptForShop(request.getUserInfo()));
        log.info("解密后内容：{}",params);
        params = params.trim();
        if (params.startsWith("{")) {
            map = JSONObject.parseObject(params, TreeMap.class);
        }else {
            String[]paraArray = params.split("&");
            for (int i=0;i<paraArray.length;i++){
                String str = paraArray[i];
                if (str.startsWith("token")){
                    map.put("token",str.substring(6));
                }else {
                    String[] pArray = str.split("=");
                    if (pArray != null && pArray.length > 1) {
                        map.put(pArray[0], pArray[1]);
                    }
                }
            }
        }
        request = mapToRequest(map,request);
        Customers tCustomers = getCustomers(request);
        LoginGameResponse response;
        String key = "register_"+tCustomers.getSource()+"_"+tCustomers.getOpenId();
        boolean isLock = redisUtil.setIfAbsent(key,"",30*1000, TimeUnit.MILLISECONDS);
        if (isLock) {
            try {
                tCustomers.setRegisterSource(1);
                response = customersService.registerCustomerForGame(tCustomers, request);
            } catch (Exception e) {
                log.error("customer login fail", e);
                return ResultUtil.result(SysConf.ERROR, MessageConf.OPERATION_FAIL);
            }finally {
                if (redisUtil.hasKey(key))
                    redisUtil.delete(key);
            }
            return ResultUtil.resultWithData(SysConf.SUCCESS, response);
        }
        return ResultUtil.result(com.moxi.mogublog.xo.global.SysConf.ERROR, BaseMessageConf.SYSTEM_BUSY);
    }

    @ApiOperation(value = "获取用户金币馀额", notes = "获取用户金币馀额")
    @PostMapping("/getUserCoins")
    public String getCoins(@Valid @RequestBody UserRequest request, HttpServletRequest httpServletRequest) {
        try {
            CoinsResponse coinsResponse = customersService.queryCoins(request);
            coinsResponse.setVersion("v1.1.1-0831");
            return ResultUtil.resultWithData(SysConf.SUCCESS, coinsResponse);
        } catch (Exception e) {
            log.error("customer getUserCoins fail", e);
            return ResultUtil.result(SysConf.ERROR, MessageConf.OPERATION_FAIL);
        }
    }
    @ApiOperation(value = "获取用户金币馀额(用户id)", notes = "获取用户金币馀额(用户id)")
    @PostMapping("/getUserCoinsByUserId")
    public String getCoinsByUserId(@Valid @RequestBody UserRequest request, HttpServletRequest httpServletRequest) {
        return customersService.queryCoinsByUserId(request);
    }

    @ApiOperation(value = "获取手机绑定状态", notes = "获取手机绑定状态")
    @PostMapping("/getBindPhone")
    public String getBindPhone(@Valid @RequestBody UserRequest request, HttpServletRequest httpServletRequest) {
        try {
            BindPhoneResponse bindPhoneResponse = customersService.queryBindPhone(request);
            bindPhoneResponse.setVersion("v1.1.1-0831");
            return ResultUtil.resultWithData(SysConf.SUCCESS, bindPhoneResponse);
        } catch (Exception e) {
            log.error("customer getBindPhone fail", e);
            return ResultUtil.result(SysConf.ERROR, MessageConf.OPERATION_FAIL);
        }
    }

    private QQCLoginRequest mapToRequest(Map<String,String>map,QQCLoginRequest request){
        request.setApp(map.get("app"));
        request.setIsVip(Integer.valueOf(map.get("is_vip")));
        request.setUserHeadimg(map.get("user_head_img"));
        request.setUserId(map.get("id"));
        request.setUserNickname(map.get("nickname"));
        request.setUserPhone(map.get("mobile"));
        request.setUserToken(map.get("token"));
        request.setDomain(map.get("domain"));
        request.setSource(Integer.valueOf(map.get("source")));
        request.setPlatform(map.get("platform")!=null&&Integer.valueOf(map.get("platform"))==1?map.get("platform"):"2");
        //request.setIsH5(map.get("is_h5")==null?0:Integer.valueOf(map.get("is_h5")));
        return request;
    }

    private Customers getCustomers(@Valid @RequestBody QQCLoginRequest request) {
        Customers tCustomers = new Customers();
        tCustomers.setProductId("V02");
        tCustomers.setPhone(request.getUserPhone());
        if (request.getUserNickname()!=null) {
            tCustomers.setLoginName(request.getUserNickname().trim());
        }else {
            tCustomers.setLoginName("熊猫游戏用户"+(int) (new Random().nextDouble() * 10E5));
        }
        tCustomers.setOpenCode("QQC");
        tCustomers.setOpenId(request.getUserId());
        String phone = request.getUserPhone();
        if (org.springframework.util.StringUtils.hasText(phone)) {
            if (phone.contains(" ")) {
                tCustomers.setPhone(request.getUserPhone().split( "\\ ")[1]);
            } else {
                tCustomers.setPhone(request.getUserPhone());
            }
        }
        tCustomers.setIsVip(request.getIsVip());
        tCustomers.setVipLevel(request.getVipLevel()==null?0:Integer.valueOf(request.getVipLevel()));
        tCustomers.setVipType(request.getVipType());
        tCustomers.setSource(request.getSource());
        return tCustomers;
    }
}

