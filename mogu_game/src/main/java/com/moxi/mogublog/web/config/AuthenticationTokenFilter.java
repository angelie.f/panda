package com.moxi.mogublog.web.config;

import com.alibaba.fastjson.JSONObject;
import com.moxi.mogublog.commons.entity.Customers;
import com.moxi.mogublog.utils.JsonUtils;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.web.annotion.exception.UserException;
import com.moxi.mogublog.web.global.RedisConf;
import com.moxi.mogublog.web.global.SysConf;
import com.moxi.mogublog.xo.service.CustomersService;
import com.moxi.mougblog.base.global.Constants;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.stream.Collectors;

import static com.moxi.mogublog.web.controller.ExceptionController.ERROR_CONTROLLER_PATH;

/**
 * 拦截器
 */
@Component
@Slf4j
public class AuthenticationTokenFilter extends OncePerRequestFilter {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private CustomersService customersService;


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {

        //得到请求头信息authorization信息
        String accessToken = request.getHeader("Authorization");

        final RepeatReadHttpRequest requestWrapper = handlerOneMember(request,response);
        if (accessToken != null) {
            //从Redis中获取内容
            String userInfo = stringRedisTemplate.opsForValue().get(RedisConf.USER_TOKEN + Constants.SYMBOL_COLON + accessToken);
            if (!StringUtils.isEmpty(userInfo)) {
                Map<String, Object> map = JsonUtils.jsonToMap(userInfo);
                //把userUid存储到 request中
                requestWrapper.setAttribute(SysConf.TOKEN, accessToken);
                requestWrapper.setAttribute(SysConf.USER_UID, map.get(SysConf.UID));
                requestWrapper.setAttribute(SysConf.USER_NAME, map.get(SysConf.NICK_NAME));
                log.info("解析出来的用户:{}", map.get(SysConf.NICK_NAME));
            }
        }
        chain.doFilter(requestWrapper, response);
    }

    /***
     * 处理转移到one游戏后，用户无法访问和用户相关的接口
     * @param request
     * @return
     * @throws IOException
     * @throws ServletException
     */
    @NotNull
    private RepeatReadHttpRequest handlerOneMember(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        final RepeatReadHttpRequest requestWrapper = new RepeatReadHttpRequest(request);
        String reqParams = requestWrapper.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
        if (StringUtils.isEmpty(reqParams)) return requestWrapper;
        JSONObject parse = JSONObject.parseObject(reqParams);
        String customerId = parse.getString("customer_id");
        if (StringUtils.isEmpty(customerId)) return requestWrapper;
        Customers customers = customersService.queryIsOneMemberCustomerId(customerId);
        if (customers == null) return requestWrapper;
        int isOneMember = customers.getIsOneMember();
        if (isOneMember == 1 || isOneMember == 2) {
            request.setAttribute("filterError", new UserException(isOneMember == 1 ? "用户已转移至one游戏" : "用户维护中", isOneMember, customers.getUid()));
            // 指定处理该请求的处理器
            request.getRequestDispatcher(ERROR_CONTROLLER_PATH).forward(request, response);

        }
        return requestWrapper;
    }
}
		

