package com.moxi.mogublog.web.restapi;

import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.CallbackWithdrawalRequest;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.service.BlacklistService;
import com.moxi.mogublog.xo.service.GameDepositConfigService;
import com.moxi.mogublog.xo.service.GameDepositRequestService;
import com.moxi.mogublog.xo.service.GameWithdrawalRequestService;
import com.moxi.mogublog.xo.vo.*;
import com.moxi.mougblog.base.global.BaseMessageConf;
import com.moxi.mougblog.base.validator.group.GetList;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Map;


@RestController
@RequestMapping("/game/withdrawal")
@Api(value = "取款相关接口", tags = {"取款相关接口"})
@Slf4j
public class WithdrawalRestApi {

    @Value(value = "${T08.url}")
    private String t08Url;
    @Autowired
    private GameWithdrawalRequestService gameWithdrawalRequestService;
    @Autowired
    private BlacklistService blacklistService;


    @ApiOperation(value = "创建取款订单", notes = "创建取款订单", response = String.class)
    @PostMapping(value = "/createWithdrawalOrder")
    public String createDepositOrder(@RequestBody GameWithdrawalVO gameWithdrawalVO) {
        if (StringUtils.isEmpty(gameWithdrawalVO.getCustomerId())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.CUSTOMER_ID_NULL);
        }
        if(blacklistService.checkInBlackByCustomerId(gameWithdrawalVO.getCustomerId())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.USER_IS_BLACK);
        }
        if (gameWithdrawalVO.getAmount()==null){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.AMOUNT_NULL);
        }
        if (gameWithdrawalVO.getAmount().compareTo(BigDecimal.ZERO)<=0){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.AMOUNT_LE_ZERO);
        }
        return ResultUtil.result(SysConf.SUCCESS, t08Url+gameWithdrawalRequestService.createOrder(gameWithdrawalVO));
    }

    @ApiOperation(value = "获取提现列表", notes = "获取提现列表", response = String.class)
    @PostMapping(value = "/getList")
    public String getList(@Validated({GetList.class}) @RequestBody GameWithdrawalRequestVo gameWithdrawalRequestVo) {
        log.info("获取提现列表: {}", gameWithdrawalRequestVo);
        return ResultUtil.result(com.moxi.mogublog.xo.global.SysConf.SUCCESS, gameWithdrawalRequestService.getPageList(gameWithdrawalRequestVo));
    }

    @ApiOperation(value = "取款回调", notes = "取款回调", response = String.class)
    @PostMapping(value = "/callbackOrder")
    public Map<String,Object> callbackOrder(@RequestBody CallbackWithdrawalRequest request) {
        log.info("回调参数: {}",request.getParam());
        return gameWithdrawalRequestService.callbackOrder(request.getParam());
    }

}

