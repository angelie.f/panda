package com.moxi.mogublog.admin.restapi;


import com.moxi.mogublog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.moxi.mogublog.admin.global.SysConf;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.service.TpBooksService;
import com.moxi.mogublog.xo.vo.TpBooksVO;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.validator.group.GetList;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 漫画設定 RestApi
 *
 * @author Sid
 * @since 2022-05-31
 */
@Api(value = "漫画管理相关接口", tags = {"漫画管理相关接口"})
@RestController
@RequestMapping("/booksAdmin")
@Slf4j
public class BooksAdminRestApi {

    @Autowired
    private TpBooksService tpBooksService;

    @AuthorityVerify
    @ApiOperation(value = "获取漫画管理列表", notes = "获取漫画管理列表", response = String.class)
    @PostMapping("/getList")
    public String getList(@Validated({GetList.class}) @RequestBody TpBooksVO tpBooksVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("获取漫画管理列表");
        return ResultUtil.result(SysConf.SUCCESS, tpBooksService.getPageList(tpBooksVO));
    }

}

