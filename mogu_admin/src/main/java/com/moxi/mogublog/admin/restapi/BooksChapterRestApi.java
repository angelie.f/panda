package com.moxi.mogublog.admin.restapi;


import com.moxi.mogublog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.moxi.mogublog.admin.global.SysConf;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.service.TpBookChaptersService;
import com.moxi.mogublog.xo.vo.TpBookChaptersVO;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.validator.group.GetList;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 漫画設定 RestApi
 *
 * @author Sid
 * @since 2022-05-31
 */
@Api(value = "漫画管理章节相关接口", tags = {"漫画管理章节相关接口"})
@RestController
@RequestMapping("/booksChapter")
@Slf4j
public class BooksChapterRestApi {

    @Autowired
    private TpBookChaptersService tpBookChaptersService;

    @AuthorityVerify
    @ApiOperation(value = "获取漫画章节列表", notes = "获取漫画章节列表", response = String.class)
    @PostMapping("/getList")
    public String getList(@Validated({GetList.class}) @RequestBody TpBookChaptersVO tpBookChaptersVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("获取漫画章节列表");
        return ResultUtil.result(SysConf.SUCCESS, tpBookChaptersService.getPageList(tpBookChaptersVO));
    }

}

