package com.moxi.mogublog.admin.restapi.panda;

import com.moxi.mogublog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.moxi.mogublog.admin.annotion.OperationLogger.OperationLogger;
import com.moxi.mogublog.admin.global.SysConf;
import com.moxi.mogublog.admin.task.PandnGameReportTask;
import com.moxi.mogublog.commons.entity.Admin;
import com.moxi.mogublog.commons.entity.GameDepositAward;
import com.moxi.mogublog.commons.entity.Role;
import com.moxi.mogublog.utils.DateUtils;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.service.*;
import com.moxi.mogublog.xo.vo.*;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.validator.group.GetList;
import com.moxi.mougblog.base.validator.group.Update;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 熊猫充值订单列表 RestApi
 *
 */
@Api(value = "熊猫游戏后台接口", tags = {"熊猫游戏后台接口"})
@RestController
@RequestMapping("/pandaUser")
@Slf4j
public class GameCustomerRestApi {

    @Autowired
    private GameDepositRequestService gameDepositRequestService;
    @Autowired
    private CustomersService customersService;
    @Autowired
    private GameWithdrawalRequestService gameWithdrawalRequestService;
    @Autowired
    private GameActRecordService gameActRecordService;
    @Autowired
    private GameAppReportService gameAppReportService;
    @Autowired
    private GameActReportService gameActReportService;
    @Autowired
    private GameDepositAwardService gameDepositAwardService;
    @Autowired
    private PandnGameReportTask pandnGameReportTask;
    @Autowired
    private AdminService adminService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private BlacklistService blacklistService;
    @Autowired
    private BlacklistLogService blacklistLogService;



    @AuthorityVerify
    @ApiOperation(value = "获取用户列表", notes = "获取用户列表", response = String.class)
    @PostMapping("/getPandaUserList")
    public String getPandaUserList(HttpServletRequest request, @Validated({GetList.class}) @RequestBody PandaUserVO pandaUserVO, BindingResult result) {
        if(StringUtils.isNotEmpty(pandaUserVO.getBeginCreateTime())){
            pandaUserVO.setBeginCreateTime(pandaUserVO.getBeginCreateTime().substring(0,13)+":00:00");
        }
        if(StringUtils.isNotEmpty(pandaUserVO.getEndCreateTime())){
            pandaUserVO.setEndCreateTime(pandaUserVO.getEndCreateTime().substring(0,13)+":59:59");
        }
        ThrowableUtils.checkParamArgument(result);
        pandaUserVO.setPhoneHide(isPhoneHide(request));
        return ResultUtil.successWithData(customersService.getPageList(pandaUserVO));
    }

    @AuthorityVerify
    @ApiOperation(value = "获取熊猫充值订单列表", notes = "获取熊猫充值订单列表", response = String.class)
    @PostMapping(value = "/getDepositList")
    public String getDepositList(@Validated({GetList.class}) @RequestBody GameDepositRequestVo gameDepositRequestVo, BindingResult result) {
        if(StringUtils.isNotEmpty(gameDepositRequestVo.getBeginCreateTime())){
            gameDepositRequestVo.setBeginCreateTime(gameDepositRequestVo.getBeginCreateTime().substring(0,13)+":00:00");
        }
        if(StringUtils.isNotEmpty(gameDepositRequestVo.getEndCreateTime())){
            gameDepositRequestVo.setEndCreateTime(gameDepositRequestVo.getEndCreateTime().substring(0,13)+":59:59");
        }
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        return ResultUtil.result(SysConf.SUCCESS, gameDepositRequestService.getOfficePageList(gameDepositRequestVo));
    }

    @AuthorityVerify
    @ApiOperation(value = "获取熊猫充值订单总和", notes = "获取熊猫充值订单总和", response = String.class)
    @PostMapping(value = "/getDepositTotal")
    public String getDepositTotal(@Validated({GetList.class}) @RequestBody GameDepositRequestVo gameDepositRequestVo, BindingResult result) {
        if(StringUtils.isNotEmpty(gameDepositRequestVo.getBeginCreateTime())){
            gameDepositRequestVo.setBeginCreateTime(gameDepositRequestVo.getBeginCreateTime().substring(0,13)+":00:00");
        }
        if(StringUtils.isNotEmpty(gameDepositRequestVo.getEndCreateTime())){
            gameDepositRequestVo.setEndCreateTime(gameDepositRequestVo.getEndCreateTime().substring(0,13)+":59:59");
        }
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        return ResultUtil.result(SysConf.SUCCESS, gameDepositRequestService.getDepositTotal(gameDepositRequestVo));
    }

    @AuthorityVerify
    @ApiOperation(value = "提现审核列表", notes = "提现审核列表", response = String.class)
    @PostMapping(value = "/getWithdrawalList")
    public String getWithdrawalList(@Validated({GetList.class}) @RequestBody GameWithdrawalRequestVo gameWithdrawalRequestVo, BindingResult result) {
        if(StringUtils.isNotEmpty(gameWithdrawalRequestVo.getBeginCreateTime())){
            gameWithdrawalRequestVo.setBeginCreateTime(gameWithdrawalRequestVo.getBeginCreateTime().substring(0,13)+":00:00");
        }
        if(StringUtils.isNotEmpty(gameWithdrawalRequestVo.getEndCreateTime())){
            gameWithdrawalRequestVo.setEndCreateTime(gameWithdrawalRequestVo.getEndCreateTime().substring(0,13)+":59:59");
        }
        if(StringUtils.isNotEmpty(gameWithdrawalRequestVo.getBeginUpdateTime())){
            gameWithdrawalRequestVo.setBeginUpdateTime(gameWithdrawalRequestVo.getBeginUpdateTime().substring(0,13)+":00:00");
        }
        if(StringUtils.isNotEmpty(gameWithdrawalRequestVo.getEndUpdateTime())){
            gameWithdrawalRequestVo.setEndUpdateTime(gameWithdrawalRequestVo.getEndUpdateTime().substring(0,13)+":59:59");
        }
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        return ResultUtil.result(SysConf.SUCCESS, gameWithdrawalRequestService.queryWithdrawalList(gameWithdrawalRequestVo));
    }

    @AuthorityVerify
    @ApiOperation(value = "编辑提现审核列表", notes = "编辑提现审核列表", response = String.class)
    @PostMapping(value = "/editWithdrawalInfo")
    public String editWithdrawalInfo(@Validated({GetList.class}) @RequestBody GameWithdrawalBackVo gameWithdrawalBackVo, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        return ResultUtil.result(SysConf.SUCCESS, gameWithdrawalRequestService.editWithdrawalInfo(gameWithdrawalBackVo));
    }



    @AuthorityVerify
    @ApiOperation(value = "获取熊猫提现审核列表总和", notes = "获取熊猫提现审核列表总和", response = String.class)
    @PostMapping(value = "/getWithdrawalTotal")
    public String getWithdrawalTotal(@Validated({GetList.class}) @RequestBody GameWithdrawalRequestVo gameWithdrawalRequestVo, BindingResult result) {
        if(StringUtils.isNotEmpty(gameWithdrawalRequestVo.getBeginCreateTime())){
            gameWithdrawalRequestVo.setBeginCreateTime(gameWithdrawalRequestVo.getBeginCreateTime().substring(0,13)+":00:00");
        }
        if(StringUtils.isNotEmpty(gameWithdrawalRequestVo.getEndCreateTime())){
            gameWithdrawalRequestVo.setEndCreateTime(gameWithdrawalRequestVo.getEndCreateTime().substring(0,13)+":59:59");
        }
        if(StringUtils.isNotEmpty(gameWithdrawalRequestVo.getBeginUpdateTime())){
            gameWithdrawalRequestVo.setBeginUpdateTime(gameWithdrawalRequestVo.getBeginUpdateTime().substring(0,13)+":00:00");
        }
        if(StringUtils.isNotEmpty(gameWithdrawalRequestVo.getEndUpdateTime())){
            gameWithdrawalRequestVo.setEndUpdateTime(gameWithdrawalRequestVo.getEndUpdateTime().substring(0,13)+":59:59");
        }
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        return ResultUtil.result(SysConf.SUCCESS, gameWithdrawalRequestService.getWithdrawalTotal(gameWithdrawalRequestVo));
    }

    @AuthorityVerify
    @ApiOperation(value = "赠币列表", notes = "赠币列表", response = String.class)
    @PostMapping(value = "/getActRecordList")
    public String getActRecordList(@Validated({GetList.class}) @RequestBody GameActRecordVo gameActRecordVo, BindingResult result) {
        if(StringUtils.isNotEmpty(gameActRecordVo.getBeginCreateTime())){
            gameActRecordVo.setBeginCreateTime(gameActRecordVo.getBeginCreateTime().substring(0,13)+":00:00");

        }
        if(StringUtils.isNotEmpty(gameActRecordVo.getEndCreateTime())){
            gameActRecordVo.setEndCreateTime(gameActRecordVo.getEndCreateTime().substring(0,13)+":59:59");

        }
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        return ResultUtil.result(SysConf.SUCCESS, gameActRecordService.getPageList(gameActRecordVo));
    }

    @AuthorityVerify
    @ApiOperation(value = "平台报表", notes = "平台报表", response = String.class)
    @PostMapping(value = "/getAppReport")
    public String getAppReport(@Validated({GetList.class}) @RequestBody GameAppReportVo gameAppReportVo, BindingResult result) {
        // 参数校验
        if(StringUtils.isNotEmpty(gameAppReportVo.getBeginCreateTime())){
            gameAppReportVo.setBeginCreateTime(DateUtils.strToStringTime(gameAppReportVo.getBeginCreateTime()));
        }
        if(StringUtils.isNotEmpty(gameAppReportVo.getEndCreateTime())){
            gameAppReportVo.setEndCreateTime(DateUtils.strToStringTime(gameAppReportVo.getEndCreateTime()));
        }
        ThrowableUtils.checkParamArgument(result);
        return ResultUtil.result(SysConf.SUCCESS, gameAppReportService.getPageList(gameAppReportVo));
    }

    @AuthorityVerify
    @ApiOperation(value = "活动报表", notes = "活动报表", response = String.class)
    @PostMapping(value = "/getActReport")
    public String getActReport(@Validated({GetList.class}) @RequestBody GameActReportVo gameActReportVo, BindingResult result) {
        if(StringUtils.isNotEmpty(gameActReportVo.getBeginCreateTime())){
            gameActReportVo.setBeginCreateTime(DateUtils.strToStringTime(gameActReportVo.getBeginCreateTime()));
        }
        if(StringUtils.isNotEmpty(gameActReportVo.getEndCreateTime())){
            gameActReportVo.setEndCreateTime(DateUtils.strToStringTime(gameActReportVo.getEndCreateTime()));
        }
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        return ResultUtil.result(SysConf.SUCCESS, gameActReportService.getPageList(gameActReportVo));
    }

    @AuthorityVerify
    @ApiOperation(value = "首存活动报表", notes = "首存活动报表", response = String.class)
    @PostMapping(value = "/getFirstDepReport")
    public String getFirstDepReport(@Validated({GetList.class}) @RequestBody GameDepositAwardVo gameDepositAwardVo, BindingResult result) {
        if(StringUtils.isNotEmpty(gameDepositAwardVo.getBeginCreateTime())){
            gameDepositAwardVo.setBeginCreateTime(DateUtils.strToStringTime(gameDepositAwardVo.getBeginCreateTime()));
        }
        if(StringUtils.isNotEmpty(gameDepositAwardVo.getEndCreateTime())){
            gameDepositAwardVo.setEndCreateTime(DateUtils.strToStringTime(gameDepositAwardVo.getEndCreateTime()));
        }
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        return ResultUtil.result(SysConf.SUCCESS, gameDepositAwardService.getPageList(gameDepositAwardVo));
    }

    @ApiOperation(value = "报表接口", notes = "报表接口", response = String.class)
    @GetMapping(value = "/pandnGameReportTask")
    public String getActReportTask() {
        Date date = new Date();
        DateTime dateTime = new DateTime(date);
        // 参数校验
       pandnGameReportTask.scheduled(dateTime);
        return ResultUtil.result(SysConf.SUCCESS, "执行定时任务结束");
    }
    @AuthorityVerify
    @ApiOperation(value = "黑名单接口", notes = "黑名单接口", response = String.class)
    @PostMapping(value = "/getBlacklist")
    public String getBlacklist(@Validated({GetList.class}) @RequestBody BlacklistVO blacklistVO, BindingResult result) {
        return ResultUtil.result(SysConf.SUCCESS, blacklistService.getBlacklist(blacklistVO));
    }
    @AuthorityVerify
    @OperationLogger(value = "编辑黑名单")
    @ApiOperation(value = "编辑黑名单", notes = "编辑黑名单", response = String.class)
    @PostMapping("/editBlacklist")
    public String edit(@Validated({Update.class}) @RequestBody BlacklistVO blacklistVO, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("编辑黑名单");
        return blacklistService.edit(blacklistVO);
    }

    @AuthorityVerify
    @OperationLogger(value = "导入one用户")
    @ApiOperation(value = "导入one用户", notes = "导入one用户", response = String.class)
    @PostMapping("/editOne")
    public String editOne(@Validated({Update.class}) @RequestBody PandaUserVO pandaUserVO, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("导入one用户");
        customersService.editOne(pandaUserVO);
        return ResultUtil.successWithMessage(MessageConf.UPDATE_SUCCESS);
    }
    @AuthorityVerify
    @ApiOperation(value = "获取黑名单操作列表", notes = "获取黑名单操作列表", response = String.class)
    @PostMapping("/getLogList")
    public String getLogList(@Validated({GetList.class}) @RequestBody BlacklistLogVO blacklistLogVO, BindingResult result) {
        ThrowableUtils.checkParamArgument(result);
        return ResultUtil.successWithData(blacklistLogService.getList(blacklistLogVO));
    }
    private boolean isPhoneHide(HttpServletRequest request){
        Admin admin = adminService.getById(request.getAttribute(SysConf.ADMIN_UID).toString());
        List<String> roleUid = new ArrayList<>();
        roleUid.add(admin.getRoleUid());
        Collection<Role> roleList = roleService.listByIds(roleUid);
        List<String> others = new ArrayList<>();
        for (Role item:roleList){
            String itemOthers = item.getOthers();
            String[] sl = itemOthers.replace("[", "").replace("]", "").replace("\"", "").split(",");
            boolean contains = Arrays.stream(sl).anyMatch("999"::equals);
            if(contains){
                return false;
            }
        }
        return true;
    }

}
