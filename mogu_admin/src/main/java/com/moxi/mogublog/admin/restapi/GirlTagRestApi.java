package com.moxi.mogublog.admin.restapi;


import com.moxi.mogublog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.moxi.mogublog.admin.annotion.AvoidRepeatableCommit.AvoidRepeatableCommit;
import com.moxi.mogublog.admin.annotion.OperationLogger.OperationLogger;
import com.moxi.mogublog.admin.global.SysConf;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.service.GirlTagService;
import com.moxi.mogublog.xo.vo.GirlTagVO;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.validator.group.GetList;
import com.moxi.mougblog.base.validator.group.Insert;
import com.moxi.mougblog.base.validator.group.Update;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 妹子标签表 RestApi
 *
 * @author Vergift
 * @since 2022-01-05
 */
@Api(value = "妹子标签相关接口", tags = {"妹子标签相关接口"})
@RestController
@RequestMapping("/girlTag")
@Slf4j
public class GirlTagRestApi {

    @Autowired
    private GirlTagService girlTagService;

    @AuthorityVerify
    @ApiOperation(value = "获取妹子标签列表", notes = "获取妹子标签列表", response = String.class)
    @PostMapping("/getList")
    public String getList(@Validated({GetList.class}) @RequestBody GirlTagVO girlTagVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("获取妹子标签列表");
        return ResultUtil.result(SysConf.SUCCESS, girlTagService.getPageList(girlTagVO));
    }

    @AvoidRepeatableCommit
    @AuthorityVerify
    @OperationLogger(value = "增加妹子标签")
    @ApiOperation(value = "增加妹子标签", notes = "增加妹子标签", response = String.class)
    @PostMapping("/add")
    public String add(@Validated({Insert.class}) @RequestBody GirlTagVO girlTagVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("增加妹子标签");
        return girlTagService.addTag(girlTagVO);
    }

    @AuthorityVerify
    @OperationLogger(value = "编辑妹子标签")
    @ApiOperation(value = "编辑妹子标签", notes = "编辑妹子标签", response = String.class)
    @PostMapping("/edit")
    public String edit(@Validated({Update.class}) @RequestBody GirlTagVO girlTagVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("编辑妹子标签");
        return girlTagService.editTag(girlTagVO);
    }

    @AuthorityVerify
    @OperationLogger(value = "删除妹子标签")
    @ApiOperation(value = "删除妹子标签", notes = "删除妹子标签", response = String.class)
    @PostMapping("/delete")
    public String delete(@Validated({Update.class}) @RequestBody GirlTagVO girlTagVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("删除妹子标签");
        girlTagService.removeById(girlTagVO.getUid());
        return ResultUtil.successWithMessage(MessageConf.UPDATE_SUCCESS);
    }
}

