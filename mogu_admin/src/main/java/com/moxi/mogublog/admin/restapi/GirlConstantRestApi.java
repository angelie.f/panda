package com.moxi.mogublog.admin.restapi;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.moxi.mogublog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.moxi.mogublog.admin.annotion.OperationLogger.OperationLogger;
import com.moxi.mogublog.admin.global.SysConf;
import com.moxi.mogublog.commons.entity.GirlConstant;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.service.GirlConstantService;
import com.moxi.mogublog.xo.service.GirlGiftService;
import com.moxi.mogublog.xo.vo.GirlGiftVO;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.validator.group.GetList;
import com.moxi.mougblog.base.validator.group.Update;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 妹子常量 RestApi
 *
 * @author Vergift
 * @since 2022-01-05
 */
@Api(value = "妹子常量相关接口", tags = {"妹子常量相关接口"})
@RestController
@RequestMapping("/girlConstant")
@Slf4j
public class GirlConstantRestApi {

    @Autowired
    private GirlConstantService girlConstantService;

    @AuthorityVerify
    @ApiOperation(value = "获取妹子礼物派送列表", notes = "获取妹子礼物派送列表", response = String.class)
    @PostMapping("/getOne")
    public String getOne( @RequestBody GirlConstant girlConstant, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("获取妹子常量");
        QueryWrapper<GirlConstant>query = new QueryWrapper<>();
        query.eq("param",girlConstant.getParam());
        return ResultUtil.result(SysConf.SUCCESS, girlConstantService.getOne(query));
    }

    @AuthorityVerify
    @OperationLogger(value = "编辑妹子常量")
    @ApiOperation(value = "编辑妹子常量", notes = "编辑妹子常量", response = String.class)
    @PostMapping("/edit")
    public String edit(@Validated({Update.class}) @RequestBody GirlConstant girlConstant, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("编辑妹子订单");
        girlConstant.updateById();
        QueryWrapper<GirlConstant>query = new QueryWrapper<>();
        query.eq("param",girlConstant.getParam());
        return ResultUtil.result(SysConf.SUCCESS, girlConstantService.getOne(query));
    }
}

