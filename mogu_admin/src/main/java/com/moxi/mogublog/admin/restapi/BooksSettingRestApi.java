package com.moxi.mogublog.admin.restapi;


import com.moxi.mogublog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.moxi.mogublog.admin.annotion.OperationLogger.OperationLogger;
import com.moxi.mogublog.admin.global.SysConf;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.service.BooksSettingService;
import com.moxi.mogublog.xo.vo.BooksSettingVO;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.validator.group.GetList;
import com.moxi.mougblog.base.validator.group.Update;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 漫画設定 RestApi
 *
 * @author Sid
 * @since 2022-05-31
 */
@Api(value = "漫画設定相关接口", tags = {"漫画設定相关接口"})
@RestController
@RequestMapping("/booksSetting")
@Slf4j
public class BooksSettingRestApi {

    @Autowired
    private BooksSettingService booksSettingService;

    @AuthorityVerify
    @ApiOperation(value = "获取漫画設定列表", notes = "获取漫画設定列表", response = String.class)
    @PostMapping("/getList")
    public String getList(@Validated({GetList.class}) @RequestBody BooksSettingVO booksSettingVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("获取漫画設定列表");
        return ResultUtil.result(SysConf.SUCCESS, booksSettingService.getPageList(booksSettingVO));
    }

    @AuthorityVerify
    @OperationLogger(value = "编辑漫画設定")
    @ApiOperation(value = "编辑漫画設定", notes = "编辑漫画設定", response = String.class)
    @PostMapping("/edit")
    public String edit(@Validated({Update.class}) @RequestBody BooksSettingVO booksSettingVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("编辑漫画設定");
        return booksSettingService.editSetting(booksSettingVO);
    }

}

