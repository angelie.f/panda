package com.moxi.mogublog.admin.restapi;

import com.moxi.mogublog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.moxi.mogublog.admin.annotion.AvoidRepeatableCommit.AvoidRepeatableCommit;
import com.moxi.mogublog.admin.annotion.OperationLogger.OperationLogger;
import com.moxi.mogublog.admin.global.SysConf;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.service.DomainService;
import com.moxi.mogublog.xo.vo.DomainVO;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.validator.group.GetList;
import com.moxi.mougblog.base.validator.group.Insert;
import com.moxi.mougblog.base.validator.group.Update;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "熊猫游戏域名相关接口", tags = {"熊猫游戏域名相关接口"})
@RestController
@RequestMapping("/domain")
@Slf4j
public class DomainRestApi {

    @Autowired
    private DomainService domainService;

    @AuthorityVerify
    @ApiOperation(value = "获取熊猫游戏域名列表", notes = "获取熊猫游戏域名列表", response = String.class)
    @PostMapping("/getList")
    public String getList(@Validated({GetList.class}) @RequestBody DomainVO domainVO, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("获取熊猫游戏域名列表");
        return ResultUtil.result(SysConf.SUCCESS, domainService.getPageList(domainVO));
    }

    @AvoidRepeatableCommit
    @AuthorityVerify
    @OperationLogger(value = "增加熊猫游戏域名")
    @ApiOperation(value = "增加熊猫游戏域名", notes = "增加熊猫游戏域名", response = String.class)
    @PostMapping("/add")
    public String add(@Validated({Insert.class}) @RequestBody DomainVO domainVO, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("增加熊猫游戏域名");
        return domainService.add(domainVO);
    }

    @AuthorityVerify
    @OperationLogger(value = "编辑熊猫游戏域名")
    @ApiOperation(value = "编辑熊猫游戏域名", notes = "编辑熊猫游戏域名", response = String.class)
    @PostMapping("/edit")
    public String edit(@Validated({Update.class}) @RequestBody DomainVO domainVO, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("编辑熊猫游戏域名");
        return domainService.edit(domainVO);
    }
    @AuthorityVerify
    @OperationLogger(value = "删除熊猫游戏域名")
    @ApiOperation(value = "删除熊猫游戏域名", notes = "删除熊猫游戏域名", response = String.class)
    @PostMapping("/delete")
    public String delete(@Validated({Update.class}) @RequestBody DomainVO domainVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("删除熊猫游戏域名");
        domainService.removeById(domainVO.getUid());
        return ResultUtil.successWithMessage(MessageConf.UPDATE_SUCCESS);
    }
}

