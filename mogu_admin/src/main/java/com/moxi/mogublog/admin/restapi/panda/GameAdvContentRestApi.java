package com.moxi.mogublog.admin.restapi.panda;

import com.moxi.mogublog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.moxi.mogublog.admin.annotion.OperationLogger.OperationLogger;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.service.AnnouncementService;
import com.moxi.mogublog.xo.vo.AnnouncementVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 公告文字配置
 *
 */
@Api(value = "公告文字配置接口", tags = {"公告文字配置接口"})
@RestController
@RequestMapping("/advContentConfig")
@Slf4j
public class GameAdvContentRestApi {

    @Autowired
    private AnnouncementService announcementService;

    @AuthorityVerify
    @ApiOperation(value = "获取系统配置", notes = "获取系统配置")
    @GetMapping("/getAdvContentConfig")
    public String getAdvContentConfig() {
        return ResultUtil.successWithData(announcementService.getAdvConfig());
    }

    @AuthorityVerify
    @OperationLogger(value = "修改系统配置")
    @ApiOperation(value = "修改系统配置", notes = "修改系统配置")
    @PostMapping("/editAdvConfig")
    public String editAdvConfig(@RequestBody AnnouncementVo announcementVo) {
        return announcementService.editAdvConfig(announcementVo);
    }
}
