package com.moxi.mogublog.admin.config.datasource;

public class DBConstants {
    public static final String MOGU_DATA_SOURCE = "moguDataSource";
    public static final String THIRDPARTY_DATA_SOURCE = "thirdpartyDataSource";

    public static final String MOGU_SQL_SESSION_FACTORY = "moguSqlSessionFactory";
    public static final String THIRDPARTY_SQL_SESSION_FACTORY = "thirdpartySqlSessionFactory";    /**

     * mapper接口
     */
    public static final String MOGU_DAO = "com.moxi.mogublog.xo.mapper";
    public static final String THIRDPARTY_DAO = "com.moxi.mogublog.xo.thirdparty.mapper";

    public static final String MOGU_MAPPER_LOCATIONS = "moguMapperLocations";
    public static final String THIRDPARTY_MAPPER_LOCATIONS = "thirdpartyMapperLocations";

}
