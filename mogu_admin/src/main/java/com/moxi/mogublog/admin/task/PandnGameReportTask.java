package com.moxi.mogublog.admin.task;

import com.moxi.mogublog.commons.entity.PandaProject;
import com.moxi.mogublog.xo.service.GameReportService;
import com.moxi.mogublog.xo.service.PandaProjectService;
import com.moxi.mogublog.xo.vo.ProjectVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

@Component
@Slf4j
public class PandnGameReportTask {
    //private final  List<String> sourceList = Arrays.asList("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","41","42","99","101");

    @Autowired
    private ThreadPoolTaskExecutor gameActReportTaskExecutor;

    @Autowired
    private GameReportService gameReportService;

    @Autowired
    private PandaProjectService pandaProjectService;

    public void scheduled(DateTime time){
        log.info("开始执行平台概况和活动概况异步定时任务!");

        List<PandaProject>  list =pandaProjectService.getList(new ProjectVO());
        List<String> sourceList = new ArrayList<>();
        for(PandaProject project:list){
            sourceList.add(project.getSource());
        }
        CountDownLatch countDownLatch = new CountDownLatch(sourceList.size());
        for(String source : sourceList){
            gameActReportTaskExecutor.execute(()->{
                oprGameReport(source, time);
                countDownLatch.countDown();
            });
        }
        log.info("平台概况和活动概况异步定时任务执行结束!");
    }

    private void oprGameReport(String source,DateTime time){
        if(StringUtils.isNotBlank(source)){
            gameReportService.addActReport(source,time);
            gameReportService.addAppReport(source,time);
        }
    }
}
