package com.moxi.mogublog.admin.config.datasource;

import com.alibaba.druid.pool.DruidDataSource;
import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * DruidConfig
 *
 * @author Administrator
 * @Date 2020年1月9日19:06:23
 */
@Slf4j
@Configuration
@MapperScan(basePackages = DBConstants.THIRDPARTY_DAO, sqlSessionFactoryRef = DBConstants.THIRDPARTY_SQL_SESSION_FACTORY)
@ConditionalOnProperty(prefix = "books", name = "synthird", havingValue = "true")
public class ThirdPartyDataSourceConfig {
    
    @Autowired
    private ThirdpartyDataSourceProperties thirdpartyDataSourceProperties;

    @Autowired
    private PaginationInterceptor paginationInterceptor;
    /**
     * 声明其为Bean实例
     * 在同样的DataSource中，首先使用被标注的DataSource
     *
     * @return
     */
    @Bean(DBConstants.THIRDPARTY_DATA_SOURCE)
    public DataSource thirdPartyDataSource() {
        DruidDataSource datasource = new DruidDataSource();
        datasource.setUrl(thirdpartyDataSourceProperties.getUrl());
        datasource.setUsername(thirdpartyDataSourceProperties.getUsername());
        datasource.setPassword(thirdpartyDataSourceProperties.getPassword());
        datasource.setDriverClassName(thirdpartyDataSourceProperties.getDriverClassName());
        // configuration
        datasource.setInitialSize(thirdpartyDataSourceProperties.getInitialSize());
        datasource.setMinIdle(thirdpartyDataSourceProperties.getMinIdle());
        datasource.setMaxActive(thirdpartyDataSourceProperties.getMaxActive());
        datasource.setMaxWait(thirdpartyDataSourceProperties.getMaxWait());
        datasource.setTimeBetweenEvictionRunsMillis(thirdpartyDataSourceProperties.getTimeBetweenEvictionRunsMillis());
        datasource.setMinEvictableIdleTimeMillis(thirdpartyDataSourceProperties.getMinEvictableIdleTimeMillis());
        datasource.setValidationQuery(thirdpartyDataSourceProperties.getValidationQuery());
        datasource.setTestWhileIdle(thirdpartyDataSourceProperties.isTestWhileIdle());
        datasource.setTestOnBorrow(thirdpartyDataSourceProperties.isTestOnBorrow());
        datasource.setTestOnReturn(thirdpartyDataSourceProperties.isTestOnReturn());
        datasource.setPoolPreparedStatements(thirdpartyDataSourceProperties.isPoolPreparedStatements());
        datasource.setMaxPoolPreparedStatementPerConnectionSize(thirdpartyDataSourceProperties.getMaxPoolPreparedStatementPerConnectionSize());
        try {
            datasource.setFilters(thirdpartyDataSourceProperties.getFilters());
        } catch (SQLException e) {
            log.error("thirdparty druid configuration initialization filter");
        }
        datasource.setConnectionProperties(thirdpartyDataSourceProperties.getConnectionProperties());

        return datasource;
    }
    /**
     * 自定义的mapper
     *
     * @return
     */
    @Bean(DBConstants.THIRDPARTY_MAPPER_LOCATIONS)
    public Resource[] resolveMapperLocations() {
        List<Resource> resources = new ArrayList();
        try {
            String mapperLocations = thirdpartyDataSourceProperties.getMapperLocations();
            if (mapperLocations != null) {
                ResourcePatternResolver resourceResolver = new PathMatchingResourcePatternResolver();
                Resource[] mappers = null;
                for (String mapperLocation : mapperLocations.split(",")) {
                    mappers = resourceResolver.getResources(mapperLocation.trim());
                    resources.addAll(Arrays.asList(mappers));
                }
            }
        } catch (IOException e) {
        }
        return resources.toArray(new Resource[resources.size()]);
    }

    /**
     * 创建第一个SqlSessionFactory
     *
     * @param dataSource
     * @return
     * @throws Exception
     */
    @Bean(DBConstants.THIRDPARTY_SQL_SESSION_FACTORY)
    public SqlSessionFactory thirdpartySqlSessionFactory(@Qualifier(DBConstants.THIRDPARTY_DATA_SOURCE) DataSource dataSource,
                                                   @Qualifier(DBConstants.THIRDPARTY_MAPPER_LOCATIONS) Resource[] mapperLocations)
            throws Exception {
        /**
         * 必须使用MybatisSqlSessionFactoryBean，
         * 不能使用SqlSessionFactoryBean，不然会报invalid bound statement (not found)
         *
         * com.baomidou.mybatisplus.autoconfigure.MybatisPlusAutoConfiguration#sqlSessionFactory(javax.sql.DataSource)
         * 源码中也是使用MybatisSqlSessionFactoryBean
         * 并且源码中使用了@ConditionalOnMissingBean，即IOC中如果存在了SqlSessionFactory实例，mybatis-plus就不创建SqlSessionFactory实例了
         */
        MybatisSqlSessionFactoryBean sessionFactoryBean = new MybatisSqlSessionFactoryBean();
        sessionFactoryBean.setDataSource(dataSource);
        MybatisConfiguration configuration = new MybatisConfiguration();
        sessionFactoryBean.setConfiguration(configuration);
        Interceptor[] plugins = {paginationInterceptor};
        sessionFactoryBean.setPlugins(plugins);
        sessionFactoryBean.setMapperLocations(mapperLocations);
        return sessionFactoryBean.getObject();
    }
}
