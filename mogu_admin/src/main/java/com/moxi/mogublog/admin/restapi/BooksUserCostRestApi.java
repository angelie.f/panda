package com.moxi.mogublog.admin.restapi;

import com.moxi.mogublog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.moxi.mogublog.admin.annotion.AvoidRepeatableCommit.AvoidRepeatableCommit;
import com.moxi.mogublog.admin.annotion.OperationLogger.OperationLogger;
import com.moxi.mogublog.admin.global.SysConf;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.service.BooksUserCostService;
import com.moxi.mogublog.xo.vo.BooksUserCostVO;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.validator.group.GetList;
import com.moxi.mougblog.base.validator.group.Insert;
import com.moxi.mougblog.base.validator.group.Update;
import com.moxi.mougblog.base.vo.BaseVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 漫画用戶费用设定 RestApi
 *
 * @author Sid
 * @since 2022-05-31
 */
@Api(value = "漫画用戶费用设定相关接口", tags = {"漫画用戶费用设定相关接口"})
@RestController
@RequestMapping("/booksUserCost")
@Slf4j
public class BooksUserCostRestApi {

    @Autowired
    private BooksUserCostService booksUserCostService;

    @AuthorityVerify
    @ApiOperation(value = "获取漫画用戶费用设定列表", notes = "获取漫画用戶费用设定列表", response = String.class)
    @PostMapping("/getList")
    public String getList(@Validated({GetList.class}) @RequestBody BooksUserCostVO booksUserCostVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("获取漫画用戶费用设定列表");
        return ResultUtil.result(SysConf.SUCCESS, booksUserCostService.getPageList(booksUserCostVO));
    }

    @AvoidRepeatableCommit
    @AuthorityVerify
    @OperationLogger(value = "增加漫画用戶费用设定")
    @ApiOperation(value = "增加漫画用戶费用设定", notes = "增加漫画用戶费用设定", response = String.class)
    @PostMapping("/add")
    public String add(@Validated({Insert.class}) @RequestBody BooksUserCostVO booksUserCostVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("增加漫画用戶费用设定: {}", booksUserCostVO);
        return booksUserCostService.add(booksUserCostVO);
    }

    @AuthorityVerify
    @OperationLogger(value = "编辑漫画用戶费用设定")
    @ApiOperation(value = "编辑漫画用戶费用设定", notes = "编辑漫画用戶费用设定", response = String.class)
    @PostMapping("/edit")
    public String edit(@Validated({Update.class}) @RequestBody BooksUserCostVO booksUserCostVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("编辑漫画用戶费用设定");
        return booksUserCostService.edit(booksUserCostVO);
    }
    @AuthorityVerify
    @ApiOperation(value = "获取漫画用户会员等级", notes = "获取漫画用户会员等级", response = String.class)
    @PostMapping("/levels")
    public String levels(@Validated({GetList.class}) @RequestBody BooksUserCostVO booksUserCostVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("获取漫画用户会员等级");
        return ResultUtil.result(SysConf.SUCCESS, booksUserCostService.levels(booksUserCostVO));
    }
}

