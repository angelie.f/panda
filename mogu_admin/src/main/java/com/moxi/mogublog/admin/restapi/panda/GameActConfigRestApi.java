package com.moxi.mogublog.admin.restapi.panda;

import com.moxi.mogublog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.moxi.mogublog.admin.annotion.AvoidRepeatableCommit.AvoidRepeatableCommit;
import com.moxi.mogublog.admin.annotion.OperationLogger.OperationLogger;
import com.moxi.mogublog.admin.global.SysConf;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.service.GameActConfigService;
import com.moxi.mogublog.xo.vo.ActConfigVo;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.validator.group.GetList;
import com.moxi.mougblog.base.validator.group.Insert;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "添加活动设置接口", tags = {"添加活动设置接口"})
@RestController
@RequestMapping("/gameActConfig")
@Slf4j
public class GameActConfigRestApi {

    @Autowired
    private GameActConfigService gameActConfigService;

    @AuthorityVerify
    @ApiOperation(value = "活动设置列表", notes = "活动设置列表", response = String.class)
    @PostMapping(value = "/getActConfigList")
    public String getActConfigList(@Validated({GetList.class}) @RequestBody ActConfigVo actConfigVo, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        return ResultUtil.result(SysConf.SUCCESS, gameActConfigService.getPageList(actConfigVo));
    }

    @AvoidRepeatableCommit
    @AuthorityVerify
    @OperationLogger(value = "活动设置设置")
    @ApiOperation(value = "活动设置设置", notes = "活动设置设置")
    @PostMapping("/add")
    public String addActConfig(@Validated({Insert.class}) @RequestBody ActConfigVo actConfigVo, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        return gameActConfigService.addBanner(actConfigVo);
    }

    @AuthorityVerify
    @OperationLogger(value = "活动设置设置")
    @ApiOperation(value = "活动设置设置", notes = "活动设置设置")
    @PostMapping("/edit")
    public String editActConfig(@Validated({Insert.class}) @RequestBody ActConfigVo actConfigVo, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        return gameActConfigService.editBanner(actConfigVo);
    }
}
