package com.moxi.mogublog.admin.restapi;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.moxi.mogublog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.moxi.mogublog.admin.annotion.OperationLogger.OperationLogger;
import com.moxi.mogublog.admin.global.SysConf;
import com.moxi.mogublog.commons.entity.Admin;
import com.moxi.mogublog.commons.entity.CategoryMenu;
import com.moxi.mogublog.commons.entity.GirlGift;
import com.moxi.mogublog.commons.entity.Role;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.service.AdminService;
import com.moxi.mogublog.xo.service.GirlGiftService;
import com.moxi.mogublog.xo.service.GirlOrderService;
import com.moxi.mogublog.xo.service.RoleService;
import com.moxi.mogublog.xo.vo.GirlGiftVO;
import com.moxi.mogublog.xo.vo.GirlOrderVO;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.validator.group.GetList;
import com.moxi.mougblog.base.validator.group.Update;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.sql.Wrapper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * 妹子订单类别 RestApi
 *
 * @author Vergift
 * @since 2022-01-05
 */
@Api(value = "妹子礼物相关接口", tags = {"妹子礼物相关接口"})
@RestController
@RequestMapping("/girlGift")
@Slf4j
public class GirlGiftRestApi {

    @Autowired
    private GirlGiftService girlGiftService;

    @Autowired
    private AdminService adminService;

    @Autowired
    private RoleService roleService;

    @AuthorityVerify
    @ApiOperation(value = "获取妹子礼物派送列表", notes = "获取妹子礼物派送列表", response = String.class)
    @PostMapping("/getList")
    public String getList(HttpServletRequest request, @Validated({GetList.class}) @RequestBody GirlGiftVO girlGiftVO, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("获取妹子订单列表");
        girlGiftVO.setPhoneHide(isPhoneHide(request));
        return ResultUtil.result(SysConf.SUCCESS, girlGiftService.getPageList(girlGiftVO));
    }
    private boolean isPhoneHide(HttpServletRequest request){
        Admin admin = adminService.getById(request.getAttribute(SysConf.ADMIN_UID).toString());
        List<String> roleUid = new ArrayList<>();
        roleUid.add(admin.getRoleUid());
        Collection<Role> roleList = roleService.listByIds(roleUid);
        List<String> others = new ArrayList<>();
        for (Role item:roleList){
            String itemOthers = item.getOthers();
            String[] sl = itemOthers.replace("[", "").replace("]", "").replace("\"", "").split(",");
            boolean contains = Arrays.stream(sl).anyMatch("999"::equals);
            if(contains){
                return false;
            }
        }
        return true;
    }

//    @AuthorityVerify
//    @OperationLogger(value = "编辑妹子订单")
//    @ApiOperation(value = "编辑妹子订单", notes = "编辑妹子订单", response = String.class)
//    @PostMapping("/edit")
//    public String edit(@Validated({Update.class}) @RequestBody GirlOrderVO girlOrderVO, BindingResult result) {
//
//        // 参数校验
//        ThrowableUtils.checkParamArgument(result);
//        log.info("编辑妹子订单");
//        return girlOrderService.editOrder(girlOrderVO);
//    }
}

