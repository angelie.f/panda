package com.moxi.mogublog.admin.restapi.panda;

import com.moxi.mogublog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.moxi.mogublog.admin.annotion.AvoidRepeatableCommit.AvoidRepeatableCommit;
import com.moxi.mogublog.admin.annotion.OperationLogger.OperationLogger;
import com.moxi.mogublog.admin.global.SysConf;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.service.BannerService;
import com.moxi.mogublog.xo.vo.BannerVo;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.validator.group.GetList;
import com.moxi.mougblog.base.validator.group.Insert;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 广告设置
 *
 */
@Api(value = "广告设置后台接口", tags = {"广告设置后台接口"})
@RestController
@RequestMapping("/bannnerManager")
@Slf4j
public class GameBannerRestApi {

    @Autowired
    private BannerService bannerService;


    @AuthorityVerify
    @ApiOperation(value = "广告设置列表", notes = "广告设置列表", response = String.class)
    @PostMapping(value = "/getbannnerList")
    public String getWithdrawalList(@Validated({GetList.class}) @RequestBody BannerVo bannerVo, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        return ResultUtil.result(SysConf.SUCCESS, bannerService.getPageList(bannerVo));
    }

    @AvoidRepeatableCommit
    @AuthorityVerify
    @OperationLogger(value = "新增广告设置")
    @ApiOperation(value = "新增广告设置", notes = "新增广告设置")
    @PostMapping("/add")
    public String add(@Validated({Insert.class}) @RequestBody BannerVo bannerVo, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        return bannerService.addBanner(bannerVo);
    }

    @AuthorityVerify
    @OperationLogger(value = "编辑广告设置")
    @ApiOperation(value = "编辑广告设置", notes = "编辑广告设置")
    @PostMapping("/edit")
    public String edit(@Validated({Insert.class}) @RequestBody BannerVo bannerVo, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        return bannerService.editBanner(bannerVo);
    }
}
