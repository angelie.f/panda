package com.moxi.mogublog.admin.scheduler.job;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.*;
import com.moxi.mogublog.xo.mapper.*;
import com.moxi.mogublog.xo.thirdparty.mapper.*;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import javax.annotation.Resource;
import java.time.LocalDate;


@Slf4j
@Data
@Service
@Scope("prototype")
public class SynBooksJob {

    @Resource
    TpBooksMapper tpBooksMapper;

    @Resource
    DTpBooksMapper dTpBooksMapper;

    public void execute(LocalDate executeDate) {
        log.info("{} started",getClass().getName());
        try {
            synBooks(executeDate);
        } catch (Exception e) {
            log.info("{} Error -- cause by :{}" ,getClass().getName(),e.getMessage());
        }
        log.info("{} end",getClass().getName());

    }
    private void synBooks(LocalDate synDate) throws Exception{
        StopWatch watch = new StopWatch("synBooks");
        watch.start();
        QueryWrapper<TpBooks> wrapper = new QueryWrapper<>();
        log.info("synBooks start lastDay:{}",synDate.toString());
        wrapper.between("updated_at",synDate,synDate.plusDays(1));
        wrapper.orderByAsc("updated_at");
        int pageSize=100;
        Integer finish =0;
        Integer total =dTpBooksMapper.selectCount(wrapper);
        int exetimes =total/pageSize;
        if(total%pageSize>0){
            exetimes++;
        }
        Page<TpBooks> page = new Page<>();
        for(int current=1;current<=exetimes;current++){
            page.setCurrent(current);
            page.setSize(pageSize);
            IPage<TpBooks> pageBooks =dTpBooksMapper.selectPage(page,wrapper);
            if(pageBooks.getRecords().size()>0){
                int count = tpBooksMapper.insertOrUpdateBatch(pageBooks.getRecords());
                finish+= pageBooks.getRecords().size();
                log.info("synBooks: process current:{}, per count:{},total:{},currentPage:{},finish total:{}", current,pageBooks.getRecords().size(),total, current,finish);
            }
        }
        watch.stop();
        watch.prettyPrint();
    }

}
