package com.moxi.mogublog.admin.scheduler.job;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.*;
import com.moxi.mogublog.xo.mapper.*;
import com.moxi.mogublog.xo.thirdparty.mapper.*;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;
import javax.annotation.Resource;
import java.time.LocalDate;


@Slf4j
@Data
@Service
@Scope("prototype")
public class SynTopicsJob {

    @Resource
    TpTopicsMapper tpTopicsMapper;

    @Resource
    TpTopicItemsMapper tpTopicItemsMapper;

    @Resource
    DTpTopicsMapper dTpTopicsMapper;

    @Resource
    DTpTopicItemsMapper dTpTopicItemsMapper;

    public void execute(LocalDate executeDate) {
        log.info("{} started",getClass().getName());
        try {
            synTopics(executeDate);
            synTopicItems(executeDate);

        } catch (Exception e) {
            log.info("{} Error -- cause by :{}" ,getClass().getName(),e.getMessage());
        }
        log.info("{} end",getClass().getName());

    }
    private void synTopics(LocalDate synDate) throws Exception{
        StopWatch watch = new StopWatch("synTopics");
        watch.start();
        QueryWrapper<TpTopics> wrapper = new QueryWrapper<>();
        log.info("synTopics start lastDay:{}",synDate.toString());
        wrapper.between("updated_at",synDate,synDate.plusDays(1));
        wrapper.eq("causer","book");
        wrapper.orderByAsc("updated_at");
        int pageSize=100;
        Integer finish =0;
        Integer total =dTpTopicsMapper.selectCount(wrapper);
        int exetimes =total/pageSize;
        if(total%pageSize>0){
            exetimes++;
        }
        Page<TpTopics> page = new Page<>();
        for(int current=1;current<=exetimes;current++){
            page.setCurrent(current);
            page.setSize(pageSize);
            IPage<TpTopics> pageTopics =dTpTopicsMapper.selectPageForUpdate(page,wrapper);
            if(pageTopics.getRecords().size()>0){
                int count = tpTopicsMapper.insertOrUpdateBatch(pageTopics.getRecords());
                finish+= pageTopics.getRecords().size();
                log.info("synTopics process current:{}, per count:{},total:{},currentPage:{},finish total:{}",current,pageTopics.getRecords().size(),total, current,finish);
            }
        }
        watch.stop();
        watch.prettyPrint();
    }
    private void synTopicItems(LocalDate synDate) throws Exception{
        StopWatch watch = new StopWatch("synTopicItems");
        watch.start();
        QueryWrapper<TpTopicItems> wrapper = new QueryWrapper<>();
        log.info("synTopicItems start lastDay:{}",synDate.toString());
        wrapper.between("updated_at",synDate,synDate.plusDays(1));
        wrapper.orderByAsc("updated_at");
        int pageSize=100;
        Integer finish =0;
        Integer total =dTpTopicItemsMapper.selectCount(wrapper);
        int exetimes =total/pageSize;
        if(total%pageSize>0){
            exetimes++;
        }
        Page<TpTopicItems> page = new Page<>();
        for(int current=1;current<=exetimes;current++){
            page.setCurrent(current);
            page.setSize(pageSize);
            IPage<TpTopicItems> pageTopicItems =dTpTopicItemsMapper.selectPage(page,wrapper);
            if(pageTopicItems.getRecords().size()>0){
                int count = tpTopicItemsMapper.insertOrUpdateBatch(pageTopicItems.getRecords());
                finish+= pageTopicItems.getRecords().size();
                log.info("synTopicItems process current:{}, per count:{},total:{},currentPage:{},finish total:{}",current,pageTopicItems.getRecords().size(),total, current,finish);
            }
        }
        watch.stop();
        watch.prettyPrint();
    }

}
