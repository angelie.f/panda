package com.moxi.mogublog.admin.restapi.panda;

import com.moxi.mogublog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.moxi.mogublog.admin.annotion.AvoidRepeatableCommit.AvoidRepeatableCommit;
import com.moxi.mogublog.admin.annotion.OperationLogger.OperationLogger;
import com.moxi.mogublog.admin.global.SysConf;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.service.GameDepositConfigService;
import com.moxi.mogublog.xo.vo.GameDepositConfigVo;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.validator.group.GetList;
import com.moxi.mougblog.base.validator.group.Insert;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 充值设置
 *
 */
@Api(value = "充值设置后台接口", tags = {"充值设置后台接口"})
@RestController
@RequestMapping("/depositConfig")
@Slf4j
public class GameDepositConfigRestApi {

    @Autowired
    private GameDepositConfigService gameDepositConfigService;

    @AuthorityVerify
    @ApiOperation(value = "充值设置列表", notes = "充值设置列表", response = String.class)
    @PostMapping(value = "/getDCList")
    public String getWithdrawalList(@Validated({GetList.class}) @RequestBody GameDepositConfigVo gameDepositConfigVo, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        return ResultUtil.result(SysConf.SUCCESS, gameDepositConfigService.getPageList(gameDepositConfigVo));
    }

    @AvoidRepeatableCommit
    @AuthorityVerify
    @OperationLogger(value = "新增充值设置")
    @ApiOperation(value = "新增充值设置", notes = "新增充值设置")
    @PostMapping("/add")
    public String add(@Validated({Insert.class}) @RequestBody GameDepositConfigVo gameDepositConfigVo, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        return gameDepositConfigService.addDeposit(gameDepositConfigVo);
    }

    @AuthorityVerify
    @OperationLogger(value = "编辑充值设置")
    @ApiOperation(value = "编辑充值设置", notes = "编辑充值设置")
    @PostMapping("/edit")
    public String edit(@Validated({Insert.class}) @RequestBody GameDepositConfigVo gameDepositConfigVo, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        return gameDepositConfigService.editDeposit(gameDepositConfigVo);
    }

    @AuthorityVerify
    @OperationLogger(value = "删除充值设置")
    @ApiOperation(value = "删除充值设置", notes = "删除充值设置")
    @PostMapping("/delete")
    public String deleteDepConfig(@Validated({Insert.class}) @RequestBody GameDepositConfigVo gameDepositConfigVo, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        return gameDepositConfigService.deleteDepConfig(gameDepositConfigVo);
    }
}
