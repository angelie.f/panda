package com.moxi.mogublog.admin.restapi;


import com.moxi.mogublog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.moxi.mogublog.admin.annotion.AvoidRepeatableCommit.AvoidRepeatableCommit;
import com.moxi.mogublog.admin.annotion.OperationLogger.OperationLogger;
import com.moxi.mogublog.admin.global.SysConf;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.service.GirlCityService;
import com.moxi.mogublog.xo.vo.GirlCityVO;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.validator.group.GetList;
import com.moxi.mougblog.base.validator.group.Insert;
import com.moxi.mougblog.base.validator.group.Update;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 妹子城市表 RestApi
 *
 * @author Vergift
 * @since 2022-01-05
 */
@Api(value = "妹子城市相关接口", tags = {"妹子城市相关接口"})
@RestController
@RequestMapping("/girlCity")
@Slf4j
public class GirlCityRestApi {

    @Autowired
    private GirlCityService girlCityService;

    @AuthorityVerify
    @ApiOperation(value = "获取妹子城市列表", notes = "获取妹子城市列表", response = String.class)
    @PostMapping("/getList")
    public String getList(@Validated({GetList.class}) @RequestBody GirlCityVO girlCityVO, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("获取妹子城市列表");
        return ResultUtil.result(SysConf.SUCCESS, girlCityService.getPageList(girlCityVO));
    }

    @AvoidRepeatableCommit
    @AuthorityVerify
    @OperationLogger(value = "增加妹子城市")
    @ApiOperation(value = "增加妹子城市", notes = "增加妹子城市", response = String.class)
    @PostMapping("/add")
    public String add(@Validated({Insert.class}) @RequestBody GirlCityVO girlCityVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("增加妹子城市");
        return girlCityService.addCity(girlCityVO);
    }

    @AuthorityVerify
    @OperationLogger(value = "编辑妹子城市")
    @ApiOperation(value = "编辑妹子城市", notes = "编辑妹子城市", response = String.class)
    @PostMapping("/edit")
    public String edit(@Validated({Update.class}) @RequestBody GirlCityVO girlCityVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("编辑妹子城市");
        return girlCityService.editCity(girlCityVO);
    }

    @AuthorityVerify
    @OperationLogger(value = "删除妹子城市")
    @ApiOperation(value = "删除妹子城市", notes = "编辑妹子城市", response = String.class)
    @PostMapping("/delete")
    public String delete(@Validated({Update.class}) @RequestBody GirlCityVO girlCityVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("删除妹子城市");
        girlCityService.removeById(girlCityVO.getUid());
        return ResultUtil.successWithMessage(MessageConf.UPDATE_SUCCESS);
    }
}

