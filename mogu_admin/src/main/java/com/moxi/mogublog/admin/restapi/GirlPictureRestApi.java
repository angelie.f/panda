package com.moxi.mogublog.admin.restapi;


import com.moxi.mogublog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.service.GirlPictureService;
import com.moxi.mogublog.xo.vo.GirlPictureVO;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.validator.group.GetList;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 妹子图片表 RestApi
 *
 * @author vergift
 * @date 2022-01-13
 */
@RestController
@RequestMapping("/girlPicture")
@Api(value = "妹子图片相关接口", tags = {"妹子图片相关接口"})
@Slf4j
public class GirlPictureRestApi {

    @Autowired
    private GirlPictureService girlpictureService;

    @AuthorityVerify
    @ApiOperation(value = "获取妹子图片列表", notes = "获取妹子图片列表", response = String.class)
    @PostMapping(value = "/getList")
    public String getList(@Validated({GetList.class}) @RequestBody GirlPictureVO girlPictureVO, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("获取妹子图片列表:", girlPictureVO);
        return ResultUtil.successWithData(girlpictureService.getPageList(girlPictureVO));
    }

    @AuthorityVerify
    @ApiOperation(value = "编辑妹子信息时获取图片列表", notes = "编辑妹子信息时获取图片列表", response = String.class)
    @PostMapping(value = "/editGirlPicList")
    public String editGirlPicList(@Validated({GetList.class}) @RequestBody GirlPictureVO girlPictureVO, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("编辑妹子信息时获取图片列表:", girlPictureVO);
        return ResultUtil.successWithData(girlpictureService.editGirlPicList(girlPictureVO));
    }

}

