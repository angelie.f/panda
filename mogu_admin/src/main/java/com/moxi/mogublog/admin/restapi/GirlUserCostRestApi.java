package com.moxi.mogublog.admin.restapi;


import com.moxi.mogublog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.moxi.mogublog.admin.annotion.AvoidRepeatableCommit.AvoidRepeatableCommit;
import com.moxi.mogublog.admin.annotion.OperationLogger.OperationLogger;
import com.moxi.mogublog.admin.global.SysConf;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.service.GirlUserCostService;
import com.moxi.mogublog.xo.vo.GirlUserCostVO;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.validator.group.GetList;
import com.moxi.mougblog.base.validator.group.Insert;
import com.moxi.mougblog.base.validator.group.Update;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户费用表 RestApi
 *
 * @author Vergift
 * @since 2022-01-05
 */
@Api(value = "用户费用相关接口", tags = {"用户费用相关接口"})
@RestController
@RequestMapping("/girlUserCost")
@Slf4j
public class GirlUserCostRestApi {

    @Autowired
    private GirlUserCostService girlUserCostService;

    @AuthorityVerify
    @ApiOperation(value = "获取用户费用列表", notes = "获取用户费用列表", response = String.class)
    @PostMapping("/getList")
    public String getList(@Validated({GetList.class}) @RequestBody GirlUserCostVO girlUserCostVO, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("获取用户费用列表");
        return ResultUtil.result(SysConf.SUCCESS, girlUserCostService.getPageList(girlUserCostVO));
    }

    @AvoidRepeatableCommit
    @AuthorityVerify
    @OperationLogger(value = "增加用户费用")
    @ApiOperation(value = "增加用户费用", notes = "增加用户费用", response = String.class)
    @PostMapping("/add")
    public String add(@Validated({Insert.class}) @RequestBody GirlUserCostVO girlUserCostVO, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("增加用户费用");
        return girlUserCostService.addUserCost(girlUserCostVO);
    }

    @AuthorityVerify
    @OperationLogger(value = "编辑用户费用")
    @ApiOperation(value = "编辑用户费用", notes = "编辑用户费用", response = String.class)
    @PostMapping("/edit")
    public String edit(@Validated({Update.class}) @RequestBody GirlUserCostVO girlUserCostVO, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("编辑用户费用");
        return girlUserCostService.editUserCost(girlUserCostVO);
    }

    @AuthorityVerify
    @OperationLogger(value = "删除用户费用")
    @ApiOperation(value = "删除用户费用", notes = "删除用户费用", response = String.class)
    @PostMapping("/delete")
    public String delete(@Validated({Update.class}) @RequestBody GirlUserCostVO girlUserCostVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("删除用户费用");
        return girlUserCostService.deleteUserCost(girlUserCostVO);
    }
}

