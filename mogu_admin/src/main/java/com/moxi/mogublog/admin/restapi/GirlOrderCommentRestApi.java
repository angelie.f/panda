package com.moxi.mogublog.admin.restapi;


import com.moxi.mogublog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.moxi.mogublog.admin.annotion.OperationLogger.OperationLogger;
import com.moxi.mogublog.admin.global.SysConf;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.service.GirlGiftService;
import com.moxi.mogublog.xo.service.GirlOrderCommentService;
import com.moxi.mogublog.xo.vo.GirlGiftVO;
import com.moxi.mogublog.xo.vo.GirlOrderCommentVO;
import com.moxi.mogublog.xo.vo.LocalGirlVO;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.validator.group.GetList;
import com.moxi.mougblog.base.validator.group.Insert;
import com.moxi.mougblog.base.validator.group.Update;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 妹子订单评价 RestApi
 *
 * @author Vergift
 * @since 2022-01-05
 */
@Api(value = "妹子订单评价相关接口", tags = {"妹子订单评价相关接口"})
@RestController
@RequestMapping("/girlOrderComment")
@Slf4j
public class GirlOrderCommentRestApi {

    @Autowired
    private GirlOrderCommentService girlOrderCommentService;

    @AuthorityVerify
    @ApiOperation(value = "获取妹子评价列表", notes = "获取妹子评价列表", response = String.class)
    @PostMapping("/getList")
    public String getList(@Validated({GetList.class}) @RequestBody GirlOrderCommentVO girlOrderCommentVO, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("获取妹子评价列表");
        return ResultUtil.result(SysConf.SUCCESS, girlOrderCommentService.getPageList(girlOrderCommentVO));
    }

    @AuthorityVerify
    @OperationLogger(value = "编辑妹子评价")
    @ApiOperation(value = "编辑妹子评价", notes = "编辑妹子评价", response = String.class)
    @PostMapping("/edit")
    public String edit(@Validated({Insert.class}) @RequestBody GirlOrderCommentVO girlOrderCommentVO, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("编辑妹子评价");
        return girlOrderCommentService.edit(girlOrderCommentVO);
    }

    @AuthorityVerify
    @OperationLogger(value = "创建妹子评价回复")
    @ApiOperation(value = "编辑妹子评价", notes = "创建妹子评价回复", response = String.class)
    @PostMapping("/create")
    public String create(@Validated({Update.class}) @RequestBody GirlOrderCommentVO girlOrderCommentVO, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("创建妹子评价回复");
        return girlOrderCommentService.createCommentByOffice(girlOrderCommentVO);
    }
    @AuthorityVerify
    @OperationLogger(value = "创建妹子评价")
    @ApiOperation(value = "编辑妹子评价", notes = "创建妹子评价", response = String.class)
    @PostMapping("/createComment")
    public String createComment(@Validated({Update.class}) @RequestBody LocalGirlVO localGirlVO, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("创建妹子评价");
        return girlOrderCommentService.createComment(localGirlVO);
    }
}

