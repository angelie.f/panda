package com.moxi.mogublog.admin.restapi;

import com.moxi.mogublog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.moxi.mogublog.admin.annotion.AvoidRepeatableCommit.AvoidRepeatableCommit;
import com.moxi.mogublog.admin.annotion.OperationLogger.OperationLogger;
import com.moxi.mogublog.admin.global.SysConf;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.service.BooksProjectService;
import com.moxi.mogublog.xo.vo.BooksProjectVO;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.validator.group.GetList;
import com.moxi.mougblog.base.validator.group.Insert;
import com.moxi.mougblog.base.validator.group.Update;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 漫画项目 RestApi
 *
 * @author Sid
 * @since 2022-05-31
 */
@Api(value = "漫画项目相关接口", tags = {"漫画项目相关接口"})
@RestController
@RequestMapping("/booksProject")
@Slf4j
public class BooksProjectRestApi {

    @Autowired
    private BooksProjectService booksProjectService;

    @AuthorityVerify
    @ApiOperation(value = "获取漫画项目列表", notes = "获取漫画项目列表", response = String.class)
    @PostMapping("/getList")
    public String getList(@Validated({GetList.class}) @RequestBody BooksProjectVO booksProjectVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("获取漫画設定列表");
        return ResultUtil.result(SysConf.SUCCESS, booksProjectService.getPageList(booksProjectVO));
    }

    @AvoidRepeatableCommit
    @AuthorityVerify
    @OperationLogger(value = "增加漫画项目")
    @ApiOperation(value = "增加漫画项目", notes = "增加漫画项目", response = String.class)
    @PostMapping("/add")
    public String add(@Validated({Insert.class}) @RequestBody BooksProjectVO booksProjectVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("增加漫画项目: {}", booksProjectVO);
        return booksProjectService.add(booksProjectVO);
    }

    @AuthorityVerify
    @OperationLogger(value = "编辑漫画项目")
    @ApiOperation(value = "编辑漫画项目", notes = "编辑漫画项目", response = String.class)
    @PostMapping("/edit")
    public String edit(@Validated({Update.class}) @RequestBody BooksProjectVO booksProjectVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("编辑漫画项目");
        return booksProjectService.edit(booksProjectVO);
    }
    @AuthorityVerify
    @ApiOperation(value = "获取漫画项目列表", notes = "获取项目列表", response = String.class)
    @PostMapping("/sources")
    public String sources(@Validated({GetList.class}) @RequestBody BooksProjectVO booksProjectVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("获取漫画项目列表-项目列表");
        return ResultUtil.result(SysConf.SUCCESS, booksProjectService.sources(booksProjectVO));
    }
}

