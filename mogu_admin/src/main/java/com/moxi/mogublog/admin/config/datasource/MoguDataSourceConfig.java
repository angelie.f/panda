package com.moxi.mogublog.admin.config.datasource;

import com.alibaba.druid.pool.DruidDataSource;
import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.annotation.MapperScan;
import org.apache.ibatis.plugin.Interceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

@Slf4j
@Configuration
@MapperScan(basePackages = DBConstants.MOGU_DAO, sqlSessionFactoryRef = DBConstants.MOGU_SQL_SESSION_FACTORY)
public class MoguDataSourceConfig {

    @Autowired
    private MoguDataSourceProperties moguDataSourceProperties;

    @Autowired
    private PaginationInterceptor paginationInterceptor;
    /**
     * 声明其为Bean实例
     * 在同样的DataSource中，首先使用被标注的DataSource
     *
     * @return
     */
    @Bean(DBConstants.MOGU_DATA_SOURCE)
    @Primary
    public DataSource moguDataSource() {
        DruidDataSource datasource = new DruidDataSource();

        datasource.setUrl(moguDataSourceProperties.getUrl());
        datasource.setUsername(moguDataSourceProperties.getUsername());
        datasource.setPassword(moguDataSourceProperties.getPassword());
        datasource.setDriverClassName(moguDataSourceProperties.getDriverClassName());
        // configuration
        datasource.setInitialSize(moguDataSourceProperties.getInitialSize());
        datasource.setMinIdle(moguDataSourceProperties.getMinIdle());
        datasource.setMaxActive(moguDataSourceProperties.getMaxActive());
        datasource.setMaxWait(moguDataSourceProperties.getMaxWait());
        datasource.setTimeBetweenEvictionRunsMillis(moguDataSourceProperties.getTimeBetweenEvictionRunsMillis());
        datasource.setMinEvictableIdleTimeMillis(moguDataSourceProperties.getMinEvictableIdleTimeMillis());
        datasource.setValidationQuery(moguDataSourceProperties.getValidationQuery());
        datasource.setTestWhileIdle(moguDataSourceProperties.isTestWhileIdle());
        datasource.setTestOnBorrow(moguDataSourceProperties.isTestOnBorrow());
        datasource.setTestOnReturn(moguDataSourceProperties.isTestOnReturn());
        datasource.setPoolPreparedStatements(moguDataSourceProperties.isPoolPreparedStatements());
        datasource.setMaxPoolPreparedStatementPerConnectionSize(moguDataSourceProperties.getMaxPoolPreparedStatementPerConnectionSize());
        try {
            datasource.setFilters(moguDataSourceProperties.getFilters());
        } catch (SQLException e) {
            log.error("mogu druid configuration initialization filter");
        }
        datasource.setConnectionProperties(moguDataSourceProperties.getConnectionProperties());

        return datasource;
    }
    /**
     * 自定义的mapper
     *
     * @return
     */
    @Primary
    @Bean(DBConstants.MOGU_MAPPER_LOCATIONS)
    public Resource[] resolveMapperLocations() {
        List<Resource> resources = new ArrayList();
        try {
            String mapperLocations = moguDataSourceProperties.getMapperLocations();
            if (mapperLocations != null) {
                ResourcePatternResolver resourceResolver = new PathMatchingResourcePatternResolver();
                Resource[] mappers = null;
                for (String mapperLocation : mapperLocations.split(",")) {
                    mappers = resourceResolver.getResources(mapperLocation.trim());
                    resources.addAll(Arrays.asList(mappers));
                }
            }
        } catch (IOException e) {
        }
        return resources.toArray(new Resource[resources.size()]);
    }

    /**
     * 创建第一个SqlSessionFactory
     *
     * @param dataSource
     * @return
     * @throws Exception
     */
    @Bean(DBConstants.MOGU_SQL_SESSION_FACTORY)
    @Primary
    public SqlSessionFactory moguSqlSessionFactory(@Qualifier(DBConstants.MOGU_DATA_SOURCE) DataSource dataSource,
                                                     @Qualifier(DBConstants.MOGU_MAPPER_LOCATIONS) Resource[] mapperLocations)
            throws Exception {
        /**
         * 必须使用MybatisSqlSessionFactoryBean，
         * 不能使用SqlSessionFactoryBean，不然会报invalid bound statement (not found)
         *
         * com.baomidou.mybatisplus.autoconfigure.MybatisPlusAutoConfiguration#sqlSessionFactory(javax.sql.DataSource)
         * 源码中也是使用MybatisSqlSessionFactoryBean
         * 并且源码中使用了@ConditionalOnMissingBean，即IOC中如果存在了SqlSessionFactory实例，mybatis-plus就不创建SqlSessionFactory实例了
         */
        MybatisSqlSessionFactoryBean sessionFactoryBean = new MybatisSqlSessionFactoryBean();
        sessionFactoryBean.setDataSource(dataSource);
        MybatisConfiguration configuration = new MybatisConfiguration();
        sessionFactoryBean.setConfiguration(configuration);
        sessionFactoryBean.setMapperLocations(mapperLocations);
        Interceptor[] plugins = {paginationInterceptor};
        sessionFactoryBean.setPlugins(plugins);
        return sessionFactoryBean.getObject();
    }


}
