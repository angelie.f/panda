package com.moxi.mogublog.admin.restapi;


import com.moxi.mogublog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.moxi.mogublog.admin.annotion.AvoidRepeatableCommit.AvoidRepeatableCommit;
import com.moxi.mogublog.admin.annotion.OperationLogger.OperationLogger;
import com.moxi.mogublog.admin.global.SysConf;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.service.GirlSortService;
import com.moxi.mogublog.xo.vo.GirlSortVO;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.validator.group.GetList;
import com.moxi.mougblog.base.validator.group.Insert;
import com.moxi.mougblog.base.validator.group.Update;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 妹子分类表 RestApi
 *
 * @author Vergift
 * @date 2022-01-12
 */
@Api(value = "妹子分类相关接口", tags = {"妹子分类相关接口"})
@RestController
@RequestMapping("/girlSort")
@Slf4j
public class GirlSortRestApi {

    @Autowired
    private GirlSortService girlSortService;

    @AuthorityVerify
    @ApiOperation(value = "获取妹子分类表", notes = "获取妹子分类表", response = String.class)
    @PostMapping(value = "/getList")
    public String getList(@Validated({GetList.class}) @RequestBody GirlSortVO girlSortVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("获取妹子分类表列表: {}", girlSortVO);
        return ResultUtil.result(SysConf.SUCCESS, girlSortService.getPageList(girlSortVO));
    }

    @AvoidRepeatableCommit
    @AuthorityVerify
    @OperationLogger(value = "增加妹子分类表")
    @ApiOperation(value = "增加妹子分类表", notes = "增加妹子分类表", response = String.class)
    @PostMapping("/add")
    public String add(@Validated({Insert.class}) @RequestBody GirlSortVO girlSortVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("增加妹子分类表: {}", girlSortVO);
        return girlSortService.addGirlSort(girlSortVO);
    }

    @AuthorityVerify
    @OperationLogger(value = "编辑妹子分类表")
    @ApiOperation(value = "编辑妹子分类表", notes = "编辑妹子分类表", response = String.class)
    @PostMapping("/edit")
    public String edit(@Validated({Update.class}) @RequestBody GirlSortVO girlSortVO, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("编辑妹子分类表: {}", girlSortVO);
        return girlSortService.editGirlSort(girlSortVO);
    }

    @AuthorityVerify
    @OperationLogger(value = "删除妹子分类表")
    @ApiOperation(value = "删除妹子分类表", notes = "删除妹子分类表", response = String.class)
    @PostMapping("/delete")
    public String delete(@Validated({Update.class}) @RequestBody GirlSortVO girlSortVO, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("编辑妹子分类表: {}", girlSortVO);
        return girlSortService.deleteGirlSort(girlSortVO);
    }

}

