package com.moxi.mogublog.admin.restapi;


import com.moxi.mogublog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.moxi.mogublog.admin.annotion.AvoidRepeatableCommit.AvoidRepeatableCommit;
import com.moxi.mogublog.admin.annotion.OperationLogger.OperationLogger;
import com.moxi.mogublog.admin.global.SysConf;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.service.PictureTagService;
import com.moxi.mogublog.xo.vo.PictureTagVO;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.validator.group.Delete;
import com.moxi.mougblog.base.validator.group.GetList;
import com.moxi.mougblog.base.validator.group.Insert;
import com.moxi.mougblog.base.validator.group.Update;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 图片标签表 RestApi
 *
 * @author Vergift
 * @since 2021-11-22
 */
@Api(value = "图片标签相关接口", tags = {"图片标签相关接口"})
@RestController
@RequestMapping("/pictureTag")
@Slf4j
public class PictureTagRestApi {

    @Autowired
    private PictureTagService pictureTagService;

    @AuthorityVerify
    @ApiOperation(value = "获取图片标签列表", notes = "获取图片标签列表", response = String.class)
    @PostMapping("/getList")
    public String getList(@Validated({GetList.class}) @RequestBody PictureTagVO pictureTagVO, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("获取标签列表");
        return ResultUtil.result(SysConf.SUCCESS, pictureTagService.getPageList(pictureTagVO));
    }

    @AvoidRepeatableCommit
    @AuthorityVerify
    @OperationLogger(value = "增加图片标签")
    @ApiOperation(value = "增加图片标签", notes = "增加图片标签", response = String.class)
    @PostMapping("/add")
    public String add(@Validated({Insert.class}) @RequestBody PictureTagVO pictureTagVO, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("增加图片标签");
        return pictureTagService.addTag(pictureTagVO);
    }

    @AuthorityVerify
    @OperationLogger(value = "编辑图片标签")
    @ApiOperation(value = "编辑图片标签", notes = "编辑图片标签", response = String.class)
    @PostMapping("/edit")
    public String edit(@Validated({Update.class}) @RequestBody PictureTagVO pictureTagVO, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("编辑图片标签");
        return pictureTagService.editTag(pictureTagVO);
    }

    @AuthorityVerify
    @OperationLogger(value = "批量删除图片标签")
    @ApiOperation(value = "批量删除图片标签", notes = "批量删除图片标签", response = String.class)
    @PostMapping("/deleteBatch")
    public String delete(@Validated({Delete.class}) @RequestBody List<PictureTagVO> tagVoList, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("批量删除图片标签");
        return pictureTagService.deleteBatchTag(tagVoList);
    }

    @AuthorityVerify
    @OperationLogger(value = "置顶图片标签")
    @ApiOperation(value = "置顶图片标签", notes = "置顶图片标签", response = String.class)
    @PostMapping("/stick")
    public String stick(@Validated({Delete.class}) @RequestBody PictureTagVO pictureTagVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("置顶图片标签");
        return pictureTagService.stickTag(pictureTagVO);
    }

    @AuthorityVerify
    @OperationLogger(value = "通过点击量排序图片标签")
    @ApiOperation(value = "通过点击量图片排序标签", notes = "通过点击量排序图片标签", response = String.class)
    @PostMapping("/tagSortByClickCount")
    public String tagSortByClickCount() {
        log.info("通过点击量排序图片标签");
        return pictureTagService.tagSortByClickCount();
    }

    /**
     * 通过引用量排序图片标签
     * 引用量就是所有的图集中，有多少使用了该标签，如果使用的越多，该图片标签的引用量越大，那么排名越靠前
     *
     * @return
     */
    @AuthorityVerify
    @OperationLogger(value = "通过引用量排序图片标签")
    @ApiOperation(value = "通过引用量排序图片标签", notes = "通过引用量排序图片标签", response = String.class)
    @PostMapping("/tagSortByCite")
    public String tagSortByCite() {
        log.info("通过引用量排序图片标签");
        return pictureTagService.tagSortByCite();
    }
}

