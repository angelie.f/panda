package com.moxi.mogublog.admin.restapi;


import com.moxi.mogublog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.moxi.mogublog.admin.annotion.OperationLogger.OperationLogger;
import com.moxi.mogublog.admin.global.SysConf;
import com.moxi.mogublog.commons.entity.UsePower;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.service.UsePowerService;
import com.moxi.mogublog.xo.vo.PictureTagVO;
import com.moxi.mogublog.xo.vo.UsePowerVO;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.validator.group.GetList;
import com.moxi.mougblog.base.validator.group.Update;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 使用权限表 RestApi
 *
 * @author Vergift
 * @since 2021-12-04
 */
@Api(value = "使用权限关接口", tags = {"图使用权限相关接口"})
@RestController
@RequestMapping("/usePower")
@Slf4j
public class UsePowerRestApi {

    @Autowired
    private UsePowerService usePowerService;

    @AuthorityVerify
    @ApiOperation(value = "使用权限列表", notes = "使用权限列表", response = String.class)
    @PostMapping("/getList")
    public String getList(@Validated({GetList.class}) @RequestBody UsePowerVO usePowerVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("使用权限列表");
        return ResultUtil.result(SysConf.SUCCESS, usePowerService.getPageList(usePowerVO));
    }


    @AuthorityVerify
    @ApiOperation(value = "使用权限消息", notes = "使用权限消息", response = String.class)
    @PostMapping("/getUsePower")
    public String getUsePower(@Validated({GetList.class}) @RequestBody UsePowerVO usePowerVO, BindingResult result) {
        log.info("获取权限消息: {}", usePowerVO);
        return ResultUtil.result(SysConf.SUCCESS, usePowerService.getUsePower(usePowerVO));
    }

    @AuthorityVerify
    @OperationLogger(value = "编辑使用权限")
    @ApiOperation(value = "编辑图使用权限", notes = "编辑使用权限", response = String.class)
    @PostMapping("/edit")
    public String edit(@Validated({Update.class}) @RequestBody UsePowerVO usePowerVO, BindingResult result) {
        // 参数校验
        log.info("编辑权限消息");
        return usePowerService.editUsePower(usePowerVO);
    }

    @AuthorityVerify
    @OperationLogger(value = "编辑使用权限")
    @ApiOperation(value = "编辑图使用权限", notes = "编辑使用权限", response = String.class)
    @PostMapping("/add")
    public String add(@Validated({Update.class}) @RequestBody UsePower usePower, BindingResult result) {
        // 参数校验
        log.info("编辑权限消息");
        return usePowerService.addUsePower(usePower);
    }

}

