package com.moxi.mogublog.admin.task;

import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

@Slf4j
public class QuartzJob implements Job {
    @Autowired
    private PandnGameReportTask pandnGameReportTask;
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        DateTime dateTime = new DateTime(convertTimezone(new Date(),TimeZone.getDefault(),TimeZone.getTimeZone("Asia/Shanghai")));
        pandnGameReportTask.scheduled(dateTime);
        log.info("{}:开始执行平台概况和活动概况异步定时任务!,当前时区:{}",dateTime,TimeZone.getDefault());
    }
    private  Date convertTimezone(Date sourceDate, TimeZone sourceTimezone, TimeZone targetTimezone){
        Calendar calendar=Calendar.getInstance();
        long sourceTime=sourceDate.getTime();
        calendar.setTimeZone(sourceTimezone);
        calendar.setTimeInMillis(sourceTime);
        int sourceZoneOffset=calendar.get(Calendar.ZONE_OFFSET);
        calendar.setTimeZone(targetTimezone);
        calendar.setTimeInMillis(sourceTime);
        int targetZoneOffset=calendar.get(Calendar.ZONE_OFFSET);
        int targetDaylightOffset=calendar.get(Calendar.DST_OFFSET);
        long targetTime=sourceTime+ (targetZoneOffset+targetDaylightOffset) -sourceZoneOffset;
        return new Date(targetTime);
    }

}
