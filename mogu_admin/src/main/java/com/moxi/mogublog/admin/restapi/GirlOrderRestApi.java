package com.moxi.mogublog.admin.restapi;


import com.moxi.mogublog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.moxi.mogublog.admin.annotion.OperationLogger.OperationLogger;
import com.moxi.mogublog.admin.global.SysConf;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.service.GirlOrderService;
import com.moxi.mogublog.xo.vo.GirlOrderVO;
import com.moxi.mogublog.xo.vo.LocalGirlVO;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.validator.group.GetList;
import com.moxi.mougblog.base.validator.group.Update;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 妹子订单类别 RestApi
 *
 * @author Vergift
 * @since 2022-01-05
 */
@Api(value = "妹子订单相关接口", tags = {"妹子订单相关接口"})
@RestController
@RequestMapping("/girlOrder")
@Slf4j
public class GirlOrderRestApi {

    @Autowired
    private GirlOrderService girlOrderService;

    @AuthorityVerify
    @ApiOperation(value = "获取妹子订单", notes = "获取妹子订单", response = String.class)
    @PostMapping("/getList")
    public String getList(@Validated({GetList.class}) @RequestBody GirlOrderVO girlOrderVO, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("获取妹子订单列表");
        return ResultUtil.result(SysConf.SUCCESS, girlOrderService.getPageList(girlOrderVO));
    }

    @AuthorityVerify
    @ApiOperation(value = "获取妹子订单", notes = "获取金額列表", response = String.class)
    @PostMapping("/amounts")
    public String amounts(@Validated({GetList.class}) @RequestBody LocalGirlVO localGirlVO, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("获取妹子订单列表");
        return ResultUtil.result(SysConf.SUCCESS, girlOrderService.amounts(localGirlVO));
    }

    @AuthorityVerify
    @ApiOperation(value = "获取妹子订单", notes = "获取會員等級列表", response = String.class)
    @PostMapping("/vipLevels")
    public String vipLevels(@Validated({GetList.class}) @RequestBody LocalGirlVO localGirlVO, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("获取會員等級列表");
        return ResultUtil.result(SysConf.SUCCESS, girlOrderService.vipLevels(localGirlVO));
    }

    @AuthorityVerify
    @ApiOperation(value = "获取妹子订单", notes = "获取来源渠道列表", response = String.class)
    @PostMapping("/sources")
    public String sources(@Validated({GetList.class}) @RequestBody LocalGirlVO localGirlVO, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("获取来源渠道列表");
        return ResultUtil.result(SysConf.SUCCESS, girlOrderService.sources(localGirlVO));
    }

    @AuthorityVerify
    @ApiOperation(value = "获取妹子订单", notes = "获取订单统计数据", response = String.class)
    @PostMapping("/sumAndCount")
    public String sumAndCount(@Validated({GetList.class}) @RequestBody GirlOrderVO girlOrderVO, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("获取订单统计数据");
        return ResultUtil.result(SysConf.SUCCESS, girlOrderService.getGirlOrderCountSum(girlOrderVO));
    }

    @AuthorityVerify
    @OperationLogger(value = "编辑妹子订单")
    @ApiOperation(value = "编辑妹子订单", notes = "编辑妹子订单", response = String.class)
    @PostMapping("/edit")
    public String edit(@Validated({Update.class}) @RequestBody GirlOrderVO girlOrderVO, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("编辑妹子订单");
        return girlOrderService.editOrder(girlOrderVO);
    }
}

