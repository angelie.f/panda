package com.moxi.mogublog.admin.scheduler.job;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.*;
import com.moxi.mogublog.xo.mapper.*;
import com.moxi.mogublog.xo.thirdparty.mapper.*;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import javax.annotation.Resource;
import java.time.LocalDate;


@Slf4j
@Data
@Service
@Scope("prototype")
public class SynChaptersJob {

    @Resource
    TpBookChaptersMapper tpBookChaptersMapper;
    @Resource
    DTpBookChaptersMapper dTpBookChaptersMapper;

    public void execute(LocalDate executeDate) {
        log.info("{} started",getClass().getName());

        try {
            synBookChapters(executeDate);
        } catch (Exception e) {
            log.info("{} Error -- cause by :{}" ,getClass().getName(),e.getMessage());
        }
        log.info("{} end",getClass().getName());

    }
    private void synBookChapters(LocalDate synDate) throws Exception{
        StopWatch watch = new StopWatch("synBookChapters");
        watch.start();
        QueryWrapper<TpBookChapters> wrapper = new QueryWrapper<>();
        log.info("synBookChapters start lastDay:{}",synDate.toString());
        wrapper.between("updated_at",synDate,synDate.plusDays(1));
        wrapper.orderByAsc("updated_at");
        int pageSize=100;
        Integer finish =0;
        Integer total =dTpBookChaptersMapper.selectCount(wrapper);
        int exetimes =total/pageSize;
        if(total%pageSize>0){
            exetimes++;
        }
        Page<TpBookChapters> page = new Page<>();
        for(int current=1;current<=exetimes;current++){
            page.setCurrent(current);
            page.setSize(pageSize);
            IPage<TpBookChapters> pageBookChapters =dTpBookChaptersMapper.selectPage(page,wrapper);
            if(pageBookChapters.getRecords().size()>0){
                int count = tpBookChaptersMapper.insertOrUpdateBatch(pageBookChapters.getRecords());
                finish+= pageBookChapters.getRecords().size();
                log.info("synBookChapters process current:{}, per count:{},total:{},currentPage:{},finish total:{}",current,pageBookChapters.getRecords().size(),total, current,finish);
            }
        }
        watch.stop();
        watch.prettyPrint();
    }
}
