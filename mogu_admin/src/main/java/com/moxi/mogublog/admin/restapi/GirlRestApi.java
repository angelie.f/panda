package com.moxi.mogublog.admin.restapi;


import com.moxi.mogublog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.moxi.mogublog.admin.annotion.AvoidRepeatableCommit.AvoidRepeatableCommit;
import com.moxi.mogublog.admin.annotion.OperationLogger.OperationLogger;
import com.moxi.mogublog.admin.global.SysConf;
import com.moxi.mogublog.commons.entity.GirlSortItem;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.service.GirlService;
import com.moxi.mogublog.xo.vo.GirlBySortVO;
import com.moxi.mogublog.xo.vo.GirlVO;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.validator.group.Delete;
import com.moxi.mougblog.base.validator.group.GetList;
import com.moxi.mougblog.base.validator.group.Insert;
import com.moxi.mougblog.base.validator.group.Update;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 妹子资源表 RestApi
 *
 * @author Vergift
 * @since 2022-01-10
 */
@Api(value = "妹子资源相关接口", tags = {"妹子资源相关接口"})
@RestController
@RequestMapping("/girl")
@Slf4j
public class GirlRestApi {

    @Autowired
    private GirlService girlService;

    @AuthorityVerify
    @ApiOperation(value = "获取妹子资源列表", notes = "获取妹子资源列表", response = String.class)
    @PostMapping("/getList")
    public String getList(@Validated({GetList.class}) @RequestBody GirlVO girlVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("获取妹子资源列表");
        return ResultUtil.result(SysConf.SUCCESS, girlService.getPageList(girlVO));
    }

    @AvoidRepeatableCommit
    @AuthorityVerify
    @OperationLogger(value = "增加妹子资源")
    @ApiOperation(value = "增加妹子资源", notes = "增加妹子资源", response = String.class)
    @PostMapping("/add")
    public String add(@Validated({Insert.class}) @RequestBody GirlVO girlVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("增加妹子资源");
        return girlService.addGirl(girlVO);
    }

    @AuthorityVerify
    @OperationLogger(value = "编辑妹子资源")
    @ApiOperation(value = "编辑妹子资源", notes = "编辑妹子资源", response = String.class)
    @PostMapping("/edit")
    public String edit(@Validated({Update.class}) @RequestBody GirlVO girlVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("编辑妹子资源");
        return girlService.editGirl(girlVO);
    }

    @AuthorityVerify
    @OperationLogger(value = "删除妹子资源")
    @ApiOperation(value = "删除妹子资源", notes = "删除妹子资源", response = String.class)
    @PostMapping("/delete")
    public String delete(@Validated({Update.class}) @RequestBody GirlVO girlVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("删除妹子资源");
        return girlService.deleteGirl(girlVO);
    }
    @AuthorityVerify
    @OperationLogger(value = "改变妹子空闲状态")
    @ApiOperation(value = "改变妹子空闲状态", notes = "改变妹子空闲状态", response = String.class)
    @PostMapping("/changeAvailable")
    public String changeAvailable(@Validated({Update.class}) @RequestBody GirlVO girlVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("改变妹子空闲状态");
        return girlService.changeAvailable(girlVO);
    }
    @AuthorityVerify
    @ApiOperation(value = "获取妹子列表", notes = "获取妹子列表", response = String.class)
    @PostMapping(value = "/getGirlListBySort")
    public String getGirlListBySort(@Validated({GetList.class}) @RequestBody GirlBySortVO girlBySortVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("获取妹子列表: {}", girlBySortVO);
        return ResultUtil.result(SysConf.SUCCESS, girlService.getGirlListBySort(girlBySortVO));
    }

    @AuthorityVerify
    @OperationLogger(value = "妹子上下架")
    @ApiOperation(value = "妹子上下架", notes = "妹子上下架", response = String.class)
    @PostMapping("/upOrOut")
    public String upOrOut(@Validated({Delete.class}) @RequestBody List<GirlSortItem> list, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("妹子上下架: {}", list);
        return girlService.upOrOut(list);
    }
}

