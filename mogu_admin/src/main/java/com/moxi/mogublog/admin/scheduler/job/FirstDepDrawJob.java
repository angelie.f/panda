package com.moxi.mogublog.admin.scheduler.job;

import com.moxi.mogublog.commons.entity.GameAddCoins;
import com.moxi.mogublog.commons.entity.GameDepositAward;
import com.moxi.mogublog.commons.entity.GameDepositRequest;
import com.moxi.mogublog.xo.mapper.GameDepositAwardMapper;
import com.moxi.mogublog.xo.mapper.GameDepositRequestMapper;
import com.moxi.mogublog.xo.service.GameAddcoinsService;
import com.moxi.mogublog.xo.vo.GameAddCoinsVo;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;


@Slf4j
@Data
@Service
@Scope("prototype")
public class FirstDepDrawJob {

    @Resource
    GameDepositAwardMapper gameDepositAwardMapper;
    @Resource
    GameDepositRequestMapper gameDepositRequestMapper;

    @Resource
    GameAddcoinsService gameAddcoinsService;

    public void execute(LocalDate executeDate) {
        log.info("{} started",getClass().getName());
        try {
            draw(executeDate);

        } catch (Exception e) {
            log.info("{} Error -- cause by :{}" ,getClass().getName(),e.getMessage());
        }
        log.info("{} end",getClass().getName());

    }
    private void draw(LocalDate executeDate) throws Exception{

        log.info("draw start awardDay:{}",executeDate.toString());
        int awardCount = gameDepositRequestMapper.countFirstDepositByDate(executeDate);
        if(awardCount>0){
            GameDepositRequest gameDepositRequest = gameDepositRequestMapper.getFirstDepRandomWinner(executeDate);
            GameDepositAward gameDepositAward = new GameDepositAward();
            gameDepositAward.setCustomerId(gameDepositRequest.getCustomerId());
            gameDepositAward.setSource(gameDepositRequest.getSource());
            gameDepositAward.setPeriod(executeDate.toString());
            gameDepositAward.setFirstDepositNum(awardCount);
            Integer coins =awardCount*2;
            gameDepositAward.setAward(BigDecimal.valueOf(coins));
            gameDepositAward.insert();
            GameAddCoinsVo gameAddCoinsVo = new GameAddCoinsVo();
            gameAddCoinsVo.setOpenId(gameDepositRequest.getOpenId());
            gameAddCoinsVo.setCoins(coins);
            gameAddCoinsVo.setRemark("首存奖励赠币");
            gameAddCoinsVo.setOperName("admin");
            String result = gameAddcoinsService.addConis(gameAddCoinsVo);
            log.info("draw addcoins result:{}",result);
        }
        log.info("draw award count :{}",awardCount);
    }


}
