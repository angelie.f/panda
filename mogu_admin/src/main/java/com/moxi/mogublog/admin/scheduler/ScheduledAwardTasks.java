package com.moxi.mogublog.admin.scheduler;

import com.moxi.mogublog.admin.scheduler.job.*;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalTime;


@Slf4j
@Component
@Data
@ConditionalOnProperty(prefix = "game", name = "firstdepdraw", havingValue = "true")
public class ScheduledAwardTasks {

    @Resource
    FirstDepDrawJob firstDepDrawJob;

    private LocalDate getExecuteDate() {
        LocalDate executeDate = LocalDate.now();
        executeDate = executeDate.plusDays(-1);
        return executeDate;
    }

    @Scheduled(cron = "0 5 0 * * ?")//每天00:05执行開獎
    //@PostConstruct //启动项目先执行
    public void firstDepDraw() {
        LocalDate executeDate = getExecuteDate();
        firstDepDrawJob.execute(executeDate);
    }



}
