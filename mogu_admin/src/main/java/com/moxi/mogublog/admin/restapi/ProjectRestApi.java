package com.moxi.mogublog.admin.restapi;


import com.moxi.mogublog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.moxi.mogublog.admin.annotion.AvoidRepeatableCommit.AvoidRepeatableCommit;
import com.moxi.mogublog.admin.annotion.OperationLogger.OperationLogger;
import com.moxi.mogublog.admin.global.SysConf;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.service.ProjectService;
import com.moxi.mogublog.xo.vo.ProjectVO;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.validator.group.Delete;
import com.moxi.mougblog.base.validator.group.GetList;
import com.moxi.mougblog.base.validator.group.Insert;
import com.moxi.mougblog.base.validator.group.Update;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 项目表 RestApi
 *
 * @author Vergift
 * @since 2021-11-25
 */
@Api(value = "项目相关接口", tags = {"项目相关接口"})
@RestController
@RequestMapping("/project")
@Slf4j
public class ProjectRestApi {

    @Autowired
    private ProjectService projectService;

    @AuthorityVerify
    @ApiOperation(value = "获取项目列表", notes = "获取项目列表", response = String.class)
    @PostMapping("/getList")
    public String getList(@Validated({GetList.class}) @RequestBody ProjectVO projectVO, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("获取项目列表");
        return ResultUtil.result(SysConf.SUCCESS, projectService.getPageList(projectVO));
    }

    @AvoidRepeatableCommit
    @AuthorityVerify
    @OperationLogger(value = "增加项目")
    @ApiOperation(value = "增加项目", notes = "增加项目", response = String.class)
    @PostMapping("/add")
    public String add(@Validated({Insert.class}) @RequestBody ProjectVO projectVO, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("增加项目");
        return projectService.addTag(projectVO);
    }

    @AuthorityVerify
    @OperationLogger(value = "编辑项目")
    @ApiOperation(value = "编辑项目", notes = "编辑项目", response = String.class)
    @PostMapping("/edit")
    public String edit(@Validated({Update.class}) @RequestBody ProjectVO projectVO, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("编辑项目");
        return projectService.editTag(projectVO);
    }

    @AuthorityVerify
    @OperationLogger(value = "批量删除项目")
    @ApiOperation(value = "批量删除项目", notes = "批量删除项目", response = String.class)
    @PostMapping("/deleteBatch")
    public String delete(@Validated({Delete.class}) @RequestBody List<ProjectVO> projectVoList, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("批量删除项目");
        return projectService.deleteBatchTag(projectVoList);
    }

}

