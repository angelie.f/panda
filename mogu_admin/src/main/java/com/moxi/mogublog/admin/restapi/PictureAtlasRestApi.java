package com.moxi.mogublog.admin.restapi;


import com.moxi.mogublog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.moxi.mogublog.admin.annotion.AvoidRepeatableCommit.AvoidRepeatableCommit;
import com.moxi.mogublog.admin.annotion.OperationLogger.OperationLogger;
import com.moxi.mogublog.admin.global.SysConf;
import com.moxi.mogublog.commons.entity.PictureAtlas;
import com.moxi.mogublog.commons.entity.PictureSortItem;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.service.PictureAtlasService;
import com.moxi.mogublog.xo.vo.PictureAtlasBySortVO;
import com.moxi.mogublog.xo.vo.PictureAtlasVO;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.validator.group.Delete;
import com.moxi.mougblog.base.validator.group.GetList;
import com.moxi.mougblog.base.validator.group.Insert;
import com.moxi.mougblog.base.validator.group.Update;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 图集表 RestApi
 *
 * @author 陌溪
 * @date 2018年9月17日16:37:13
 */
@Api(value = "图集相关接口", tags = {"图集关接口"})
@RestController
@RequestMapping("/pictureAtlas")
@Slf4j
public class PictureAtlasRestApi {

    @Autowired
    private PictureAtlasService pictureAtlasService;

    @AuthorityVerify
    @ApiOperation(value = "获取图集列表", notes = "获取图集列表", response = String.class)
    @PostMapping(value = "/getList")
    public String getList(@Validated({GetList.class}) @RequestBody PictureAtlasVO pictureAtlasVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("获取图集列表: {}", pictureAtlasVO);
        return ResultUtil.result(SysConf.SUCCESS, pictureAtlasService.getPageList(pictureAtlasVO));
    }

    @AuthorityVerify
    @ApiOperation(value = "获取图片来源列表", notes = "获取图片来源列表", response = String.class)
    @PostMapping(value = "/getResourceList")
    public String getResourceList() {
        log.info("获取图集列表: ");
        return ResultUtil.result(SysConf.SUCCESS, pictureAtlasService.getResourceList());
    }

    @AvoidRepeatableCommit
    @AuthorityVerify
    @OperationLogger(value = "增加图集")
    @ApiOperation(value = "增加图集", notes = "增加图集", response = String.class)
    @PostMapping("/add")
    public String add(@Validated({Insert.class}) @RequestBody PictureAtlasVO pictureAtlasVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("增加图集: {}", pictureAtlasVO);
        return pictureAtlasService.addPictureAtlas(pictureAtlasVO);
    }

    @AuthorityVerify
    @OperationLogger(value = "编辑图集")
    @ApiOperation(value = "编辑图集", notes = "编辑图集", response = String.class)
    @PostMapping("/edit")
    public String edit(@Validated({Update.class}) @RequestBody PictureAtlasVO pictureAtlasVO, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("编辑图集: {}", pictureAtlasVO);
        return pictureAtlasService.editPictureAtlas(pictureAtlasVO);
    }

    @AuthorityVerify
    @OperationLogger(value = "删除图集")
    @ApiOperation(value = "删除图集", notes = "删除图集", response = String.class)
    @PostMapping("/delete")
    public String delete(@Validated({Delete.class}) @RequestBody PictureAtlasVO pictureAtlasVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("删除图集: {}", pictureAtlasVO);
        return pictureAtlasService.deletePictureAtlas(pictureAtlasVO);
    }

    @AuthorityVerify
    @OperationLogger(value = "修改标签")
    @ApiOperation(value = "修改标签", notes = "修改标签", response = String.class)
    @PostMapping("/updateTag")
    public String updateTag(@Validated({Delete.class}) @RequestBody List<PictureAtlasVO> list, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("修改标签: {}", list);
        return pictureAtlasService.updateTag(list);
    }

    @AuthorityVerify
    @ApiOperation(value = "获取图集列表", notes = "获取图集列表", response = String.class)
    @PostMapping(value = "/getAtlasListBySort")
    public String getAtlasListBySort(@Validated({GetList.class}) @RequestBody PictureAtlasBySortVO pictureAtlasBySortVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("获取图集列表: {}", pictureAtlasBySortVO);
        return ResultUtil.result(SysConf.SUCCESS, pictureAtlasService.getAtlasListBySort(pictureAtlasBySortVO));
    }

    @AuthorityVerify
    @OperationLogger(value = "图集上下架")
    @ApiOperation(value = "图集上下架", notes = "图集上下架", response = String.class)
    @PostMapping("/upOrOut")
    public String upOrOut(@Validated({Delete.class}) @RequestBody List<PictureSortItem> list, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("图集上下架: {}", list);
        return pictureAtlasService.upOrOut(list);
    }

}

