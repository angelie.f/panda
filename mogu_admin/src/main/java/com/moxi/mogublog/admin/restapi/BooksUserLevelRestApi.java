package com.moxi.mogublog.admin.restapi;

import com.moxi.mogublog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.moxi.mogublog.admin.annotion.AvoidRepeatableCommit.AvoidRepeatableCommit;
import com.moxi.mogublog.admin.annotion.OperationLogger.OperationLogger;
import com.moxi.mogublog.admin.global.SysConf;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.service.BooksUserLevelService;
import com.moxi.mogublog.xo.vo.BooksUserLevelVO;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.validator.group.GetList;
import com.moxi.mougblog.base.validator.group.Insert;
import com.moxi.mougblog.base.validator.group.Update;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 漫画用戶等级设定 RestApi
 *
 * @author Sid
 * @since 2022-05-31
 */
@Api(value = "漫画用戶等级设定相关接口", tags = {"漫画用戶等级设定相关接口"})
@RestController
@RequestMapping("/booksUserLevel")
@Slf4j
public class BooksUserLevelRestApi {

    @Autowired
    private BooksUserLevelService booksUserLevelService;

    @AuthorityVerify
    @ApiOperation(value = "获取漫画用戶等级设定列表", notes = "获取漫画用戶等级设定列表", response = String.class)
    @PostMapping("/getList")
    public String getList(@Validated({GetList.class}) @RequestBody BooksUserLevelVO booksUserLevelVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("获取漫画用戶等级设定列表");
        return ResultUtil.result(SysConf.SUCCESS, booksUserLevelService.getPageList(booksUserLevelVO));
    }

    @AvoidRepeatableCommit
    @AuthorityVerify
    @OperationLogger(value = "增加漫画用戶等级设定")
    @ApiOperation(value = "增加漫画用戶等级设定", notes = "增加漫画用戶等级设定", response = String.class)
    @PostMapping("/add")
    public String add(@Validated({Insert.class}) @RequestBody BooksUserLevelVO booksUserLevelVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("增加漫画用戶等级设定: {}", booksUserLevelVO);
        return booksUserLevelService.add(booksUserLevelVO);
    }

    @AuthorityVerify
    @OperationLogger(value = "编辑漫画用戶等级设定")
    @ApiOperation(value = "编辑漫画用戶等级设定", notes = "编辑漫画用戶等级设定", response = String.class)
    @PostMapping("/edit")
    public String edit(@Validated({Update.class}) @RequestBody BooksUserLevelVO booksUserLevelVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("编辑漫画用戶等级设定");
        return booksUserLevelService.edit(booksUserLevelVO);
    }
    @AuthorityVerify
    @OperationLogger(value = "删除漫画用戶等级设定")
    @ApiOperation(value = "删除漫画用戶等级设定", notes = "删除漫画用戶等级设定", response = String.class)
    @PostMapping("/delete")
    public String delete(@Validated({Update.class}) @RequestBody BooksUserLevelVO booksUserLevelVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("删除漫画用戶等级设定");
        return booksUserLevelService.delete(booksUserLevelVO);
    }

}

