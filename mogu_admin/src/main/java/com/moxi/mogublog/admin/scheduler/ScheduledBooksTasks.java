package com.moxi.mogublog.admin.scheduler;

import com.moxi.mogublog.admin.scheduler.job.SynBooksJob;
import com.moxi.mogublog.admin.scheduler.job.SynChaptersJob;
import com.moxi.mogublog.admin.scheduler.job.SynTagsJob;
import com.moxi.mogublog.admin.scheduler.job.SynTopicsJob;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalTime;


@Slf4j
@Component
@Data
@ConditionalOnProperty(prefix = "books", name = "synthird", havingValue = "true")
public class ScheduledBooksTasks {

    @Resource
    SynBooksJob synBooksJob;
    @Resource
    SynChaptersJob synChaptersJob;
    @Resource
    SynTopicsJob synTopicsJob;
    @Resource
    SynTagsJob synTagsJob;

    private LocalDate getExecuteDate() {
        LocalTime now = LocalTime.now();
        LocalDate executeDate = LocalDate.now();
        executeDate = executeDate.plusDays(-1);
        return executeDate;
    }

    @Scheduled(cron = "0 0 3 * * ?")//每天03:00执行
    //@PostConstruct //启动项目先执行
    public void synBooksData() {
        LocalDate executeDate = getExecuteDate();
        synBooksJob.execute(executeDate);
    }

    @Scheduled(cron = "0 30 3 * * ?")//每天03:30执行
    //@PostConstruct //启动项目先执行
    public void synChaptersData() {
        LocalDate executeDate = getExecuteDate();
        synChaptersJob.execute(executeDate);
    }
    @Scheduled(cron = "0 30 2 * * ?")//每天02:30执行
    //@PostConstruct //启动项目先执行
    public void synTopicsData() {
        LocalDate executeDate = getExecuteDate();
        synTopicsJob.execute(executeDate);
    }
    @Scheduled(cron = "0 0 2 * * ?")//每天02:00执行
    //@PostConstruct //启动项目先执行
    public void synTagsData() {
        LocalDate executeDate = getExecuteDate();
        synTagsJob.execute(executeDate);
    }

}
