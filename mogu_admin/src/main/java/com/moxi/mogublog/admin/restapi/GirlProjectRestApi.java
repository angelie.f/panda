package com.moxi.mogublog.admin.restapi;


import com.moxi.mogublog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.moxi.mogublog.admin.annotion.AvoidRepeatableCommit.AvoidRepeatableCommit;
import com.moxi.mogublog.admin.annotion.OperationLogger.OperationLogger;
import com.moxi.mogublog.admin.global.SysConf;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.service.GirlProjectService;
import com.moxi.mogublog.xo.vo.GirlProjectVO;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.validator.group.GetList;
import com.moxi.mougblog.base.validator.group.Insert;
import com.moxi.mougblog.base.validator.group.Update;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 同城约妹项目表 RestApi
 *
 * @author Vergift
 * @since 2022-01-05
 */
@Api(value = "同城约妹项目相关接口", tags = {"同城约妹项目相关接口"})
@RestController
@RequestMapping("/girlProject")
@Slf4j
public class GirlProjectRestApi {

    @Autowired
    private GirlProjectService girlProjectService;

    @AuthorityVerify
    @ApiOperation(value = "获取项目列表", notes = "获取项目列表", response = String.class)
    @PostMapping("/getList")
    public String getList(@Validated({GetList.class}) @RequestBody GirlProjectVO girlProjectVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("获取项目列表");
        return ResultUtil.result(SysConf.SUCCESS, girlProjectService.getPageList(girlProjectVO));
    }

    @AvoidRepeatableCommit
    @AuthorityVerify
    @OperationLogger(value = "增加项目")
    @ApiOperation(value = "增加项目", notes = "增加项目", response = String.class)
    @PostMapping("/add")
    public String add(@Validated({Insert.class}) @RequestBody GirlProjectVO girlProjectVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("增加项目");
        return girlProjectService.addGirlProject(girlProjectVO);
    }

    @AuthorityVerify
    @OperationLogger(value = "编辑项目")
    @ApiOperation(value = "编辑项目", notes = "编辑项目", response = String.class)
    @PostMapping("/edit")
    public String edit(@Validated({Update.class}) @RequestBody GirlProjectVO girlProjectVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("编辑项目");
        return girlProjectService.editGirlProject(girlProjectVO);
    }

    @AuthorityVerify
    @ApiOperation(value = "初始化用户项目", notes = "初始化用户项目", response = String.class)
    @PostMapping(value = "/initPorject")
    public String initPorject() {
        log.info("初始化用户项目: {}");
        return ResultUtil.result(SysConf.SUCCESS, girlProjectService.initPorject());
    }

}

