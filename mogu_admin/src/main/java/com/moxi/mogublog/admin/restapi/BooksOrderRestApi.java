package com.moxi.mogublog.admin.restapi;


import com.moxi.mogublog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.moxi.mogublog.admin.annotion.OperationLogger.OperationLogger;
import com.moxi.mogublog.admin.global.SysConf;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.service.BooksSettingService;
import com.moxi.mogublog.xo.service.TpPayService;
import com.moxi.mogublog.xo.vo.BooksSettingVO;
import com.moxi.mogublog.xo.vo.TpPayLogVO;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.validator.group.GetList;
import com.moxi.mougblog.base.validator.group.Update;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 漫画订单 RestApi
 *
 * @author Sid
 * @since 2022-05-31
 */
@Api(value = "漫画订单相关接口", tags = {"漫画订单相关接口"})
@RestController
@RequestMapping("/booksOrder")
@Slf4j
public class BooksOrderRestApi {

    @Autowired
    private TpPayService tpPayService;

    @AuthorityVerify
    @ApiOperation(value = "获取漫画订单列表", notes = "获取漫画订单列表", response = String.class)
    @PostMapping("/getList")
    public String getList(@Validated({GetList.class}) @RequestBody TpPayLogVO tpPayLogVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("获取漫画订单列表");
        return ResultUtil.result(SysConf.SUCCESS, tpPayService.getPageList(tpPayLogVO));
    }


}

