package com.moxi.mogublog.admin.scheduler.job;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.*;
import com.moxi.mogublog.xo.mapper.*;
import com.moxi.mogublog.xo.thirdparty.mapper.*;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import javax.annotation.Resource;
import java.time.LocalDate;


@Slf4j
@Data
@Service
@Scope("prototype")
public class SynTagsJob {

    @Resource
    TpTagsMapper tpTagsMapper;
    @Resource
    TpTaggablesMapper tpTaggablesMapper;
    @Resource
    DTpTagsMapper dTpTagsMapper;
    @Resource
    DTpTaggablesMapper dTpTaggablesMapper;

    public void execute(LocalDate executeDate) {
        log.info("{} started",getClass().getName());
        try {
            synTaggables(executeDate);
            synTags(executeDate);

        } catch (Exception e) {
            log.info("{} Error -- cause by :{}" ,getClass().getName(),e.getMessage());
        }
        log.info("{} end",getClass().getName());

    }
    private void synTags(LocalDate synDate) throws Exception{
        StopWatch watch = new StopWatch("synTags");
        watch.start();
        QueryWrapper<TpTags> wrapper = new QueryWrapper<>();
        wrapper.between("updated_at",synDate,synDate.plusDays(1));
        wrapper.orderByAsc("updated_at");
        log.info("synTags start lastDay:{}",synDate.toString());
        int pageSize=100;
        Integer finish =0;
        Integer total =dTpTagsMapper.selectCount(wrapper);
        int exetimes =total/pageSize;
        if(total%pageSize>0){
            exetimes++;
        }
        Page<TpTags> page = new Page<>();
        for(int current=1;current<=exetimes;current++){
            page.setCurrent(current);
            page.setSize(pageSize);
            IPage<TpTags> pageTags =dTpTagsMapper.selectPageForUpdate(page,wrapper);
            if(pageTags.getRecords().size()>0){
                int count = tpTagsMapper.insertOrUpdateBatch(pageTags.getRecords());
                finish+= pageTags.getRecords().size();
                log.info("synTags process current:{}, per count:{},total:{},currentPage:{},finish total:{}",current,pageTags.getRecords().size(),total, current,finish);
            }
        }
        watch.stop();
        watch.prettyPrint();
    }
    private void synTaggables(LocalDate synDate) throws Exception{
        StopWatch watch = new StopWatch("synTaggables");
        watch.start();
        QueryWrapper<TpTaggables> wrapper = new QueryWrapper<>();
        log.info("synTaggables start lastDay:{}",synDate.toString());
        wrapper.eq("taggable_type","App\\Models\\Book");
        wrapper.orderByAsc("tag_id","taggable_id");
        int pageSize=100;
        Integer finish =0;
        Integer total =dTpTaggablesMapper.selectCount(wrapper);
        int exetimes =total/pageSize;
        if(total%pageSize>0){
            exetimes++;
        }
        for(int current=1;current<=exetimes;current++){
            Page<TpTaggables> page = new Page<>();
            page.setCurrent(current);
            page.setSize(pageSize);
            IPage<TpTaggables> pageTaggables =dTpTaggablesMapper.selectPage(page,wrapper);
            if(pageTaggables.getRecords().size()>0){
                int count = tpTaggablesMapper.insertOrUpdateBatch(pageTaggables.getRecords());
                finish+= pageTaggables.getRecords().size();
                log.info("synTaggables process current:{}, per count:{},total:{},currentPage:{},finish total:{}",current,pageTaggables.getRecords().size(),total, current,finish);
            }
        }
        watch.stop();
        watch.prettyPrint();
    }



}
