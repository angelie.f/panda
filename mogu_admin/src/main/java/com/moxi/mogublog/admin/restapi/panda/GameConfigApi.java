package com.moxi.mogublog.admin.restapi.panda;

import com.moxi.mogublog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.moxi.mogublog.admin.annotion.OperationLogger.OperationLogger;
import com.moxi.mogublog.admin.global.SysConf;
import com.moxi.mogublog.commons.entity.GameConfig;
import com.moxi.mogublog.commons.entity.GameConfigTrial;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.service.GameConfigService;
import com.moxi.mogublog.xo.vo.GameConfigTrialVO;
import com.moxi.mogublog.xo.vo.GameConfigVO;
import com.moxi.mogublog.xo.vo.GameManagerVo;
import com.moxi.mogublog.xo.vo.GirlVO;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.validator.group.GetList;
import com.moxi.mougblog.base.validator.group.Insert;
import com.moxi.mougblog.base.validator.group.Update;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 游戏设置
 *
 */
@Api(value = "游戏设置后台接口", tags = {"游戏设置后台接口"})
@RestController
@RequestMapping("/gameConfig")
@Slf4j
public class GameConfigApi {
    @Autowired
    private GameConfigService gameConfigService;

    @AuthorityVerify
    @ApiOperation(value = "游戏设置列表", notes = "游戏设置列表", response = String.class)
    @PostMapping(value = "/getGameConfigList")
    public String getWithdrawalList(@Validated({GetList.class}) @RequestBody GameManagerVo gameManagerVo, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        return ResultUtil.result(SysConf.SUCCESS, gameConfigService.getPageList(gameManagerVo));
    }

    @AuthorityVerify
    @OperationLogger(value = "编辑游戏设置")
    @ApiOperation(value = "编辑游戏设置", notes = "编辑游戏设置")
    @PostMapping("/edit")
    public String edit(@Validated({Insert.class}) @RequestBody GameManagerVo gameManagerVo, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        return gameConfigService.editGameConfig(gameManagerVo);
    }
    @AuthorityVerify
    @OperationLogger(value = "改变试玩状态")
    @ApiOperation(value = "改变试玩状态", notes = "改变试玩状态", response = String.class)
    @PostMapping("/changeAvailable")
    public String changeAvailable(@Validated({Update.class}) @RequestBody GameConfigTrialVO gameConfigTrialVo, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("改变试玩状态");
        return gameConfigService.changeAvailable(gameConfigTrialVo);
    }
    @AuthorityVerify
    @OperationLogger(value = "取得试玩状态")
    @ApiOperation(value = "取得试玩状态", notes = "取得试玩状态", response = String.class)
    @PostMapping("/getGameTrial")
    public String getGameTrial(@Validated({Update.class}) @RequestBody GameConfigTrialVO gameConfigTrialVo, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("取得试玩状态");
        return gameConfigService.getGameTrail(gameConfigTrialVo);
    }
}
