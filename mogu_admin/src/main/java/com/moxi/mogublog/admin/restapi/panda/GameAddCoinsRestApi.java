package com.moxi.mogublog.admin.restapi.panda;

import com.moxi.mogublog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.moxi.mogublog.admin.annotion.AvoidRepeatableCommit.AvoidRepeatableCommit;
import com.moxi.mogublog.admin.annotion.OperationLogger.OperationLogger;
import com.moxi.mogublog.admin.global.MessageConf;
import com.moxi.mogublog.admin.global.RedisConf;
import com.moxi.mogublog.admin.global.SysConf;
import com.moxi.mogublog.commons.entity.OnlineAdmin;
import com.moxi.mogublog.utils.JsonUtils;
import com.moxi.mogublog.utils.RedisUtil;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.service.GameAddcoinsService;
import com.moxi.mogublog.xo.vo.GameAddCoinsVo;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.validator.group.GetList;
import com.moxi.mougblog.base.validator.group.Insert;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * 添加游戏币
 *
 */
@Api(value = "添加游戏币后台接口", tags = {"添加游戏币后台接口"})
@RestController
@RequestMapping("/gameAddUserCoins")
@Slf4j
public class GameAddCoinsRestApi {
    @Autowired
    private GameAddcoinsService gameAddcoinsService;
    @Autowired
    private RedisUtil redisUtil;

    @AuthorityVerify
    @ApiOperation(value = "添加游戏币列表", notes = "添加游戏币列表", response = String.class)
    @PostMapping(value = "/getCoinsList")
    public String getCoinsList(@Validated({GetList.class}) @RequestBody GameAddCoinsVo gameAddCoinsVo, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        if(StringUtils.isNotEmpty(gameAddCoinsVo.getBeginCreateTime())){
            gameAddCoinsVo.setBeginCreateTime(gameAddCoinsVo.getBeginCreateTime().substring(0,13)+":00:00");
        }
        if(StringUtils.isNotEmpty(gameAddCoinsVo.getEndCreateTime())){
            gameAddCoinsVo.setEndCreateTime(gameAddCoinsVo.getEndCreateTime().substring(0,13)+":59:59");
        }
        return ResultUtil.result(SysConf.SUCCESS, gameAddcoinsService.getPageList(gameAddCoinsVo));
    }

    @AvoidRepeatableCommit
    @AuthorityVerify
    @OperationLogger(value = "添加游戏币")
    @ApiOperation(value = "添加游戏币", notes = "添加游戏币")
    @PostMapping("/add")
    public String addCoins(@Validated({Insert.class}) @RequestBody GameAddCoinsVo gameAddCoinsVo, BindingResult result) {
        ServletRequestAttributes attribute = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attribute.getRequest();
        String token = request.getAttribute(SysConf.TOKEN).toString();
        if (StringUtils.isEmpty(token)) {
            return ResultUtil.result(SysConf.ERROR, MessageConf.OPERATION_FAIL);
        } else {
            String adminJson = redisUtil.get(RedisConf.LOGIN_TOKEN_KEY + RedisConf.SEGMENTATION + token);
            if (StringUtils.isNotEmpty(adminJson)) {
                OnlineAdmin onlineAdmin = JsonUtils.jsonToPojo(adminJson, OnlineAdmin.class);
                gameAddCoinsVo.setOperName(onlineAdmin.getUserName());
            }
        }
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        return gameAddcoinsService.addConis(gameAddCoinsVo);
    }
}
