package com.moxi.mogublog.admin.restapi;


import com.moxi.mogublog.admin.annotion.AuthorityVerify.AuthorityVerify;
import com.moxi.mogublog.admin.annotion.AvoidRepeatableCommit.AvoidRepeatableCommit;
import com.moxi.mogublog.admin.annotion.OperationLogger.OperationLogger;
import com.moxi.mogublog.admin.global.SysConf;
import com.moxi.mogublog.commons.entity.PictureSort;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.service.PictureSortService;
import com.moxi.mogublog.xo.vo.PictureSortVO;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.validator.group.GetList;
import com.moxi.mougblog.base.validator.group.Insert;
import com.moxi.mougblog.base.validator.group.Update;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 图集分类表 RestApi
 *
 * @author Vergift
 * @date 2021年12月02日17:07:38
 */
@Api(value = "图集分类相关接口", tags = {"图集分类相关接口"})
@RestController
@RequestMapping("/pictureSort")
@Slf4j
public class PictureSortRestApi {

    @Autowired
    private PictureSortService pictureSortService;

    @AuthorityVerify
    @ApiOperation(value = "获取图集分类表", notes = "获取图集分类表", response = String.class)
    @PostMapping(value = "/initPorject")
    public String initPorject() {
        log.info("获取项目列表1: {}");
        return ResultUtil.result(SysConf.SUCCESS, pictureSortService.initPorject());
    }

    @AuthorityVerify
    @ApiOperation(value = "获取图集分类表", notes = "获取图集分类表", response = String.class)
    @PostMapping(value = "/getList")
    public String getList(@Validated({GetList.class}) @RequestBody PictureSortVO pictureSortVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("获取图集分类表列表: {}", pictureSortVO);
        return ResultUtil.result(SysConf.SUCCESS, pictureSortService.getPageList(pictureSortVO));
    }

    @AuthorityVerify
    @ApiOperation(value = "获取图片来源列表", notes = "获取图片来源列表", response = String.class)
    @PostMapping(value = "/getResourceList")
    public String getResourceList() {
        log.info("获取图集列表: ");
        return ResultUtil.result(SysConf.SUCCESS, pictureSortService.getResourceList());
    }

    @AvoidRepeatableCommit
    @AuthorityVerify
    @OperationLogger(value = "增加图集分类表")
    @ApiOperation(value = "增加图集分类表", notes = "增加图集分类表", response = String.class)
    @PostMapping("/add")
    public String add(@Validated({Insert.class}) @RequestBody PictureSort pictureSort, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("增加图集分类表: {}", pictureSort);
        return pictureSortService.addPictureSort(pictureSort);
    }

    @AuthorityVerify
    @OperationLogger(value = "编辑图集分类表")
    @ApiOperation(value = "编辑图集分类表", notes = "编辑图集分类表", response = String.class)
    @PostMapping("/edit")
    public String edit(@Validated({Update.class}) @RequestBody PictureSort pictureSort, BindingResult result) {

        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("编辑图集分类表: {}", pictureSort);
        return pictureSortService.editPictureSort(pictureSort);
    }

    /*@AuthorityVerify
    @OperationLogger(value = "删除图集")
    @ApiOperation(value = "删除图集", notes = "删除图集", response = String.class)
    @PostMapping("/delete")
    public String delete(@Validated({Delete.class}) @RequestBody PictureAtlasVO pictureAtlasVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("删除图集: {}", pictureAtlasVO);
        return pictureAtlasService.deletePictureAtlas(pictureAtlasVO);
    }

    @AuthorityVerify
    @OperationLogger(value = "置顶图集")
    @ApiOperation(value = "置顶图集", notes = "置顶图集", response = String.class)
    @PostMapping("/stick")
    public String stick(@Validated({Delete.class}) @RequestBody PictureAtlasVO pictureAtlasVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("置顶图集: {}", pictureAtlasVO);
        return pictureAtlasService.stickPictureAtlas(pictureAtlasVO);
    }

    @OperationLogger(value = "通过Uid获取图集")
    @ApiOperation(value = "通过Uid获取图集", notes = "通过Uid获取图集", response = String.class)
    @PostMapping("/getPictureAtlasByUid")
    public String getPictureAtlasByUid(@Validated({Delete.class}) @RequestBody PictureAtlasVO pictureAtlasVO, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        PictureAtlas PictureAtlas = pictureAtlasService.getById(pictureAtlasVO.getUid());
        log.info("通过Uid获取图集: {}", PictureAtlas);
        return ResultUtil.successWithData(PictureAtlas);
    }

    @AuthorityVerify
    @OperationLogger(value = "修改标签")
    @ApiOperation(value = "修改标签", notes = "修改标签", response = String.class)
    @PostMapping("/updateTag")
    public String updateTag(@Validated({Delete.class}) @RequestBody List<PictureAtlasVO> list, BindingResult result) {
        // 参数校验
        ThrowableUtils.checkParamArgument(result);
        log.info("修改标签: {}", list);
        return pictureAtlasService.updateTag(list);
    }*/

}

