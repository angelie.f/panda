package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

/**
 * 妹子项目类
 *
 * @author Vergift
 * @date 2021-12-29
 */
@Data
@TableName("t_girl_project")
public class GirlProject extends SuperEntity<GirlProject> {

    private static final long serialVersionUID = 2646201532621057453L;

    /**
     * 项目名称
     */
    private String name;

    /**
     * 来源
     */
    private Integer source;

    /**
     * 是否会员:1.会员；0.普通用户
     */
    @TableField(value = "is_vip", strategy= FieldStrategy.IGNORED)
    private Integer isVip;

    /**
     * 项目开关:1.开启；0.关闭
     */
    @TableField(value = "on_off", strategy= FieldStrategy.IGNORED)
    private Integer onOff;

    /**
     * 认证信息
     */
    @TableField(value = "authentication", strategy= FieldStrategy.IGNORED)
    private String authentication;

    /**
     * KEY
     */
    @TableField(value = "pro_key", strategy= FieldStrategy.IGNORED)
    private String proKey;

}
