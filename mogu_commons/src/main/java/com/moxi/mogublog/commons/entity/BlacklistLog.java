package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;


@Data
@TableName("t_blacklist_log")
public class BlacklistLog extends SuperEntity<BlacklistLog> {
    private static final long serialVersionUID = -2902364796815077581L;
    private Integer valiUid;
    private String userName;
    private String operation;
    @TableField(exist = false)
    private String content;


}
