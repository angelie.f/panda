package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

/**
 * 妹子图片类
 *
 * @author Vergift
 * @date 2021-12-29
 */
@Data
@TableName("t_girl_picture")
public class GirlPicture extends SuperEntity<GirlPicture> {

    private static final long serialVersionUID = 2646201532621057453L;

    /**
     * 图片文件uid
     */
    private String fileUid;

    /**
     * 图片名
     */
    private String picName;

    /**
     * 妹子uid
     */
    private Integer girlUid;

    /**
     * 图片路径
     */
    @TableField(exist = false)
    private String pictureUrl;

}
