package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

import java.util.List;

/**
 * <p>
 * 图片标签表
 * </p>
 *
 * @author Vergift
 * @since 2021-11-22
 */
@Data
@TableName("t_picture_tag")
public class PictureTag extends SuperEntity<PictureTag> {

    private static final long serialVersionUID = 1L;

    /**
     * 标签名称
     */
    private String content;

    /**
     * 标签简介
     */
    private int clickCount;

    /**
     * 排序字段，数值越大，越靠前
     */
    private int sort;

    /**
     * 标签介绍
     */
    @TableField(value = "introduce", strategy= FieldStrategy.IGNORED)
    private String introduce;

    /**
     * 标签在所属分类里面的上架图集数量
     */
    @TableField(exist = false)
    private Integer count;
}
