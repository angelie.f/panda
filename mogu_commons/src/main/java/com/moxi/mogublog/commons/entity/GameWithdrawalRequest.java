package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 游戏充值提案
 */
@Data
@TableName("t_game_withdrawal_request")
public class GameWithdrawalRequest extends SuperEntity<GameWithdrawalRequest> {

    private static final long serialVersionUID = 2646201532621057453L;


    private String requestId;
    private BigDecimal amount;
    private String customerId;
    private String openId;
    private Integer source;
    private String remarks;
    private Integer flag;
    private String loginName;
    private String thirdOrderNo;
    @TableField(exist = false)
    private int vipLevel;
    @TableField(exist = false)
    private String vipType;
    @TableField(exist = false)
    private String cusUid;
    @TableField(exist = false)
    private Integer isVip;
    @TableField(exist = false)
    private BigDecimal taxAmount;
    @TableField(exist = false)
    private String sourceName;

}
