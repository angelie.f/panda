package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

import java.util.List;

/**
 * 妹子分类
 *
 * @author Vergift
 * @date 2021-12-29
 */
@Data
@TableName("t_girl_sort")
public class GirlSort extends SuperEntity<GirlSort> {

    private static final long serialVersionUID = 2646201532621057453L;

    /**
     * 妹子分类名称
     */
    private String name;

    /**
     * 分类介绍
     */
    private String introduce;

    /**
     * 分类图标图片uid
     */
    private String fileUid;

    /**
     * 排序字段，越大越靠前
     */
    private Integer sort;

    /**
     * 妹子标签uid:1,2,...
     */
    private String tagUid;

    /**
     * 妹子分类所属项目uid
     */
    private String projectUid;

    /**
     * 分类图标
     */
    @TableField(exist = false)
    private List<String> photoList;

    /**
     * 标签,一篇分类对应多个标签
     */
    @TableField(exist = false)
    private List<GirlTag> tagList;

    /**
     * 项目
     */
    @TableField(exist = false)
    private GirlProject project;

    /**
     * 上架图集数量
     */
    @TableField(exist = false)
    private Integer upCount;

    /**
     * 下架图集数量
     */
    @TableField(exist = false)
    private Integer outCount;
}
