package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 游戏充值提案
 */
@Data
@TableName("t_game_deposit_award")
public class GameDepositAward extends SuperEntity<GameDepositAward> {

    private Integer source;
    private String customerId;
    private String period;
    private BigDecimal award;
    private Integer firstDepositNum;

    @TableField(exist = false)
    private String sourceName;

    @TableField(exist = false)
    private String wuid;

    @TableField(exist = false)
    private String openId;

}
