package com.moxi.mogublog.commons.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

@Data
@TableName("tp_tags")
public class TpTags extends Model<TpTags> {

    @TableId(type= IdType.AUTO)
    private Integer id;
    
    private String name;
    
    private String type;
    //前端是否顯示
    private Integer suggest;
    //前端查詢次數
    private Integer queries;
    
    private Integer orderColumn;
    
    private Date createdAt;
    
    private Date updatedAt;
}

