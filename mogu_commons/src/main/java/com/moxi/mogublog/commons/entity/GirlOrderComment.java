package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 妹子订单评论
 *
 * @author Vergift
 * @date 2021-12-29
 */
@Data
@TableName("t_girl_order_comment")
public class GirlOrderComment extends SuperEntity<GirlOrderComment> {

    private static final long serialVersionUID = 2646201532621057453L;

    /**
     * 订单ID
     */
    private String orderId;

    private Integer girlId;

    private Float score;
    private Float score1;
    private Float score2;

    /**
     * 评论内容
     */
    private String content;

    /**
     * 回复某条评论的uid
     */
    private Integer toUid;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 一级评论uid
     */
    private Integer firstCommentUid;

    /**
     * 是否為官方ID
     */
    private Integer isOfficial;

    @TableField(exist = false)
    private String createTimeStr;

    @TableField(exist = false)
    private List<GirlOrderComment> commentList;

    @TableField(exist = false)
    private String girlName;


}
