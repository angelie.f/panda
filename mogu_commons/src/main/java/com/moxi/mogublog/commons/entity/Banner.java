package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

/**
 * banner图
 */
@Data
@TableName("t_banner")
public class Banner extends SuperEntity<Banner> {

    private static final long serialVersionUID = 2646201532621057453L;
    private String imageUrl;
    private String jumpUrl;
    private Integer sort;
    private Integer openFlag;
    private String title;
    private Integer fileUid;
    private String showSource;
    private String bannerType;


}
