package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

/**
 * 游戏配置
 */
@Data
@TableName("t_game_config")
public class GameConfig extends SuperEntity<GameConfig> {

    private static final long serialVersionUID = 2646201532621057453L;


    private String thirdNo;
    private String name;
    private Integer sort;
    private String imageUrl;
    private Integer fileUid;
    private Integer gameSource;
    private String gameTypeUid;
    @TableField(exist = false)
    private String gameTypeName;

}
