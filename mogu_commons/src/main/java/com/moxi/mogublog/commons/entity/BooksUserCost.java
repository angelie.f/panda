package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

import java.math.BigDecimal;

@Data
@TableName("t_books_user_cost")
public class BooksUserCost extends SuperEntity<BooksUserCost> {


    /**
     * 会员等级ID
     */
    private Integer levelUid;

    /**
     * 会员等级ID
     */
    @TableField(exist = false)
    private Integer levelId;

    /**
     * 会员等级名称
     */
    @TableField(exist = false)
    private String levelName;

    /**
     * 每日可看话数或集数
     */
    private Integer enableEpisode;

    /**
     * 妹子项目uid
     */
    private Integer projectUid;

}
