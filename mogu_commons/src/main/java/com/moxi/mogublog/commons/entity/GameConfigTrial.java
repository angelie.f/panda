package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

/**
 * 游戏试玩配置
 */
@Data
@TableName("t_game_config_trial")
public class GameConfigTrial extends SuperEntity<GameConfigTrial> {

    private Integer gameSource;

}
