package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

import java.util.List;

/**
 * 上下架表实体类
 *
 * @author Vergift
 * @date 2021年12月03日11:31:38
 */
@Data
@TableName("t_picture_sort_item")
public class PictureSortItem extends SuperEntity<PictureSortItem> {

    /**
     *
     */
    private static final long serialVersionUID = 3454006152368184626L;

    /**
     * 图集分类uid
     */
    private String sortUid;

    /**
     * 图集uid
     */
    private Integer atlasUid;

}
