package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 妹子信息类
 *
 * @author Vergift
 * @date 2021-12-29
 */
@Data
@TableName("t_girl")
public class Girl extends SuperEntity<Girl> {

    private static final long serialVersionUID = 2646201532621057453L;

    /**
     * 小视频uid
     */
    private String videoUid;

    /**
     * 姓名
     */
    private String name;

    /**
     * 简介
     */
    @TableField(value = "introduce", strategy= FieldStrategy.IGNORED)
    private String introduce;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 价格（次）
     */
    private BigDecimal priceOne;

    /**
     * 价格（包夜）
     */
    private BigDecimal priceNight;

    /**
     * 城市uid
     */
    private String cityUid;

    /**
     * 身高（cm）
     */
    private Integer height;

    /**
     * 体重（kg）
     */
    private Integer weight;

    /**
     * 罩杯
     */
    @TableField(value = "cup", strategy= FieldStrategy.IGNORED)
    private String cup;

    /**
     * 职业
     */
    @TableField(value = "occupation", strategy= FieldStrategy.IGNORED)
    private String occupation;

    /**
     * 妹子标签uid:1,2,...
     */
    private String tagUid;


    /**
     * 操作人uid
     */
    private String adminUid;


    /**
     * 操作人
     */
    private String userName;

    /**
     * 操作时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date operatorTime;

    /**
     * 图片数量
     */
    @TableField(exist = false)
    private Integer count;

    /**
     * 小视频地址
     */
    @TableField(exist = false)
    private String videoUrl;

    /**
     * 封面
     */
    @TableField(exist = false)
    private List<String> photoList;

    /**
     * 认证信息
     */
    @TableField(exist = false)
    private String authentication;

    /**
     * 标签
     */
    @TableField(exist = false)
    private List<GirlTag> tagList;
    /**
     * 城市
     */
    @TableField(exist = false)
    private GirlCity city;

    /**
     * 是否有小视频:1.是；2.否
     */
    @TableField(exist = false)
    private Integer isVideo;

    /**
     * 妹子图片uids集合
     */
    @TableField(exist = false)
    private List<String> picUids;
    @TableField(exist = false)
    private Float score;

    /**
     * 是否可約:1.是；2.否
     */
    @TableField(exist = false)
    private Integer dateable;

    /**
     * 上次服務時間:
     */
    @TableField(exist = false)
    private String lastServiceTime;

    /**
     * 是否空閒:1.是；2.否
     */
    private Integer available;
}

