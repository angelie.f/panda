package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName("t_game_act_record")
public class GameActRecord extends SuperEntity<GameActRecord> {
    private static final long serialVersionUID = -4617395879182114354L;

    private String actName;
    private String actPeriod;
    private String actType;
    private Date actBeginTime;
    private Date actEndTime;
    private BigDecimal actPreAmount;
    private Integer actPreTimes;
    private String loginName;

    private String customerId;
    private Integer popTimes;

    /**
     * 会员在第三方系统里的唯一id
     */
    private String openId;
    /**
     * 会员有效标记
     */
    private Integer flag;

    private String actDesc;
    private String actId;
    private Integer isVip;
    private Integer vipLevel;
    private String vipType;
    private String requestId;
    private String source;
    @TableField(exist = false)
    private String cusUid;
    @TableField(exist = false)
    private String sourceName;
}
