package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

/**
 * 使用权限类
 *
 * @author Vergift
 * @date 2021-12-03
 */
@Data
@TableName("t_use_power")
public class UsePower extends SuperEntity<UsePower> {

    private static final long serialVersionUID = 2646201532621057453L;

    /**
     * 普通用户是否限制:0不限制；1限制
     */
    private int userAstrict;

    /**
     * 普通用户限制图片数量
     */
    private int userLimit;

    /**
     * vip否限制:0不限制；1限制
     */
    private int vipAstrict;

    /**
     * vip户限制图片数量
     */
    private int vipLimit;

    /**
     * 所属项目
     */
    private String projectUid;

    /**
     * 项目名称
     */
    @TableField(exist = false)
    private String projectName;

}
