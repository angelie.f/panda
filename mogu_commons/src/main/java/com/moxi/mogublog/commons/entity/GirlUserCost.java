package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 用户费用类
 *
 * @author Vergift
 * @date 2021-12-29
 */
@Data
@TableName("t_girl_user_cost")
public class GirlUserCost extends SuperEntity<GirlUserCost> {

    private static final long serialVersionUID = 2646201532621057453L;

    /**
     * 会员等级ID
     */
    private String levelId;

    /**
     * 会员等级名称
     */
    private String levelName;

    /**
     * 平台费用
     */
    private BigDecimal cost;

    /**
     * 妹子项目uid
     */
    private Integer projectUid;

}
