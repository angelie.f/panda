package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 游戏充值提案
 */
@Data
@TableName("t_game_deposit_request")
public class GameDepositRequest extends SuperEntity<GameDepositRequest> {

    private static final long serialVersionUID = 2646201532621057453L;


    private String requestId;
    private BigDecimal amount;
    private Integer coins;
    private String customerId;
    private String openId;
    private Integer source;
    private Date payTime;
    private Integer payCount;
    private Integer flag;
    private String loginName;
    private Integer isFirstDep;
    @TableField(exist = false)
    private int vipLevel;
    @TableField(exist = false)
    private String vipType;
    @TableField(exist = false)
    private String cusUid;
    @TableField(exist = false)
    private Integer isVip;
    @TableField(exist = false)
    private String sourceName;
    @TableField(exist = false)
    private Integer isFirstDepShow;
}
