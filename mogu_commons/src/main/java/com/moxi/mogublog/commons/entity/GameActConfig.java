package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName("t_game_act_config")
public class GameActConfig extends SuperEntity<GameActConfig> {

    private static final long serialVersionUID = 4283534199989870299L;

    private String actTitle;
    private String actType;
    private Date actBeginTime;
    private Date actEndTime;
    private String actPreAmount;
    private Integer actPreTimes;
    private String actDesc;
    private String actId;
    private String actChannel;
    private String actInfo;
}
