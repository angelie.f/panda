package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@TableName("tp_topic_items")
public class TpTopicItems extends Model<TpTopicItems> {

    @TableId(type= IdType.AUTO)
    private Integer id;
    private Integer topicId;
    private String topicCauser;
    private Integer itemId;
    private Integer sortNum;
    private Date createdAt;
    private Date updatedAt;
}

