package com.moxi.mogublog.commons.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.List;

import lombok.Data;

@Data
@TableName("tp_book_chapters")
public class TpBookChapters extends Model<TpBookChapters> {

    @TableId(type= IdType.AUTO)
    private Integer id;
    
    private Integer bookId;
    //集数
    private Integer episode;

    //标题
    private String title;
    //HTML 图片
    private String content;
    //JSON 图片
    private String jsonImages;
    //上架: 1=上架, -1=下架
    private Integer status;
    //收费: 1=VIP, -1=免费
    private Integer charge;
    //审核: 0=待审核, 1=审核成功, 2=审核失败, 3=屏蔽, 4=未审核
    private Integer review;
    //添加方式: 1=手动, 2=自动
    private Integer operating;
    
    private Date createdAt;
    
    private Date updatedAt;
    
    private Date deletedAt;
    //访问数
    private Integer views;
    //收藏數
    private Integer collectCounts;

    @TableField(exist = false)
    private List<String> imageUrls;

    @TableField(exist = false)
    private Integer isNew;

    @TableField(exist = false)
    private Integer isPay;

    @TableField(exist = false)
    private Integer imageCount;


}

