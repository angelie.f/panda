package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 游戏用户钱包
 */
@Data
@TableName("t_game_customers_balance")
public class GameCustomersBalance extends SuperEntity<GameCustomersBalance> {

    private static final long serialVersionUID = 2646201532621057453L;
    private String customerId;
    private BigDecimal coins;
    private BigDecimal frozenCoins;
    private String meVersion;


}
