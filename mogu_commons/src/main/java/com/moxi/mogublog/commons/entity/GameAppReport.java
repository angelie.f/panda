package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@TableName("t_game_app_report")
public class GameAppReport extends SuperEntity<GameAppReport> implements Serializable {
    private static final long serialVersionUID = 1121232385700370878L;

    private String source;

    private Integer onlineNum;

    private Integer registerNum;

    private Integer totalOrder;

    private Integer totalFinshOrder;

    private Integer totalFirstDep;

    private Integer totalVipOrder;

    private Integer totalEtcOrder;

    private BigDecimal finshOrderAmount;

    private BigDecimal firstDepAmount;

    private BigDecimal wdlAmount;

    private BigDecimal wdlTaxAmount;

    private String remark;

    private String period;

    private Integer depNum;

    private Integer wdlNum;

    private BigDecimal depWdlSub;

    private Integer totalDepNum;

    @TableField(exist = false)
    private String sourceName;
}
