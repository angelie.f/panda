package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 妹子订单类
 *
 * @author Vergift
 * @date 2021-12-29
 */
@Data
@TableName("t_girl_order")
public class GirlOrder extends SuperEntity<GirlOrder> {

    private static final long serialVersionUID = 2646201532621057453L;

    /**
     * 订单ID
     */
    private String orderId;

    /**
     * 流水号
     */
    private String thirdOrderId;

    /**
     * 金额
     */
    private BigDecimal amount;


    /**
     * 用户ID
     */
    private String userId;

    /**
     * 用户昵称
     */
    private String userNick;

    /**
     * 妹子uid
     */
    private Integer girlUid;

    /**
     * 妹子名称
     */
    private String girlName;

    /**
     * 支付次数:1.首次;2.续费2;...;n.续费n
     */
    private Integer payCount;

    /**
     * 支付状态:0.待付款.已支付2.已过期
     */
    private Integer payStatus;

    /**
     * 会员ID
     */
    private String customerId;

    /**
     * 支付时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date payTime;

    /**
     * 会员等级ID
     */
    @TableField(exist = false)
    private Integer vipLevel;

    /**
     * 会员等级名称
     */
    @TableField(exist = false)
    private String vipType;

    @TableField(exist = false)
    private List<String> photoList;

    @TableField(exist = false)
    private String commentStatus;

    /**
     * 尾款
     */
    @TableField(exist = false)
    private BigDecimal extraAmount;
    /**
     * 总价
     */
    @TableField(exist = false)
    private BigDecimal totalAmount;

    @TableField(exist = false)
    private String cityName;
    @TableField(exist = false)
    private String authentication;
    @TableField(exist = false)
    private String source;

    @TableField(exist = false)
    private String sourceName;


    /**
     * 服務时间
     */

    private String serviceTime;
    /**
     * 服務地點 1:上門;2:租屋;3:酒店
     */
    private Integer serviceType;

    /**
     * 线下尾款
     */
    private String offPrice;

}
