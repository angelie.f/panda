package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 游戏充值提案
 */
@Data
@TableName("t_game_transfer")
public class GameTransfer extends SuperEntity<GameTransfer> {

    private static final long serialVersionUID = 2646201532621057453L;


    private String requestId;
    private Integer type;
    private Integer amount;
    private String customerId;

}
