package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

/**
 * 妹子标签类
 *
 * @author Vergift
 * @date 2021-12-29
 */
@Data
@TableName("t_girl_tag")
public class GirlTag extends SuperEntity<GirlTag> {

    private static final long serialVersionUID = 2646201532621057453L;

    /**
     * 妹子标签内容
     */
    private String name;

    /**
     * 标签介绍
     */
    @TableField(value = "introduce", strategy= FieldStrategy.IGNORED)
    private String introduce;

}
