package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@TableName("tp_pay_log")
public class TpPayLog extends SuperEntity<TpPayLog> {

    private static final long serialVersionUID = 2646201532621057453L;

    private String openId;

    private Integer source;



    private Integer bookId;


    private Integer episode;

    private BigDecimal price;

    private String platform;

    @TableField(exist = false)
    private String sourceName;

    @TableField(exist = false)
    private String bookName;

    @TableField(exist = false)
    private String chapterName;

    @TableField(exist = false)
    private String customerId;



}

