package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;


@Data
@TableName("t_blacklist")
public class Blacklist extends SuperEntity<Blacklist> {
    private static final long serialVersionUID = 5920767358604834840L;
    private Integer valiUid;
    private String phone;
    private Integer isBlack;
    private String reason;

}
