package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 游戏充值配置
 */
@Data
@TableName("t_game_deposit_config")
public class GameDepositConfig extends SuperEntity<GameDepositConfig> {

    private static final long serialVersionUID = 2646201532621057453L;


    private BigDecimal price;
    private Integer coins;
    private Integer promotionCoins;
    private String description;
    private Integer depositType;

}
