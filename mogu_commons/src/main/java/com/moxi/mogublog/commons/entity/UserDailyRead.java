package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

@Data
@TableName("t_user_daily_read")
public class UserDailyRead extends SuperEntity<UserDailyRead> {
    //阅读日期
    private String readDate;
    //用户ID
    private String customerId;
    //漫画Id
    private Integer bookId;
    //阅读集数
    private Integer episode;


}

