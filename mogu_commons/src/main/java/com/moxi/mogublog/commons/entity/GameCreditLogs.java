package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 游戏用户账变记录
 */
@Data
@TableName("t_game_credit_logs")
public class GameCreditLogs extends SuperEntity<GameCreditLogs> {

    private static final long serialVersionUID = 2646201532621057453L;


    private Integer type;
    private BigDecimal coinsBefore;
    private BigDecimal coinsAfter;
    private String customerId;
    private Integer source;
    private String referenceId;

}
