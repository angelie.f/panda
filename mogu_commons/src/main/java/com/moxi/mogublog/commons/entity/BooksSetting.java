package com.moxi.mogublog.commons.entity;
import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

@Data
@TableName("t_books_setting")
public class BooksSetting extends SuperEntity<BooksSetting> {
    
    //key
    private String appKey;
    //标题
    private String title;
    //说明
    private String content;
    //值
    private String value;


}

