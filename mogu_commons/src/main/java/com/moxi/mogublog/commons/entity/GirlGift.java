package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

@Data
@TableName("t_girl_gift")
public class GirlGift extends SuperEntity<GirlGift> {

    private static final long serialVersionUID = 2646201532621057453L;

    private String orderId;

    private String customerId;

    private String receiveName;

    private String receiveNumber;

    private String receiveAddress;

    @TableField(exist = false)
    private String userId;

}
