package com.moxi.mogublog.commons.entity;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

@Data
@TableName("tp_topics")
public class TpTopics extends Model<TpTopics> {

    @TableId(type= IdType.AUTO)
    private Integer id;
    //标题
    private String title;
    //排序
    private Integer sort;
    //聚焦数量
    private Integer spotlight;
    //每行几笔
    private Integer row;
    
    private String causer;
    
    private String tag;
    
    private String icon;
    //狀態 [-1:下架,1:上架]
    private Integer status;
    
    private Date createdAt;
    
    private Date updatedAt;

    private List<TpBooks>booksList;
}

