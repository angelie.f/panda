package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

@Data
@TableName("t_books_user_level")
public class BooksUserLevel extends SuperEntity<BooksUserLevel> {

    /**
     * 等级名称
     */
    @TableField(exist = false)
    private Integer levelKey;

    private String levelName;

    private Integer levelId;

    private Integer source;

}
