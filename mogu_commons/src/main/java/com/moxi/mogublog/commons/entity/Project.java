package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

/**
 * <p>
 * 项目表
 * </p>
 *
 * @author Vergift
 * @since 2021-11-25
 */
@Data
@TableName("t_project")
public class Project extends SuperEntity<Project> {

    private static final long serialVersionUID = 1L;

    /**
     * 项目名称
     */
    private String name;

    /**
     * 来源:1.青青草；2.小黄鸭
     */
    private Integer source;
}
