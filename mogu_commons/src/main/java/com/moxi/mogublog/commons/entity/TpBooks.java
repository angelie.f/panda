package com.moxi.mogublog.commons.entity;

import java.util.ArrayList;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
@TableName("tp_books")
public class TpBooks extends Model<TpBooks> {
    
    @TableId(type= IdType.AUTO)
    private Integer id;
    //标题
    private String title;
    //作者
    private String author;
    //简介
    private String description;
    //完结: 1=完结, -1=未完结
    private Integer end;
    //竖向封面
    private String verticalCover;
    //横向封面
    private String horizontalCover;
    //类型: 1=日漫, 2=韩漫
    private Integer type;
    //访问数
    private Integer visits;
    //状态: 1=上架, -1=下架
    private Integer status;
    //审核状态: 0=待审核, 1=审核成功, 2=审核失败, 3=屏蔽, 4=未审核
    private Integer review;
    //添加方式: 1=手动, 2=自动
    private Integer operating;
    
    private Date createdAt;
    
    private Date updatedAt;
    
    private Date deletedAt;
    //收藏数
    private Integer collectCounts;
    //访问数
    private Integer viewCounts;
    //主題排序
    private Integer topicSort;
    //标签列表
    @TableField(exist = false)
    private List<String> tags ;

    @TableField(exist = false)
    private List<String> topics ;
}

