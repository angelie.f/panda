package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 第三方会员表
 * </p>
 *
 * @author Vergift
 * @since 2011-12-07
 */
@Data
@TableName("t_customers")
public class Customers extends SuperEntity<Customers> {

    private static final long serialVersionUID = 1L;


    /**
     *产品id.
     */
    private String productId;

    /**
     * 会员登录名
     */
    private String loginName;

    private String customerId;

    /**
     * 会员在第三方系统里的唯一id
     */
    private String openId;

    /**
     * 第三方系统代码，与open_id两个字段来标记第三方会员在系统的唯一
     */
    private String openCode;

    /**
     * 会员有效标记
     */
    private Integer flag;

    /**
     * 会员手机号
     */
    private String phone;

    private Date lastLoginDate;

    private Date lastDepositDate;

    /**
     * 会员积分
     */
    private BigDecimal points;
    private Integer customerType;
    private Integer registerSource;
    private Integer isVip;
    private Integer source;

    private String param;
    private Integer vipLevel;

    private String vipType;

    private Integer isOneMember;

    @TableField(exist = false)
    private BigDecimal coins;
    @TableField(exist = false)
    private String sourceName;
    @TableField(exist = false)
    private BigDecimal sumAmount;
    @TableField(exist = false)
    private String reason="";
    @TableField(exist = false)
    private Integer isBlack;
    private Date oneDate;
    private String oneSecret;
}
