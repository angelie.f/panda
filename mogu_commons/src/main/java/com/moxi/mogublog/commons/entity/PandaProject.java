package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;


@Data
@TableName("t_panda_project")
public class PandaProject extends SuperEntity<PandaProject> {

    private static final long serialVersionUID = 1L;

    /**
     * 项目名称
     */
    private String name;

    /**
     * 来源:1.青青草；2.小黄鸭
     */
    private String source;
}
