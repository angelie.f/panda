package com.moxi.mogublog.commons.entity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

import java.util.List;

@Data
@TableName("t_books_project")
public class BooksProject extends SuperEntity<BooksProject> {
    
    private String name;
    //标题
    private Integer source;
    //普通用戶是否限制:1.限制；0.不限制
    private Integer ordmemberLimit;

    private Integer onOff;

    private String authentication;

    private String proKey;

    //币别名称
    private String currency;
    //会员升级提示文字
    private String upgradeMemo;
    //会员升级跳转URL
    private String upgradeUrl;
    //会员升级提示图档uid
    private Integer fileUid;

    /**
     * 会员升级提示图
     */
    @TableField(exist = false)
    private String photo;
}

