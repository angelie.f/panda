package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

import java.math.BigDecimal;


@Data
@TableName("t_game_add_coins")
public class GameAddCoins extends SuperEntity<GameAddCoins> {
    private static final long serialVersionUID = 1431592317170253105L;
    private BigDecimal coins;
    private String remark;
    private String openId;
    private String customerId;
    private String requestId;
    private String cusUid;
    private String source;
    private String operName;

}
