package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

@Data
@TableName("t_reading_log")
public class ReadingLog extends SuperEntity<ReadingLog> {
    
    //项目ID
    private Integer source;
    //用户ID
    private String customerId;
    //漫画Id
    private Integer bookId;
    //阅读集数
    private Integer episode;

    //目前集数
    @TableField(exist = false)
    private Integer maxEpisode;
    //是否完结 1:完结

    @TableField(exist = false)
    private Integer end;

    @TableField(exist = false)
    private String sourceName;

    @TableField(exist = false)
    private String bookName;

    @TableField(exist = false)
    private String capterName;

    @TableField(exist = false)
    private String imageUrl;

    @TableField(exist = false)
    private Integer isToday;

}

