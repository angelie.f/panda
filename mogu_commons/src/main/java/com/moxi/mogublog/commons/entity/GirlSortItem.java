package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

/**
 * 妹子上下架表类
 *
 * @author Vergift
 * @date 2021-12-29
 */
@Data
@TableName("t_girl_sort_item")
public class GirlSortItem extends SuperEntity<GirlSortItem> {

    private static final long serialVersionUID = 2646201532621057453L;

    /**
     * 妹子分类uid
     */
    private Integer sortUid;

    /**
     * 妹子uid
     */
    private Integer girlUid;

}
