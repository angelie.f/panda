package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

import java.util.List;

/**
 * 分类实体类
 *
 * @author Vergift
 * @date 2021年12月02日17:07:38
 */
@Data
@TableName("t_picture_sort")
public class PictureSort extends SuperEntity<PictureSort> {

    /**
     *
     */
    private static final long serialVersionUID = 3454006152368184626L;

    /**
     * 分类名
     */
    private String name;

    /**
     * 分类名
     */
    private String introduce;

    /**
     * 分类图标封面图片uid
     */
    private String fileUid;


    /**
     * 分类标签uid
     */
    private String tagUid;

    /**
     * 排序字段，数值越大，越靠前
     */
    private int sort;

    /**
     * 所属项目
     */
    private String projectUid;

    /**
     * 分类图
     */
    @TableField(exist = false)
    private List<String> photoList;

    /**
     * 标签,一篇图集对应多个标签
     */
    @TableField(exist = false)
    private List<PictureTag> tagList;

    /**
     * 项目
     */
    @TableField(exist = false)
    private Project project;

    /**
     * 上架图集数量
     */
    @TableField(exist = false)
    private Integer upCount;

    /**
     * 下架图集数量
     */
    @TableField(exist = false)
    private Integer outCount;

    /**
     * 使用权限
     */
    @TableField(exist = false)
    private String uerPower;

}
