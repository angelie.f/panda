package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;


@Data
@TableName("t_domain")
public class Domain extends SuperEntity<Domain> {


    private static final long serialVersionUID = -5890730289654409980L;
    /**
     * 類別名称
     */
    private String domainName;
    /***
     * 域名类别
     */
    private String domainType;

}
