package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

/**
 * 妹子城市类
 *
 * @author Vergift
 * @date 2021-12-29
 */
@Data
@TableName("t_girl_city")
public class GirlCity extends SuperEntity<GirlCity> {

    private static final long serialVersionUID = 2646201532621057453L;

    /**
     * 城市名称
     */
    private String name;

}
