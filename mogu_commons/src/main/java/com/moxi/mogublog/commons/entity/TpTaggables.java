package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

@Data
@TableName("tp_taggables")
public class TpTaggables extends Model<TpTaggables> {
    
    private Integer tagId;
    
    private String taggableType;
    
    private Integer taggableId;
}

