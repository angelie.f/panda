package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

/**
 * 跑马灯广告
 */
@Data
@TableName("t_announcement")
public class Announcement extends SuperEntity<Announcement> {

    private static final long serialVersionUID = 2646201532621057453L;
    private String content;
    private Integer sort;
    private String title;


}
