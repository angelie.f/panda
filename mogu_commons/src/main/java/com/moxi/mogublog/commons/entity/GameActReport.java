package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@TableName("t_game_act_report")
public class GameActReport extends SuperEntity<GameActReport> implements Serializable {
    private static final long serialVersionUID = 7264194658336128824L;
    private String source;

    private Integer loginFirstNum;

    private Integer bindNum;

    private Integer vipNum;

    private Integer giftCoinsNum;

    private BigDecimal loginFirstAmount;

    private BigDecimal bindAmount;

    private BigDecimal vipAmount;

    private BigDecimal giftCoinsAmount;

    private BigDecimal gameCoinsAmount;

    private BigDecimal betAmount;

    private String period;

    private String remark;

    private Integer betNoDepNum;

    private Integer betDepNum;

    @TableField(exist = false)
    private String sourceName;
}
