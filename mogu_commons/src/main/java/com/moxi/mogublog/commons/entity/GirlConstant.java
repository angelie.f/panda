package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;

import java.util.List;

/**
 * 妹子订单评论
 *
 * @author Vergift
 * @date 2021-12-29
 */
@Data
@TableName("t_girl_constant")
public class GirlConstant extends SuperEntity<GirlConstant> {

    private static final long serialVersionUID = 2646201532621057453L;

    private String param;

    private String value;



}
