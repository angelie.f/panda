package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;


@Data
@TableName("t_game_type")
public class GameType extends SuperEntity<GameType> {


    private static final long serialVersionUID = -7160878889304111775L;
    /**
     * 類別名称
     */
    private String name;

}
