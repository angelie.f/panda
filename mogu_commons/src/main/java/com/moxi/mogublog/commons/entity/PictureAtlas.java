package com.moxi.mogublog.commons.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.moxi.mougblog.base.entity.SuperEntity;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * 图集类
 *
 * @author xuzhixiang
 * @date 2018年9月17日16:10:38
 */
@Data
@TableName("t_picture_atlas")
public class PictureAtlas extends SuperEntity<PictureAtlas> {

    /**
     *
     */
    private static final long serialVersionUID = 3454006152368184626L;

    /**
     * 父UID
     */
    private String parentUid;

    /**
     * 图集名
     */
    private String name;

    /**
     * 图集风图片Uid
     */
    private String fileUid;

    /**
     * 排序字段，数值越大，越靠前
     */
    private int sort;

    /**
     * 是否显示  1: 是  0: 否
     */
    private Integer isShow;

    //以下字段不存入数据库

    /**
     * 图集图片
     */
    @TableField(exist = false)
    private List<String> photoList;

    /**
     * 图集封面
     */
    @TableField(exist = false)
    private String PictureUrl;

    /**
     * 标签,一篇图集对应多个标签
     */
    @TableField(exist = false)
    private List<PictureTag> tagList;

    /**
     * 图集标签uid
     */
    private String tagUid;

    /**
     * 模特
     */
    private String model;

    /**
     * 出品商
     */
    private String producer;

    /**
     * 图集分类uid
     */
    private String sortUid;

    /**
     * 资源图集id
     */
    private String resourceId;

    /**
     * 来源
     */
    private String resource;

    /**
     * @TableField 配置需要填充的字段
     * 图片最新入库时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date storageTime;

    /**
     * 图集图片数量
     */
    private Integer count;

    /**
     * 所属产品
     */
    @TableField(exist = false)
    private String projectName;

    /**
     * 普通用户限制图片数量
     */
    @TableField(exist = false)
    private Integer userLimit;
}
