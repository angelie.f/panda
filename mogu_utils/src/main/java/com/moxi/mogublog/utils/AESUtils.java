package com.moxi.mogublog.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.annotation.PostConstruct;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AESUtils {
    private static Logger LOGGER = LoggerFactory.getLogger(AESUtils.class);
    public static String decrypt(String content){
        try {
            content = content.replace(" ", "+");
            byte[] raw = "l*bv%ZiqBiaogaKm".getBytes("ASCII");
            SecretKeySpec secretKeySpec = new SecretKeySpec(raw, "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            IvParameterSpec ivParameterSpec = new IvParameterSpec("8597506029392492".getBytes());
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);
            byte[] encrypted = new BASE64Decoder().decodeBuffer(content);
            byte[] original = cipher.doFinal(encrypted);
            String originalString = new String(original, "UTF-8");
            return originalString;
        }catch (Exception e){
            LOGGER.error("userInfo解密失败");
            return null;
        }
    }
    public static String encrypt(String content){
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE,new SecretKeySpec("xPxo2S5uGPhKHx5g".getBytes("UTF-8"),"AES"),
                    new IvParameterSpec("0a1b2c3d4e5f6789".getBytes()));
            int blockSize = cipher.getBlockSize();
            StringBuilder stringBuilder = new StringBuilder(content);
            if (content.length()%blockSize!=0){
                for (int i=0;i<blockSize-(content.length()%blockSize);i++){
                    stringBuilder.append("\0");
                }
            }
            byte[]encypted = cipher.doFinal(stringBuilder.toString().getBytes("UTF-8"));
            return new BASE64Encoder().encodeBuffer(encypted).trim();
        }catch (Exception e){
            LOGGER.error("userInfo加密失败");
            return null;
        }
    }
    public static String encryptForShop(String content){
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE,new SecretKeySpec("gqWxkyGwiA1hTxJx".getBytes("UTF-8"),"AES"),
                    new IvParameterSpec("ikbBTZUmbDnwb7M4".getBytes()));
            int blockSize = cipher.getBlockSize();
            StringBuilder stringBuilder = new StringBuilder(content);
            if (content.length()%blockSize!=0){
                for (int i=0;i<blockSize-(content.length()%blockSize);i++){
                    stringBuilder.append("\0");
                }
            }
            byte[]encypted = cipher.doFinal(stringBuilder.toString().getBytes("UTF-8"));
            return new BASE64Encoder().encodeBuffer(encypted).trim();
        }catch (Exception e){
            LOGGER.error("userInfo加密失败");
            return null;
        }
    }
    public static String decryptForShop(String content){
        try {
            content = content.replace(" ", "+");
            String key = "gqWxkyGwiA1hTxJx";
            String iv = "ikbBTZUmbDnwb7M4";
            byte[] raw = key.getBytes("ASCII");
            SecretKeySpec secretKeySpec = new SecretKeySpec(raw, "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            IvParameterSpec ivParameterSpec = new IvParameterSpec(iv.getBytes());
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);
            byte[] encrypted = new BASE64Decoder().decodeBuffer(content);
            byte[] original = cipher.doFinal(encrypted);
            String originalString = new String(original, "UTF-8");
            return originalString;
        }catch (Exception e){
            LOGGER.error("userInfo解密失败");
            return null;
        }
    }

    public static String encryptForGame(String input, String key) {
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE,new SecretKeySpec(key.getBytes("UTF-8"),"AES"));
            byte[]encypted = cipher.doFinal(input.getBytes("UTF-8"));
            return new BASE64Encoder().encodeBuffer(encypted).trim();
        } catch (Exception e) {
            LOGGER.error("userInfo加密失败");
            return null;
        }

        //return new String(Base64.encodeBase64(crypted));

    }
}
