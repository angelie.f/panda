package com.moxi.mogublog.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.DigestUtils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * MD5工具类
 *
 * @author xzx19950624@qq.com
 * @date 2017年9月24日16:40:20
 */
public class MD5Utils {
    private static Logger logger = LoggerFactory.getLogger(MD5Utils.class);

    /**
     * MD5加码 生成32位md5码(不可逆的)
     *
     * @param inStr
     * @return
     * @author xuzhixiang
     * @date 2017年9月24日17:22:13
     */
    public static String string2MD5(String inStr) {
        MessageDigest md5;
        String string = "";
        try {
            md5 = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            logger.error("MD5加密实现的错误日志-->>" + e.getMessage(), e);
            return string;
        }
        char[] charArray = inStr.toCharArray();
        byte[] byteArray = new byte[charArray.length];
        for (int i = 0; i < charArray.length; i++) {
            byteArray[i] = (byte) charArray[i];
        }
        byte[] md5Bytes = md5.digest(byteArray);
        StringBuffer hexValue = new StringBuffer();
        for (int i = 0; i < md5Bytes.length; i++) {
            int val = ((int) md5Bytes[i]) & 0xff;
            if (val < 16) {
                hexValue.append("0");
            }
            hexValue.append(Integer.toHexString(val));
        }
        string = hexValue.toString();
        logger.debug("MD5加密的32位密钥的调试日志-->>" + string);
        return string;
    }

    /**
     * 加密解密算法 执行一次加密，两次解密
     *
     * @param inStr
     * @return
     * @throws Exception
     */
    public static String convertMD5(String inStr) throws Exception {
        char[] a = inStr.toCharArray();
        for (int i = 0; i < a.length; i++) {
            a[i] = (char) (a[i] ^ 't');
        }
        String string = new String(a);
        logger.debug("MD5加密的二次加密的字符串的调试日志-->>" + string);
        return string;
    }

    /**
     * 获得字符串的md5值.
     *
     * @param sginStr 待加密的字符串
     * @return md5 加密后的字符串
     */
    public static String md5Sign(String sginStr){
        String sign = DigestUtils.md5DigestAsHex(sginStr.toString().getBytes());
        return sign;
    }

    /**
     * 获得字符串的md5大写值.
     *
     * @param str 待加密的字符串
     * @return md5 加密后的字符串
     */
    public static String md5SignUpper(String str) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        return md5Sign(str).toUpperCase();
    }

    /**
     * map请求参数按照ascii升序排序后拼装字符
     * 排序后的拼装规则 key1=value1&key2=value2&...keyn=valuen+key
     * @param map 待拼装的map
     * @param signKey md5秘钥
     * @return md5 拼装后的字符串
     */
    public static String getMD5String(Map<String,Object> map, String signKey){
        if (map == null){
            return null;
        }
        StringBuffer sb = new StringBuffer();
        List<String> keyList = new ArrayList<>(map.keySet());
        Collections.sort(keyList);
        for (int i=0; i<keyList.size(); i++) {
            String key = keyList.get(i);
            Object value = map.get(key);
            if (StringUtils.isNotEmpty(String.valueOf(value))){
                sb.append(key).append("=").append(String.valueOf(value)).append("&");
            }
        }
        String sginStr = sb + signKey;
        return sginStr;
    }

}
