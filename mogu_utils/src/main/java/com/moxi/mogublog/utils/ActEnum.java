package com.moxi.mogublog.utils;


public enum ActEnum {
    FIRSTLOGINPRE("FIRSTLOGINPRE","FIRSTLOGINPRE","3"),
    VIPLEVELPRE("VIPLEVELPRE","FIRSTLOGINPRE","15"),
    BINDPHONEPRE("BINDPHONEPRE","FIRSTLOGINPRE","3");


    private String actType;
    private String actDec;
    private String  preAmont;

    ActEnum(String actType, String actDec, String preAmont) {
        this.actType = actType;
        this.actDec = actDec;
        this.preAmont = preAmont;
    }

    public String getActType() {
        return actType;
    }

    public String getActDec() {
        return actDec;
    }

    public String getPreAmont() {
        return preAmont;
    }
}
