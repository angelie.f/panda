package com.moxi.mogublog.utils;

import java.util.Arrays;
import java.util.Optional;

public enum GameActEnum {
    ONE("1","actPreAmount1"),
    TWO ("2","actPreAmount2" ),
    THREE   ("3","actPreAmount3" ),
    FOUR  ("4","actPreAmount4" ),
    FIVE  ("5","actPreAmount5" ),
    SIX  ("6","actPreAmount6" ),
    SEVEN ("7","actPreAmount7" ),
    EIGHT("8","actPreAmount8" ),
    NINE("9","actPreAmount9" ),
    TEN("10","actPreAmount10"),
    ELEVEN("11","actPreAmount11"),
    TWELVE("12","actPreAmount12"),
    THIRTEEN("13","actPreAmount13"),
    FOURTEEN("14","actPreAmount14"),
    FIFTEEN("15","actPreAmount15"),
    SIXTEEN("16","actPreAmount16"),
    SEVENTEEN("17","actPreAmount17"),
    EIGHTEEN("18","actPreAmount18"),
    NINETEEN("19","actPreAmount19"),
    TWENTY("20","actPreAmount20"),
    TWENTY1("21","actPreAmount21"),
    TWENTY2("22","actPreAmount22"),
    TWENTY3("23","actPreAmount23"),
    TWENTY4("24","actPreAmount24"),
    TWENTY5("25","actPreAmount25"),
    TWENTY6("26","actPreAmount26"),
    TWENTY7("27","actPreAmount27"),
    TWENTY8("28","actPreAmount28"),
    TWENTY9("29","actPreAmount29"),
    THIRTY("30","actPreAmount30"),
    THIRTY1("31","actPreAmount31"),
    THIRTY2("32","actPreAmount32"),
    THIRTY3 ("33","actPreAmount33"),
    THIRTY4("34","actPreAmount34"),
    THIRTY5("35","actPreAmount35"),
    FORTY1("41","actPreAmount41"),
    FORTY2("42","actPreAmount42"),
    THIRTY99("99","actPreAmount99"),
    HUNDRED101("101","actPreAmount101"),
    THIRTY6("36","actPreAmount36"),
    THIRTY7("37","actPreAmount37"),
    THIRTY8("38","actPreAmount38"),
    THIRTY9("39","actPreAmount39"),
    FORTY0("40","actPreAmount40"),
    FORTY3("43","actPreAmount43"),
    FORTY4("44","actPreAmount44"),
    FORTY5("45","actPreAmount45"),
    FORTY6("46","actPreAmount46"),
    FORTY7("47","actPreAmount47"),
    FORTY8("48","actPreAmount48"),
    ;


    private String source;
    private String key;

    GameActEnum(String source, String key) {
        this.source = source;
        this.key = key;
    }

    public String getSource() {
        return source;
    }

    public String getKey() {
        return key;
    }

    public static Optional<GameActEnum> findActSource(String source){
     return  Arrays.asList(GameActEnum.values()).stream().filter(item->item.getSource().equals(source)).findFirst();
    }
}
