/*
 Navicat Premium Data Transfer

 Source Server         : 18.204.12.13
 Source Server Type    : MySQL
 Source Server Version : 50733
 Source Host           : 18.204.12.13:13306
 Source Schema         : mogu_blog

 Target Server Type    : MySQL
 Target Server Version : 50733
 File Encoding         : 65001

 Date: 07/12/2021 13:02:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_admin
-- ----------------------------
DROP TABLE IF EXISTS "t_admin";
CREATE TABLE "t_admin"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '唯一uid',
  "user_name" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  "pass_word" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  "gender" varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别(1:男2:女)',
  "avatar" varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '个人头像',
  "email" varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  "birthday" date NULL DEFAULT NULL COMMENT '出生年月日',
  "mobile" varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机',
  "valid_code" varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱验证码',
  "summary" varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自我简介最多150字',
  "login_count" int(11) UNSIGNED NULL DEFAULT 0 COMMENT '登录次数',
  "last_login_time" datetime NULL DEFAULT NULL COMMENT '最后登录时间',
  "last_login_ip" varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '127.0.0.1' COMMENT '最后登录IP',
  "status" tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态',
  "create_time" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  "nick_name" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  "qq_number" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'QQ号',
  "we_chat" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信号',
  "occupation" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '职业',
  "github" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'github地址',
  "gitee" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'gitee地址',
  "role_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '拥有的角色uid',
  "person_resume" text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '履历',
  "project_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '项目uid',
  PRIMARY KEY ("uid") USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_blog
-- ----------------------------
DROP TABLE IF EXISTS "t_blog";
CREATE TABLE "t_blog"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '唯一uid',
  "title" varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '博客标题',
  "summary" varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '博客简介',
  "content" longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '博客内容',
  "tag_uid" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签uid',
  "click_count" int(11) NULL DEFAULT 0 COMMENT '博客点击数',
  "collect_count" int(11) NULL DEFAULT 0 COMMENT '博客收藏数',
  "file_uid" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题图片uid',
  "status" tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态',
  "create_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  "admin_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '管理员uid',
  "is_original" varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '是否原创（0:不是 1：是）',
  "author" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '作者',
  "articles_part" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文章出处',
  "blog_sort_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '博客分类UID',
  "level" tinyint(1) NULL DEFAULT 0 COMMENT '推荐等级(0:正常)',
  "is_publish" varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '是否发布：0：否，1：是',
  "sort" int(11) NOT NULL DEFAULT 0 COMMENT '排序字段',
  "open_comment" tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否开启评论(0:否 1:是)',
  "type" tinyint(1) NOT NULL DEFAULT 0 COMMENT '类型【0 博客， 1：推广】',
  "outside_link" varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '外链【如果是推广，那么将跳转到外链】',
  "oid" int(11) NOT NULL AUTO_INCREMENT COMMENT '唯一oid',
  "user_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '投稿用户UID',
  "article_source" tinyint(1) NOT NULL DEFAULT 0 COMMENT '文章来源【0 后台添加，1 用户投稿】',
  PRIMARY KEY ("uid", "oid") USING BTREE,
  INDEX "oid"("oid") USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 60 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '博客表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_blog_sort
-- ----------------------------
DROP TABLE IF EXISTS "t_blog_sort";
CREATE TABLE "t_blog_sort"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '唯一uid',
  "sort_name" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类内容',
  "content" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类简介',
  "create_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  "status" tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态',
  "sort" int(11) NULL DEFAULT 0 COMMENT '排序字段，越大越靠前',
  "click_count" int(11) NULL DEFAULT 0 COMMENT '点击数',
  PRIMARY KEY ("uid") USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '博客分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_blog_spider
-- ----------------------------
DROP TABLE IF EXISTS "t_blog_spider";
CREATE TABLE "t_blog_spider"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '唯一uid',
  "title" varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '博客标题',
  "summary" varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '博客简介',
  "content" longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '博客内容',
  "tag_uid" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签uid',
  "click_count" int(11) NULL DEFAULT 0 COMMENT '博客点击数',
  "collect_count" int(11) NULL DEFAULT 0 COMMENT '博客收藏数',
  "file_uid" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题图片uid',
  "status" tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态',
  "create_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  "admin_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '管理员uid',
  "is_original" varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '是否原创（0:不是 1：是）',
  "author" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '作者',
  "articles_part" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文章出处',
  "blog_sort_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '博客分类UID',
  "level" tinyint(1) NULL DEFAULT 0 COMMENT '推荐等级(0:正常)',
  "is_publish" varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '是否发布：0：否，1：是',
  "sort" int(11) NOT NULL DEFAULT 0 COMMENT '排序字段',
  PRIMARY KEY ("uid") USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '博客爬取表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_category_menu
-- ----------------------------
DROP TABLE IF EXISTS "t_category_menu";
CREATE TABLE "t_category_menu"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '唯一uid',
  "name" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
  "menu_level" tinyint(1) NULL DEFAULT NULL COMMENT '菜单级别',
  "summary" varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '简介',
  "parent_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父uid',
  "url" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'url地址',
  "icon" varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  "sort" int(11) NULL DEFAULT 0 COMMENT '排序字段，越大越靠前',
  "status" tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态',
  "create_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  "is_show" tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否显示 1:是 0:否',
  "menu_type" tinyint(1) NOT NULL DEFAULT 0 COMMENT '菜单类型 0: 菜单   1: 按钮',
  "is_jump_external_url" tinyint(1) NULL DEFAULT 0 COMMENT '是否跳转外部链接 0：否，1：是',
  PRIMARY KEY ("uid") USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_collect
-- ----------------------------
DROP TABLE IF EXISTS "t_collect";
CREATE TABLE "t_collect"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '唯一uid',
  "user_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户的uid',
  "blog_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '博客的uid',
  "status" tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态',
  "create_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  PRIMARY KEY ("uid") USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '收藏表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_comment
-- ----------------------------
DROP TABLE IF EXISTS "t_comment";
CREATE TABLE "t_comment"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '唯一uid',
  "user_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户uid',
  "to_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '回复某条评论的uid',
  "to_user_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '回复某个人的uid',
  "content" varchar(2048) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评论内容',
  "blog_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '博客uid',
  "status" tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态',
  "create_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  "source" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '评论来源： MESSAGE_BOARD，ABOUT，BLOG_INFO 等',
  "TYPE" tinyint(1) NOT NULL DEFAULT 0 COMMENT '评论类型 1:点赞 0:评论',
  "first_comment_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '一级评论UID',
  PRIMARY KEY ("uid") USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '评论表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_comment_report
-- ----------------------------
DROP TABLE IF EXISTS "t_comment_report";
CREATE TABLE "t_comment_report"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '唯一uid',
  "user_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '举报人uid',
  "report_comment_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '被举报的评论Uid',
  "report_user_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '被举报的用户uid',
  "content" varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '举报的原因',
  "progress" tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '进展状态: 0 未查看   1: 已查看  2：已处理',
  "status" tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态',
  "create_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  PRIMARY KEY ("uid") USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '评论举报表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_exception_log
-- ----------------------------
DROP TABLE IF EXISTS "t_exception_log";
CREATE TABLE "t_exception_log"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '唯一uid',
  "exception_json" mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '异常对象json格式',
  "exception_message" mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '异常信息',
  "status" tinyint(1) NULL DEFAULT 1 COMMENT '状态',
  "create_time" timestamp NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  "update_time" timestamp NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  "ip" varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ip地址',
  "ip_source" varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ip来源',
  "method" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求方法',
  "operation" varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '方法描述',
  "params" longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '请求参数',
  PRIMARY KEY ("uid") USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_feedback
-- ----------------------------
DROP TABLE IF EXISTS "t_feedback";
CREATE TABLE "t_feedback"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '唯一uid',
  "user_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户uid',
  "content" varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '反馈的内容',
  "status" tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态',
  "create_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  "title" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  "feedback_status" tinyint(1) NOT NULL DEFAULT 0 COMMENT '反馈状态： 0：已开启  1：进行中  2：已完成  3：已拒绝',
  "reply" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '回复',
  "admin_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '管理员uid',
  PRIMARY KEY ("uid") USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '反馈表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_link
-- ----------------------------
DROP TABLE IF EXISTS "t_link";
CREATE TABLE "t_link"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '唯一uid',
  "title" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '友情链接标题',
  "summary" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '友情链接介绍',
  "url" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '友情链接URL',
  "click_count" int(11) NULL DEFAULT 0 COMMENT '点击数',
  "create_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  "status" tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态',
  "sort" int(11) NULL DEFAULT 0 COMMENT '排序字段，越大越靠前',
  "link_status" tinyint(1) NOT NULL DEFAULT 1 COMMENT '友链状态： 0 申请中， 1：已上线，  2：已下架',
  "user_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '申请用户UID',
  "admin_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作管理员UID',
  "email" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '站长邮箱',
  "file_uid" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '网站图标',
  PRIMARY KEY ("uid") USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '友情链接表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_picture
-- ----------------------------
DROP TABLE IF EXISTS "t_picture";
CREATE TABLE "t_picture"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '唯一uid',
  "file_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片uid',
  "pic_name" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片名',
  "picture_atlas_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图集uid',
  "status" tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态',
  "create_time" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY ("uid") USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '图片表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_picture_atlas
-- ----------------------------
DROP TABLE IF EXISTS "t_picture_atlas";
CREATE TABLE "t_picture_atlas"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '唯一uid',
  "file_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图集封面图片uid',
  "name" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图集名',
  "status" tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '上下架状态，1：上架，0，下架',
  "create_time" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  "parent_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  "sort" int(11) NULL DEFAULT 0 COMMENT '排序字段，越大越靠前',
  "is_show" tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否显示，1：是，0，否',
  "tag_uid" varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片标签uid',
  "model" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模特',
  "producer" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '出品商',
  "sort_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图集分类uid',
  "resource_id" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '资源图集id',
  "resource" varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '来源',
  "storage_time" timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '图片最新入库时间',
  "count" int(100) NULL DEFAULT NULL COMMENT '数量',
  "id" int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  PRIMARY KEY ("id") USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 60 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '图集分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_picture_sort
-- ----------------------------
DROP TABLE IF EXISTS "t_picture_sort";
CREATE TABLE "t_picture_sort"  (
  "uid" varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '唯一uid',
  "name" varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '分类名称',
  "introduce" varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分类介绍',
  "file_uid" varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分类图标封面图片uid',
  "sort" int(11) NULL DEFAULT NULL COMMENT '排序字段，越大越靠前',
  "tag_uid" varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标签uid',
  "create_time" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  "project_uid" varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '图集所属项目uid',
  "status" tinyint(1) NULL DEFAULT NULL COMMENT '状态',
  "id" int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  PRIMARY KEY ("id") USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '图集分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_picture_sort_item
-- ----------------------------
DROP TABLE IF EXISTS "t_picture_sort_item";
CREATE TABLE "t_picture_sort_item"  (
  "uid" varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '唯一uid',
  "sort_uid" varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图集分类uid',
  "atlas_uid" varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图集uid',
  "status" int(1) NULL DEFAULT NULL COMMENT '转态:1上架，0下架',
  "create_time" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY ("uid") USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '上下架表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_picture_tag
-- ----------------------------
DROP TABLE IF EXISTS "t_picture_tag";
CREATE TABLE "t_picture_tag"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '唯一uid',
  "content" varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签内容',
  "status" tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态',
  "click_count" int(11) NULL DEFAULT 0 COMMENT '标签简介',
  "create_time" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  "sort" int(11) NULL DEFAULT 0 COMMENT '排序字段，越大越靠前',
  "introduce" varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签介绍',
  PRIMARY KEY ("uid") USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '图片标签表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for t_project
-- ----------------------------
DROP TABLE IF EXISTS "t_project";
CREATE TABLE "t_project"  (
  "uid" varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '唯一uid',
  "name" varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '项目名称',
  "create_time" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  "status" tinyint(1) NULL DEFAULT NULL COMMENT '状态',
  "source" int(11) NULL DEFAULT NULL COMMENT '来源:1.青青草；2.小黄鸭',
  PRIMARY KEY ("uid") USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '项目表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_question
-- ----------------------------
DROP TABLE IF EXISTS "t_question";
CREATE TABLE "t_question"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '唯一uid',
  "oid" int(11) NOT NULL AUTO_INCREMENT COMMENT '唯一oid',
  "title" varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '问答标题',
  "summary" varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '问答简介',
  "content" longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '问答内容',
  "question_tag_uid" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '问答uid',
  "click_count" int(11) NULL DEFAULT 0 COMMENT '问答点击数',
  "collect_count" int(11) NULL DEFAULT 0 COMMENT '问答收藏数',
  "file_uid" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题图片uid',
  "status" tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态',
  "create_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  "admin_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '管理员uid',
  "user_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户UID',
  "is_publish" varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '是否发布：0：否，1：是',
  "question_status" tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '问答状态，0:创建，1:进行，2:已采纳',
  "sort" int(11) NOT NULL DEFAULT 0 COMMENT '排序字段',
  "open_comment" tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否开启评论(0:否 1:是)',
  "reply_count" int(11) NOT NULL DEFAULT 0 COMMENT '回答次数',
  PRIMARY KEY ("uid", "oid") USING BTREE,
  INDEX "oid"("oid") USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '问答表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_question_tag
-- ----------------------------
DROP TABLE IF EXISTS "t_question_tag";
CREATE TABLE "t_question_tag"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '唯一uid',
  "parent_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父uid',
  "name" varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签名',
  "summary" varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签简介',
  "status" tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态',
  "click_count" int(11) NULL DEFAULT 0 COMMENT '点击数',
  "create_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  "sort" int(11) NULL DEFAULT 0 COMMENT '排序字段，越大越靠前',
  PRIMARY KEY ("uid") USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '问答标签表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_resource_sort
-- ----------------------------
DROP TABLE IF EXISTS "t_resource_sort";
CREATE TABLE "t_resource_sort"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '唯一uid',
  "file_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类图片uid',
  "sort_name" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类名',
  "content" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类介绍',
  "click_count" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '点击数',
  "status" tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态',
  "create_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  "parent_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父UID',
  "sort" int(11) NULL DEFAULT 0 COMMENT '排序字段',
  PRIMARY KEY ("uid") USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '资源分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS "t_role";
CREATE TABLE "t_role"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色id',
  "role_name" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名',
  "create_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  "status" tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态',
  "summary" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色介绍',
  "category_menu_uids" text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '角色管辖的菜单的UID',
  PRIMARY KEY ("uid") USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_study_video
-- ----------------------------
DROP TABLE IF EXISTS "t_study_video";
CREATE TABLE "t_study_video"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '唯一uid',
  "file_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '视频封面图片uid',
  "resource_sort_uid" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '资源分类UID',
  "name" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '视频名称',
  "summary" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '视频简介',
  "content" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类介绍',
  "baidu_path" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '百度云完整路径',
  "click_count" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '点击数',
  "status" tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态',
  "create_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  "parent_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY ("uid") USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '学习视频表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_subject
-- ----------------------------
DROP TABLE IF EXISTS "t_subject";
CREATE TABLE "t_subject"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  "subject_name" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '专题名称',
  "summary" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '简介',
  "file_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '封面图片UID',
  "click_count" int(11) NOT NULL DEFAULT 0 COMMENT '专题点击数',
  "collect_count" int(11) NOT NULL DEFAULT 0 COMMENT '专题收藏数',
  "status" tinyint(1) NULL DEFAULT 1 COMMENT '状态',
  "sort" int(11) NOT NULL DEFAULT 0 COMMENT '排序字段',
  "create_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  PRIMARY KEY ("uid") USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '专题表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_subject_item
-- ----------------------------
DROP TABLE IF EXISTS "t_subject_item";
CREATE TABLE "t_subject_item"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  "subject_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '专题uid',
  "blog_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '博客uid',
  "status" tinyint(1) NULL DEFAULT 1 COMMENT '状态',
  "sort" int(11) NOT NULL DEFAULT 0 COMMENT '排序字段',
  "create_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  PRIMARY KEY ("uid") USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '专题Item表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS "t_sys_dict_data";
CREATE TABLE "t_sys_dict_data"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  "oid" int(11) NOT NULL AUTO_INCREMENT COMMENT '自增键oid',
  "dict_type_uid" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典类型UID',
  "dict_label" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典标签',
  "dict_value" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典键值',
  "css_class" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  "list_class" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  "is_default" tinyint(1) NULL DEFAULT 0 COMMENT '是否默认（1是 0否）,默认为0',
  "create_by_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人UID',
  "update_by_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后更新人UID',
  "remark" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  "status" tinyint(1) NULL DEFAULT 1 COMMENT '状态',
  "create_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  "is_publish" varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '是否发布(1:是，0:否)',
  "sort" int(11) NOT NULL DEFAULT 0 COMMENT '排序字段',
  PRIMARY KEY ("uid") USING BTREE,
  INDEX "oid"("oid") USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 81 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS "t_sys_dict_type";
CREATE TABLE "t_sys_dict_type"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  "oid" int(11) NOT NULL AUTO_INCREMENT COMMENT '自增键oid',
  "dict_name" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典名称',
  "dict_type" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典类型',
  "create_by_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人UID',
  "update_by_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后更新人UID',
  "remark" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  "status" tinyint(1) NULL DEFAULT 1 COMMENT '状态',
  "create_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  "is_publish" varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '是否发布(1:是，0:否)',
  "sort" int(11) NOT NULL DEFAULT 0 COMMENT '排序字段',
  PRIMARY KEY ("uid") USING BTREE,
  INDEX "oid"("oid") USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sys_log
-- ----------------------------
DROP TABLE IF EXISTS "t_sys_log";
CREATE TABLE "t_sys_log"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '唯一uid',
  "user_name" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  "admin_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '管理员uid',
  "ip" varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求ip地址',
  "url" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求url',
  "type" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求方式',
  "class_path" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求类路径',
  "method" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求方法名',
  "params" longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '请求参数',
  "operation" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  "status" tinyint(1) NULL DEFAULT 1 COMMENT '状态',
  "create_time" timestamp NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  "update_time" timestamp NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  "ip_source" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ip来源',
  "spend_time" int(11) NULL DEFAULT 0 COMMENT '方法请求花费的时间',
  PRIMARY KEY ("uid") USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_sys_params
-- ----------------------------
DROP TABLE IF EXISTS "t_sys_params";
CREATE TABLE "t_sys_params"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  "params_type" varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '配置类型 是否系统内置(1:，是 0:否)',
  "params_name" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数名称',
  "params_key" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数键名',
  "remark" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  "params_value" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数键值',
  "status" tinyint(1) NULL DEFAULT 1 COMMENT '状态',
  "create_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  "sort" int(11) NOT NULL DEFAULT 0 COMMENT '排序字段',
  PRIMARY KEY ("uid") USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_system_config
-- ----------------------------
DROP TABLE IF EXISTS "t_system_config";
CREATE TABLE "t_system_config"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  "qi_niu_access_key" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '七牛云公钥',
  "qi_niu_secret_key" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '七牛云私钥',
  "email" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱账号',
  "email_user_name" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱发件人用户名',
  "email_password" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱密码',
  "smtp_address" varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SMTP地址',
  "smtp_port" varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT 'SMTP端口',
  "status" tinyint(1) NULL DEFAULT 1 COMMENT '状态',
  "create_time" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  "qi_niu_bucket" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '七牛云上传空间',
  "qi_niu_area" varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '七牛云存储区域 华东（z0），华北(z1)，华南(z2)，北美(na0)，东南亚(as0)',
  "upload_qi_niu" varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '图片是否上传七牛云 (0:否， 1：是)',
  "upload_local" varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '图片是否上传本地存储 (0:否， 1：是)',
  "picture_priority" varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '图片显示优先级（ 1 展示 七牛云,  0 本地）',
  "qi_niu_picture_base_url" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '七牛云域名前缀：http://images.moguit.cn',
  "local_picture_base_url" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '本地服务器域名前缀：http://localhost:8600',
  "start_email_notification" varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '是否开启邮件通知(0:否， 1:是)',
  "editor_model" tinyint(1) NOT NULL DEFAULT 0 COMMENT '编辑器模式，(0：富文本编辑器CKEditor，1：markdown编辑器Veditor)',
  "theme_color" varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '#409EFF' COMMENT '主题颜色',
  "minio_end_point" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Minio远程连接地址',
  "minio_access_key" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Minio公钥',
  "minio_secret_key" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Minio私钥',
  "minio_bucket" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Minio桶',
  "upload_minio" tinyint(1) NOT NULL DEFAULT 0 COMMENT '图片是否上传Minio (0:否， 1：是)',
  "minio_picture_base_url" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Minio服务器文件域名前缀',
  "open_dashboard_notification" tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否开启仪表盘通知(0:否， 1:是)',
  "dashboard_notification" longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '仪表盘通知【用于首次登录弹框】',
  "content_picture_priority" tinyint(1) NOT NULL DEFAULT 0 COMMENT '博客详情图片显示优先级（ 0:本地  1: 七牛云 2: Minio）',
  "open_email_activate" tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否开启用户邮件激活功能【0 关闭，1 开启】',
  "search_model" tinyint(1) NOT NULL DEFAULT 0 COMMENT '搜索模式【0:SQL搜索 、1：全文检索】',
  "md5_key" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '前端md5秘钥',
  PRIMARY KEY ("uid") USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_tag
-- ----------------------------
DROP TABLE IF EXISTS "t_tag";
CREATE TABLE "t_tag"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '唯一uid',
  "content" varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签内容',
  "status" tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态',
  "click_count" int(11) NULL DEFAULT 0 COMMENT '标签简介',
  "sort" int(11) NULL DEFAULT 0 COMMENT '排序字段，越大越靠前',
  "create_time" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY ("uid") USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '标签表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_todo
-- ----------------------------
DROP TABLE IF EXISTS "t_todo";
CREATE TABLE "t_todo"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '唯一uid',
  "admin_uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '管理员uid',
  "text" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '内容',
  "done" tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '表示事项是否完成（0：未完成 1：已完成）',
  "status" tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态',
  "create_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  PRIMARY KEY ("uid") USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代办事项表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_use_power
-- ----------------------------
DROP TABLE IF EXISTS "t_use_power";
CREATE TABLE "t_use_power"  (
  "uid" varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '唯一uid',
  "user_astrict" int(1) NULL DEFAULT NULL COMMENT '普通用户是否限制:0不限制；1限制',
  "user_limit" int(11) NULL DEFAULT NULL COMMENT '普通用户限制页数',
  "vip_astrict" int(1) NULL DEFAULT NULL COMMENT 'vip是否限制:0不限制；1限制',
  "vip_limit" int(11) NULL DEFAULT NULL COMMENT 'vip限制页数',
  "create_time" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  "status" int(1) NOT NULL COMMENT '状态',
  "project_uid" varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '所属项目id',
  PRIMARY KEY ("uid") USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '使用权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS "t_user";
CREATE TABLE "t_user"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '唯一uid',
  "user_name" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  "pass_word" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  "gender" tinyint(1) UNSIGNED NULL DEFAULT NULL COMMENT '性别(1:男2:女)',
  "avatar" varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '个人头像',
  "email" varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  "birthday" date NULL DEFAULT NULL COMMENT '出生年月日',
  "mobile" varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机',
  "valid_code" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱验证码',
  "summary" varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自我简介最多150字',
  "login_count" int(11) UNSIGNED NULL DEFAULT 0 COMMENT '登录次数',
  "last_login_time" datetime NULL DEFAULT NULL COMMENT '最后登录时间',
  "last_login_ip" varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '127.0.0.1' COMMENT '最后登录IP',
  "status" tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态',
  "create_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  "nick_name" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '昵称',
  "source" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '资料来源',
  "uuid" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '平台uuid',
  "qq_number" varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'QQ号',
  "we_chat" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信号',
  "occupation" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '职业',
  "comment_status" tinyint(1) NOT NULL DEFAULT 1 COMMENT '评论状态 1:正常 0:禁言',
  "ip_source" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ip来源',
  "browser" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '浏览器',
  "os" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作系统',
  "start_email_notification" tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否开启邮件通知 1:开启 0:关闭',
  "user_tag" tinyint(1) NOT NULL DEFAULT 0 COMMENT '用户标签：0：普通用户，1：管理员，2：博主 等',
  "loading_valid" tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否通过加载校验【0 未通过，1 已通过】',
  PRIMARY KEY ("uid") USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_visitor
-- ----------------------------
DROP TABLE IF EXISTS "t_visitor";
CREATE TABLE "t_visitor"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '唯一uid',
  "user_name" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  "email" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '邮箱',
  "login_count" int(11) UNSIGNED NULL DEFAULT 0 COMMENT '登录次数',
  "last_login_time" datetime NULL DEFAULT NULL COMMENT '最后登录时间',
  "last_login_ip" varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '127.0.0.1' COMMENT '最后登录IP',
  "status" tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态',
  "create_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  PRIMARY KEY ("uid") USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '游客表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_web_config
-- ----------------------------
DROP TABLE IF EXISTS "t_web_config";
CREATE TABLE "t_web_config"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  "logo" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'logo(文件UID)',
  "name" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '网站名称',
  "summary" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '介绍',
  "keyword" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '关键字',
  "author" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '作者',
  "record_num" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '备案号',
  "status" tinyint(1) NULL DEFAULT 1 COMMENT '状态',
  "create_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  "title" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  "ali_pay" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '支付宝收款码FileId',
  "weixin_pay" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信收款码FileId',
  "github" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'github地址',
  "gitee" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'gitee地址',
  "qq_number" varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'QQ号',
  "qq_group" varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'QQ群',
  "we_chat" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信号',
  "email" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  "show_list" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示的列表（用于控制邮箱、QQ、QQ群、Github、Gitee、微信是否显示在前端）',
  "login_type_list" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录方式列表（用于控制前端登录方式，如账号密码,码云,Github,QQ,微信）',
  "open_comment" varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '是否开启评论(0:否 1:是)',
  "open_mobile_comment" tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否开启移动端评论(0:否， 1:是)',
  "open_admiration" tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否开启赞赏(0:否， 1:是)',
  "open_mobile_admiration" tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否开启移动端赞赏(0:否， 1:是)',
  "link_apply_template" varchar(2018) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '友链申请模板,添加友链申请模板格式',
  PRIMARY KEY ("uid") USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_web_navbar
-- ----------------------------
DROP TABLE IF EXISTS "t_web_navbar";
CREATE TABLE "t_web_navbar"  (
  "uid" varchar(96) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  "name" varchar(765) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  "navbar_level" tinyint(1) NULL DEFAULT NULL,
  "summary" varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  "parent_uid" varchar(96) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  "url" varchar(765) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  "icon" varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  "is_show" tinyint(1) NULL DEFAULT NULL,
  "is_jump_external_url" tinyint(1) NULL DEFAULT NULL,
  "sort" int(11) NULL DEFAULT NULL,
  "status" tinyint(1) NULL DEFAULT NULL,
  "create_time" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  "update_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for t_web_visit
-- ----------------------------
DROP TABLE IF EXISTS "t_web_visit";
CREATE TABLE "t_web_visit"  (
  "uid" varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  "user_uid" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户uid',
  "ip" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '访问ip地址',
  "behavior" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户行为',
  "module_uid" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模块uid（文章uid，标签uid，分类uid）',
  "other_data" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附加数据(比如搜索内容)',
  "status" tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态',
  "create_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  "update_time" timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  "os" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作系统',
  "browser" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '浏览器',
  "ip_source" varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ip来源',
  PRIMARY KEY ("uid") USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Web访问记录表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;


INSERT INTO t_admin ("uid", "user_name", "pass_word", "gender", "avatar", "email", "birthday", "mobile", "valid_code", "summary", "login_count", "last_login_time", "last_login_ip", "status", "create_time", "update_time", "nick_name", "qq_number", "we_chat", "occupation", "github", "gitee", "role_uid", "person_resume", "project_uid") VALUES ('1f01cd1d2f474743b241d74008b12333', 'admin', '$2a$10$cXnLgqBerVFT0Q5uBuaG7.3MjKa6iCEZYM3PBqezCwEYo8atBChA6', '1', '7ca58be3f8de6a7337dd082e77ef10d5', NULL, NULL, NULL, NULL, NULL, 792, '2021-12-05 17:59:09', '0:0:0:0:0:0:0:1', 1, '2020-12-26 15:39:05', '2021-12-05 17:59:09', 'admin', NULL, NULL, NULL, '', '', '434994947c5a4ee3a710cd277357c7c3', '', '0');
INSERT INTO t_admin ("uid", "user_name", "pass_word", "gender", "avatar", "email", "birthday", "mobile", "valid_code", "summary", "login_count", "last_login_time", "last_login_ip", "status", "create_time", "update_time", "nick_name", "qq_number", "we_chat", "occupation", "github", "gitee", "role_uid", "person_resume", "project_uid") VALUES ('63183b3137a12b156069da1f81c761dc', 'qqc', '$2a$10$nWB73N7wtpFHX3qOKfBYm.dSA.Fm7gaA7a4BChAZjeW4AplSg6rr2', '1', 'e0a615793a9d5c3662e4081d7f1d6637', '', NULL, NULL, NULL, NULL, 18, '2021-12-06 17:14:08', '0:0:0:0:0:0:0:1', 1, '2021-11-23 19:06:41', '2021-12-06 17:14:08', '青青草管理员', NULL, NULL, NULL, NULL, NULL, 'ad92f1639508ca405728f4981770793b', NULL, '8489b33dbf74e545548af99e3daab352');
INSERT INTO t_admin ("uid", "user_name", "pass_word", "gender", "avatar", "email", "birthday", "mobile", "valid_code", "summary", "login_count", "last_login_time", "last_login_ip", "status", "create_time", "update_time", "nick_name", "qq_number", "we_chat", "occupation", "github", "gitee", "role_uid", "person_resume", "project_uid") VALUES ('7621746caa93ce605e2de7143a3787b5', 'mogu2018', '$2a$10$mPlEFU73rmV3rIZD0fMjMua5Gcj.YOiKV.F1uyCvXIB3ajG6oBLGK', '1', 'a40fd86bf4d77aa516766ef95f8e3b39', '', NULL, NULL, NULL, NULL, 34, '2021-12-02 18:43:09', '0:0:0:0:0:0:0:1', 1, '2020-07-21 22:09:42', '2021-12-02 18:43:09', 'mogu2018mogu2018', NULL, NULL, NULL, NULL, NULL, '434994947c5a4ee3a710cd277357c7c3', '', 'b7f0ed2b247a49498e138db5dd62dc85');
INSERT INTO t_admin ("uid", "user_name", "pass_word", "gender", "avatar", "email", "birthday", "mobile", "valid_code", "summary", "login_count", "last_login_time", "last_login_ip", "status", "create_time", "update_time", "nick_name", "qq_number", "we_chat", "occupation", "github", "gitee", "role_uid", "person_resume", "project_uid") VALUES ('e9a6990bf0367b1eddbe141cdf8e0dfb', 'xhy', '$2a$10$HeclLa7B8ZWdvXpCfgTwX.P/rMNWN/c/TxMEBg1tmNuH8nCQi3GyG', '1', NULL, '', NULL, NULL, NULL, NULL, 4, '2021-12-06 21:16:47', '0:0:0:0:0:0:0:1', 1, '2021-11-23 19:07:24', '2021-12-06 21:16:47', '小黄鸭管理员', NULL, NULL, NULL, NULL, NULL, 'ad92f1639508ca405728f4981770793b', NULL, 'b7f0ed2b247a49498e138db5dd62dc85');


INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('02ea2f9ef5d44f559fb66189b05f6769', 'Solr', 2, 'Solr监控中心', '147cd431cbb9007bde87444d7987b151', '/monitor/Solr', 'el-icon-lightning', 0, 0, '2018-11-30 03:55:39', '2020-12-09 20:41:22', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('062087bce19d00312b3787b6e24c21d1', '字典数据', 2, '字典数据', 'badf0010422b432ba6ec9c83a25012ed', '/system/sysDictData', 'el-icon-data-line', 0, 0, '2020-02-21 18:06:11', '2020-05-30 08:44:04', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('065cda845549289b2afcd0129d87c2c0', '新增用户', 3, '新增用户', 'fb4237a353d0418ab42c748b7c1d64c6', '/user/add', NULL, 0, 1, '2020-09-29 20:40:09', '2020-09-29 20:40:30', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('079f0cfdb7a7017d827f5c349983eebc', 'Eureka', 2, 'Eureka监控中心', '147cd431cbb9007bde87444d7987b151', '/monitor/eureka', 'el-icon-moon-night', 0, 0, '2020-01-06 05:27:30', '2020-12-05 15:21:41', 1, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('0a035547bbec404eb3ee0ef43312148d', '分类管理', 2, '管理博客分类', '49b42250abcb47ff876bad699cf34f03', '/blog/blogSort', 'el-icon-brush', 10, 1, '2018-11-26 03:07:14', '2020-05-30 08:34:17', 1, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('0cab1fcdcd01f394768e2e2674e56773', '本地上传', 3, '本地文件上传', '1f01cd1d2f474743b241d74008b12333', '/blog/uploadLocalBlog', NULL, 1, 1, '2020-04-15 17:28:36', '2020-07-09 21:40:01', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('10e5cc3ea9e530a18303ee92449e53a5', '编辑', 3, '图集分类 管理编辑', 'f4d7f3a657fa12043f65ad58e9baeaa8', '/pictureSort/edit', NULL, 0, 1, '2021-11-25 16:07:06', '2021-11-25 16:07:06', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('147cd431cbb9007bde87444d7987b151', '监控中心', 1, '监控中心', NULL, '/monitor', 'el-icon-monitor', 0, 0, '2020-01-06 13:25:32', '2020-07-09 21:33:11', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('14bcfc15d02f0d568adf2d383c0342d8', '查询', 3, '服务监控查询', 'a0c40ddd3fe15bb3f2edf9ec242a1511', '/monitor/getServerInfo', NULL, 0, 1, '2020-06-03 09:27:41', '2020-06-03 09:27:41', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('152216a4fd9b995aa20bfa1eec6312c2', 'Nacos', 2, 'Nacos图形化界面', '147cd431cbb9007bde87444d7987b151', 'http://localhost:8848/nacos', 'el-icon-star-off', 5, 0, '2020-07-03 21:39:58', '2020-12-05 15:31:11', 0, 0, 1);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('16a75a3c38e71c1046b443b4b64dd930', '删除选中', 3, '博客管理 删除选中', '1f01cd1d2f474743b241d74008b12333', '/blog/deleteBatch', NULL, 0, 1, '2020-03-21 18:21:01', '2020-03-21 18:21:01', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('1cc493d36e17fad535f2bf70242162b0', '图片管理', 2, '图片管理', '65e22f3d36d94bcea47478aba02895a1', '/picture/picture', 'el-icon-brush', 1, 0, '2020-02-21 22:27:12', '2020-10-12 10:12:30', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('1d9a5030142e9fd7690f554c20e3bc90', '推荐管理', 2, '博客推荐管理', '49b42250abcb47ff876bad699cf34f03', '/blog/blogRecommend', 'el-icon-ice-cream-round', 0, 0, '2020-01-28 10:06:32', '2020-05-30 08:34:54', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('1dd262b88b63e8f6bd9a6ca72ed0622c', '导航栏管理 删除', 3, '导航栏管理 删除', '6275bc5189e2e595b621d744d68278af', '/webNavbar/delete', NULL, 0, 1, '2021-02-23 13:02:12', '2021-02-23 13:02:12', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('1f01cd1d2f474743b241d74008b12333', '博客管理', 2, '对博客进行增删改查', '49b42250abcb47ff876bad699cf34f03', '/blog/blog', 'el-icon-notebook-2', 11, 1, '2018-11-27 03:47:07', '2020-05-30 08:33:22', 1, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('21a411858fc22b5feb4c44fcad00e529', '导航栏管理 编辑', 3, '导航栏管理 编辑', '6275bc5189e2e595b621d744d68278af', '/webNavbar/edit', NULL, 0, 1, '2021-02-23 13:01:36', '2021-02-23 13:01:36', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('237d410f90d0c70b894f87a87718d88c', '删除全选', 3, '专题元素管理 删除全选', '7cb1a6b7462832bf831a18a28eea94cd', '/subjectItem/deleteBatch', NULL, 0, 1, '2020-08-23 09:17:22', '2020-08-23 09:17:22', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('238352fc7e86340c339b9a575b1b7086', 'Sentinel', 2, 'Sentinel监控中心', '147cd431cbb9007bde87444d7987b151', 'http://localhost:8070/sentinel/', 'el-icon-warning-outline', 4, 0, '2020-07-06 21:25:52', '2020-12-05 15:31:27', 0, 0, 1);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('2496af227b283bc79d9261b73e3ea743', '删除选中', 3, '参数配置 删除选中', '3eacc357e23b0b17e4f835c2f998ed34', '/sysParams/deleteBatch', NULL, 0, 1, '2020-07-21 16:49:18', '2020-07-21 16:49:18', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('250619ada446f8ea2e8bd0a1a2451e6a', '标签管理', 2, '标签管理', '510483ce569b4fc88299f346147b1314', '/resource/pictureTag', 'el-icon-folder-opened', 100, 1, '2021-11-22 18:42:18', '2021-11-25 16:25:54', 1, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('25b3ea9639872aa730ad6147baf61641', '删除', 3, '参数配置 删除', '3eacc357e23b0b17e4f835c2f998ed34', '/sysParams/deleteBatch', NULL, 0, 1, '2020-07-21 16:48:57', '2020-07-21 16:48:57', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('26bcccf0164bf84f12ab20448388d346', '待办事项', 2, '首页  代办事项', 'badf0010422b432ba6ec9c83a25012ed', '/dashboard/todo', 'el-icon-date', 0, 0, '2020-03-23 07:51:52', '2020-05-30 09:06:59', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('2a733ff390af9b44ecda4e8c4634d73k', '删除', 3, '菜单管理 删除', 'aa225cdae6464bc0acebd732192f8362', '/categoryMenu/delete', NULL, 0, 1, '2020-03-21 18:23:19', '2020-03-21 18:23:19', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('2a733ff390af9b44ecda4e8c4634d75d', '删除选中', 3, '分类管理 删除选中', '0a035547bbec404eb3ee0ef43312148d', '/blogSort/deleteBatch', NULL, 0, 1, '2020-03-21 18:23:19', '2020-03-21 18:23:19', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('2a733ff390af9b44ecda4e8c4634d75f', '删除选中', 3, '标签管理 删除选中', '6606b7e646d545e5a25c70b5e5fade9f', '/tag/deleteBatch', NULL, 0, 1, '2020-03-21 18:23:19', '2020-03-21 18:23:19', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('2a733ff390af9b44ecda4e8c4634d75k', '删除选中', 3, '评论管理 删除选中', '9beb7caa2c844b36a02789262dc76fbe', '/comment/deleteBatch', NULL, 0, 1, '2020-03-21 18:23:19', '2020-04-21 08:33:27', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('2a733ff390af9b44ecda4e8c4634d75t', '重置密码', 3, '用户管理 重置密码', 'fb4237a353d0418ab42c748b7c1d64c6', '/user/resetUserPassword', NULL, 0, 1, '2020-03-21 18:23:19', '2020-04-21 08:28:37', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('2a733ff390af9b44ecda4e8c4634d75u', '初始化ElasticSearch索引', 3, 'ElasticSearch 初始化ElasticSearch索引', 'bfc9463e59a3ca250dcfc1c86627e034', '/search/initElasticIndex', NULL, 0, 1, '2020-03-21 18:23:19', '2020-03-21 18:23:19', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('2a733ff390af9b44ecda4e8c4634d75v', '初始化Solr索引', 3, 'Solr 初始化Solr索引', '02ea2f9ef5d44f559fb66189b05f6769', '/search/initSolrIndex', NULL, 0, 1, '2020-03-21 18:23:19', '2020-03-21 18:23:19', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('2a733ff390af9b44ecda4e8c4634d78k', '删除', 3, '友情链接 删除', '9002d1ae905c4cb79c2a485333dad2f7', '/link/delete', NULL, 0, 1, '2020-03-21 18:23:19', '2020-03-21 18:23:19', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('2a733ff390af9b44ecda4e8c4634d7ck', '删除选中', 3, '字典数据 删除选中', '062087bce19d00312b3787b6e24c21d1', '/sysDictData/deleteBatch', NULL, 0, 1, '2020-03-21 18:23:19', '2020-03-21 18:23:19', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('2a733ff390af9b44ecda4e8c4634d7gk', '删除选中', 3, '图片管理 删除选中', '1cc493d36e17fad535f2bf70242162b0', '/picture/delete', NULL, 0, 0, '2020-03-21 18:23:19', '2020-03-21 18:23:19', 0, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('2a733ff390af9b44ecda4e8c4634d7lk', '删除', 3, '管理员管理 删除', '2de247af3b0a459095e937d7ab9f5864', '/admin/delete', NULL, 0, 1, '2020-03-21 18:23:19', '2020-03-21 18:23:19', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('2a733ff390af9b44ecda4e8c4634d7pk', '修改标签', 3, '图集管理 修改标签', '4dea9c4f39d2480983e8c4333d35e036', '/pictureAtlas/updateTag', NULL, 0, 1, '2020-03-21 18:23:19', '2021-12-02 15:05:39', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('2a733ff390af9b44ecda4e8c4634d7uk', '删除', 3, '角色管理 删除', '5010ae46511e4c0b9f30d1c63ad3f0c1', '/role/delete', NULL, 0, 1, '2020-03-21 18:23:19', '2020-03-21 18:23:19', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('2a733ff390af9b44ecda4e8c4634db8k', '删除', 3, '资源分类 删除', '9449ce5dd5e24b21a9d15f806cb36e87', '/resourceSort/deleteBatch', NULL, 0, 1, '2020-03-21 18:23:19', '2020-03-21 18:23:19', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('2a733ff390af9b44ecda4e8c4634dh8c', '删除', 3, '视频管理 删除', 'ffc6e9ca2cc243febf6d2f476b849163', '/studyVideo/deleteBatch', NULL, 0, 1, '2020-03-21 18:23:19', '2020-03-21 18:23:19', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('2a733ff390af9b44ecda4e8c4634dh8k', '删除', 3, '字典管理 删除', 'c28f0b052e0b930299dd53de59cc32d7', '/sysDictType/delete', NULL, 0, 1, '2020-03-21 18:23:19', '2020-03-21 18:23:19', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('2a733ff390af9b44ecda4e8c4634dm8k', '修改密码', 3, '关于我 修改密码', 'faccfe476b89483791c05019ad5b4906', '/system/changePwd', NULL, 0, 1, '2020-03-21 18:23:19', '2020-03-21 18:23:19', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('2b983c5439ac07f2cf07437ba9fff6be', '编辑', 3, '博客管理 编辑', '1f01cd1d2f474743b241d74008b12333', '/blog/edit', NULL, 0, 1, '2020-03-21 18:15:26', '2020-03-21 18:15:26', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('2d5cd8a387fc2d010c245acd65fefd3b', '强退用户', 3, '强退后台管理员', 'bfcb9b002c3de18f269189c573b985f8', '/admin/forceLogout', NULL, 0, 1, '2020-06-09 18:48:28', '2020-06-09 18:48:28', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('2de247af3b0a459095e937d7ab9f5864', '管理员管理', 2, '管理员增删改查', 'd3a19221259d439b916f475e43edb13d', '/authority/admin', 'el-icon-trophy', 0, 1, '2018-11-25 19:09:21', '2020-05-30 08:45:43', 1, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('2efd760fc2d8b2dd243ca4802c37bdb9', '查询', 3, '项目管理 查询', '7ed32cbc289d7db31ed13738970a6b8b', '/project/getList', NULL, 0, 1, '2021-11-25 14:11:42', '2021-11-25 14:12:31', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('2fb47d3b6dbd44279c8206740a263543', '网站配置', 2, '网站配置', 'badf0010422b432ba6ec9c83a25012ed', '/system/webConfig', 'el-icon-setting', 1, 0, '2018-11-28 19:59:04', '2020-08-30 08:01:11', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('327d945daf4ddb71976c4ab3830e7c4i', '新增', 3, '菜单管理 新增', 'aa225cdae6464bc0acebd732192f8362', '/categoryMenu/add', NULL, 0, 1, '2020-03-21 18:22:06', '2020-03-21 18:22:06', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('327d945daf4ddb71976c4ab3830e7c66', '新增', 3, '分类管理 新增', '0a035547bbec404eb3ee0ef43312148d', '/blogSort/add', NULL, 0, 1, '2020-03-21 18:22:06', '2020-03-21 18:22:06', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('327d945daf4ddb71976c4ab3830e7c6h', '新增', 3, '标签管理 新增', '6606b7e646d545e5a25c70b5e5fade9f', '/tag/add', NULL, 0, 1, '2020-03-21 18:22:06', '2020-03-21 18:22:06', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('327d945daf4ddb71976c4ab3830e7c6i', '新增', 3, '评论管理 新增', '9beb7caa2c844b36a02789262dc76fbe', '/comment/add', NULL, 0, 1, '2020-03-21 18:22:06', '2020-04-21 08:33:12', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('327d945daf4ddb71976c4ab3830e7c7i', '新增', 3, '友情链接 新增', '9002d1ae905c4cb79c2a485333dad2f7', '/link/add', NULL, 0, 1, '2020-03-21 18:22:06', '2020-03-21 18:22:06', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('327d945daf4ddb71976c4ab3830e7cdi', '新增', 3, '字典数据 新增', '062087bce19d00312b3787b6e24c21d1', '/sysDictData/add', NULL, 0, 1, '2020-03-21 18:22:06', '2020-03-21 18:22:06', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('327d945daf4ddb71976c4ab3830e7cfi', '新增', 3, '图片管理 新增', '1cc493d36e17fad535f2bf70242162b0', '/picture/add', NULL, 0, 1, '2020-03-21 18:22:06', '2020-03-21 18:22:06', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('327d945daf4ddb71976c4ab3830e7cmi', '新增', 3, '管理员管理 新增', '2de247af3b0a459095e937d7ab9f5864', '/admin/add', NULL, 0, 1, '2020-03-21 18:22:06', '2020-03-21 18:22:06', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('327d945daf4ddb71976c4ab3830e7cqi', '新增', 3, '图集管理 新增', '4dea9c4f39d2480983e8c4333d35e036', '/pictureAtlas/add', NULL, 0, 1, '2020-03-21 18:22:06', '2021-11-24 14:47:05', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('327d945daf4ddb71976c4ab3830e7cvi', '新增', 3, '角色管理 新增', '5010ae46511e4c0b9f30d1c63ad3f0c1', '/role/add', NULL, 0, 1, '2020-03-21 18:22:06', '2020-03-21 18:22:06', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('327d945daf4ddb71976c4ab3830e7d7i', '新增', 3, '资源分类 新增', '9449ce5dd5e24b21a9d15f806cb36e87', '/resourceSort/add', NULL, 0, 1, '2020-03-21 18:22:06', '2020-03-21 18:22:06', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('327d945daf4ddb71976c4ab3830e7g7d', '新增', 3, '视频管理 新增', 'ffc6e9ca2cc243febf6d2f476b849163', '/studyVideo/add', NULL, 0, 1, '2020-03-21 18:22:06', '2020-03-21 18:22:06', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('327d945daf4ddb71976c4ab3830e7g7i', '新增', 3, '字典管理 新增', 'c28f0b052e0b930299dd53de59cc32d7', '/sysDictType/add', NULL, 0, 1, '2020-03-21 18:22:06', '2020-03-21 18:22:06', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('34c1b7b1bd3118979fe1f9f9a1147fa5', '表单构建', 2, '表单构建', 'f4697cdf85920369179b90ff45a5982d', '/test/FormBuild', 'el-icon-milk-tea', 0, 0, '2020-05-26 22:33:16', '2020-05-30 08:46:31', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('3545a821ab5c6cc274664e0bd9a4b5d4', '使用权限设置', 2, '使用权限设置', '65e22f3d36d94bcea47478aba02895a1', '/picture/usePower', 'el-icon-setting', 0, 1, '2021-12-04 18:22:54', '2021-12-04 18:23:12', 1, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('3cc70407e2089c85489195498647c98f', '编辑', 3, '图片标签管理 编辑', '250619ada446f8ea2e8bd0a1a2451e6a', '/pictureTag/edit', NULL, 0, 1, '2021-11-22 18:50:55', '2021-11-22 18:55:18', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('3e92f2fd6cf012d30bfae2d9cdb7092d', '专题管理', 2, '博客专题管理', '49b42250abcb47ff876bad699cf34f03', '/blog/subject', 'el-icon-collection', 0, 1, '2020-08-23 08:51:12', '2020-08-23 09:18:13', 1, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('3eacc357e23b0b17e4f835c2f998ed34', '参数配置', 2, '配置项目中使用的参数', 'badf0010422b432ba6ec9c83a25012ed', '/system/sysParams', 'el-icon-document', 0, 0, '2020-07-21 16:45:15', '2020-07-21 16:45:15', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('4062db4658392e71c177758bb51bb4cb', '新建文件夹', 3, '网盘管理 新建文件夹', 'e1e54aea65cc22d9f8a4c74ce8d23749', '/networkDisk/create', NULL, 0, 1, '2020-06-15 10:36:51', '2020-06-15 10:47:02', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('407a263eb12eff5aac31e9f62901cea0', 'Markdown', 2, 'Markdown编辑器', 'f4697cdf85920369179b90ff45a5982d', '/test/Markdown', 'el-icon-watermelon', 0, 0, '2020-01-30 10:36:43', '2020-05-30 08:46:22', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('4337f63d13d84b9aba64b9d7a69fd066', '异常日志', 2, '异常日志', '98b82be8785e41dc939b6a5517fdfa53', '/log/exceptionLog', 'el-icon-ice-cream', 0, 0, '2018-11-28 20:01:36', '2020-05-30 08:47:53', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('49b42250abcb47ff876bad699cf34f03', '博客管理', 1, '用于博客的一些相关操作', NULL, '/blog', 'el-icon-edit', 20, 0, '2018-11-25 05:15:07', '2020-10-07 15:35:48', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('4dea9c4f39d2480983e8c4333d35e036', '图集管理', 2, '图集管理', '510483ce569b4fc88299f346147b1314', '/resource/pictureAtlas', 'el-icon-printer', 2, 1, '2018-11-28 19:50:31', '2021-12-02 16:31:39', 1, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('4dea9c4f39d2480983e8c4333d35e156', '标签管理', 2, '标签管理', '65e22f3d36d94bcea47478aba02895a1', '/picture/pictureTag', 'el-icon-printer', 2, 0, '2018-11-28 19:50:31', '2021-11-22 18:42:50', 1, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('4fe7725159ced4a238b816a4595109d1', '门户管理', 1, '管理门户页面', NULL, '/web', 'el-icon-help', 0, 0, '2021-02-22 18:25:34', '2021-02-22 18:25:34', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('5010ae46511e4c0b9f30d1c63ad3f0c1', '角色管理', 2, '管理用户角色信息', 'd3a19221259d439b916f475e43edb13d', '/authority/role', 'el-icon-user', 0, 1, '2018-11-25 19:10:34', '2020-05-30 09:06:22', 1, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('505b4769b77617a314a3ed78e4acdff7', 'Zipkin', 2, 'Zipkin链路追踪', '147cd431cbb9007bde87444d7987b151', 'http://localhost:9411/zipkin/', 'el-icon-moon', 2, 0, '2020-02-06 20:22:18', '2020-12-05 15:31:42', 0, 0, 1);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('510483ce569b4fc88299f346147b1314', '资源管理', 1, '资源管理', '', '/resource', 'el-icon-present', 2, 1, '2018-11-28 19:42:13', '2020-05-30 08:51:30', 1, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('5636803d8fb7911ee00ad96271600ec8', '选中删除', 3, '图片标签管理 选中删除', '250619ada446f8ea2e8bd0a1a2451e6a', '/pictureTag/deleteBatch', NULL, 0, 1, '2021-11-22 18:51:41', '2021-11-22 18:53:38', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('587e2697fa4d85046feece8ab9d0706c', 'Redis操作', 3, 'Redis操作 清空缓存', '78f24799307cb63bc3759413dadf4d1a', '/systemConfig/cleanRedisByKey', NULL, 0, 1, '2020-04-03 19:38:01', '2020-04-03 19:38:00', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('5bf9bd28d387ef923f2c5d11ec01fbbd', '按创建时间排序', 3, '按创建时间排序', '7cb1a6b7462832bf831a18a28eea94cd', '/subjectItem/sortByCreateTime', NULL, 0, 1, '2020-12-08 20:38:10', '2020-12-08 20:38:10', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('5ef38a4f70c6d859155528776a30cda7', '上传文件', 3, '网盘管理 上传文件', 'e1e54aea65cc22d9f8a4c74ce8d23749', '/networkDisk/add', NULL, 0, 1, '2020-06-15 10:35:45', '2020-06-15 10:46:53', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('6228ff4e9ebd42c89599b322201a0345', '反馈管理', 2, '反馈管理', 'bcf4a9bc21c14b559bcb015fb7912266', '/message/feedback', 'el-icon-microphone', 0, 0, '2018-11-28 19:48:30', '2020-05-30 08:48:39', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('6275bc5189e2e595b621d744d68278af', '导航栏管理', 2, '导航栏管理', '4fe7725159ced4a238b816a4595109d1', '/web/webNavbar', 'el-icon-c-scale-to-original', 0, 0, '2021-02-22 18:26:13', '2021-02-22 18:26:13', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('62c91c47c4646661c045727f0a8eb9d4', '查询', 3, '代办事项  查询', '26bcccf0164bf84f12ab20448388d346', '/todo/getList', NULL, 0, 1, '2020-03-23 07:52:42', '2020-03-23 07:52:42', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('65e22f3d36d94bcea47478aba02895a1', '展现管理', 1, '展现管理', '', '/picture', 'el-icon-picture-outline', 3, 1, '2018-11-28 19:48:53', '2021-12-02 16:28:20', 1, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('6606b7e646d545e5a25c70b5e5fade9f', '标签管理', 2, '对博客标签进行管理', '49b42250abcb47ff876bad699cf34f03', '/blog/blogTag', 'el-icon-folder-opened', 4, 0, '2018-11-26 02:57:38', '2020-05-30 08:34:43', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('672b093002b618293befd8e1a164e4cc', '图片列表', 2, '图片列表', 'f4697cdf85920369179b90ff45a5982d', '/test/PictureList', 'el-icon-sugar', 0, 0, '2020-04-19 08:31:23', '2020-05-30 08:46:14', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('688beda820a236b50f18ccdd5dc9fee9', '下载', 3, '网盘管理 下载', 'e1e54aea65cc22d9f8a4c74ce8d23749', '/networkDisk/download', NULL, 0, 1, '2020-06-15 10:37:17', '2020-06-15 10:46:45', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('6c8a8c50c77429fab210bd52ed8c50ba', '引用量排序', 3, '标签管理 引用量排序', '6606b7e646d545e5a25c70b5e5fade9f', '/tag/tagSortByCite', NULL, 0, 1, '2020-03-21 21:38:19', '2020-03-21 21:38:19', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('6c8a8c50c77429fab210bd52ed8c50bb', '引用量排序', 3, '分类管理 引用量排序', '0a035547bbec404eb3ee0ef43312148d', '/blogSort/blogSortByCite', NULL, 0, 1, '2020-03-21 21:38:19', '2020-03-21 21:38:19', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('7022c764d5676afa66f18ad61d525e14', '角色所属项目', 3, '分类里面角色所属项目', 'f4d7f3a657fa12043f65ad58e9baeaa8', '/pictureSort/initPorject', NULL, 0, 1, '2021-12-02 19:33:18', '2021-12-02 19:33:18', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('72d26cf940bf9dfb6bde0a590ff40882', '删除', 3, '分类管理，删除分类', '0a035547bbec404eb3ee0ef43312148d', '/blogSort/delete', NULL, 0, 1, '2020-03-21 18:22:51', '2020-03-21 18:22:51', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('72d26cf940bf9dfb6bde0a590ff4088g', '删除', 3, '标签管理，删除分类', '6606b7e646d545e5a25c70b5e5fade9f', '/tag/delete', NULL, 0, 1, '2020-03-21 18:22:51', '2020-03-21 18:22:51', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('72d26cf940bf9dfb6bde0a590ff4088j', '删除', 3, '评论管理，删除', '9beb7caa2c844b36a02789262dc76fbe', '/comment/delete', NULL, 0, 1, '2020-03-21 18:22:51', '2020-04-21 08:33:21', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('72d26cf940bf9dfb6bde0a590ff4088s', '删除', 3, '用户管理，删除', 'fb4237a353d0418ab42c748b7c1d64c6', '/user/delete', NULL, 0, 1, '2020-03-21 18:22:51', '2020-04-21 08:28:30', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('72d26cf940bf9dfb6bde0a590ff408ej', '设为封面', 3, '图片管理，设为封面', '1cc493d36e17fad535f2bf70242162b0', '/picture/setCover', NULL, 0, 0, '2020-03-21 18:22:51', '2020-03-21 18:22:51', 0, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('72d26cf940bf9dfb6bde0a590ff408nj', '重置密码', 3, '管理员管理，重置密码', '2de247af3b0a459095e937d7ab9f5864', '/admin/restPwd', NULL, 0, 1, '2020-03-21 18:22:51', '2020-03-21 18:22:51', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('749b7d0df2f74fda4c2f059ad797cb54', '下架', 3, '当前分类里面的图集下架', 'f4d7f3a657fa12043f65ad58e9baeaa8', '/pictureAtlas/out', NULL, 0, 1, '2021-12-06 18:01:43', '2021-12-06 18:01:43', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('7668dabe69473f59d1516d84cb99d583', '爬虫管理', 1, '爬虫管理', NULL, '/spider', 'el-icon-search', 0, 0, '2021-01-08 22:07:12', '2021-01-08 22:07:12', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('78ab104b123f4950af14d65798afb756', '收藏管理', 2, '管理用户收藏', '49b42250abcb47ff876bad699cf34f03', '/blog/collect', 'el-icon-folder-add', 8, 0, '2018-11-25 19:07:48', '2020-05-30 08:34:29', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('78f24799307cb63bc3759413dadf4d1a', '系统配置', 2, '设置七牛云和邮箱等相关配置', 'badf0010422b432ba6ec9c83a25012ed', '/system/systemConfig', 'el-icon-s-tools', 2, 1, '2020-01-21 09:29:04', '2020-08-30 08:01:17', 1, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('7ada0a7053be4ad4b31f6be64315d03a', '删除', 3, '博客管理 删除', '1f01cd1d2f474743b241d74008b12333', '/blog/delete', NULL, 0, 1, '2020-03-21 15:16:54', '2020-03-21 15:16:54', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('7b3fd0e29feb05cfc6f79ea654a2d5a0', '上架', 3, '当前分类里面的图集上架', 'f4d7f3a657fa12043f65ad58e9baeaa8', '/pictureAtlas/up', NULL, 0, 1, '2021-12-06 18:01:58', '2021-12-06 18:02:18', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('7be9dee2f91165d8562aaa80c7e5b936', '编辑', 3, '代办事项', '26bcccf0164bf84f12ab20448388d346', '/todo/edit', NULL, 0, 1, '2020-03-23 07:53:38', '2020-03-23 07:53:38', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('7cb1a6b7462832bf831a18a28eea94cd', '专题元素管理', 2, '专题元素管理', '49b42250abcb47ff876bad699cf34f03', '/blog/subjectItem', 'el-icon-star-off', 0, 0, '2020-08-23 09:15:06', '2020-08-23 09:15:06', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('7ed32cbc289d7db31ed13738970a6b8b', '项目管理', 2, '项目管理', 'd3a19221259d439b916f475e43edb13d', '/authority/project', 'el-icon-picture', 0, 1, '2021-11-23 19:12:18', '2021-11-25 14:07:03', 1, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('80ee135af885f02d52ecb67d5a05b173', '点击量排序', 3, '分类管理 点击量排序', '0a035547bbec404eb3ee0ef43312148d', '/blogSort/blogSortByClickCount', NULL, 0, 1, '2020-03-21 21:37:42', '2020-03-21 21:37:42', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('80ee135af885f02d52ecb67d5a05b17b', '点击量排序', 3, '标签管理 点击量排序', '6606b7e646d545e5a25c70b5e5fade9f', '/tag/tagSortByClickCount', NULL, 0, 1, '2020-03-21 21:37:42', '2020-03-21 21:37:42', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('82962bbcdb674a3fdd57aec131114d56', '编辑', 3, '参数配置 编辑', '3eacc357e23b0b17e4f835c2f998ed34', '/sysParams/edit', NULL, 0, 1, '2020-07-21 16:49:41', '2020-07-21 16:49:41', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('86508603e6d78036db0bf9222946f68c', '新增', 3, '专题元素管理 新增', '7cb1a6b7462832bf831a18a28eea94cd', '/subjectItem/add', NULL, 0, 1, '2020-08-23 09:16:00', '2020-08-23 09:16:00', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('879cfcd4dfd3e5bc1bb6ea2c0f1f82c0', '接口管理', 2, '接口管理', 'd3a19221259d439b916f475e43edb13d', '/authority/button', 'el-icon-connection', 0, 1, '2020-03-21 17:39:12', '2021-01-08 22:04:14', 1, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('8b40ea466efc42f497c49c8a610c9c20', '添加参数', 3, '参数配置 新增', '3eacc357e23b0b17e4f835c2f998ed34', '/sysParams/add', NULL, 0, 1, '2020-07-21 16:47:08', '2020-07-21 16:47:08', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('9002d1ae905c4cb79c2a485333dad2f7', '友情链接', 2, '友情链接', 'badf0010422b432ba6ec9c83a25012ed', '/system/blogLink', 'el-icon-ship', 0, 0, '2018-11-29 03:56:35', '2020-05-30 08:36:01', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('917b88e61c0e250b82ec3021b51e1d20', '使用权限设置', 3, '图集分类管理 使用权限设置', '3545a821ab5c6cc274664e0bd9a4b5d4', '/usePower/edit', NULL, 0, 1, '2021-11-25 16:08:19', '2021-12-06 18:08:39', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('93f7fd9a6e81735c47649e6b36042b5d', 'Druid', 2, 'Druid监控中心', '147cd431cbb9007bde87444d7987b151', 'http://localhost:8607/mogu-admin/druid/login.html', 'el-icon-cloudy-and-sunny', 1, 0, '2020-01-06 13:26:51', '2020-12-05 15:31:47', 0, 0, 1);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('9449ce5dd5e24b21a9d15f806cb36e87', '资源分类', 2, '资源分类', '510483ce569b4fc88299f346147b1314', '/resource/resourceSort', 'el-icon-folder-opened', 0, 0, '2018-11-29 03:43:27', '2020-05-30 08:51:58', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('94bee9c48a4611c96b89112b845c7f28', '移动', 3, '网盘管理  移动文件', 'e1e54aea65cc22d9f8a4c74ce8d23749', '/networkDisk/move', NULL, 0, 1, '2020-06-15 10:36:23', '2020-06-15 10:46:39', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('98b82be8785e41dc939b6a5517fdfa53', '操作日志', 1, '操作日志', '', '/log', 'el-icon-paperclip', 9, 0, '2018-11-28 20:00:19', '2020-05-30 08:47:14', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('99f5d4233d446a809b9937961c590766', '获取在线管理员', 3, '获取在线管理员列表', 'bfcb9b002c3de18f269189c573b985f8', '/admin/getOnlineAdminList', NULL, 0, 1, '2020-06-09 18:49:32', '2020-06-09 18:49:32', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('9bbe311a4ccb087560e6e2c6d40cf271', '图片爬取', 2, '用于标题图片的爬取', '7668dabe69473f59d1516d84cb99d583', '/spider/pictureSpider', 'el-icon-picture-outline', 0, 0, '2021-01-08 22:08:16', '2021-01-08 22:08:16', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('9beb7caa2c844b36a02789262dc76fbe', '评论管理', 2, '评论管理', 'bcf4a9bc21c14b559bcb015fb7912266', '/message/comment', 'el-icon-chat-line-square', 1, 0, '2018-11-28 19:47:23', '2020-05-30 08:48:28', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('9c91231f1682aabd765225a7e503bb43', '删除', 3, '反馈管理 删除', '6228ff4e9ebd42c89599b322201a0345', '/feedback/deleteBatch', NULL, 0, 1, '2020-03-22 11:17:27', '2020-03-22 11:17:27', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('9db7ffbded9717f13a1a97fca46bc17c', '导出选中', 3, '导出选中，前端实现', '1f01cd1d2f474743b241d74008b12333', '/blog/downloadBatch', NULL, 0, 1, '2020-04-27 22:14:35', '2020-04-27 22:14:35', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('9e73a9b1de18e4f1cb19eca5e8cd321d', '批量编辑', 3, '代办事项 批量编辑', '26bcccf0164bf84f12ab20448388d346', '/todo/toggleAll', NULL, 0, 1, '2020-03-23 07:54:37', '2020-03-23 07:54:37', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('9e91b4f993c946cba4bf720b2c1b2e90', '用户日志', 2, '用户Web端访问情况', '98b82be8785e41dc939b6a5517fdfa53', '/log/webVisit', 'el-icon-hot-water', 0, 0, '2019-05-17 10:16:47', '2019-05-17 10:16:47', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('a0c40ddd3fe15bb3f2edf9ec242a1511', '服务器监控', 2, '服务器监控', '147cd431cbb9007bde87444d7987b151', '/monitor/serverMonitor', 'el-icon-light-rain', 7, 0, '2020-06-03 09:26:13', '2020-12-05 15:27:45', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('a5902692a3ed4fd794895bf634f97b8e', '操作日志', 2, '操作日志', '98b82be8785e41dc939b6a5517fdfa53', '/log/log', 'el-icon-lollipop', 0, 0, '2018-11-28 20:01:02', '2020-05-30 08:47:27', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('a8bad1abec6c8fc8d4bce5a27829c878', '编辑', 3, '专题管理 编辑', '3e92f2fd6cf012d30bfae2d9cdb7092d', '/subject/edit', NULL, 0, 1, '2020-08-23 08:55:55', '2020-08-23 08:55:55', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('a9396f1a3fbdec3d4cb614f388a22bea', 'Monitor', 2, 'SpringBootAdmin监控中心', '147cd431cbb9007bde87444d7987b151', 'http://localhost:8607/mogu-monitor/wallboard', 'el-icon-partly-cloudy', 5, 0, '2020-01-05 21:30:16', '2020-12-05 15:30:27', 0, 0, 1);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('aa225cdae6464bc0acebd732192f8362', '菜单管理', 2, '对页面菜单进行管理', 'd3a19221259d439b916f475e43edb13d', '/authority/categoryMenu', 'el-icon-tickets', 0, 1, '2018-11-25 11:12:01', '2020-05-30 08:44:32', 1, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('aa6c5d513421aa50cac1ee9ec647d100', '导航栏管理 新增', 3, '导航栏管理 新增', '6275bc5189e2e595b621d744d68278af', '/webNavbar/add', NULL, 0, 1, '2021-02-23 13:01:16', '2021-02-23 13:01:16', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('ab1289c29b336dccda87a9fa8b711aa2', '删除', 3, '代办事项 删除', '26bcccf0164bf84f12ab20448388d346', '/todo/delete', NULL, 0, 1, '2020-03-23 07:54:01', '2020-03-23 07:54:01', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('acbb5d09da25e6c9e019cc361b35d159', 'Search接口', 2, 'Search接口', 'baace3dc03d34c54b81761dce8243814', 'http://localhost:8606/swagger-ui/index.html', 'el-icon-sunrise', 0, 0, '2020-01-19 19:56:23', '2021-12-04 16:41:12', 0, 0, 1);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('aef85c40b54320d7c5a9d78697e12910', '置顶', 3, '分类管理 置顶', '0a035547bbec404eb3ee0ef43312148d', '/blogSort/stick', NULL, 0, 1, '2020-03-21 21:37:07', '2020-03-21 21:37:07', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('aef85c40b54320d7c5a9d78697e1291c', '置顶', 3, '标签管理 置顶', '6606b7e646d545e5a25c70b5e5fade9f', '/tag/stick', NULL, 0, 1, '2020-03-21 21:37:07', '2020-03-21 21:37:07', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('aef85c40b54320d7c5a9d78697e1295n', '置顶', 3, '菜单管理 置顶', 'aa225cdae6464bc0acebd732192f8362', '/categoryMenu/stick', NULL, 0, 1, '2020-03-21 21:37:07', '2020-03-21 21:37:07', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('aef85c40b54320d7c5a9d78697e1296n', '置顶', 3, '友情链接 置顶', '9002d1ae905c4cb79c2a485333dad2f7', '/link/stick', NULL, 0, 1, '2020-03-21 21:37:07', '2020-03-21 21:37:07', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('aef85c40b54320d7c5a9d78697e129rn', '图片来源', 3, '图集管理 图片来源', '4dea9c4f39d2480983e8c4333d35e036', '/pictureAtlas/getResourceList', NULL, 0, 1, '2020-03-21 21:37:07', '2021-12-01 16:42:12', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('aef85c40b54320d7c5a9d78697e12e6n', '置顶', 3, '资源分类 置顶', '9449ce5dd5e24b21a9d15f806cb36e87', '/resourceSort/stick', NULL, 0, 1, '2020-03-21 21:37:07', '2020-03-21 21:37:07', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('af0e753d3ea0adf5cd8cf1dd55f162c2', '接口聚合', 2, '聚合所有模块的接口', 'baace3dc03d34c54b81761dce8243814', 'http://localhost:8607/doc.html', 'el-icon-ice-cream-round', 5, 0, '2020-12-05 15:42:51', '2021-12-04 16:40:47', 0, 0, 1);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('b21105b915a5b54588c2cd458a94d2d5', '新增', 3, '专题管理 新增', '3e92f2fd6cf012d30bfae2d9cdb7092d', '/subject/add', NULL, 0, 1, '2020-08-23 08:55:29', '2020-08-23 08:55:29', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('b511cae571834971a392ae4779270034', '游客管理', 2, '游客管理', 'c519725da92b42f3acf0cc9fad58c664', '/user/visitor', 'el-icon-news', 2, 0, '2018-11-28 19:54:28', '2020-05-30 08:47:06', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('b51481cb5c016e62a98b396270b98371', '新增', 3, '图集分类管理 新增', 'f4d7f3a657fa12043f65ad58e9baeaa8', '/pictureSort/add', NULL, 0, 1, '2021-11-25 16:06:43', '2021-11-25 16:06:43', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('b6b54cdedaff0d8c92238f37c14c96be', '新增', 3, '项目管理 新增', '7ed32cbc289d7db31ed13738970a6b8b', '/project/add', NULL, 0, 1, '2021-11-25 14:13:27', '2021-11-25 14:14:54', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('b7fc36f7efc9738bddc9b09fedeccf60', '导航栏管理 查询全部', 3, '导航栏管理 查询全部', '6275bc5189e2e595b621d744d68278af', '/webNavbar/getAllList', NULL, 0, 1, '2021-02-23 13:00:24', '2021-02-23 13:00:24', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('baa21ccb45ee133b064187185edb2ac0', '删除', 3, '网盘管理 删除', 'e1e54aea65cc22d9f8a4c74ce8d23749', '/networkDisk/delete', NULL, 0, 1, '2020-06-15 10:36:07', '2020-06-15 10:46:33', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('baace3dc03d34c54b81761dce8243814', '接口管理', 1, '接口管理', '', '/restapi', 'el-icon-stopwatch', 4, 1, '2018-11-28 20:01:57', '2020-05-30 08:49:22', 1, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('badf0010422b432ba6ec9c83a25012ed', '系统管理', 1, '系统管理', '', '/system', 'el-icon-setting', 19, 1, '2018-11-28 19:54:47', '2020-10-07 15:35:52', 1, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('bcf4a9bc21c14b559bcb015fb7912266', '消息管理', 1, '消息管理', '', '/message', 'el-icon-message', 6, 0, '2018-11-28 19:45:29', '2020-05-30 08:48:21', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('bcfac78203944094040851cc0a9bb095', '删除', 3, '专题元素管理 删除', '7cb1a6b7462832bf831a18a28eea94cd', '/subjectItem/delete', NULL, 0, 1, '2020-08-23 09:16:50', '2020-08-23 09:16:50', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('bfc9463e59a3ca250dcfc1c86627e034', 'ElasticSearch', 2, 'ElasticSearch监控页面', '147cd431cbb9007bde87444d7987b151', '/monitor/ElasticSearch', 'el-icon-cloudy', 0, 0, '2020-01-15 22:58:00', '2020-12-09 20:41:11', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('bfcb9b002c3de18f269189c573b985f8', '在线用户', 2, '在线的用户和管理员', '147cd431cbb9007bde87444d7987b151', '/monitor/OnlineAdmin', 'el-icon-sunset', 6, 0, '2020-06-09 17:14:02', '2020-12-05 15:27:39', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('c28f0b052e0b930299dd53de59cc32d7', '字典管理', 2, '字典管理', 'badf0010422b432ba6ec9c83a25012ed', '/system/sysDictType', 'el-icon-lightning', 0, 0, '2020-02-16 18:11:10', '2020-05-30 08:35:46', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('c2a1218dd07747a57b055f184ddae217', '查询', 3, '参数配置 查询', '3eacc357e23b0b17e4f835c2f998ed34', '/sysParams/getList', NULL, 0, 1, '2020-07-21 16:48:17', '2020-07-21 16:48:17', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('c519725da92b42f3acf0cc9fad58c664', '客户管理', 1, '客户管理', '', '/user', 'el-icon-house', 15, 0, '2018-11-28 19:51:47', '2021-11-23 19:13:25', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('ca69ce2193b3ede88d8e0190bce3a7df', '查询', 3, '查到当前分类下面的图集', 'f4d7f3a657fa12043f65ad58e9baeaa8', '/pictureAtlas/getlistBySort', NULL, 0, 1, '2021-12-06 18:04:35', '2021-12-06 18:11:14', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('cbd7ba11c1b38c66b569405ed9185f35', 'RabbitMQ', 2, 'RabbitMQ监控中心', '147cd431cbb9007bde87444d7987b151', 'http://localhost:15672', 'el-icon-sunny', 3, 0, '2020-01-05 21:29:39', '2020-12-05 15:31:33', 0, 0, 1);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('ccc0dced06919403832647a871312f09', '删除选中', 3, '专题管理 删除选中', '3e92f2fd6cf012d30bfae2d9cdb7092d', '/subject/deleteBatch', NULL, 0, 1, '2020-08-23 08:57:45', '2020-08-23 08:57:45', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('ce679931bff05745ad9b1716d47f2790', '图片管理', 2, '图片管理 资源管理', '510483ce569b4fc88299f346147b1314', '/resource/picture', 'el-icon-brush', 0, 0, '2021-12-02 16:43:44', '2021-12-05 19:54:13', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('d3a19221259d439b916f475e43edb13d', '权限管理', 1, '对管理员权限分配进行管理', '', '/authority', 'el-icon-user', 18, 1, '2018-11-25 19:08:42', '2020-05-30 08:44:17', 1, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('d4d92c53d3614d00865e9219b8292a90', 'Picture接口', 2, 'Picture接口', 'baace3dc03d34c54b81761dce8243814', 'http://localhost:8602/swagger-ui/index.html', 'el-icon-heavy-rain', 0, 0, '2018-11-28 20:04:33', '2021-12-04 16:41:05', 0, 0, 1);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('d893ee4375165f37c2b14a8eeb69c1a3', '查询', 3, '图片标签管理 查询', '250619ada446f8ea2e8bd0a1a2451e6a', '/pictureTag/getList', NULL, 0, 1, '2021-11-22 18:45:16', '2021-11-22 18:49:44', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('da32aa8f92ae7fe7e7f445bf1028d2df', '查询', 3, '专题元素管理  查询', '7cb1a6b7462832bf831a18a28eea94cd', '/subjectItem/getList', NULL, 0, 1, '2020-08-23 09:15:37', '2020-08-23 09:15:37', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('e1e54aea65cc22d9f8a4c74ce8d23749', '网盘管理', 2, '管理网盘的资源', '510483ce569b4fc88299f346147b1314', '/resource/file', 'el-icon-unlock', 1, 0, '2020-06-13 16:36:11', '2020-10-10 14:38:13', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('e4a482c089d04a30b6ecbaadb81b70f8', 'Admin接口', 2, 'Admin接口', 'baace3dc03d34c54b81761dce8243814', 'http://localhost:8601/swagger-ui/index.html', 'el-icon-lightning', 0, 1, '2018-11-28 20:03:32', '2020-12-24 09:20:17', 1, 0, 1);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('e5218b0b1cf016ae21eced4e47b628ac', '新增', 3, '博客管理 新增', '1f01cd1d2f474743b241d74008b12333', '/blog/add', NULL, 3, 1, '2020-03-21 10:55:34', '2020-10-07 15:38:58', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('e74c472522bd9858ac4976cfc07a7d72', '删除', 3, '项目管理 删除', '7ed32cbc289d7db31ed13738970a6b8b', '/project/deleteBatch', NULL, 0, 1, '2021-11-25 14:14:16', '2021-11-25 15:26:48', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('e76ec0b7226985a414f035e2ecbd00b4', '查询', 3, '专题管理 查询', '3e92f2fd6cf012d30bfae2d9cdb7092d', '/subject/getList', NULL, 0, 1, '2020-08-23 08:55:08', '2020-08-23 08:55:08', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('e91945e49c5fdb207f996bc9668cd965', '增加', 3, '代办事项 增加', '26bcccf0164bf84f12ab20448388d346', '/todo/add', NULL, 0, 1, '2020-03-23 07:53:07', '2020-03-23 07:53:07', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('eacd6ffdfaa2280bdd702bd6060387da', '编辑', 3, '项目管理 编辑', '7ed32cbc289d7db31ed13738970a6b8b', '/project/edit', NULL, 0, 1, '2021-11-25 14:13:45', '2021-11-25 14:14:37', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('ed535411a72bb5cdd62aade25ca6e9c9', '编辑', 3, '专题元素管理 编辑', '7cb1a6b7462832bf831a18a28eea94cd', '/subjectItem/edit', NULL, 0, 1, '2020-08-23 09:16:23', '2020-08-23 09:16:23', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('f3a559635f9d46ee3356d072f5896fcb', '图片裁剪', 2, '用于图片裁剪', 'f4697cdf85920369179b90ff45a5982d', '/test/CropperPicture', 'el-icon-cherry', 0, 0, '2020-01-30 10:38:09', '2020-05-30 08:46:05', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('f4697cdf85920369179b90ff45a5982d', '测试页面', 1, '用于一些功能的测试', NULL, '/test', 'el-icon-cpu', 17, 0, '2020-01-30 10:36:00', '2020-05-30 08:45:54', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('f4d7f3a657fa12043f65ad58e9baeaa8', '图集分类', 2, '图集分类', '65e22f3d36d94bcea47478aba02895a1', '/picture/pictureSort', 'el-icon-folder-opened', 3, 1, '2021-11-24 13:39:27', '2021-12-04 21:45:28', 1, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('f65e258ede9fd768041bfd10e5fa9d5c', '图集标签查询', 3, '图集标签查询 分类管理', 'f4d7f3a657fa12043f65ad58e9baeaa8', '/pictureTag/getList', NULL, 0, 1, '2021-12-04 18:55:55', '2021-12-04 18:55:55', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('f874529321a37e84f099488a4eaf5a0b', '编辑推荐', 3, '编辑推荐管理', '1d9a5030142e9fd7690f554c20e3bc90', '/blog/editBatch', NULL, 0, 1, '2020-04-26 13:38:15', '2020-04-26 13:38:15', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('f87d2f9b4539abbade38583420dc8b2l', '编辑', 3, '菜单管理 编辑', 'aa225cdae6464bc0acebd732192f8362', '/categoryMenu/edit', NULL, 0, 1, '2020-03-21 21:35:57', '2020-03-21 21:35:57', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('f87d2f9b4539abbade38583420dc8b89', '编辑', 3, '分类管理 编辑', '0a035547bbec404eb3ee0ef43312148d', '/blogSort/edit', NULL, 0, 1, '2020-03-21 21:35:57', '2020-03-21 21:35:57', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('f87d2f9b4539abbade38583420dc8b8e', '编辑', 3, '标签管理 编辑', '6606b7e646d545e5a25c70b5e5fade9f', '/tag/edit', NULL, 0, 1, '2020-03-21 21:35:57', '2020-03-21 21:35:57', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('f87d2f9b4539abbade38583420dc8b8l', '编辑', 3, '评论管理 编辑', '9beb7caa2c844b36a02789262dc76fbe', '/comment/edit', NULL, 0, 1, '2020-03-21 21:35:57', '2020-03-21 21:35:57', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('f87d2f9b4539abbade38583420dc8b8r', '编辑', 3, '用户管理 编辑', 'fb4237a353d0418ab42c748b7c1d64c6', '/user/edit', NULL, 0, 1, '2020-03-21 21:35:57', '2020-03-21 21:35:57', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('f87d2f9b4539abbade38583420dc8b9l', '编辑', 3, '友情链接 编辑', '9002d1ae905c4cb79c2a485333dad2f7', '/link/edit', NULL, 0, 1, '2020-03-21 21:35:57', '2020-03-21 21:35:57', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('f87d2f9b4539abbade38583420dc8bbl', '编辑', 3, '字典数据 编辑', '062087bce19d00312b3787b6e24c21d1', '/sysDictData/edit', NULL, 0, 1, '2020-03-21 21:35:57', '2020-03-21 21:35:57', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('f87d2f9b4539abbade38583420dc8bhl', '编辑', 3, '图片管理 编辑', '1cc493d36e17fad535f2bf70242162b0', '/picture/edit', NULL, 0, 1, '2020-03-21 21:35:57', '2020-03-21 21:35:57', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('f87d2f9b4539abbade38583420dc8bkl', '编辑', 3, '管理员管理 编辑', '2de247af3b0a459095e937d7ab9f5864', '/admin/edit', NULL, 0, 1, '2020-03-21 21:35:57', '2020-03-21 21:35:57', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('f87d2f9b4539abbade38583420dc8bol', '编辑', 3, '图集管理 编辑', '4dea9c4f39d2480983e8c4333d35e036', '/pictureAtlas/edit', NULL, 0, 1, '2020-03-21 21:35:57', '2021-11-24 14:46:42', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('f87d2f9b4539abbade38583420dc8bpl', '编辑', 3, '网站配置 编辑', '2fb47d3b6dbd44279c8206740a263543', '/webConfig/editWebConfig', NULL, 0, 1, '2020-03-21 21:35:57', '2020-03-21 21:35:57', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('f87d2f9b4539abbade38583420dc8btl', '编辑', 3, '角色管理 编辑', '5010ae46511e4c0b9f30d1c63ad3f0c1', '/role/edit', NULL, 0, 1, '2020-03-21 21:35:57', '2020-03-21 21:35:57', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('f87d2f9b4539abbade38583420dc8bxl', '编辑', 3, '反馈管理 编辑', '6228ff4e9ebd42c89599b322201a0345', '/feedback/edit', NULL, 0, 1, '2020-03-21 21:35:57', '2020-03-21 21:35:57', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('f87d2f9b4539abbade38583420dc8byl', '编辑', 3, '系统配置 编辑', '78f24799307cb63bc3759413dadf4d1a', '/systemConfig/editSystemConfig', NULL, 0, 1, '2020-03-21 21:35:57', '2020-03-21 21:35:57', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('f87d2f9b4539abbade38583420dc8c9l', '编辑', 3, '资源分类 编辑', '9449ce5dd5e24b21a9d15f806cb36e87', '/resourceSort/edit', NULL, 0, 1, '2020-03-21 21:35:57', '2020-03-21 21:35:57', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('f87d2f9b4539abbade38583420dc8i9b', '编辑', 3, '视频管理 编辑', 'ffc6e9ca2cc243febf6d2f476b849163', '/studyVideo/edit', NULL, 0, 1, '2020-03-21 21:35:57', '2020-03-21 21:35:57', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('f87d2f9b4539abbade38583420dc8i9l', '编辑', 3, '字典管理 编辑', 'c28f0b052e0b930299dd53de59cc32d7', '/sysDictType/edit', NULL, 0, 1, '2020-03-21 21:35:57', '2020-03-21 21:35:57', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('f87d2f9b4539abbade38583420dc8l9l', '编辑', 3, '关于我 编辑', 'faccfe476b89483791c05019ad5b4906', '/system/editMe', NULL, 0, 1, '2020-03-21 21:35:57', '2020-03-21 21:35:57', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('f9276eb8e3274c8aa05577c86e4dc8c1', 'Web接口', 2, 'Web接口', 'baace3dc03d34c54b81761dce8243814', 'http://localhost:8603/swagger-ui/index.html', 'el-icon-light-rain', 0, 0, '2018-11-28 20:04:52', '2021-12-04 16:40:57', 0, 0, 1);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('fa138c7ebdf9ddbc9a52b2389fad45bc', '添加', 3, '图片标签管理 添加', '250619ada446f8ea2e8bd0a1a2451e6a', '/pictureTag/add', NULL, 0, 1, '2021-11-22 18:53:02', '2021-11-22 18:53:02', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('fa1e85a9c7734d27df07bc730206bd1a', '删除', 3, '编辑管理 删除', '3e92f2fd6cf012d30bfae2d9cdb7092d', '/subject/delete', NULL, 0, 1, '2020-08-23 08:56:36', '2020-08-23 08:58:33', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('faccfe476b89483791c05019ad5b4906', '关于我', 2, '关于我', 'badf0010422b432ba6ec9c83a25012ed', '/system/aboutMe', 'el-icon-sugar', 0, 0, '2018-11-29 03:55:17', '2020-05-30 08:35:21', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('fb4237a353d0418ab42c748b7c1d64c6', '客户管理', 2, '客户管理', 'c519725da92b42f3acf0cc9fad58c664', '/user/user', 'el-icon-headset', 3, 0, '2018-11-28 19:52:20', '2021-11-23 19:19:25', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('fbc30e4ae5bb33b39baca7bf6bd8ca0m', '查询', 3, '资源分类 查询', '9449ce5dd5e24b21a9d15f806cb36e87', '/resourceSort/getList', NULL, 0, 1, '2020-03-21 21:36:28', '2020-03-21 21:36:28', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('fbc30e4ae5bb33b39baca7bf6bd8ce0m', '查询', 3, '友情链接 查询', '9002d1ae905c4cb79c2a485333dad2f7', '/link/getList', NULL, 0, 1, '2020-03-21 21:36:28', '2020-03-21 21:36:28', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('fbc30e4ae5bb33b39baca7bf6bd8ce1m', '查询', 3, '菜单管理 查询', 'aa225cdae6464bc0acebd732192f8362', '/categoryMenu/getList', NULL, 0, 1, '2020-03-21 21:36:28', '2020-03-21 21:36:28', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('fbc30e4ae5bb33b39baca7bf6bd8ce99', '查询', 3, '分类管理 查询', '0a035547bbec404eb3ee0ef43312148d', '/blogSort/getList', NULL, 0, 1, '2020-03-21 21:36:28', '2020-03-21 21:36:28', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('fbc30e4ae5bb33b39baca7bf6bd8ce9d', '查询', 3, '标签管理 查询', '6606b7e646d545e5a25c70b5e5fade9f', '/tag/getList', NULL, 0, 1, '2020-03-21 21:36:28', '2020-03-21 21:36:28', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('fbc30e4ae5bb33b39baca7bf6bd8ce9m', '查询', 3, '评论管理 查询', '9beb7caa2c844b36a02789262dc76fbe', '/comment/getList', NULL, 1, 1, '2020-03-21 21:36:28', '2020-03-21 21:36:28', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('fbc30e4ae5bb33b39baca7bf6bd8ce9q', '查询', 3, '用户管理 查询', 'fb4237a353d0418ab42c748b7c1d64c6', '/user/getList', NULL, 0, 1, '2020-03-21 21:36:28', '2020-03-21 21:36:28', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('fbc30e4ae5bb33b39baca7bf6bd8ceam', '查询', 3, '字典数据 查询', '062087bce19d00312b3787b6e24c21d1', '/sysDictData/getList', NULL, 0, 1, '2020-03-21 21:36:28', '2020-03-21 21:36:28', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('fbc30e4ae5bb33b39baca7bf6bd8ceim', '查询', 3, '图片管理 查询', '1cc493d36e17fad535f2bf70242162b0', '/picture/getList', NULL, 0, 1, '2020-03-21 21:36:28', '2020-03-21 21:36:28', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('fbc30e4ae5bb33b39baca7bf6bd8cejm', '查询', 3, '管理员管理 查询', '2de247af3b0a459095e937d7ab9f5864', '/admin/getList', NULL, 0, 1, '2020-03-21 21:36:28', '2020-03-21 21:36:28', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('fbc30e4ae5bb33b39baca7bf6bd8cekm', '查询', 3, '异常日志 查询', '4337f63d13d84b9aba64b9d7a69fd066', '/log/getExceptionList', NULL, 0, 1, '2020-03-21 21:36:28', '2020-03-21 21:36:28', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('fbc30e4ae5bb33b39baca7bf6bd8celm', '查询', 3, '操作日志 查询', 'a5902692a3ed4fd794895bf634f97b8e', '/log/getLogList', NULL, 0, 1, '2020-03-21 21:36:28', '2020-03-21 21:36:28', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('fbc30e4ae5bb33b39baca7bf6bd8cemm', '查询', 3, '用户日志 查询', '9e91b4f993c946cba4bf720b2c1b2e90', '/webVisit/getList', NULL, 0, 1, '2020-03-21 21:36:28', '2020-03-21 21:36:28', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('fbc30e4ae5bb33b39baca7bf6bd8cenm', '查询', 3, '图集管理 查询', '4dea9c4f39d2480983e8c4333d35e036', '/pictureAtlas/getList', NULL, 0, 1, '2020-03-21 21:36:28', '2021-11-24 14:46:28', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('fbc30e4ae5bb33b39baca7bf6bd8ceom', '查询', 3, '网站配置 查询', '2fb47d3b6dbd44279c8206740a263543', '/webConfig/getWebConfig', NULL, 0, 1, '2020-03-21 21:36:28', '2020-03-21 21:36:28', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('fbc30e4ae5bb33b39baca7bf6bd8cesm', '查询', 3, '角色管理 查询', '5010ae46511e4c0b9f30d1c63ad3f0c1', '/role/getList', NULL, 0, 1, '2020-03-21 21:36:28', '2020-03-21 21:36:28', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('fbc30e4ae5bb33b39baca7bf6bd8cewm', '查询', 3, '反馈管理 查询', '6228ff4e9ebd42c89599b322201a0345', '/feedback/getList', NULL, 0, 1, '2020-03-21 21:36:28', '2020-03-21 21:36:28', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('fbc30e4ae5bb33b39baca7bf6bd8cezm', '查询', 3, '系统配置 查询', '78f24799307cb63bc3759413dadf4d1a', '/systemConfig/getSystemConfig', NULL, 0, 1, '2020-03-21 21:36:28', '2020-03-21 21:36:28', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('fbc30e4ae5bb33b39baca7bf6bd8cj0a', '查询', 3, '视频管理 查询', 'ffc6e9ca2cc243febf6d2f476b849163', '/studyVideo/getList', NULL, 0, 1, '2020-03-21 21:36:28', '2020-03-21 21:36:28', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('fbc30e4ae5bb33b39baca7bf6bd8cj0m', '查询', 3, '字典管理 查询', 'c28f0b052e0b930299dd53de59cc32d7', '/sysDictType/getList', NULL, 0, 1, '2020-03-21 21:36:28', '2020-03-21 21:36:28', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('fbc30e4ae5bb33b39baca7bf6bd8ck0m', '查询', 3, '关于我 查询', 'faccfe476b89483791c05019ad5b4906', '/system/getMe', NULL, 0, 1, '2020-03-21 21:36:28', '2020-03-21 21:36:28', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('fe45ea293f75dc88b96cab96c218512a', '查询', 3, '博客管理 查询', '1f01cd1d2f474743b241d74008b12333', '/blog/getList', NULL, 2, 1, '2020-03-21 18:17:36', '2020-10-07 15:36:19', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('ff4ec0fd980a3b46c59f37f45b31d383', '查询', 3, '图集分类管理 查询', 'f4d7f3a657fa12043f65ad58e9baeaa8', '/pictureSort/getList', NULL, 0, 1, '2021-11-25 16:06:25', '2021-11-25 16:06:25', 1, 1, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('ffc6e9ca2cc243febf6d2f476b849163', '视频管理', 2, '视频管理', '510483ce569b4fc88299f346147b1314', '/resource/studyVideo', 'el-icon-video-camera', 0, 0, '2018-11-28 19:43:50', '2020-05-30 08:51:41', 0, 0, 0);
INSERT INTO t_category_menu ("uid", "name", "menu_level", "summary", "parent_uid", "url", "icon", "sort", "status", "create_time", "update_time", "is_show", "menu_type", "is_jump_external_url") VALUES ('ffe445828071ce87a851ad58100f1340', '导航栏管理 分页查询', 3, '导航栏管理 分页查询', '6275bc5189e2e595b621d744d68278af', '/webNavbar/getList', NULL, 0, 1, '2021-02-23 13:00:52', '2021-02-23 13:00:52', 1, 1, 0);


INSERT INTO t_project ("uid", "name", "create_time", "update_time", "status", "source") VALUES ('8489b33dbf74e545548af99e3daab352', '青青草', '2021-11-27 15:12:16', '2021-12-06 12:04:27', 1, 1);
INSERT INTO t_project ("uid", "name", "create_time", "update_time", "status", "source") VALUES ('b7f0ed2b247a49498e138db5dd62dc85', '小黄鸭', '2021-11-27 15:12:54', '2021-12-06 12:04:34', 1, 2);

INSERT INTO t_role (`uid`, `role_name`, `create_time`, `update_time`, `status`, `summary`, `category_menu_uids`) VALUES ('2ada95f1e75a39426a44f6dd089ab672', ' 操作专员', '2021-11-23 19:16:46', '2021-11-27 18:10:20', 1, '操作专员', '[]');
INSERT INTO t_role (`uid`, `role_name`, `create_time`, `update_time`, `status`, `summary`, `category_menu_uids`) VALUES ('434994947c5a4ee3a710cd277357c7c3', '系统管理员', '2018-10-16 07:56:26', '2021-12-07 14:55:29', 1, '系统管理员，管理全部菜单和功能', '[\"49b42250abcb47ff876bad699cf34f03\",\"1f01cd1d2f474743b241d74008b12333\",\"e5218b0b1cf016ae21eced4e47b628ac\",\"fe45ea293f75dc88b96cab96c218512a\",\"0cab1fcdcd01f394768e2e2674e56773\",\"16a75a3c38e71c1046b443b4b64dd930\",\"2b983c5439ac07f2cf07437ba9fff6be\",\"7ada0a7053be4ad4b31f6be64315d03a\",\"9db7ffbded9717f13a1a97fca46bc17c\",\"0a035547bbec404eb3ee0ef43312148d\",\"2a733ff390af9b44ecda4e8c4634d75d\",\"327d945daf4ddb71976c4ab3830e7c66\",\"6c8a8c50c77429fab210bd52ed8c50bb\",\"72d26cf940bf9dfb6bde0a590ff40882\",\"80ee135af885f02d52ecb67d5a05b173\",\"aef85c40b54320d7c5a9d78697e12910\",\"f87d2f9b4539abbade38583420dc8b89\",\"fbc30e4ae5bb33b39baca7bf6bd8ce99\",\"3e92f2fd6cf012d30bfae2d9cdb7092d\",\"a8bad1abec6c8fc8d4bce5a27829c878\",\"b21105b915a5b54588c2cd458a94d2d5\",\"ccc0dced06919403832647a871312f09\",\"e76ec0b7226985a414f035e2ecbd00b4\",\"fa1e85a9c7734d27df07bc730206bd1a\",\"badf0010422b432ba6ec9c83a25012ed\",\"78f24799307cb63bc3759413dadf4d1a\",\"587e2697fa4d85046feece8ab9d0706c\",\"f87d2f9b4539abbade38583420dc8byl\",\"fbc30e4ae5bb33b39baca7bf6bd8cezm\",\"d3a19221259d439b916f475e43edb13d\",\"aa225cdae6464bc0acebd732192f8362\",\"2a733ff390af9b44ecda4e8c4634d73k\",\"327d945daf4ddb71976c4ab3830e7c4i\",\"aef85c40b54320d7c5a9d78697e1295n\",\"f87d2f9b4539abbade38583420dc8b2l\",\"fbc30e4ae5bb33b39baca7bf6bd8ce1m\",\"879cfcd4dfd3e5bc1bb6ea2c0f1f82c0\",\"7ed32cbc289d7db31ed13738970a6b8b\",\"2efd760fc2d8b2dd243ca4802c37bdb9\",\"b6b54cdedaff0d8c92238f37c14c96be\",\"e74c472522bd9858ac4976cfc07a7d72\",\"eacd6ffdfaa2280bdd702bd6060387da\",\"5010ae46511e4c0b9f30d1c63ad3f0c1\",\"2a733ff390af9b44ecda4e8c4634d7uk\",\"327d945daf4ddb71976c4ab3830e7cvi\",\"f87d2f9b4539abbade38583420dc8btl\",\"fbc30e4ae5bb33b39baca7bf6bd8cesm\",\"2de247af3b0a459095e937d7ab9f5864\",\"2a733ff390af9b44ecda4e8c4634d7lk\",\"327d945daf4ddb71976c4ab3830e7cmi\",\"72d26cf940bf9dfb6bde0a590ff408nj\",\"f87d2f9b4539abbade38583420dc8bkl\",\"fbc30e4ae5bb33b39baca7bf6bd8cejm\",\"baace3dc03d34c54b81761dce8243814\",\"e4a482c089d04a30b6ecbaadb81b70f8\",\"7022c764d5676afa66f18ad61d525e14\",\"ca69ce2193b3ede88d8e0190bce3a7df\",\"f65e258ede9fd768041bfd10e5fa9d5c\",\"ff4ec0fd980a3b46c59f37f45b31d383\",\"510483ce569b4fc88299f346147b1314\",\"250619ada446f8ea2e8bd0a1a2451e6a\",\"3cc70407e2089c85489195498647c98f\",\"5636803d8fb7911ee00ad96271600ec8\",\"d893ee4375165f37c2b14a8eeb69c1a3\",\"fa138c7ebdf9ddbc9a52b2389fad45bc\",\"4dea9c4f39d2480983e8c4333d35e036\",\"2a733ff390af9b44ecda4e8c4634d7pk\",\"327d945daf4ddb71976c4ab3830e7cqi\",\"aef85c40b54320d7c5a9d78697e129rn\",\"f87d2f9b4539abbade38583420dc8bol\",\"fbc30e4ae5bb33b39baca7bf6bd8cenm\",\"ce679931bff05745ad9b1716d47f2790\"]');
INSERT INTO t_role (`uid`, `role_name`, `create_time`, `update_time`, `status`, `summary`, `category_menu_uids`) VALUES ('ad92f1639508ca405728f4981770793b', '项目运营', '2020-03-22 08:24:29', '2021-12-07 14:36:05', 1, '项目运营管理员', '[\"badf0010422b432ba6ec9c83a25012ed\",\"78f24799307cb63bc3759413dadf4d1a\",\"587e2697fa4d85046feece8ab9d0706c\",\"f87d2f9b4539abbade38583420dc8byl\",\"fbc30e4ae5bb33b39baca7bf6bd8cezm\",\"d3a19221259d439b916f475e43edb13d\",\"aa225cdae6464bc0acebd732192f8362\",\"2a733ff390af9b44ecda4e8c4634d73k\",\"327d945daf4ddb71976c4ab3830e7c4i\",\"aef85c40b54320d7c5a9d78697e1295n\",\"f87d2f9b4539abbade38583420dc8b2l\",\"fbc30e4ae5bb33b39baca7bf6bd8ce1m\",\"879cfcd4dfd3e5bc1bb6ea2c0f1f82c0\",\"7ed32cbc289d7db31ed13738970a6b8b\",\"2efd760fc2d8b2dd243ca4802c37bdb9\",\"b6b54cdedaff0d8c92238f37c14c96be\",\"e74c472522bd9858ac4976cfc07a7d72\",\"eacd6ffdfaa2280bdd702bd6060387da\",\"5010ae46511e4c0b9f30d1c63ad3f0c1\",\"2a733ff390af9b44ecda4e8c4634d7uk\",\"327d945daf4ddb71976c4ab3830e7cvi\",\"f87d2f9b4539abbade38583420dc8btl\",\"fbc30e4ae5bb33b39baca7bf6bd8cesm\",\"2de247af3b0a459095e937d7ab9f5864\",\"2a733ff390af9b44ecda4e8c4634d7lk\",\"327d945daf4ddb71976c4ab3830e7cmi\",\"72d26cf940bf9dfb6bde0a590ff408nj\",\"f87d2f9b4539abbade38583420dc8bkl\",\"fbc30e4ae5bb33b39baca7bf6bd8cejm\",\"65e22f3d36d94bcea47478aba02895a1\",\"f4d7f3a657fa12043f65ad58e9baeaa8\",\"10e5cc3ea9e530a18303ee92449e53a5\",\"7022c764d5676afa66f18ad61d525e14\",\"749b7d0df2f74fda4c2f059ad797cb54\",\"7b3fd0e29feb05cfc6f79ea654a2d5a0\",\"b51481cb5c016e62a98b396270b98371\",\"ca69ce2193b3ede88d8e0190bce3a7df\",\"f65e258ede9fd768041bfd10e5fa9d5c\",\"ff4ec0fd980a3b46c59f37f45b31d383\",\"3545a821ab5c6cc274664e0bd9a4b5d4\",\"917b88e61c0e250b82ec3021b51e1d20\"]');
INSERT INTO t_role (`uid`, `role_name`, `create_time`, `update_time`, `status`, `summary`, `category_menu_uids`) VALUES ('be6b6b4be3423c31196d9bcc1a31354f', '项目管理员', '2021-11-23 19:16:22', '2021-12-07 15:06:24', 1, '项目管理员', '[\"badf0010422b432ba6ec9c83a25012ed\",\"78f24799307cb63bc3759413dadf4d1a\",\"587e2697fa4d85046feece8ab9d0706c\",\"f87d2f9b4539abbade38583420dc8byl\",\"fbc30e4ae5bb33b39baca7bf6bd8cezm\",\"65e22f3d36d94bcea47478aba02895a1\",\"f4d7f3a657fa12043f65ad58e9baeaa8\",\"10e5cc3ea9e530a18303ee92449e53a5\",\"7022c764d5676afa66f18ad61d525e14\",\"749b7d0df2f74fda4c2f059ad797cb54\",\"7b3fd0e29feb05cfc6f79ea654a2d5a0\",\"b51481cb5c016e62a98b396270b98371\",\"ca69ce2193b3ede88d8e0190bce3a7df\",\"f65e258ede9fd768041bfd10e5fa9d5c\",\"ff4ec0fd980a3b46c59f37f45b31d383\",\"3545a821ab5c6cc274664e0bd9a4b5d4\",\"917b88e61c0e250b82ec3021b51e1d20\",\"510483ce569b4fc88299f346147b1314\",\"250619ada446f8ea2e8bd0a1a2451e6a\",\"3cc70407e2089c85489195498647c98f\",\"5636803d8fb7911ee00ad96271600ec8\",\"d893ee4375165f37c2b14a8eeb69c1a3\",\"fa138c7ebdf9ddbc9a52b2389fad45bc\",\"4dea9c4f39d2480983e8c4333d35e036\",\"2a733ff390af9b44ecda4e8c4634d7pk\",\"327d945daf4ddb71976c4ab3830e7cqi\",\"aef85c40b54320d7c5a9d78697e129rn\",\"f87d2f9b4539abbade38583420dc8bol\",\"fbc30e4ae5bb33b39baca7bf6bd8cenm\",\"ce679931bff05745ad9b1716d47f2790\"]');

INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('026b7d3453ff74d3d6a31a10abc39a47', 10, 'b0c90de7ebeb138e9a0487f6ba86275a', '删除', '3', NULL, NULL, 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '删除操作', 1, '2020-02-17 21:25:04', '2020-02-17 21:25:04', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('03bfb8183ef66828f7b04487bfbe35c7', 27, '904965b87673d2dd762c9ac2b6726813', '七牛云对象存储', '1', NULL, 'primary', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '图片显示优先级  七牛云', 1, '2020-02-18 09:45:27', '2020-10-22 10:04:42', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('03ebe4234670f7cfeb0b19c8794bdaea', 19, 'e582ff889b2e64fffed194737d78fa98', '二级推荐', '2', NULL, 'success', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '推荐等级 二级推荐', 1, '2020-02-17 22:05:34', '2020-02-17 22:05:34', '1', 3);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('0cb01485ad75f7ec37a29d388e7d6013', 7, 'e2fa1d1024a2570f13ec7f684c08bd25', '女', '2', NULL, NULL, 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '性别 女', 1, '2020-02-16 20:49:56', '2020-02-16 20:49:56', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('0d5183928308f7945bfb300f873b1f1e', 44, '147b16259a5f482e86b75893d09e10d4', '上架', '1', NULL, 'success', 1, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '友链状态 上线中', 1, '2020-03-15 08:50:28', '2020-03-15 08:50:28', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('0f2833c5c24480bab9c4e3e5a815002a', 70, '904965b87673d2dd762c9ac2b6726813', 'Minio对象存储', '2', NULL, 'primary', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '使用Minio构建本地对象存储服务', 1, '2020-10-22 10:04:02', '2020-10-22 10:05:39', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('0fb5f67e224f91638e71935eb6a38467', 21, 'e582ff889b2e64fffed194737d78fa98', '四级推荐', '4', NULL, 'primary', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '推荐等级 四级推荐', 1, '2020-02-17 22:06:13', '2020-02-17 22:06:13', '1', 1);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('10e538229ea9ee8552672ed1cb5575e0', 61, '5c956299119cbbcbcf2009558ce503d0', '是', '1', NULL, 'danger', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '是否跳转外链  是', 1, '2020-07-03 21:20:09', '2020-07-03 21:43:33', '1', 1);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('1e666c133099dc361ec0dbaa8b6203cc', 56, '80dfd5fa0b8226c8c0102da80cc6fedb', '三级菜单', '3', NULL, 'info', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '菜单管理 三级菜单  按钮', 1, '2020-03-21 18:02:22', '2020-03-21 18:02:22', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('2388fa41c446c4f870b6010bc404473f', 31, '94ba24e6adb46cd094bb2217f1028285', '原创', '1', NULL, 'success', 1, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '原创博客', 1, '2020-02-19 09:54:52', '2020-02-19 09:54:52', '1', 1);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('23b55a610d84470e052e09abf110dccc', 17, 'e582ff889b2e64fffed194737d78fa98', '正常', '0', NULL, 'info', 1, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '推荐等级 正常', 1, '2020-02-17 22:01:56', '2020-02-17 22:01:56', '1', 5);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('24f6c115ecfbb9c1818f9603990c8971', 80, 'f1929a18eac0a6dfa9007aa8024899a2', 'Solr搜索', '2', NULL, NULL, 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '搜索模式：Solr搜索', 1, '2021-09-11 16:18:17', '2021-09-11 16:18:17', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('2ebdf915ac4003d4ed5e2b4c78038acf', 59, 'eaef85e78f7f88bd0efd428f32033f2f', '博客详情', 'BLOG_INFO', NULL, 'warning', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '评论来源 博客详情', 1, '2020-04-04 21:27:11', '2020-04-04 21:27:11', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('30a04bbb52add36a22211f6b93fc882e', 13, '5ce79da03dbedef627e8c6fb002b1a29', '是', '1', NULL, 'success', 1, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '系统是否  是', 1, '2020-02-17 21:56:35', '2020-02-17 21:56:35', '1', 1);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('3151f138e5c2858112da60b70a699469', 23, 'ba386b930c4a3580357b1df8a2e24c8a', '华北', 'z1', NULL, NULL, 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '存储区域 华北', 1, '2020-02-17 23:37:31', '2020-02-17 23:37:31', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('3555747751ef69d66109280797dd2859', 57, 'eaef85e78f7f88bd0efd428f32033f2f', '留言板', 'MESSAGE_BOARD', NULL, 'primary', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '评论来源 留言板', 1, '2020-04-04 21:25:34', '2020-04-04 21:25:34', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('39cf99bab1bd3ca0683424f99f1a9fee', 26, 'ba386b930c4a3580357b1df8a2e24c8a', '东南亚', 'as0', NULL, NULL, 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '存储区域 东南亚', 1, '2020-02-17 23:38:32', '2020-02-17 23:38:32', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('3b38f16f39b491acb52d6a3a2be6e9c6', 58, 'eaef85e78f7f88bd0efd428f32033f2f', '关于我', 'ABOUT', NULL, 'success', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '评论来源 关于我', 1, '2020-04-04 21:26:14', '2020-04-04 21:26:14', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('3f47a55713b1ea101b7f38af0fe1efa9', 37, '20a4dd3551aa6264f7e43c2274820763', 'GITEE', 'GITEE', NULL, 'primary', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '账号类型 Gitee', 1, '2020-03-10 12:09:07', '2020-03-10 12:09:07', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('40b08947b62bc34793b0655c944d2eec', 34, '20a4dd3551aa6264f7e43c2274820763', 'Gitee', 'Gitee', NULL, NULL, 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '账号类型 Gitee', 0, '2020-03-10 11:59:26', '2020-03-10 11:59:26', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('416abb075fd0f8d4a512d204121c15b2', 15, 'f4c0b7c14e1857a8453af396e1537556', '发布', '1', NULL, 'success', 1, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '发布状态  上架', 1, '2020-02-17 22:00:01', '2020-02-17 22:00:01', '1', 1);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('431eb64cba73239ebd01b0b823e5f36f', 64, 'b96483ba6cd5122b2d33981681db8b1c', '否', '0', NULL, 'info', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '参数类型 是否内置  否', 1, '2020-07-21 17:16:16', '2020-07-21 17:16:16', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('464f9e4a13d53494bff10df5e966b0cf', 39, '397114076512e421458806e5d0050af6', '禁言', '0', NULL, 'danger', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '评论状态 禁言', 1, '2020-03-10 13:01:20', '2020-03-10 13:01:20', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('47559bf170909797709381e3b8c1a796', 38, '20a4dd3551aa6264f7e43c2274820763', 'GITHUB', 'GITHUB', NULL, 'danger', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '账号类型 Github', 1, '2020-03-10 12:09:20', '2020-03-10 12:09:20', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('490b20a757bc11fbb0b089a91c169830', 14, '5ce79da03dbedef627e8c6fb002b1a29', '否', '0', NULL, 'warning', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '系统是否  否', 1, '2020-02-17 21:56:47', '2020-02-17 21:56:47', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('49d73b6c112e6d3233e11cc97c4ae231', 71, 'a7be558b1c601b94126cc6ab5d5d008d', '博客', '0', NULL, 'default', 1, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '文章类型：博客', 1, '2020-11-07 10:15:56', '2020-11-07 10:19:05', '1', 1);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('4a3e898c54f9ddcfa52a5c40e5a647dd', 45, '147b16259a5f482e86b75893d09e10d4', '下架', '2', NULL, 'warning', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '友链状态  已下架', 1, '2020-03-15 08:52:20', '2020-03-15 08:52:20', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('4a90a207068f8044c65cae80fbbb7309', 62, '5c956299119cbbcbcf2009558ce503d0', '否', '0', NULL, 'info', 1, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '是否跳转外链  否', 1, '2020-07-03 21:20:22', '2020-07-03 21:43:26', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('4dcc00f8566473020608d9609a1686e5', 46, '6472ff63369e0118d2e0b907437d631d', '已开启', '0', NULL, 'warning', 1, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '反馈状态 已开启', 1, '2020-03-16 09:20:38', '2020-03-16 09:20:38', '1', 4);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('4f243578b444646a6bc1349a10d1be9f', 8, 'b0c90de7ebeb138e9a0487f6ba86275a', '新增', '1', NULL, NULL, 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '新增操作', 1, '2020-02-17 21:24:29', '2020-02-17 21:24:29', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('5130d2cf209bcf02e97322491b4e611e', 4, '861dfd0f77c8b053d40e020f23702df4', '隐藏', '0', NULL, NULL, 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '菜单状态 隐藏', 1, '2020-02-16 13:23:39', '2020-02-16 13:23:39', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('57968d6ea419bc4868ef8a4518c1f90c', 69, 'a9eade09410e392988140adb1710e447', '超时', '5000_10000000', NULL, 'danger', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '接口耗时  超时  5000_100000', 1, '2020-09-22 20:15:46', '2020-09-22 20:35:19', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('5b6f8111b8296906b887ec5d4420721b', 52, '4d4a35b3dfc16d23b65a82073c88c0e6', '博主', '2', NULL, 'danger', 1, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '用户标签  博主', 1, '2020-03-18 09:25:33', '2020-03-18 09:25:33', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('5f554b9bae92a1d630a1b89b8f88bc65', 47, '6472ff63369e0118d2e0b907437d631d', '进行中', '1', NULL, 'primary', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '反馈状态 进行中', 1, '2020-03-16 09:20:56', '2020-03-16 09:20:56', '1', 2);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('614a593ba613eb6f5c591f2d2430538a', 49, '6472ff63369e0118d2e0b907437d631d', '已拒绝', '3', NULL, 'danger', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '反馈状态 已拒绝', 1, '2020-03-16 09:21:33', '2020-03-16 09:21:33', '1', 1);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('66aa82856368c573e0bfe012ec6d0ab6', 22, 'ba386b930c4a3580357b1df8a2e24c8a', '华东', 'z0', NULL, NULL, 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '存储区域 华东', 1, '2020-02-17 23:37:12', '2020-02-17 23:37:12', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('6caab28eb72a7331ecac4d01c616f023', 67, 'a9eade09410e392988140adb1710e447', '正常', '0_2000', NULL, 'primary', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '接口耗时  正常  0_2000', 1, '2020-09-22 20:14:36', '2020-09-22 20:25:39', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('6e19e89f593b2e378ae98e209972004b', 51, '4d4a35b3dfc16d23b65a82073c88c0e6', '管理员', '1', NULL, 'warning', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '用户标签 管理员', 1, '2020-03-18 09:25:09', '2020-03-18 09:25:09', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('71bb6611f581413baa7b654dd271deca', 16, 'f4c0b7c14e1857a8453af396e1537556', '下架', '0', NULL, 'danger', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '发布状态  下架', 1, '2020-02-17 22:00:15', '2020-02-17 22:00:15', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('763bb526d9aa6ebd5bc0b7db69548c57', 30, '80dfd5fa0b8226c8c0102da80cc6fedb', '二级菜单', '2', NULL, 'danger', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '菜单等级 二级菜单', 1, '2020-02-19 08:45:53', '2020-02-19 08:45:53', '1', 2);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('78a4a2573f5fe9bd22813a9e4b1f38a8', 60, '20a4dd3551aa6264f7e43c2274820763', 'QQ', 'QQ', NULL, 'primary', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '账号类型 QQ', 1, '2020-05-28 09:13:00', '2020-05-28 09:13:09', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('7a88a80ab90ed03c86ec3276bd9748cb', 65, '6634c313a8883f6c876806a15b9cc4b1', '富文本编辑器', '0', NULL, 'primary', 1, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '富文本编辑器Markdown', 1, '2020-08-29 08:14:32', '2020-08-29 08:47:11', '1', 1);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('7c99aecb7b9b50c06b1c643c19568a4d', 25, 'ba386b930c4a3580357b1df8a2e24c8a', '北美', 'na0', NULL, NULL, 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '存储区域 北美', 1, '2020-02-17 23:38:16', '2020-02-17 23:38:16', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('82d7c4c2fcbd2d3b9aa856ac33420c45', 2, '861dfd0f77c8b053d40e020f23702df4', '显示', '1', NULL, NULL, 1, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '菜单状态 显示', 1, '2020-02-16 12:40:51', '2020-10-15 10:17:39', '1', 1);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('868d1b0c1df6915a4a6ca574b46d4ac8', 18, 'e582ff889b2e64fffed194737d78fa98', '一级推荐', '1', NULL, 'danger', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '推荐等级 一级推荐', 1, '2020-02-17 22:05:19', '2020-02-17 22:05:19', '1', 4);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('8cb9f3d3fa091a581a333102dec732ab', 20, 'e582ff889b2e64fffed194737d78fa98', '三级推荐', '3', NULL, 'warning', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '推荐等级 三级推荐', 1, '2020-02-17 22:05:55', '2020-02-17 22:05:55', '1', 2);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('9288f39aa07aee842952fbc7dd1fe4a2', 66, '6634c313a8883f6c876806a15b9cc4b1', 'Markdown编辑器', '1', NULL, 'warning', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', 'Markdown编辑器', 1, '2020-08-29 08:14:59', '2020-08-29 08:14:59', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('95b43081a93a53727b0474098242d64c', 33, '20a4dd3551aa6264f7e43c2274820763', 'Github', 'Github', NULL, NULL, 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '账号类型 Github', 0, '2020-03-10 11:59:12', '2020-03-10 11:59:12', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('964945d3c156ea7dcc64b13741b02923', 63, 'b96483ba6cd5122b2d33981681db8b1c', '是', '1', NULL, 'primary', 1, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '参数类型  是否内置  是', 1, '2020-07-21 17:15:49', '2020-07-21 17:16:29', '1', 1);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('9673bcc91bb2f22c31381c8364d4726b', 9, 'b0c90de7ebeb138e9a0487f6ba86275a', '编辑', '2', NULL, NULL, 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '编辑操作', 1, '2020-02-17 21:24:45', '2020-02-17 21:24:45', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('a03e72c6f457bb282d3a7f62ff5f4598', 55, '6bf52bf12dce9d0bc56f2d0e10ceccbe', '按钮', '1', NULL, 'warning', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '菜单类型 按钮', 1, '2020-03-21 09:28:32', '2020-03-21 09:28:32', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('a94dadf4a2a5c003c3bb38dd6075ebe3', 35, '20a4dd3551aa6264f7e43c2274820763', 'Mogu', 'Mogu', NULL, NULL, 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '账号类型 蘑菇博客', 0, '2020-03-10 11:59:53', '2020-03-10 11:59:53', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('ac72222bb8ac26959460f87328c97d59', 24, 'ba386b930c4a3580357b1df8a2e24c8a', '华南', 'z2', NULL, NULL, 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '存储区域 华南', 1, '2020-02-17 23:38:00', '2020-02-17 23:38:00', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('b0e0e31c66fa52f481ea1982348ee703', 12, 'e7a80c0adce2b69035dfa505d998ba74', '关闭', '0', NULL, 'danger', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '系统开关 关闭', 1, '2020-02-17 21:50:22', '2020-02-17 21:50:22', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('b8764efefb7a78d4a2d84466daf34084', 40, '397114076512e421458806e5d0050af6', '正常', '1', NULL, 'success', 1, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '评论状态 正常', 1, '2020-03-10 13:01:35', '2020-03-10 13:01:35', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('bc852b3bbec6b9641a508fa32c49a40d', 54, '6bf52bf12dce9d0bc56f2d0e10ceccbe', '菜单', '0', NULL, 'success', 1, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '菜单类型  菜单', 1, '2020-03-21 09:28:13', '2020-03-21 09:28:13', '1', 1);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('bc9c1933052d66f8424b09a6869972d0', 79, 'f1929a18eac0a6dfa9007aa8024899a2', '全文检索', '1', NULL, NULL, 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '搜索模式：开启ElasticSearch全文检索', 1, '2021-09-11 15:27:36', '2021-09-11 15:27:36', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('bd14282cf7e3db5831e1b9e7e899024e', 42, 'd3d9f373ae0c344fa0a24eadd23a191b', '评论', '0', NULL, 'warning', 1, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '评论类型 评论', 1, '2020-03-15 08:49:18', '2020-05-14 18:50:12', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('c027f76096eb89ee6364b25f7b366388', 78, 'f1929a18eac0a6dfa9007aa8024899a2', 'SQL搜索', '0', NULL, NULL, 1, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '搜索模式：SQL搜索', 1, '2021-09-11 15:27:00', '2021-09-11 15:27:00', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('c3299bf019caeff6690e90c358e67e43', 53, '861dfd0f77c8b053d40e020f23702df4', '123', '123', NULL, NULL, 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', NULL, 0, '2020-03-20 11:51:09', '2020-03-20 11:51:09', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('ca66f4c186c3ceb69057ae0432bd43d6', 11, 'e7a80c0adce2b69035dfa505d998ba74', '开启', '1', NULL, 'primary', 1, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '系统开关 开启', 1, '2020-02-17 21:50:06', '2020-02-17 21:50:06', '1', 1);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('cc7cc52fbe8b7b4c89a6f1670d09c060', 6, 'e2fa1d1024a2570f13ec7f684c08bd25', '男', '1', NULL, NULL, 1, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '性别 男', 1, '2020-02-16 20:49:40', '2020-02-16 20:49:40', '1', 2);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('d350119289186d520e0ccbb9b320e07a', 48, '6472ff63369e0118d2e0b907437d631d', '已完成', '2', NULL, 'success', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '反馈状态 已完成', 1, '2020-03-16 09:21:13', '2020-03-16 09:21:13', '1', 3);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('d3fe3ce87880595aad04b3e8077da223', 32, '94ba24e6adb46cd094bb2217f1028285', '转载', '0', NULL, 'info', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '转载博客', 1, '2020-02-19 09:55:09', '2020-02-19 09:55:09', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('dc3c52a18d8ad99b83337751187e7359', 29, '80dfd5fa0b8226c8c0102da80cc6fedb', '一级菜单', '1', NULL, 'success', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '菜单等级 一级菜单', 1, '2020-02-19 08:45:31', '2020-02-19 08:45:31', '1', 3);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('e42847ffae93e9f494dfc6cc65eaedce', 68, 'a9eade09410e392988140adb1710e447', '一般', '2000_5000', NULL, 'warning', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '接口耗时 一般  2000_5000', 1, '2020-09-22 20:14:57', '2020-09-22 20:25:30', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('e6d918372233a8b48b9609fa70326c89', 43, '147b16259a5f482e86b75893d09e10d4', '申请', '0', NULL, 'danger', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '友链状态  申请中', 1, '2020-03-15 08:49:57', '2020-03-15 08:49:57', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('e7dbddd726ae81086364e49abb12c539', 41, 'd3d9f373ae0c344fa0a24eadd23a191b', '点赞', '1', NULL, 'success', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '评论类型  点赞', 1, '2020-03-15 08:49:02', '2020-03-15 08:49:02', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('e9d6bc86f8ffa4e8dcf85b98069211dd', 28, '904965b87673d2dd762c9ac2b6726813', '本地文件存储', '0', NULL, 'primary', 1, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '图片显示优先级  本地', 1, '2020-02-18 09:45:45', '2020-10-22 10:04:36', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('f3d2ded0e31673ce545b905aacce627f', 72, 'a7be558b1c601b94126cc6ab5d5d008d', '推广', '1', NULL, 'default', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '文章类型：推广', 1, '2020-11-07 10:16:16', '2020-11-07 10:16:16', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('f6fdd4f421d31989b57d932422ec5528', 50, '4d4a35b3dfc16d23b65a82073c88c0e6', '普通用户', '0', NULL, 'info', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '用户标签 普通用户', 1, '2020-03-18 09:24:40', '2020-03-18 09:24:40', '1', 0);
INSERT INTO t_sys_dict_data ("uid", "oid", "dict_type_uid", "dict_label", "dict_value", "css_class", "list_class", "is_default", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('f963505ec3462cb63f9a590c9df08ac8', 36, '20a4dd3551aa6264f7e43c2274820763', 'MOGU', 'MOGU', NULL, 'success', 0, '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '账号类型 蘑菇博客', 1, '2020-03-10 12:08:50', '2020-03-10 12:08:50', '1', 0);

INSERT INTO t_sys_dict_type ("uid", "oid", "dict_name", "dict_type", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('147b16259a5f482e86b75893d09e10d4', 18, '友链状态', 'sys_link_status', '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '友链状态', 1, '2020-03-15 08:47:56', '2020-03-15 08:47:56', '1', 0);
INSERT INTO t_sys_dict_type ("uid", "oid", "dict_name", "dict_type", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('20a4dd3551aa6264f7e43c2274820763', 15, '用户账号来源', 'sys_account_source', '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '用户账号来源', 1, '2020-03-10 11:57:40', '2020-03-10 12:12:45', '1', 0);
INSERT INTO t_sys_dict_type ("uid", "oid", "dict_name", "dict_type", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('397114076512e421458806e5d0050af6', 16, '评论状态', 'sys_comment_status', '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '评论状态', 1, '2020-03-10 13:00:24', '2020-03-10 13:00:24', '1', 0);
INSERT INTO t_sys_dict_type ("uid", "oid", "dict_name", "dict_type", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('4d4a35b3dfc16d23b65a82073c88c0e6', 20, '用户标签', 'sys_user_tag', '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '用户标签：普通用户，管理员，博主', 1, '2020-03-18 09:23:56', '2020-03-18 09:24:07', '1', 0);
INSERT INTO t_sys_dict_type ("uid", "oid", "dict_name", "dict_type", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('5c907ad864bc904851bde2506851cb88', 8, '通知类型', 'sys_notice_type', '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '通知类型列表', 1, '2020-02-17 21:40:53', '2020-02-17 21:40:53', '1', 0);
INSERT INTO t_sys_dict_type ("uid", "oid", "dict_name", "dict_type", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('5c956299119cbbcbcf2009558ce503d0', 23, '跳转外链', 'sys_jump_external', '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '是否跳转外部链接', 1, '2020-07-03 21:19:39', '2020-07-03 21:19:39', '1', 0);
INSERT INTO t_sys_dict_type ("uid", "oid", "dict_name", "dict_type", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('5ce79da03dbedef627e8c6fb002b1a29', 7, '系统是否', 'sys_yes_no', '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '系统是否列表', 1, '2020-02-17 21:40:24', '2020-02-17 23:28:38', '1', 0);
INSERT INTO t_sys_dict_type ("uid", "oid", "dict_name", "dict_type", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('6472ff63369e0118d2e0b907437d631d', 19, '反馈状态', 'sys_feedback_status', '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '反馈状态', 1, '2020-03-16 09:20:06', '2020-03-16 09:20:06', '1', 0);
INSERT INTO t_sys_dict_type ("uid", "oid", "dict_name", "dict_type", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('6634c313a8883f6c876806a15b9cc4b1', 25, '编辑器模式', 'sys_editor_modal', '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '用于选择编辑器是富文本或者markdown', 1, '2020-08-29 08:13:46', '2020-08-29 08:13:46', '1', 0);
INSERT INTO t_sys_dict_type ("uid", "oid", "dict_name", "dict_type", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('6bf52bf12dce9d0bc56f2d0e10ceccbe', 21, '菜单类型', 'sys_menu_type', '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '菜单类型', 1, '2020-03-21 09:27:49', '2020-03-21 09:27:49', '1', 0);
INSERT INTO t_sys_dict_type ("uid", "oid", "dict_name", "dict_type", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('80dfd5fa0b8226c8c0102da80cc6fedb', 14, '菜单等级', 'sys_menu_level', '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '菜单管理 菜单等级', 1, '2020-02-19 08:45:01', '2020-02-19 08:45:01', '1', 0);
INSERT INTO t_sys_dict_type ("uid", "oid", "dict_name", "dict_type", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('861dfd0f77c8b053d40e020f23702df4', 3, '显示状态', 'sys_show_hide', '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '显示状态', 1, '2020-02-16 10:48:38', '2020-10-15 10:20:40', '1', 3);
INSERT INTO t_sys_dict_type ("uid", "oid", "dict_name", "dict_type", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('904965b87673d2dd762c9ac2b6726813', 13, '图片显示优先级', 'sys_picture_priority', '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '七牛云、本地、Minio显示优先级', 1, '2020-02-18 09:44:59', '2020-10-22 09:59:08', '1', 0);
INSERT INTO t_sys_dict_type ("uid", "oid", "dict_name", "dict_type", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('94ba24e6adb46cd094bb2217f1028285', 11, '原创状态', 'sys_original_status', '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '原创状态列表', 1, '2020-02-17 21:45:52', '2020-02-17 23:31:37', '1', 1);
INSERT INTO t_sys_dict_type ("uid", "oid", "dict_name", "dict_type", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('a7be558b1c601b94126cc6ab5d5d008d', 27, '文章类型', 'sys_blog_type', '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '文章类型：博客，推广', 1, '2020-11-07 10:14:58', '2020-11-07 10:14:58', '1', 0);
INSERT INTO t_sys_dict_type ("uid", "oid", "dict_name", "dict_type", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('a9eade09410e392988140adb1710e447', 26, '接口耗时', 'sys_spend_time', '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '日志接口耗时区间', 1, '2020-09-22 20:13:17', '2020-09-22 20:13:52', '1', 0);
INSERT INTO t_sys_dict_type ("uid", "oid", "dict_name", "dict_type", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('b0c90de7ebeb138e9a0487f6ba86275a', 5, '操作类型', 'sys_oper_type', '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '操作类型', 1, '2020-02-16 12:56:40', '2020-02-16 12:56:40', '1', 0);
INSERT INTO t_sys_dict_type ("uid", "oid", "dict_name", "dict_type", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('b96483ba6cd5122b2d33981681db8b1c', 24, '参数类型', 'sys_params_type', '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '参数类型', 1, '2020-07-21 17:14:50', '2020-07-21 17:14:50', '1', 0);
INSERT INTO t_sys_dict_type ("uid", "oid", "dict_name", "dict_type", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('ba386b930c4a3580357b1df8a2e24c8a', 12, '存储区域', 'sys_storage_region', '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '七牛云存储区域', 1, '2020-02-17 23:36:21', '2020-02-17 23:36:21', '1', 0);
INSERT INTO t_sys_dict_type ("uid", "oid", "dict_name", "dict_type", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('d3d9f373ae0c344fa0a24eadd23a191b', 17, '评论类型', 'sys_comment_type', '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '评论类型', 1, '2020-03-15 08:47:37', '2020-03-15 08:47:37', '1', 0);
INSERT INTO t_sys_dict_type ("uid", "oid", "dict_name", "dict_type", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('e2fa1d1024a2570f13ec7f684c08bd25', 2, '用户性别', 'sys_user_sex', '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '用户性别列表', 1, '2020-02-16 10:40:49', '2020-02-17 23:31:49', '1', 2);
INSERT INTO t_sys_dict_type ("uid", "oid", "dict_name", "dict_type", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('e582ff889b2e64fffed194737d78fa98', 9, '推荐等级', 'sys_recommend_level', '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '推荐等级列表', 1, '2020-02-17 21:41:59', '2020-02-17 21:41:59', '1', 0);
INSERT INTO t_sys_dict_type ("uid", "oid", "dict_name", "dict_type", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('e7a80c0adce2b69035dfa505d998ba74', 6, '系统开关', 'sys_normal_disable', '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '系统开关列表', 1, '2020-02-17 21:38:55', '2020-02-17 21:40:00', '1', 0);
INSERT INTO t_sys_dict_type ("uid", "oid", "dict_name", "dict_type", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('eaef85e78f7f88bd0efd428f32033f2f', 22, '评论来源', 'sys_comment_source', '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '评论来源', 1, '2020-04-04 21:24:55', '2020-04-04 21:24:55', '1', 0);
INSERT INTO t_sys_dict_type ("uid", "oid", "dict_name", "dict_type", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('f1929a18eac0a6dfa9007aa8024899a2', 30, '搜索模式', 'sys_search_model', '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '搜索模式：SQL搜索、全文检索', 1, '2021-09-11 15:26:35', '2021-09-11 15:26:35', '1', 0);
INSERT INTO t_sys_dict_type ("uid", "oid", "dict_name", "dict_type", "create_by_uid", "update_by_uid", "remark", "status", "create_time", "update_time", "is_publish", "sort") VALUES ('f4c0b7c14e1857a8453af396e1537556', 10, '发布状态', 'sys_publish_status', '1f01cd1d2f474743b241d74008b12333', '1f01cd1d2f474743b241d74008b12333', '发布状态列表', 1, '2020-02-17 21:44:09', '2020-02-17 21:44:09', '1', 0);

INSERT INTO t_sys_params ("uid", "params_type", "params_name", "params_key", "remark", "params_value", "status", "create_time", "update_time", "sort") VALUES ('099427a9a5305f99ab46bb1c56080d3e', '1', '文件上传路径', 'FILE_UPLOAD_PATH', '图片本地上传路径', 'D:/mogu_blog/data/', 0, '2020-07-21 22:35:43', '2020-08-20 17:55:42', 0);
INSERT INTO t_sys_params ("uid", "params_type", "params_name", "params_key", "remark", "params_value", "status", "create_time", "update_time", "sort") VALUES ('0b2757bec99cb1c4eba3163fc6ab7cf2', '1', '二级推荐', 'BLOG_SECOND_COUNT', '首页博客 二级推荐数量', '2', 1, '2020-07-21 22:28:03', '2020-07-28 18:02:58', 0);
INSERT INTO t_sys_params ("uid", "params_type", "params_name", "params_key", "remark", "params_value", "status", "create_time", "update_time", "sort") VALUES ('0e5273aeee0058ee8737b94c6d955fe0', '1', '热门标签数量', 'HOT_TAG_COUNT', '首页 热门标签数量', '20', 1, '2020-07-21 22:41:04', '2021-03-11 19:24:18', 0);
INSERT INTO t_sys_params ("uid", "params_type", "params_name", "params_key", "remark", "params_value", "status", "create_time", "update_time", "sort") VALUES ('13102852ad56f478324ae13e9f83d7a6', '1', '一级推荐', 'BLOG_FIRST_COUNT', '首页博客 一级推荐数量', '5', 1, '2020-07-21 22:27:27', '2020-08-30 07:59:57', 0);
INSERT INTO t_sys_params ("uid", "params_type", "params_name", "params_key", "remark", "params_value", "status", "create_time", "update_time", "sort") VALUES ('1cbff65d4982066709fdfc3d4d2799eb', '1', '三级推荐', 'BLOG_THIRD_COUNT', '首页博客 三级推荐数量', '3', 1, '2020-07-21 22:28:34', '2020-07-21 22:28:34', 0);
INSERT INTO t_sys_params ("uid", "params_type", "params_name", "params_key", "remark", "params_value", "status", "create_time", "update_time", "sort") VALUES ('1ee3e63885637fcee9ab7dcdefbabeaa', '1', '默认密码', 'SYS_DEFAULT_PASSWORD', '管理员默认密码', 'mogu2018', 1, '2020-07-21 17:26:42', '2020-10-05 09:34:33', 2);
INSERT INTO t_sys_params ("uid", "params_type", "params_name", "params_key", "remark", "params_value", "status", "create_time", "update_time", "sort") VALUES ('207812ebc591b6754a5d655c24d4a58d', '1', '最新博客数量', 'BLOG_NEW_COUNT', '首页 最新显示的博客数量', '15', 1, '2020-07-21 22:26:44', '2020-08-30 07:59:46', 0);
INSERT INTO t_sys_params ("uid", "params_type", "params_name", "params_key", "remark", "params_value", "status", "create_time", "update_time", "sort") VALUES ('23b1b89ada85a09bb0075eab906958ce', '1', '令牌过期时间', 'TOKEN_EXPIRE_TIME', '后端令牌过期时间  1小时', '3600', 1, '2020-07-21 22:31:58', '2020-08-20 17:55:46', 0);
INSERT INTO t_sys_params ("uid", "params_type", "params_name", "params_key", "remark", "params_value", "status", "create_time", "update_time", "sort") VALUES ('3434f8d9895956311c1deeb71a7e52e4', '1', '友情链接数', 'FRIENDLY_LINK_COUNT', '友情链接数目', '20', 1, '2020-07-21 22:40:30', '2021-03-11 18:58:49', 0);
INSERT INTO t_sys_params ("uid", "params_type", "params_name", "params_key", "remark", "params_value", "status", "create_time", "update_time", "sort") VALUES ('62bde28353d8de9b7426d778f899830f', '1', '网盘最大容量', 'MAX_STORAGE_SIZE', '单个管理员账号，网盘最大容量，单位MB', '500', 1, '2020-10-09 10:01:25', '2020-10-09 10:12:02', 0);
INSERT INTO t_sys_params ("uid", "params_type", "params_name", "params_key", "remark", "params_value", "status", "create_time", "update_time", "sort") VALUES ('7c3c02a3874ca6fe6fd6207b978cf785', '1', '项目名称', 'PROJECT_NAME', '项目中文名称', '蘑菇博客', 1, '2020-07-28 17:18:59', '2020-07-28 17:18:59', 0);
INSERT INTO t_sys_params ("uid", "params_type", "params_name", "params_key", "remark", "params_value", "status", "create_time", "update_time", "sort") VALUES ('81270e541b3476f61fdae4435407c143', '1', '热门博客数量', 'BLOG_HOT_COUNT', '热门博客数量', '5', 1, '2020-07-21 22:26:02', '2020-07-21 22:26:02', 0);
INSERT INTO t_sys_params ("uid", "params_type", "params_name", "params_key", "remark", "params_value", "status", "create_time", "update_time", "sort") VALUES ('c69da7fccb13d22ee616e0972d5413af', '1', '四级推荐', 'BLOG_FOURTH_COUNT', '首页博客 四级推荐数量', '5', 1, '2020-07-21 22:29:11', '2020-07-21 22:33:19', 0);
INSERT INTO t_sys_params ("uid", "params_type", "params_name", "params_key", "remark", "params_value", "status", "create_time", "update_time", "sort") VALUES ('d1c75145ad11d7ba66d5f88a12c19079', '1', '令牌刷新时间', 'TOKEN_REFRESH_TIME', '后端令牌刷新时间 5分钟', '300', 0, '2020-07-21 22:33:04', '2020-08-20 17:55:44', 0);
INSERT INTO t_sys_params ("uid", "params_type", "params_name", "params_key", "remark", "params_value", "status", "create_time", "update_time", "sort") VALUES ('db28ba09b3edec8a479c6f07e742a31b', '1', '用户令牌存活时间', 'USER_TOKEN_SURVIVAL_TIME', 'toekn令牌存活时间7*24=7天', '168', 0, '2020-07-21 22:41:46', '2020-08-20 17:55:39', 0);

INSERT INTO t_system_config ("uid", "qi_niu_access_key", "qi_niu_secret_key", "email", "email_user_name", "email_password", "smtp_address", "smtp_port", "status", "create_time", "update_time", "qi_niu_bucket", "qi_niu_area", "upload_qi_niu", "upload_local", "picture_priority", "qi_niu_picture_base_url", "local_picture_base_url", "start_email_notification", "editor_model", "theme_color", "minio_end_point", "minio_access_key", "minio_secret_key", "minio_bucket", "upload_minio", "minio_picture_base_url", "open_dashboard_notification", "dashboard_notification", "content_picture_priority", "open_email_activate", "search_model") VALUES ('37d492e35dc6e3fbb9dfedfd2079a123', 'QKD378sek_yRy0AlpWEzT_U_oni0SfrrxaP2lgWX', 'Vjh0zPBLrflxYn08YNIG4rkRYv7sqtlMccgd8QzL', '123456789@qq.com', NULL, '', NULL, '', 1, '2020-01-29 19:14:26', '2021-05-17 10:50:02', 'testmogublog', 'z2', '0', '1', '0', 'http://testimage.moguit.cn/', 'http://localhost:8600/', '1', 0, '#6E6EFE', 'http://101.132.122.175:8080', 'mogu2018', 'mogu2018', 'mogublog', 0, 'http://101.132.122.175:8080', 1, '<p>欢迎来到蘑菇博客项目，开源项目离不开大家的支持，希望小伙伴能随手点赞一下，你的点赞就是我维护的动力~</p>\n\n<p>项目源码：<a href=\"https://gitee.com/moxi159753/mogu_blog_v2\" target=\"_blank\">点我传送</a>&nbsp; ，&nbsp; 项目文档：<a href=\"http://moxi159753.gitee.io/mogu_blog_doc\" target=\"_blank\">点我传送</a>&nbsp; &nbsp; ，项目官网：<a href=\"http://www.moguit.cn/#/\" target=\"_blank\">点我传送</a>，&nbsp;学习笔记：<a href=\"https://gitee.com/moxi159753/LearningNotes\" target=\"_blank\">点我传送</a></p>\n\n<p>蘑菇博客使用了一些监控的Spring Cloud组件，并不一定都需要部署启动，可以根据自身服务器配置来启动</p>\n\n<p>最低配置：1核2G 【<code>需要开启虚拟内存</code>】</p>\n\n<p>推荐配置：2核4G 【双十一特惠】</p>\n\n<p>双11活动开始喽~，本次优惠力度非常大，如果有需求的，欢迎点击下面链接购买</p>\n\n<p>【阿里云】双十一拼团 2核4G3M 664元/3年（强烈推荐）&nbsp;<a href=\"http://a.aliyun.com/f1.l0DRK\"><span style=\"color:#0000ff\">点我进入</span></a></p>\n\n<p>【阿里云】云服务器双11狂欢特惠，1核2G 最低仅需84.97元/年&nbsp;<a href=\"https://www.aliyun.com/1111/home?userCode=w7aungxw\"><span style=\"color:#0000ff\">点我传送</span></a></p>\n\n<p>【腾讯云】双十一活动2核4G 100G盘700元/3年（老用户重新用微信QQ注册即可）<span style=\"color:#0000ff\">&nbsp;</span><a href=\"https://curl.qcloud.com/8Nfp3pRy\"><span style=\"color:#0000ff\">点我进入</span></a></p>\n\n<p><span style=\"color:#daa520\">tip：仪表盘通知可以在&nbsp; 系统配置 -&gt; 仪表盘通知&nbsp; 处进行关闭</span></p>\n', 0, 1, 0);

INSERT INTO t_use_power ("uid", "user_astrict", "user_limit", "vip_astrict", "vip_limit", "create_time", "update_time", "status", "project_uid") VALUES ('1', 1, 1, 0, 10, '2021-12-04 21:38:20', '2021-12-04 21:38:20', 1, '8489b33dbf74e545548af99e3daab352');
INSERT INTO t_use_power ("uid", "user_astrict", "user_limit", "vip_astrict", "vip_limit", "create_time", "update_time", "status", "project_uid") VALUES ('2', 0, NULL, 0, NULL, '2021-12-04 21:06:39', '2021-12-04 21:06:42', 1, 'b7f0ed2b247a49498e138db5dd62dc85');
