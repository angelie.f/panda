-- 同城约妹表

CREATE TABLE `t_girl`  (
  `uid` int(32) NOT NULL AUTO_INCREMENT COMMENT '唯一uid',
  `video_uid` int(32) NULL DEFAULT NULL COMMENT '小视频uid',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '妹子名称',
  `introduce` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '简介',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态，1',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `age` int(11) NULL DEFAULT NULL COMMENT '年龄',
  `price_one` decimal(10, 2) NULL DEFAULT NULL COMMENT '价格（次）',
  `price_night` decimal(10, 2) NULL DEFAULT NULL COMMENT '价格（包夜）',
  `city_uid` int(32) NULL DEFAULT NULL COMMENT '城市uid',
  `height` int(11) NULL DEFAULT NULL COMMENT '身高（cm）',
  `weight` int(11) NULL DEFAULT NULL COMMENT '体重（kg）',
  `cup` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '罩杯',
  `occupation` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '职业',
  `tag_uid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '妹子标签uid:1,2,...',
  `count` int(100) NULL DEFAULT NULL COMMENT '图片数量',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6293 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '妹子信息表' ROW_FORMAT = COMPACT;


CREATE TABLE `t_girl_city`  (
  `uid` int(32) NOT NULL AUTO_INCREMENT COMMENT '唯一uid',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '城市名称',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态:1',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2132 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '妹子城市表' ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_girl_order`  (
  `uid` int(32) NOT NULL AUTO_INCREMENT COMMENT '唯一uid',
  `order_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '订单ID',
  `third_order_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '流水号',
  `amount` decimal(10, 2) NOT NULL COMMENT '金额',
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户ID',
  `user_nick` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户昵称',
  `girl_uid` int(11) NOT NULL COMMENT '妹子uid',
  `girl_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '妹子名称',
  `pay_count` int(11) NULL DEFAULT NULL COMMENT '支付次数:1.首次;2.续费2;...;n.续费n',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '订单状态:0.未完成，1.已完成',
  `pay_status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '支付状态:0.待付款，.1已支付，2.已过期',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `pay_time` timestamp NULL DEFAULT NULL COMMENT '支付时间',
  `customer_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '会员ID',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 566 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_girl_picture`  (
  `uid` int(32) NOT NULL AUTO_INCREMENT COMMENT '唯一uid',
  `file_uid` int(32) NULL DEFAULT NULL COMMENT '图片文件uid',
  `pic_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片名',
  `girl_uid` int(32) NULL DEFAULT NULL COMMENT '妹子uid',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 374923 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '妹子图片表' ROW_FORMAT = COMPACT;

CREATE TABLE `t_girl_project`  (
  `uid` int(32) NOT NULL AUTO_INCREMENT COMMENT '唯一uid',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '项目名称',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `status` tinyint(1) NULL DEFAULT NULL COMMENT '状态',
  `source` int(11) NULL DEFAULT NULL COMMENT '来源:1.青青草；2.小黄鸭',
  `is_vip` tinyint(1) NULL DEFAULT NULL COMMENT '是否会员:1.会员；0.普通用户',
  `on_off` tinyint(1) NULL DEFAULT NULL COMMENT '项目开关:1.开启；0.关闭',
  `authentication` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '认证信息',
  `pro_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'KEY',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '妹子项目表' ROW_FORMAT = COMPACT;

CREATE TABLE `t_girl_sort`  (
  `uid` int(32) NOT NULL AUTO_INCREMENT COMMENT '唯一uid',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '妹子分类名称',
  `introduce` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分类介绍',
  `file_uid` int(32) NULL DEFAULT NULL COMMENT '分类图标图片uid',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序字段，越大越靠前',
  `tag_uid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签uid:1,2,...',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `project_uid` int(32) NOT NULL COMMENT '妹子分类所属项目uid',
  `status` tinyint(1) NOT NULL COMMENT '状态',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '妹子分类表' ROW_FORMAT = COMPACT;

CREATE TABLE `t_girl_sort_item`  (
  `uid` int(32) NOT NULL AUTO_INCREMENT COMMENT '唯一uid',
  `sort_uid` int(32) NULL DEFAULT NULL COMMENT '妹子分类uid',
  `girl_uid` int(32) NULL DEFAULT NULL COMMENT '妹子uid',
  `status` int(1) NULL DEFAULT NULL COMMENT '转态:1上架，0下架',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 67099 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '妹子上下架表' ROW_FORMAT = COMPACT;

CREATE TABLE `t_girl_tag`  (
  `uid` int(32) NOT NULL AUTO_INCREMENT COMMENT '唯一uid',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '妹子标签内容',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `introduce` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签介绍',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2132 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '图片标签表' ROW_FORMAT = DYNAMIC;

CREATE TABLE `t_girl_user_cost`  (
  `uid` int(32) NOT NULL AUTO_INCREMENT COMMENT '唯一uid',
  `level_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '会员等级ID',
  `level_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '会员等级名称',
  `cost` decimal(10, 2) UNSIGNED NULL DEFAULT 1.00 COMMENT '平台费用',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态:1',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `project_uid` int(32) NOT NULL COMMENT '妹子项目uid',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2133 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户费用列表' ROW_FORMAT = DYNAMIC;


-- 公共资源中心用户表新增字段
alter table t_customers add param text COMMENT '用户原始数据';
alter table t_customers add vip_level int(4) COMMENT 'vip等级';
alter table t_customers add vip_type varchar(100) COMMENT 'vip类型';

-- 公共配置表新增字段和类容
alter table t_system_config add service_url varchar(255) COMMENT '在线客服地址';
UPDATE t_system_config SET `service_url` = 'https://qqcfaq.com/new/servicemeiqia.html' WHERE `uid` = 1;

-- 新增菜单
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (221, '同城约妹', 2, '同城约妹 项目管理', '220', '/project/girlProject', 'el-icon-picture', 0, 1, '2022-01-05 20:08:09', '2022-01-05 20:09:04', 1, 0, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (220, '项目管理', 1, '项目管理 一级菜单', NULL, '/project', 'el-icon-picture', 1, 1, '2022-01-05 20:04:39', '2022-01-05 20:09:27', 1, 0, 0);
UPDATE t_category_menu SET `name` = '图集', `menu_level` = 2, `summary` = '图集 项目管理', `parent_uid` = '220', `url` = '/project/project', `icon` = 'el-icon-picture', `sort` = 0, `status` = 1, `create_time` = '2021-11-23 19:12:18', `update_time` = '2022-01-05 20:47:15', `is_show` = 1, `menu_type` = 0, `is_jump_external_url` = 0 WHERE `uid` = 101;

INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (222, '项目查询', 3, '同城约妹项目查询接口', '221', '/girlProject/getList', NULL, 0, 1, '2022-01-05 20:43:00', '2022-01-05 20:43:00', 1, 1, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (223, '新增项目', 3, '同城约妹新增项目接口', '221', '/girlProject/add', NULL, 0, 1, '2022-01-05 20:44:14', '2022-01-05 20:44:14', 1, 1, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (224, '编辑项目', 3, '同城约妹编辑项目接口', '221', '/girlProject/edit', NULL, 0, 1, '2022-01-05 20:44:51', '2022-01-05 20:44:51', 1, 1, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (225, '用户费用查询', 3, '同城约妹用户费用查询接口', '221', '/girlUserCost/getList', NULL, 0, 1, '2022-01-05 21:04:28', '2022-01-05 21:04:28', 1, 1, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (226, '用户费用新增', 3, '同城约妹用户费用新增接口', '221', '/girlUserCost/add', NULL, 0, 1, '2022-01-05 21:05:01', '2022-01-05 21:05:01', 1, 1, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (227, '用户费用编辑', 3, '同城约妹用户费用编辑接口', '221', '/girlUserCost/edit', NULL, 0, 1, '2022-01-05 21:05:23', '2022-01-05 21:05:23', 1, 1, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (228, '内容管理', 1, '同城约妹 内容管理', NULL, '/girlInfo', 'el-icon-coin', 0, 1, '2022-01-06 18:55:47', '2022-01-06 18:55:47', 1, 0, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (229, ' 妹子标签', 2, '同城约妹标签管理', '228', '/girlInfo/girlTag', 'el-icon-folder-opened', 1, 1, '2022-01-06 18:57:20', '2022-01-10 12:05:55', 1, 0, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (230, '城市管理', 2, '同城约妹 城市管理', '228', '/girlInfo/girlCity', 'el-icon-school', 0, 1, '2022-01-06 18:58:50', '2022-01-06 18:58:50', 1, 0, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (231, '查询', 3, '同城约妹 妹子标签查询', '229', '/girlTag/getList', NULL, 0, 1, '2022-01-06 19:41:11', '2022-01-06 19:41:11', 1, 1, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (232, '新增', 3, '同城约妹 妹子标签新增', '229', '/girlTag/add', NULL, 0, 1, '2022-01-06 19:41:31', '2022-01-06 19:41:31', 1, 1, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (233, '编辑', 3, '同城约妹 妹子标签编辑', '229', '/girlTag/edit', NULL, 0, 1, '2022-01-06 19:41:58', '2022-01-06 19:41:58', 1, 1, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (234, '查询', 3, '同城约妹 妹子城市查询', '230', '/girlCity/getList', NULL, 0, 1, '2022-01-06 19:43:28', '2022-01-06 19:43:28', 1, 1, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (235, '新增', 3, '同城约妹 妹子城市新增', '230', '/girlCity/add', NULL, 0, 1, '2022-01-06 19:43:52', '2022-01-06 19:43:52', 1, 1, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (236, '编辑', 3, '同城约妹 妹子城市编辑', '230', '/girlCity/edit', NULL, 0, 1, '2022-01-06 19:44:14', '2022-01-06 19:44:14', 1, 1, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (237, '订单列表', 2, '同城约妹 订单列表', '228', '/girlInfo/girlOrder', 'el-icon-tickets', 0, 1, '2022-01-07 15:54:31', '2022-01-07 15:55:08', 1, 0, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (238, '查询', 3, '同城约妹 订单列表查询', '237', '/girlOrder/getList', NULL, 0, 1, '2022-01-07 15:58:55', '2022-01-07 15:58:55', 1, 1, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (239, '编辑', 3, '同城约妹 订单列表编辑', '237', '/girlOrder/edit', NULL, 0, 1, '2022-01-07 15:59:24', '2022-01-07 15:59:24', 1, 1, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (240, '妹子资源管理', 2, '同城约妹 妹子资源管理', '228', '/girlInfo/girl', 'el-icon-female', 0, 1, '2022-01-10 11:57:44', '2022-01-10 11:57:44', 1, 0, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (241, '导出', 3, '同城约妹 订单列表导出', '237', '/girlTag/export', NULL, 0, 1, '2022-01-10 11:59:52', '2022-01-10 12:00:24', 1, 1, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (242, '查询', 3, '同城约妹 妹子资源查询', '240', '/girl/getList', NULL, 0, 1, '2022-01-10 12:12:12', '2022-01-10 12:12:12', 1, 1, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (243, '新增', 3, '同城约妹 妹子资源新增', '240', '/girl/add', NULL, 0, 1, '2022-01-10 12:13:24', '2022-01-10 12:13:24', 1, 1, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (244, '编辑', 3, '同城约妹 妹子资源编辑', '240', '/girl/edit', NULL, 0, 1, '2022-01-10 12:13:45', '2022-01-10 12:13:45', 1, 1, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (245, '配置管理', 1, '同城约妹 配置管理', NULL, '/allot', 'el-icon-wallet', 0, 1, '2022-01-11 13:31:01', '2022-01-11 13:55:00', 1, 0, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (246, '妹子分类', 2, '同城约妹 妹子分类', '245', '/allot/girlSort', 'el-icon-female', 0, 1, '2022-01-11 13:33:55', '2022-01-12 13:38:37', 1, 0, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (247, '查询', 3, '同城约妹 分类查询', '246', '/girlSort/getList', NULL, 0, 1, '2022-01-17 10:34:06', '2022-01-17 10:35:59', 1, 1, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (248, '新增', 3, '同城约妹 新增分类', '246', '/girlSort/add', NULL, 0, 1, '2022-01-17 10:34:27', '2022-01-17 10:35:49', 1, 1, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (249, '编辑', 3, '同城约妹 编辑分类', '246', '/girlSort/edit', NULL, 0, 1, '2022-01-17 10:35:35', '2022-01-17 10:35:35', 1, 1, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (250, '妹子查询', 3, '同城约妹 分类里面的妹子查询', '246', '/girl/getGirllistBySort', NULL, 0, 1, '2022-01-17 10:37:15', '2022-01-17 10:37:15', 1, 1, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (251, '上下架', 3, '同城约妹 妹子上下架', '246', '/girl/upOrOut', NULL, 0, 1, '2022-01-17 10:37:39', '2022-01-17 10:37:39', 1, 1, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (252, '删除', 3, '同城约妹 城市删除接口', '230', '/girlCity/delete', NULL, 0, 1, '2022-01-25 15:54:06', '2022-01-25 15:54:06', 1, 1, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (253, '删除', 3, '同城约妹 妹子资源删除接口', '240', '/girl/delete', NULL, 0, 1, '2022-01-25 15:54:41', '2022-01-25 15:54:41', 1, 1, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (254, '删除', 3, '同城约妹 分类删除接口', '246', '/girlSort/delete', NULL, 0, 1, '2022-01-25 15:55:19', '2022-01-25 15:55:19', 1, 1, 0);
INSERT INTO t_category_menu (`uid`, `name`, `menu_level`, `summary`, `parent_uid`, `url`, `icon`, `sort`, `status`, `create_time`, `update_time`, `is_show`, `menu_type`, `is_jump_external_url`) VALUES (255, '删除', 3, '同城约妹 标签删除接口', '229', '/girlTag/delete', NULL, 0, 1, '2022-01-25 16:43:29', '2022-01-25 16:43:29', 1, 1, 0);

UPDATE t_role SET `role_name` = '系统管理员', `create_time` = '2018-10-16 07:56:26', `update_time` = '2022-01-17 12:42:26', `status` = 1, `summary` = '系统管理员，管理全部菜单和功能', `category_menu_uids` = '[\"155\",\"125\",\"23\",\"45\",\"131\",\"172\",\"197\",\"106\",\"70\",\"35\",\"54\",\"183\",\"210\",\"42\",\"33\",\"52\",\"92\",\"180\",\"204\",\"143\",\"96\",\"74\",\"185\",\"212\",\"113\",\"122\",\"206\",\"120\",\"207\",\"65\",\"205\",\"80\",\"169\",\"7\",\"86\",\"93\",\"98\",\"138\",\"151\",\"170\",\"217\",\"59\",\"109\",\"72\",\"20\",\"60\",\"73\",\"157\",\"191\",\"67\",\"34\",\"53\",\"133\",\"181\",\"208\",\"220\",\"221\",\"222\",\"223\",\"224\",\"225\",\"226\",\"227\",\"101\",\"43\",\"139\",\"162\",\"165\",\"228\",\"229\",\"231\",\"232\",\"233\",\"240\",\"242\",\"243\",\"244\",\"237\",\"238\",\"239\",\"241\",\"230\",\"234\",\"235\",\"236\",\"245\",\"246\",\"247\",\"248\",\"249\",\"250\",\"251\"]' WHERE `uid` = 2;
