package com.moxi.mogublog.web.restapi;

import com.moxi.mogublog.utils.RedisUtil;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.service.*;
import com.moxi.mogublog.xo.vo.*;
import com.moxi.mougblog.base.global.BaseMessageConf;
import com.moxi.mougblog.base.validator.group.GetList;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 漫画模块 RestApi
 *
 * @author Sid
 * @date 2022-05-24
 */
@RestController
@Api(value = "/漫画controller", tags = "漫画")
@RequestMapping("/localBooks")
@Slf4j
public class LocalBooksRestApi {

    @Autowired
    private TpTopicsService tpTopicsService;

    @Autowired
    private TpBooksService tpBooksService;

    @Autowired
    private TpBookChaptersService tpBookChaptersService;

    @Autowired
    private ReadingLogService readingLogService;

    @Autowired
    private BooksUserCostService booksUserCostService;

    @Autowired
    private TpPayService tpPayService;

    @Autowired
    private RedisUtil redisUtil;

    @ApiOperation(value = "获取漫画主题列表", notes = "获取漫画主题列表", response = String.class)
    @PostMapping(value = "/getBooksTopicList")
    public String getTopicList(@Validated({GetList.class}) @RequestBody LocalBooksVO localBooksVO) {

        log.info("获取漫画主题列表: {}", localBooksVO);
        return ResultUtil.result(SysConf.SUCCESS,tpTopicsService.getTopicList(localBooksVO));
    }

    @ApiOperation(value = "获取漫画列表", notes = "获取漫画列表", response = String.class)
    @PostMapping(value = "/getBooksList")
    public String getBooksList(@Validated({GetList.class}) @RequestBody LocalBooksVO localBooksVO) {
        if(StringUtils.isEmpty(localBooksVO.getKeyWord())&&localBooksVO.getTopicId()==null){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.TOPICID_OR_KEYWORD_NULL);
        }

        return tpBooksService.getBooksList(localBooksVO);

    }
    @ApiOperation(value = "获取相关联漫画列表", notes = "获取相关联漫画列表", response = String.class)
    @PostMapping(value = "/getRelatedBooksList")
    public String getRelatedBooksList(@Validated({GetList.class}) @RequestBody LocalBooksVO localBooksVO) {
        return tpBooksService.getRelatedBooksList(localBooksVO);

    }
    @ApiOperation(value = "获取漫画详情列表", notes = "获取漫画详情列表", response = String.class)
    @PostMapping(value = "/getBooksDetail")
    public String getBooksDetail(@RequestBody LocalBooksVO localBooksVO) {
        if (StringUtils.isEmpty(localBooksVO.getSource())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.SOURCE_NULL);
        }
        if (StringUtils.isEmpty(localBooksVO.getCustomerId())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.CUSTOMER_ID_NULL);
        }
        if(localBooksVO.getBookId()==null){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.BOOK_ID_NULL);
        }
        log.info("获取漫画详情: {}", localBooksVO);
        return tpBookChaptersService.getBookChapters(localBooksVO);
    }

    @ApiOperation(value = "获取特权说明", notes = "获取特权说明", response = String.class)
    @PostMapping(value = "/getBooksUserCost")
    public String getBooksUserCost(@RequestBody LocalBooksVO localBooksVO) {
        log.info("获取特权说明: {}", localBooksVO);
        return booksUserCostService.getList(localBooksVO);
    }

    @ApiOperation(value = "取得阅读记录", notes = "取得阅读记录", response = String.class)
    @PostMapping(value = "/getReadingLog")
    public String getReadingLog(@RequestBody LocalBooksVO localBooksVO) {
        if (StringUtils.isEmpty(localBooksVO.getSource())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.SOURCE_NULL);
        }
        if (StringUtils.isEmpty(localBooksVO.getCustomerId())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.CUSTOMER_ID_NULL);
        }

        return ResultUtil.result(SysConf.SUCCESS, readingLogService.getReadingLogInfo(localBooksVO));
    }

    @ApiOperation(value = "更新阅读记录", notes = "更新阅读记录", response = String.class)
    @PostMapping(value = "/updateReadingLog")
    public String updateReadingLog(@RequestBody LocalBooksVO localBooksVO) {
        if (StringUtils.isEmpty(localBooksVO.getSource())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.SOURCE_NULL);
        }
        if (StringUtils.isEmpty(localBooksVO.getCustomerId())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.CUSTOMER_ID_NULL);
        }
        if(localBooksVO.getBookId()==null){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.BOOK_ID_NULL);
        }
        return ResultUtil.result(SysConf.SUCCESS, readingLogService.updateReadingLog(localBooksVO));
    }


    @ApiOperation(value = "漫画首页接口", notes = "漫画首页接口", response = String.class)
    @PostMapping(value = "/index")
    public String index(@Validated({GetList.class}) @RequestBody LocalBooksVO localBooksVO) {

        log.info("漫画首页接口参数: {}", localBooksVO);
        return ResultUtil.result(SysConf.SUCCESS,tpTopicsService.getTopicAndDetails(localBooksVO));
    }

    @ApiOperation(value = "扣费接口", notes = "扣费接口", response = String.class)
    @PostMapping(value = "/pay")
    public String pay(@RequestBody LocalBooksVO localBooksVO) {
        if (StringUtils.isEmpty(localBooksVO.getSource())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.SOURCE_NULL);
        }
        if (StringUtils.isEmpty(localBooksVO.getCustomerId())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.CUSTOMER_ID_NULL);
        }
        if(localBooksVO.getBookId()==null){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.BOOK_ID_NULL);
        }
        if(localBooksVO.getEpisode()==null){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.EPISODE_ID_NULL);
        }
        return tpPayService.pay(localBooksVO);
    }

    @ApiOperation(value = "记录使用可看漫画章节", notes = "记录使用可看漫画章节", response = String.class)
    @PostMapping(value = "/logDailyAvailable")
    public String logDailyAvailable(@RequestBody LocalBooksVO localBooksVO) {
        if (StringUtils.isEmpty(localBooksVO.getSource())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.SOURCE_NULL);
        }
        if (StringUtils.isEmpty(localBooksVO.getCustomerId())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.CUSTOMER_ID_NULL);
        }
        if (localBooksVO.getBookId()==null){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.BOOK_ID_NULL);
        }
        if (localBooksVO.getEpisode()==null){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.EPISODE_ID_NULL);
        }
        return readingLogService.logDailyAvailable(localBooksVO);
    }

    @ApiOperation(value = "取得用户可看漫画资讯", notes = "取得用户可看漫画资讯", response = String.class)
    @PostMapping(value = "/getDailyAvailable")
    public String getDailyAvailable(@RequestBody LocalBooksVO localBooksVO) {
        if (StringUtils.isEmpty(localBooksVO.getSource())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.SOURCE_NULL);
        }
        if (StringUtils.isEmpty(localBooksVO.getCustomerId())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.CUSTOMER_ID_NULL);
        }
        return readingLogService.getDailyAvailable(localBooksVO);
    }
}

