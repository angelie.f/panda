package com.moxi.mogublog.web.restapi;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.moxi.mogublog.commons.entity.Customers;
import com.moxi.mogublog.commons.entity.GirlProject;
import com.moxi.mogublog.commons.entity.SystemConfig;
import com.moxi.mogublog.commons.entity.User;
import com.moxi.mogublog.commons.feign.PictureFeignClient;
import com.moxi.mogublog.utils.*;
import com.moxi.mogublog.web.global.MessageConf;
import com.moxi.mogublog.web.global.RedisConf;
import com.moxi.mogublog.web.global.SQLConf;
import com.moxi.mogublog.web.global.SysConf;
import com.moxi.mogublog.xo.service.*;
import com.moxi.mogublog.xo.utils.RabbitMqUtil;
import com.moxi.mogublog.xo.utils.WebUtil;
import com.moxi.mogublog.xo.vo.*;
import com.moxi.mougblog.base.enums.EStatus;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.global.BaseMessageConf;
import com.moxi.mougblog.base.global.Constants;
import com.moxi.mougblog.base.holder.RequestHolder;
import com.moxi.mougblog.base.validator.group.GetList;
import com.moxi.mougblog.base.validator.group.GetOne;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URLDecoder;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * 用户登录RestApi，系统自带的登录注册功能
 * 第三方登录请移步AuthRestApi
 *
 * @author 陌溪
 * @date 2020年5月6日17:50:23
 */
@RestController
@RefreshScope
@RequestMapping("/login")
@Api(value = "登录管理相关接口", tags = {"登录管理相关接口"})
@Slf4j
public class LoginRestApi {

    @Autowired
    private RabbitMqUtil rabbitMqUtil;
    @Autowired
    private WebConfigService webConfigService;
    @Resource
    private PictureFeignClient pictureFeignClient;
    @Autowired
    private WebUtil webUtil;
    @Autowired
    private UserService userService;
    @Autowired
    private CustomersService customersService;
    @Autowired
    private GirlProjectService girlProjectService;
    @Autowired
    private GirlUserCostService girlUserCostService;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private SystemConfigService systemConfigService;
    @Value(value = "${BLOG.USER_TOKEN_SURVIVAL_TIME}")
    private Long userTokenSurvivalTime;

    @ApiOperation(value = "用户登录", notes = "用户登录")
    @PostMapping("/login")
    public String login(@Validated({GetOne.class}) @RequestBody UserVO userVO, BindingResult result) {
        ThrowableUtils.checkParamArgument(result);
        Boolean isOpenLoginType = webConfigService.isOpenLoginType(RedisConf.PASSWORD);
        if (!isOpenLoginType) {
            return ResultUtil.result(SysConf.ERROR, "后台未开启该登录方式!");
        }
        String userName = userVO.getUserName();
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.and(wrapper -> wrapper.eq(SQLConf.USER_NAME, userName).or().eq(SQLConf.EMAIL, userName));
        queryWrapper.last(SysConf.LIMIT_ONE);
        User user = userService.getOne(queryWrapper);
        if (user == null || EStatus.DISABLED == user.getStatus()) {
            return ResultUtil.result(SysConf.ERROR, "用户不存在");
        }
        if (EStatus.FREEZE == user.getStatus()) {
            return ResultUtil.result(SysConf.ERROR, "用户账号未激活");
        }
        if (StringUtils.isNotEmpty(user.getPassWord()) && user.getPassWord().equals(MD5Utils.string2MD5(userVO.getPassWord()))) {
            // 更新登录信息
            HttpServletRequest request = RequestHolder.getRequest();
            String ip = IpUtils.getIpAddr(request);
            Map<String, String> userMap = IpUtils.getOsAndBrowserInfo(request);
            user.setBrowser(userMap.get(SysConf.BROWSER));
            user.setOs(userMap.get(SysConf.OS));
            user.setLastLoginIp(ip);
            user.setLastLoginTime(new Date());
            user.updateById();
            // 获取用户头像
            if (!StringUtils.isEmpty(user.getAvatar())) {
                String avatarResult = pictureFeignClient.getPicture(user.getAvatar(), ",");
                List<String> picList = webUtil.getPicture(avatarResult);
                if (picList != null && picList.size() > 0) {
                    user.setPhotoUrl(webUtil.getPicture(avatarResult).get(0));
                }
            }
            // 生成token
            String token = StringUtils.getUUID();
            // 过滤密码
            user.setPassWord("");
            //将从数据库查询的数据缓存到redis中
            redisUtil.setEx(RedisConf.USER_TOKEN + Constants.SYMBOL_COLON + token, JsonUtils.objectToJson(user), userTokenSurvivalTime, TimeUnit.HOURS);
            log.info("登录成功，返回token: ", token);
            return ResultUtil.result(SysConf.SUCCESS, token);
        } else {
            return ResultUtil.result(SysConf.ERROR, "账号或密码错误");
        }
    }

    @ApiOperation(value = "用户注册", notes = "用户注册")
    @PostMapping("/register")
    public String register(@Valid @RequestBody QQCLoginRequest request, HttpServletRequest httpServletRequest) {
        return registerCustomers(request,"");
    }

    @ApiOperation(value = "同城约妹用户注册", notes = "同城约妹用户注册")
    @PostMapping("/localGirlRegister")
    public String localGirlRegister(@Valid @RequestBody QQCLoginRequest request, HttpServletRequest httpServletRequest) {
        return registerCustomers(request,"localGirl");
    }

    /*
    * 注册公共资源平台用户
    * */
    public String registerCustomers(@Valid @RequestBody QQCLoginRequest request, String type) {
        Map<String,String> map = new HashMap<>();
        log.info("加密串:{}",request.getUserInfo());
        String params = URLDecoder.decode(AESUtils.decryptForShop(request.getUserInfo()));
        log.info("解密后内容：{}",params);
        params = params.trim();
        if (params.startsWith("{")) {
            map = JSONObject.parseObject(params, TreeMap.class);
        }else {
            String[]paraArray = params.split("&");
            for (int i=0;i<paraArray.length;i++){
                String str = paraArray[i];
                if (str.startsWith("token")){
                    map.put("token",str.substring(6));
                }else {
                    String[] pArray = str.split("=");
                    if (pArray != null && pArray.length > 1) {
                        map.put(pArray[0], pArray[1]);
                    }
                }
            }
        }
        request = mapToRequest(map,request);
        Customers tCustomers = getCustomers(request);
        String key = "register_"+tCustomers.getSource()+"_"+tCustomers.getOpenId();
        boolean isLock = redisUtil.setIfAbsent(key,"",30*1000,TimeUnit.MILLISECONDS);
        if (isLock) {
            LoginGameResponse response;
            try {
                tCustomers.setRegisterSource(1);
                response = customersService.registerCustomer(tCustomers, request);
                if (response.getIsVip() == 1) {//如果当前用户是vip则刷新vip等级和vip类型
                    QueryWrapper<Customers> queryWrapper = new QueryWrapper<>();
                    queryWrapper.eq("customer_id", response.getCustomersId());
                    CustomersRefreshVO customersRefreshVO = new CustomersRefreshVO();
                    customersRefreshVO.setCustomerId(response.getCustomersId());
                    customersRefreshVO.setPlatform(response.getPlatform());
                    customersRefreshVO.setAppToken(response.getAppToken());
                    customersRefreshVO.setSource(tCustomers.getSource());

                    //刷新vip等级和vip类型
                    customersService.refreshCustomer(customersRefreshVO);
                }
                if ("localGirl".equals(type)) {//同城约妹需要获取vip、项目开关
                    GirlProjectVO girlProjectVO = new GirlProjectVO();
                    girlProjectVO.setSource(response.getSource());
                    List<GirlProject> girlProjectList = girlProjectService.getList(girlProjectVO);
                    response.setVipAstrict(girlProjectList.get(0).getIsVip());//vip开关
                    response.setOnOff(girlProjectList.get(0).getOnOff());//项目开关
                    response.setAuthentication(girlProjectList.get(0).getAuthentication());//认证信息
                    QueryWrapper<Customers> queryWrapper = new QueryWrapper<>();
                    queryWrapper.eq("customer_id", response.getCustomersId());
                    Customers customers = customersService.getOne(queryWrapper);
                    if (customers != null && customers.getVipLevel() != null) {
                        response.setVipLevel(customers.getVipLevel());
                    }
                }
                return ResultUtil.resultWithData(SysConf.SUCCESS, response);
            } catch (Exception e) {
                log.error("customer login fail", e);
                return ResultUtil.result(SysConf.ERROR, MessageConf.OPERATION_FAIL);
            } finally {
                if (redisUtil.hasKey(key))
                    redisUtil.delete(key);
            }
        }
        return ResultUtil.result(com.moxi.mogublog.xo.global.SysConf.ERROR, BaseMessageConf.SYSTEM_BUSY);
    }

    private QQCLoginRequest mapToRequest(Map<String,String>map,QQCLoginRequest request){
        request.setApp(map.get("app"));
        request.setIsVip(Integer.valueOf(map.get("is_vip")));
        request.setUserHeadimg(map.get("user_head_img"));
        request.setUserId(map.get("id"));
        request.setUserNickname(map.get("nickname"));
        request.setUserPhone(map.get("mobile"));
        request.setUserToken(map.get("token"));
        request.setDomain(map.get("domain"));
        request.setSource(Integer.valueOf(map.get("source")));
        request.setPlatform(map.get("platform")!=null&&Integer.valueOf(map.get("platform"))==1?map.get("platform"):"2");
        //request.setIsH5(map.get("is_h5")==null?0:Integer.valueOf(map.get("is_h5")));
        return request;
    }

    @ApiOperation(value = "用户刷新", notes = "用户刷新")
    @PostMapping("/refresh")
    public String refresh(@Validated({GetList.class}) @RequestBody CustomersRefreshVO customersRefreshVO, BindingResult result) {
        log.info("刷新用户信息: {}", customersRefreshVO);
        if (!verify(customersRefreshVO)){
            return ResultUtil.result(com.moxi.mogublog.xo.global.SysConf.ERROR, BaseMessageConf.VERIFY_ERROR);
        }
        LoginGameResponse response = customersService.refreshCustomer(customersRefreshVO);
        return ResultUtil.resultWithData(SysConf.SUCCESS, response);
    }


    private Customers getCustomers(@Valid @RequestBody QQCLoginRequest request) {
        Customers tCustomers = new Customers();
        tCustomers.setProductId("V02");
        tCustomers.setPhone(request.getUserPhone());
        if (request.getUserNickname()!=null) {
            tCustomers.setLoginName(request.getUserNickname().trim());
        }else {
            tCustomers.setLoginName("");
        }
        tCustomers.setOpenCode("QQC");
        tCustomers.setOpenId(request.getUserId());
        String phone = request.getUserPhone();
        if (org.springframework.util.StringUtils.hasText(phone)) {
            if (phone.contains(" ")) {
                tCustomers.setPhone(request.getUserPhone().split("\\ ")[1]);
            } else {
                tCustomers.setPhone(request.getUserPhone());
            }
        }
        tCustomers.setIsVip(request.getIsVip());
        tCustomers.setVipLevel(request.getVipLevel()==null?0:Integer.valueOf(request.getVipLevel()));
        tCustomers.setVipType(request.getVipType());
        tCustomers.setSource(request.getSource());
        return tCustomers;
    }

    @ApiOperation(value = "激活用户账号", notes = "激活用户账号")
    @GetMapping("/activeUser/{token}")
    public String bindUserEmail(@PathVariable("token") String token) {
        // 从redis中获取用户信息
        String userInfo = redisUtil.get(RedisConf.ACTIVATE_USER + RedisConf.SEGMENTATION + token);
        if (StringUtils.isEmpty(userInfo)) {
            return ResultUtil.result(SysConf.ERROR, MessageConf.INVALID_TOKEN);
        }
        User user = JsonUtils.jsonToPojo(userInfo, User.class);
        if (EStatus.FREEZE != user.getStatus()) {
            return ResultUtil.result(SysConf.ERROR, "用户账号已经被激活");
        }
        user.setStatus(EStatus.ENABLE);
        user.updateById();

        // 更新成功后，需要把该用户名下其它未激活的用户删除【删除】
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SQLConf.USER_NAME, user.getUserName());
        queryWrapper.ne(SQLConf.UID, user.getUid());
        queryWrapper.ne(SQLConf.STATUS, EStatus.ENABLE);
        List<User> userList = userService.list(queryWrapper);
        if (userList.size() > 0) {
            List<String> uidList = new ArrayList<>();
            userList.forEach(item -> {
                uidList.add(item.getUid());
            });
            // 移除所有未激活的用户【该用户名下的】
            userService.removeByIds(uidList);
        }

        return ResultUtil.result(SysConf.SUCCESS, MessageConf.OPERATION_SUCCESS);
    }

    @ApiOperation(value = "退出登录", notes = "退出登录", response = String.class)
    @PostMapping(value = "/logout")
    public String logout(@ApiParam(name = "token", value = "token令牌", required = false) @RequestParam(name = "token", required = false) String token) {
        if (StringUtils.isEmpty(token)) {
            return ResultUtil.result(SysConf.ERROR, MessageConf.OPERATION_FAIL);
        }
        redisUtil.set(RedisConf.USER_TOKEN + Constants.SYMBOL_COLON + token, "");
        return ResultUtil.result(SysConf.SUCCESS, "退出成功");
    }

    public Boolean verify(CustomersRefreshVO customersRefreshVO){
        SystemConfig systemConfig = systemConfigService.list().get(0);
        JSONObject jsonObj = (JSONObject) JSON.toJSON(customersRefreshVO);
        TreeMap<String, Object> map = JSONObject.parseObject(jsonObj.toJSONString(), new TypeReference<TreeMap<String, Object>>(){});
        map.remove("sign");
        String signStr = MD5Utils.getMD5String(map,systemConfig.getMd5Key());
        log.info("签名原串: {}", signStr);
        String sign = MD5Utils.md5Sign(signStr);
        log.info("md5加密后的签名: {}", sign);
        if (sign.equals(customersRefreshVO.getSign())){
            return true;
        }
        return false;
    }

}
