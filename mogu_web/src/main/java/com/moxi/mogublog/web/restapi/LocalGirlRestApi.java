package com.moxi.mogublog.web.restapi;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.*;
import com.moxi.mogublog.utils.MD5Utils;
import com.moxi.mogublog.utils.RedisUtil;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.service.*;
import com.moxi.mogublog.xo.vo.*;
import com.moxi.mougblog.base.enums.EStatus;
import com.moxi.mougblog.base.global.BaseMessageConf;
import com.moxi.mougblog.base.validator.group.GetList;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * 同城约妹模块 RestApi
 *
 * @author Vergift
 * @date 2021-12-29
 */
@RestController
@Api(value = "/同城约妹controller", tags = "同城约妹模块")
@RequestMapping("/localGirl")
@Slf4j
public class LocalGirlRestApi {

    @Autowired
    private GirlCityService girlCityService;

    @Autowired
    private GirlSortService girlSortService;

    @Autowired
    private GirlService girlService;

    @Autowired
    private GirlSortItemService girlSortItemService;

    @Autowired
    private GirlPictureService girlPictureService;

    @Autowired
    private GirlOrderService girlOrderService;

    @Autowired
    private SystemConfigService systemConfigService;

    @Autowired
    private GirlProjectService girlProjectService;

    @Autowired
    private GirlOrderCommentService girlOrderCommentService;

    @Autowired
    private GirlGiftService girlGiftService;

    @Autowired
    private GirlUserCostService girlUserCostService;

    @Value(value = "${T08.url}")
    private String t08Url;

    @Autowired
    private RedisUtil redisUtil;

    @ApiOperation(value = "获取妹子分类列表", notes = "获取妹子分类列表", response = String.class)
    @PostMapping(value = "/getGirlSortList")
    public String getGirlSortList(@Validated({GetList.class}) @RequestBody LocalGirlVO localGirlVO) {
        if (!verify(localGirlVO)){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.VERIFY_ERROR);
        }
        if (StringUtils.isEmpty(localGirlVO.getSource())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.SOURCE_NULL);
        }
        log.info("获取妹子分类列表: {}", localGirlVO);
        return girlSortService.getGirlSortList(localGirlVO);
    }

    @ApiOperation(value = "获取妹子列表", notes = "获取妹子列表", response = String.class)
    @PostMapping(value = "/getGirlList")
    public String getGirlList(@Validated({GetList.class}) @RequestBody LocalGirlVO localGirlVO) {
        if (!verify(localGirlVO)){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.VERIFY_ERROR);
        }
        if (StringUtils.isEmpty(localGirlVO.getSource())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.SOURCE_NULL);
        }

        List<Integer> girlUids = upGirlUidList(localGirlVO);
        if (girlUids.size() == 0 ){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.GIRL_UP_NULL);
        }

        log.info("获取妹子列表: {},{}", localGirlVO,girlUids);
        try {
            return ResultUtil.result(SysConf.SUCCESS, girlService.getGirlList(localGirlVO, girlUids));
        } catch (Exception e) {
            log.info("获取妹子列表异常，{},{},{}",e,e.getCause(),e.getStackTrace());
            return ResultUtil.result(SysConf.ERROR,BaseMessageConf.QUERY_DEFAULT_ERROR);
        }
    }

    @ApiOperation(value = "获取妹子详情", notes = "获取妹子详情", response = String.class)
    @PostMapping(value = "/getGirlDetails")
    public String getGirlDetails(@RequestBody LocalGirlVO localGirlVO) {
        if (!verify(localGirlVO)){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.VERIFY_ERROR);
        }
        if (StringUtils.isEmpty(localGirlVO.getSource())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.SOURCE_NULL);
        }

        log.info("获取妹子详情: {}", localGirlVO);
        List<Integer> girlUids = new ArrayList<>();
        girlUids.add(Integer.valueOf(localGirlVO.getGirlUid()));
        IPage<Girl> page = null;
        try {
            page = girlService.getGirlList(localGirlVO, girlUids);
            if (page.getRecords().size()>0) {
                return ResultUtil.result(SysConf.SUCCESS, page.getRecords().get(0));
            }else {
                return ResultUtil.result(SysConf.SUCCESS, new ArrayList<>());
            }
        } catch (Exception e) {
            log.info("获取妹子详情异常，{}，错误栈，{}",e.getMessage(),e);
            return ResultUtil.result(SysConf.ERROR,BaseMessageConf.QUERY_DEFAULT_ERROR);
        }
    }

    @ApiOperation(value = "获取妹子图片列表", notes = "获取妹子图片列表", response = String.class)
    @PostMapping(value = "/getGirlPicList")
    public String getGirlPicList(@Validated({GetList.class}) @RequestBody LocalGirlVO localGirlVO) {
        if (!verify(localGirlVO)){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.VERIFY_ERROR);
        }
        if (StringUtils.isEmpty(localGirlVO.getGirlUid())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.GIRL_UID_NULL);
        }
        GirlPictureVO girlPictureVO = new GirlPictureVO();
        girlPictureVO.setGirlUid(localGirlVO.getGirlUid());
        girlPictureVO.setCurrentPage(localGirlVO.getCurrentPage());
        girlPictureVO.setPageSize(localGirlVO.getPageSize());
        log.info("获取妹子图片列表: {}", localGirlVO);
        return ResultUtil.result(SysConf.SUCCESS, girlPictureService.getPageList(girlPictureVO));
    }

    @ApiOperation(value = "获取妹子城市列表", notes = "获取妹子城市列表", response = String.class)
    @PostMapping(value = "/getGirlCityList")
    public String getGirlCityList(@Validated({GetList.class}) @RequestBody LocalGirlVO localGirlVO) {
        if (!verify(localGirlVO)){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.VERIFY_ERROR);
        }
        String jsonList = redisUtil.get("city_list");
        if (StringUtils.isNotEmpty(jsonList)){
            List<GirlCity>list = JSONArray.parseArray(jsonList,GirlCity.class);
            return ResultUtil.result(SysConf.SUCCESS, list);
        }
        List<GirlCity> cityList = girlCityService.list();
        GirlCity city = new GirlCity();
        city.setUid("");
        city.setName("城市");
        cityList.add(0,city);
        redisUtil.set("city_list",JSONObject.toJSONString(cityList));
        return ResultUtil.result(SysConf.SUCCESS, cityList);
    }

    @ApiOperation(value = "获取妹订单列表", notes = "获取妹订单列表", response = String.class)
    @PostMapping(value = "/getGirlOrderList")
    public String getGirlOrderList(@Validated({GetList.class}) @RequestBody LocalGirlVO localGirlVO) {
        if (!verify(localGirlVO)){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.VERIFY_ERROR);
        }
        if (StringUtils.isEmpty(localGirlVO.getCustomerId())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.CUSTOMER_ID_NULL);
        }

        log.info("获取妹订单列表: {}", localGirlVO);
        return ResultUtil.result(SysConf.SUCCESS, girlOrderService.getGirlOrderList(localGirlVO));
    }

    @ApiOperation(value = "创建订单", notes = "创建订单", response = String.class)
    @PostMapping(value = "/createOrder")
    public String createOrder(@RequestBody LocalGirlVO localGirlVO) {
        if (!verify(localGirlVO)){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.VERIFY_ERROR);
        }
        if (StringUtils.isEmpty(localGirlVO.getCustomerId())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.CUSTOMER_ID_NULL);
        }
        return ResultUtil.result(SysConf.SUCCESS, t08Url+girlOrderService.createOrder(localGirlVO));
    }

    @ApiOperation(value = "支付回调", notes = "支付回调", response = String.class)
    @PostMapping(value = "/callbackOrder")
    public Map<String,Object> callbackOrder(@RequestBody CallbackOrderRequest callbackOrderRequest) {
        log.info("回调参数: {}",callbackOrderRequest);
        return girlOrderService.callbackOrder(callbackOrderRequest.getParam());
    }

    @ApiOperation(value = "在线客服", notes = "在线客服", response = String.class)
    @PostMapping(value = "/getService")
    public String getService(@Validated({GetList.class}) @RequestBody LocalGirlVO localGirlVO) {
        if (!verify(localGirlVO)){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.VERIFY_ERROR);
        }

        SystemConfig systemConfig = systemConfigService.list().get(0);
        JSONObject json = new JSONObject();
        json.put("serviceUrl",systemConfig.getServiceUrl());
        return ResultUtil.result(SysConf.SUCCESS, json);
    }

    public Boolean verify(LocalGirlVO localGirlVO){
        SystemConfig systemConfig = systemConfigService.list().get(0);
        JSONObject jsonObj = (JSONObject) JSON.toJSON(localGirlVO);
        TreeMap<String, Object> map = JSONObject.parseObject(jsonObj.toJSONString(), new TypeReference<TreeMap<String, Object>>(){});
        map.remove("sign");
        String signStr = MD5Utils.getMD5String(map,systemConfig.getMd5Key());
        log.info("签名原串: {}", signStr);
        String sign = MD5Utils.md5Sign(signStr);
        log.info("md5加密后的签名: {}", sign);
        if (sign.equals(localGirlVO.getSign())){
            return true;
        }
        return false;
    }

    public Boolean verify(LocalGirlCommentVO localGirlCommentVO){
        SystemConfig systemConfig = systemConfigService.list().get(0);
        JSONObject jsonObj = (JSONObject) JSON.toJSON(localGirlCommentVO);
        TreeMap<String, Object> map = JSONObject.parseObject(jsonObj.toJSONString(), new TypeReference<TreeMap<String, Object>>(){});
        map.remove("sign");
        String signStr = MD5Utils.getMD5String(map,systemConfig.getMd5Key());
        log.info("签名原串: {}", signStr);
        String sign = MD5Utils.md5Sign(signStr);
        log.info("md5加密后的签名: {}", sign);
        if (sign.equals(localGirlCommentVO.getSign())){
            return true;
        }
        return false;
    }

    public Boolean verify(LocalGirlGiftVO localGirlGiftVO){
        SystemConfig systemConfig = systemConfigService.list().get(0);
        JSONObject jsonObj = (JSONObject) JSON.toJSON(localGirlGiftVO);
        TreeMap<String, Object> map = JSONObject.parseObject(jsonObj.toJSONString(), new TypeReference<TreeMap<String, Object>>(){});
        map.remove("sign");
        String signStr = MD5Utils.getMD5String(map,systemConfig.getMd5Key());
        log.info("签名原串: {}", signStr);
        String sign = MD5Utils.md5Sign(signStr);
        log.info("md5加密后的签名: {}", sign);
        if (sign.equals(localGirlGiftVO.getSign())){
            return true;
        }
        return false;
    }

    public List<Integer> upGirlUidList(LocalGirlVO localGirlVO){
        List<Integer> girlUids = new ArrayList<>();
        Set<Integer> girUidSet = new HashSet<>();

        QueryWrapper<GirlSortItem> sortItemQueryWrapper = new QueryWrapper<>();

        //未指定分类则获取当前项目下所有的分类uid
        if (StringUtils.isEmpty(localGirlVO.getGirlSortUid())){
            QueryWrapper<GirlSort> sortQueryWrapper = new QueryWrapper<>();
            List<GirlSort> sortList =  girlSortService.list(sortQueryWrapper);
            List<Integer> sortlUids = new ArrayList<>();
            for (GirlSort sort:sortList) {
                sortlUids.add(Integer.valueOf(sort.getUid()));
            }
            sortItemQueryWrapper.in(SQLConf.SORT_UID, sortlUids);
        }else{
            sortItemQueryWrapper.eq(SQLConf.SORT_UID, localGirlVO.getGirlSortUid());
        }

        //分类里上架的妹子uid
        sortItemQueryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
        List<GirlSortItem> sortItemList =  girlSortItemService.list(sortItemQueryWrapper);

        for (GirlSortItem girlSortItem:sortItemList) {
            girUidSet.add(girlSortItem.getGirlUid());
        }
        girlUids.addAll(girUidSet);
        return girlUids;
    }

    @ApiOperation(value = "获取妹订单评论列表", notes = "获取妹订单评论列表")
    @PostMapping(value = "/getGirlOrderCommentList")
    public String getGirlOrderCommentList(@Validated({GetList.class}) @RequestBody LocalGirlVO localGirlVO) {
        if (!verify(localGirlVO)){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.VERIFY_ERROR);
        }
        if (StringUtils.isEmpty(localGirlVO.getCustomerId())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.CUSTOMER_ID_NULL);
        }

        log.info("获取妹订单评论列表: {}", localGirlVO);
        Map<String,Object>map = girlOrderCommentService.getScoreByGirl(localGirlVO.getGirlId());
        IPage<GirlOrderComment>page = girlOrderCommentService.getGirlOrderCommentList(localGirlVO);
        map.put("page",page);
        return ResultUtil.result(SysConf.SUCCESS, map);
    }

    @ApiOperation(value = "创建评论", notes = "创建评论")
    @PostMapping(value = "/createOrderComment")
    public String createOrderComment(@RequestBody LocalGirlVO localGirlVO) {
        if (!verify(localGirlVO)){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.VERIFY_ERROR);
        }
        if (StringUtils.isEmpty(localGirlVO.getCustomerId())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.CUSTOMER_ID_NULL);
        }
        return girlOrderCommentService.createComment(localGirlVO);
    }

    @ApiOperation(value = "检查订单是否有评论", notes = "检查订单是否有评论")
    @PostMapping(value = "/checkGirlOrderComment")
    public String checkGirlOrderComment(@RequestBody LocalGirlVO localGirlVO) {
        if (!verify(localGirlVO)){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.VERIFY_ERROR);
        }
        if (StringUtils.isEmpty(localGirlVO.getCustomerId())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.CUSTOMER_ID_NULL);
        }
        return ResultUtil.result(SysConf.SUCCESS, girlOrderService.checkOrderIfComment(localGirlVO));
    }

    @ApiOperation(value = "保存礼品收货信息", notes = "保存礼品收货信息")
    @PostMapping(value = "/createGirlGift")
    public String createGirlGift(@RequestBody LocalGirlVO localGirlVO) {
        if (!verify(localGirlVO)){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.VERIFY_ERROR);
        }
        if (StringUtils.isEmpty(localGirlVO.getOrderId())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.ORDER_ID_NULL);
        }
        if (StringUtils.isEmpty(localGirlVO.getCustomerId())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.CUSTOMER_ID_NULL);
        }
        GirlGift girlGift = new GirlGift();
        girlGift.setOrderId(localGirlVO.getOrderId());
        girlGift.setReceiveName(localGirlVO.getReceiveName());
        girlGift.setReceiveNumber(localGirlVO.getReceiveNumber());
        girlGift.setReceiveAddress(localGirlVO.getReceiveAddress());
        girlGift.setCustomerId(localGirlVO.getCustomerId());
        girlGiftService.save(girlGift);
        return ResultUtil.result(SysConf.SUCCESS, BaseMessageConf.OPERATION_SUCCESS);
    }


    @ApiOperation(value = "获取用户费率列表", notes = "获取用户费率列表")
    @PostMapping(value = "/getUserCostList")
    public String getUserCostList(@Validated({GetList.class}) @RequestBody LocalGirlVO localGirlVO) {
        if (!verify(localGirlVO)){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.VERIFY_ERROR);
        }
        log.info("获取用户费率列表: {}", localGirlVO);
        return ResultUtil.result(SysConf.SUCCESS, girlUserCostService.getListByH5(localGirlVO.getSource()));
    }





}

