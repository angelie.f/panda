package com.moxi.mogublog.web.restapi;

import com.moxi.mogublog.utils.AESUtils;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.vo.ConfigUrlVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/game/config")
@Api(value = "竞速配置相关接口", tags = {"竞速配置相关接口"})
@Slf4j
public class ConfigRestApi {

    @Autowired
    private Environment env;

    @ApiOperation(value = "获取url", notes = "获取url", response = String.class)
    @PostMapping(value = "/getUrl")
    public String getConfig(@RequestBody ConfigUrlVO configUrlVO) {
        String url=env.getProperty("config.url."+configUrlVO.getKey());
        url = AESUtils.encrypt(url);
        return ResultUtil.result(SysConf.SUCCESS, url);
    }

}

