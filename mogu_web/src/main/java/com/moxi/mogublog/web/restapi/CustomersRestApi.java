package com.moxi.mogublog.web.restapi;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.*;
import com.moxi.mogublog.utils.MD5Utils;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.service.*;
import com.moxi.mogublog.xo.vo.*;
import com.moxi.mougblog.base.enums.EStatus;
import com.moxi.mougblog.base.exception.ThrowableUtils;
import com.moxi.mougblog.base.global.BaseMessageConf;
import com.moxi.mougblog.base.validator.group.GetList;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 * 会员模块 RestApi
 *
 * @author Vergift
 * @date 2021年12月03日17:07:38
 */
@RestController
@Api(value = "/会员controller", tags = "会员模块")
@RequestMapping("/customers")
@Slf4j
public class CustomersRestApi {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private PictureSortService pictureSortService;

    @Autowired
    private PictureAtlasService pictureAtlasService;

    @Autowired
    private PictureTagService pictureTagService;

    @Autowired
    private SystemConfigService systemConfigService;

    @Autowired
    private PictureSortItemService pictureSortItemService;

    @Autowired
    private PictureService pictureService;

    @Autowired
    private UsePowerService usePowerService;

    @Autowired
    private CustomersService customersService;

    @ApiOperation(value = "获取图集分类列表", notes = "获取图集分类列表", response = String.class)
    @PostMapping(value = "/getSortList")
    public String getSortList(@Validated({GetList.class}) @RequestBody CustomersVO customersVO) {
        if (!verify(customersVO)){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.VERIFY_ERROR);
        }
        log.info("获取图集分类表列列表: {}", customersVO);
        return ResultUtil.result(SysConf.SUCCESS, pictureSortService.getCustomersSortList(customersVO));
    }

    @ApiOperation(value = "获取图集列表", notes = "获取图集列表", response = String.class)
    @PostMapping(value = "/getAtlasList")
    public String getAtlasList(@Validated({GetList.class}) @RequestBody CustomersVO customersVO) {
        if (!verify(customersVO)){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.VERIFY_ERROR);
        }
        if (StringUtils.isEmpty(customersVO.getSortUid())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.SORT_UID_NULL);
        }

        List<Integer> atlasUids = upAtlasCount(customersVO);
        if (atlasUids.size() ==0 ){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.SORT_NOT_ATLAS);
        }

        //获取当前请求的用户所属项目
        ProjectVO projectVO = new ProjectVO();
        projectVO.setSource(customersVO.getSource());
        Project project = projectService.getList(projectVO).get(0);

        //获取当前请求的用户所属项目的使用权限
        QueryWrapper<UsePower> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(SQLConf.PROJECT_UID, project.getUid().trim());
        UsePower usePower = usePowerService.list(queryWrapper).get(0);

        log.info("获取图集列表: {}", customersVO);
        return ResultUtil.result(SysConf.SUCCESS, pictureAtlasService.getCustomersAtlasList(customersVO, atlasUids, usePower));
    }

    @ApiOperation(value = "获取标签列表", notes = "获取标签列表", response = String.class)
    @PostMapping(value = "/getTagList")
    public String getTagList(@Validated({GetList.class}) @RequestBody CustomersVO customersVO, BindingResult result) {
        if (!verify(customersVO)){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.VERIFY_ERROR);
        }
        if (StringUtils.isEmpty(customersVO.getSortUid())){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.SORT_UID_NULL);
        }

        // 参数校验
        log.info("获取标签列表: {}", customersVO);
        return ResultUtil.result(SysConf.SUCCESS, pictureTagService.getCustomersTagList(customersVO));
    }

    @ApiOperation(value = "获取图片列表", notes = "获取图片列表", response = String.class)
    @PostMapping(value = "/getPictureList")
    public String getPictureList(@Validated({GetList.class}) @RequestBody CustomersVO customersVO) {
        if (!verify(customersVO)){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.VERIFY_ERROR);
        }

        //获取当前请求的用户所属项目
        ProjectVO projectVO = new ProjectVO();
        projectVO.setSource(customersVO.getSource());
        Project project = projectService.getList(projectVO).get(0);

        //获取当前请求的用户所属项目的使用权限
        QueryWrapper<UsePower> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(SQLConf.PROJECT_UID, project.getUid().trim());
        UsePower usePower = usePowerService.list(queryWrapper).get(0);

        int limit = 0;//默认取10000张图片

        //根据传入的参数获取用户信息
        QueryWrapper<Customers> customersQueryWrapper = new QueryWrapper<>();
        customersQueryWrapper.eq(SQLConf.CUSTOMER_ID,customersVO.getCustomerId());
        customersQueryWrapper.eq(SQLConf.SOURCE,customersVO.getSource());
        Customers customers = customersService.getOne(customersQueryWrapper);

        //被限制后返回的信息
        JSONObject json = new JSONObject();
        json.put("confined",1);

        //判断用户是否会员，取不同的限制
        if (customers.getIsVip() != 1){
            //非会员是否启用限制
            if (usePower.getUserAstrict() == EStatus.ENABLE){
                limit = usePower.getUserLimit();
                if(customersVO.getCurrentPage() > 1){
                    if (limit < customersVO.getCurrentPage() * customersVO.getPageSize()){
                        if (limit <= (customersVO.getCurrentPage() -1) * customersVO.getPageSize()){
                            return ResultUtil.successWithData(json);
                        }else {
                            customersVO.setPageSize(limit - ((customersVO.getCurrentPage() -1) * customersVO.getPageSize()));
                        }
                    }
                }else {
                    if (limit < customersVO.getPageSize()){
                        customersVO.setPageSize(Long.valueOf(limit));
                    }
                }
            }
        }

        // 参数校验
        log.info("获取图片列表: {}", customersVO);
        return ResultUtil.result(SysConf.SUCCESS, pictureService.getCustomersPictuerList(customersVO));
    }


    public Boolean verify(CustomersVO customersVO){
        SystemConfig systemConfig = systemConfigService.list().get(0);
        JSONObject jsonObj = (JSONObject) JSON.toJSON(customersVO);
        TreeMap<String, Object> map = JSONObject.parseObject(jsonObj.toJSONString(), new TypeReference<TreeMap<String, Object>>(){});
        map.remove("sign");
        String signStr = MD5Utils.getMD5String(map,systemConfig.getMd5Key());
        log.info("签名原串: {}", signStr);
        String sign = MD5Utils.md5Sign(signStr);
        log.info("md5加密后的签名: {}", sign);
        if (sign.equals(customersVO.getSign())){
            return true;
        }
        return false;
    }

    public List<Integer> upAtlasCount(CustomersVO customersVO){
        List<Integer> atlasUids = new ArrayList<>();

        QueryWrapper<PictureSortItem> sortItemQueryWrapper = new QueryWrapper<>();

        //根据分类获取图集
//        if (StringUtils.isNotEmpty(customersVO.getTagUid() )){
            sortItemQueryWrapper.eq(SQLConf.SORT_UID, customersVO.getSortUid());
//        }

        //根据标签获取图集
        /*if (StringUtils.isNotEmpty(customersVO.getTagUid())) {
            QueryWrapper<PictureSort> sortqueryWrapper = new QueryWrapper<>();
            sortqueryWrapper.and(wrapper ->wrapper.eq(SQLConf.TAG_UID, customersVO.getTagUid())
                    .or().likeRight(SQLConf.TAG_UID, customersVO.getTagUid() + SysConf.FILE_SEGMENTATION)
                    .or().like(SQLConf.TAG_UID, SysConf.FILE_SEGMENTATION + customersVO.getTagUid() + SysConf.FILE_SEGMENTATION)
                    .or().likeLeft(SQLConf.TAG_UID, SysConf.FILE_SEGMENTATION + customersVO.getTagUid()));

            ProjectVO projectVO = new ProjectVO();
            projectVO.setSource(customersVO.getSource());
            List<Project> projectList = projectService.getList(projectVO);

            sortqueryWrapper.eq(SQLConf.PROJECT_UID, projectList.get(0).getUid());

            List<PictureSort> pictureSortList = pictureSortService.list(sortqueryWrapper);
            List<Integer> srotUids = new ArrayList<>();
            if (pictureSortList.size() > 0){
                for (PictureSort pictureSort: pictureSortList) {
                    srotUids.add(Integer.valueOf(pictureSort.getUid()));
                }
            }
            if (srotUids.size() == 0){
                return srotUids;
            }
            sortItemQueryWrapper.in(SQLConf.SORT_UID, srotUids);
        }*/

        sortItemQueryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);

        List<PictureSortItem> sortItemList =  pictureSortItemService.list(sortItemQueryWrapper);

        for (PictureSortItem pictureSortItem:sortItemList) {
            atlasUids.add(pictureSortItem.getAtlasUid());
        }
        return atlasUids;
    }

}

