CREATE TABLE `t_game_deposit_award` (
  `uid` int(32) NOT NULL AUTO_INCREMENT,
  `source` int(4) NOT NULL COMMENT '用户来源',
  `customer_id` varchar(50) NOT NULL COMMENT '用户id',
  `period` varchar(30) NOT NULL COMMENT '日期',
  `award` decimal(10,2) NOT NULL COMMENT '奖金',
  `first_deposit_num` int(20) NOT NULL COMMENT '当日总首存人数',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`uid`),
  KEY `index_t_game_deposit_award_customer_id` (`customer_id`),
  KEY `index_t_game_deposit_award_period` (`period`),
  KEY `index_t_game_deposit_award_source` (`source`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

