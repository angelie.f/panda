package com.moxi.mogublog.xo.thirdparty.mapper;

import com.moxi.mogublog.commons.entity.TpBookChapters;
import com.moxi.mougblog.base.mapper.SuperMapper;



public interface DTpBookChaptersMapper extends SuperMapper<TpBookChapters> {
}
