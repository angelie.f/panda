package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.GameActReport;
import com.moxi.mogublog.xo.vo.GameActReportVo;
import com.moxi.mougblog.base.service.SuperService;

public interface GameActReportService extends SuperService<GameActReport> {
    IPage<GameActReport> getPageList(GameActReportVo gameActReportVo);
}
