package com.moxi.mogublog.xo.service.impl;

import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import org.springframework.stereotype.Service;
import com.moxi.mogublog.xo.mapper.TpTaggablesMapper;
import com.moxi.mogublog.xo.service.TpTaggablesService;
import com.moxi.mogublog.commons.entity.TpTaggables;

@Service
public class TpTaggablesServiceImpl extends SuperServiceImpl<TpTaggablesMapper, TpTaggables> implements TpTaggablesService {

}

