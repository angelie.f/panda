package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.GirlCity;
import com.moxi.mougblog.base.mapper.SuperMapper;

/**
 * 妹子城市表 Mapper 接口
 *
 * @author Vergift
 * @since 2021-12-29
 */
public interface GirlCityMapper extends SuperMapper<GirlCity> {

}
