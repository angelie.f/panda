package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.GirlOrder;
import com.moxi.mogublog.commons.entity.GirlOrderComment;
import com.moxi.mogublog.xo.vo.*;
import com.moxi.mougblog.base.service.SuperService;

import java.util.Map;

/**
 * 妹子订单表 服务类
 *
 * @author 陌溪
 * @date 2021-12-29
 */
public interface GirlOrderCommentService extends SuperService<GirlOrderComment> {

    /**
     * 前端获取妹子订单评论列表
     *
     * @param localGirlVO
     * @return
     */
    IPage<GirlOrderComment> getGirlOrderCommentList(LocalGirlVO localGirlVO);

    Map<String,Object> getScoreByGirl(int girlId);

    /**
     * 获取妹子订单评论列表
     *
     * @param girlOrderCommentVO
     * @return
     */
    IPage<GirlOrderComment> getPageList(GirlOrderCommentVO girlOrderCommentVO);

    /**
     * 生成评论
     */
    String createComment(LocalGirlVO localGirlVO);

    String edit(GirlOrderCommentVO girlOrderCommentVO);

    String createCommentByOffice(GirlOrderCommentVO girlOrderCommentVO);

}
