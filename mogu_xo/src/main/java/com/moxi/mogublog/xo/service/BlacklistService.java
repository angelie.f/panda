package com.moxi.mogublog.xo.service;

import com.moxi.mogublog.commons.entity.Blacklist;
import com.moxi.mogublog.xo.vo.BlacklistVO;
import com.moxi.mougblog.base.service.SuperService;


public interface BlacklistService extends SuperService<Blacklist> {

    Blacklist getBlacklist(BlacklistVO blacklistVO);
    String edit(BlacklistVO blacklistVO);
    boolean checkInBlackByCustomerId(String customerId);
}
