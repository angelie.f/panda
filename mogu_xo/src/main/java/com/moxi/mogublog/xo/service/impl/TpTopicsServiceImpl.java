package com.moxi.mogublog.xo.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.*;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.mapper.TpBooksMapper;
import com.moxi.mogublog.xo.mapper.TpTagsMapper;
import com.moxi.mogublog.xo.service.TpBooksService;
import com.moxi.mogublog.xo.vo.LocalBooksVO;
import com.moxi.mougblog.base.global.BaseSQLConf;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.moxi.mogublog.xo.mapper.TpTopicsMapper;
import com.moxi.mogublog.xo.service.TpTopicsService;

import java.util.*;

@Service
public class TpTopicsServiceImpl extends SuperServiceImpl<TpTopicsMapper, TpTopics> implements TpTopicsService {
    @Autowired
    private TpTopicsMapper tpTopicsMapper;
    @Autowired
    private TpBooksService tpBooksService;
    @Autowired
    private TpTagsMapper tpTagsMapper;
    @Autowired
    private TpBooksMapper tpBooksMapper;

    @Value("${books.domain}")
    private String imageDomain;

    @Override
    public List<TpTopics> getTopicList(LocalBooksVO localBooksVO) {
        QueryWrapper<TpTopics> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("id","title");
        queryWrapper.eq("status",1);
        queryWrapper.orderByDesc("sort");
        if(StringUtils.isNotEmpty(localBooksVO.getKeyWord())){
            queryWrapper.like("title",localBooksVO.getKeyWord());
        }
        return list(queryWrapper);
    }

    @Override
    public IPage<TpTopics> getTopicAndDetails(LocalBooksVO localBooksVO) {
        QueryWrapper<TpTopics> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("id","title");
        queryWrapper.eq("status",1);
        queryWrapper.orderByDesc("sort").orderByDesc("id");
        if(StringUtils.isNotEmpty(localBooksVO.getKeyWord())){
            queryWrapper.like("title",localBooksVO.getKeyWord());
        }
        Page<TpTopics> page = new Page<>();
        page.setCurrent(localBooksVO.getCurrentPage());
        page.setSize(localBooksVO.getPageSize());
        IPage<TpTopics> iPage = page(page,queryWrapper);
        List<TpTopics>list = iPage.getRecords();
        for (TpTopics tpTopics:list) {
            QueryWrapper<TpBooks> wrapper = new QueryWrapper<>();
            wrapper.select("id","title","author","description","view_counts","vertical_cover","horizontal_cover","end","created_at");
            wrapper.eq("status", 1);
            if (tpTopics.getId() != null) {
//                List<Integer> booksIds = tpTopicsMapper.getBooksIds(tpTopics.getId());
//                if (booksIds.size()>0) {
//                    wrapper.in("id", booksIds);
//                    wrapper.last("limit 6");
//                    List<TpBooks> booksList = tpBooksService.list(wrapper);
//                    for (TpBooks tpBooks:booksList){
//                        tpBooks.setHorizontalCover(convertImageUrl(tpBooks.getHorizontalCover()));
//                        tpBooks.setVerticalCover(convertImageUrl(tpBooks.getVerticalCover()));
//                    }

//                    tpTopics.setBooksList(booksList);
//                }else {
//                    tpTopics.setBooksList(new ArrayList<>());
//                }
                Page<TpBooks> booksPage = new Page<>();
                booksPage.setCurrent(1);
                booksPage.setSize(6);
                List<TpBooks>booksList = tpBooksMapper.getPageByTopicId(booksPage,tpTopics.getId()).getRecords();
                addTags(booksList);
                for (TpBooks tpBooks:booksList){
                    tpBooks.setHorizontalCover(convertImageUrl(tpBooks.getHorizontalCover()));
                    tpBooks.setVerticalCover(convertImageUrl(tpBooks.getVerticalCover()));
                }
                tpTopics.setBooksList(booksList);
            }
        }
        return iPage;
    }

    private  String convertImageUrl(String url){
        if(org.apache.commons.lang.StringUtils.isNotEmpty(url)){
            url = url.replaceAll("\\\\", "");
            //UAT
            url=imageDomain.concat(url);
        }
        return url;
    }

    private void addTags(List<TpBooks> list){
        for(TpBooks books:list){
            books.setTags(tpTagsMapper.getTags(books.getId()));
        }
    }
}

