package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.xo.vo.LocalBooksVO;
import com.moxi.mougblog.base.service.SuperService;
import com.moxi.mogublog.commons.entity.TpTopics;

import java.util.List;

public interface TpTopicsService extends SuperService<TpTopics> {
    /**
     * 前端漫画主题列表
     *
     * @param localBooksVO
     * @return
     */
    List<TpTopics> getTopicList(LocalBooksVO localBooksVO);

    IPage<TpTopics> getTopicAndDetails(LocalBooksVO localBooksVO);
}

