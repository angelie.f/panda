package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.GirlConstant;
import com.moxi.mogublog.commons.entity.GirlGift;
import com.moxi.mougblog.base.mapper.SuperMapper;

/**
 * 妹子订单表 Mapper 接口
 *
 * @author Vergift
 * @since 2021-12-29
 */
public interface GirlGiftMapper extends SuperMapper<GirlGift> {

}
