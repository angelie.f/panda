package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.TpTopics;
import com.moxi.mougblog.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import java.util.List;


public interface TpTopicsMapper extends SuperMapper<TpTopics> {

    @Select("<script> select item_id from tp_topic_items  " +
            "where topic_id in " +
            " <foreach item='item' index='index' collection='topicIds'" +
            "  open='(' separator=',' close=')'>" +
            "  #{item}" +
            " </foreach>" +
            "  </script> ")
    List<Integer> getBooksIdsByTopicIds(@Param("topicIds") List<Integer> topicIds);

    @Select(" select title " +
            " from tp_topics " +
            " where status =1 and causer='book' " +
            " and id in (select topic_id from tp_topic_items where item_id = #{bookId})")
    List<String> queryTopicsByBooksId(@Param("bookId") Integer bookId);

    @Insert("<script>" +
            " INSERT INTO tp_topics" +
            "  (id, title, sort, spotlight, row, causer," +
            "   tag, icon, status, created_at, updated_at) " +
            " VALUES " +
            " <foreach collection='list' item='topics' separator=','>" +
            " (" +
            "  #{topics.id},#{topics.title},#{topics.sort},#{topics.spotlight},#{topics.row},#{topics.causer}," +
            "  #{topics.tag},#{topics.icon},#{topics.status},#{topics.createdAt},#{topics.updatedAt}" +
            " )" +
            "</foreach> " +
            " ON DUPLICATE KEY UPDATE " +
            " title=VALUES(title),sort=VALUES(sort),spotlight=VALUES(spotlight),row=VALUES(row),causer=VALUES(causer)," +
            " tag=VALUES(tag),icon=VALUES(icon),status=VALUES(status),created_at=VALUES(created_at),updated_at=VALUES(updated_at) " +
            "</script>"
    )
    int insertOrUpdateBatch(@Param("list") List<TpTopics> list);
}
