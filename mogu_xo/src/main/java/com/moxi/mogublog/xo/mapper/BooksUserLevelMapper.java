package com.moxi.mogublog.xo.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.BooksUserCost;
import com.moxi.mogublog.commons.entity.BooksUserLevel;
import com.moxi.mougblog.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;


public interface BooksUserLevelMapper extends SuperMapper<BooksUserLevel> {
    @Select(" select uid as level_key,level_id,level_name " +
            " from t_books_user_level " +
            " where source =" +
            "   (select source from t_books_project  where uid =#{projectUid}) " +
            " order by level_id")
    List<BooksUserLevel> selectListByProjectUid(@Param("projectUid") Integer projectUid);

    @Select("select uid,level_id, " +
            " level_name, " +
            " source,status " +
            " from t_books_user_level ul " +
            " where source = #{source}" )
    IPage<BooksUserLevel> getPage(Page<BooksUserLevel> page, @Param("source") Integer source);

}
