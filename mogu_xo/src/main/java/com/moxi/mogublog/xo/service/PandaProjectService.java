package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.BooksProject;
import com.moxi.mogublog.commons.entity.PandaProject;
import com.moxi.mogublog.xo.vo.BooksProjectVO;
import com.moxi.mogublog.xo.vo.ProjectVO;
import com.moxi.mougblog.base.service.SuperService;

import java.util.List;


public interface PandaProjectService extends SuperService<PandaProject> {
    /**
     * 获取项目列表
     *
     * @param projectVO
     * @return
     */
    public IPage<PandaProject> getPageList(ProjectVO projectVO);

    /**
     * 获取全部项目列表
     *
     * @return
     */
    public List<PandaProject> getList(ProjectVO projectVO);

    /**
     * 新增项目
     *
     * @param projectVO
     */
    public String addTag(ProjectVO projectVO);

    /**
     * 编辑项目
     *
     * @param projectVO
     */
    public String editTag(ProjectVO projectVO);

    /**
     * 批量删除项目
     *
     * @param projectVOList
     */
    public String deleteBatchTag(List<ProjectVO> projectVOList);

    public IPage<PandaProject> sources(ProjectVO projectVO);

}
