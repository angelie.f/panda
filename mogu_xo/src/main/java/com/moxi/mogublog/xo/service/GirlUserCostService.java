package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.GirlUserCost;
import com.moxi.mogublog.xo.vo.GirlUserCostVO;
import com.moxi.mougblog.base.service.SuperService;

import java.util.List;

/**
 * 用户费用表 服务类
 *
 * @author Vergift
 * @date 2022-01-09
 */
public interface GirlUserCostService extends SuperService<GirlUserCost> {

    /**
     * 获取用户费用列表
     *
     * @param girlUserCostVO
     * @return
     */
    IPage<GirlUserCost> getPageList(GirlUserCostVO girlUserCostVO);

    /**
     * 新增用户费用
     *
     * @param girlUserCostVO
     */
    String addUserCost(GirlUserCostVO girlUserCostVO);

    /**
     * 编辑用户费用
     *
     * @param girlUserCostVO
     */
    String editUserCost(GirlUserCostVO girlUserCostVO);

    /**
     * 编辑用户费用
     *
     * @param girlUserCostVO
     */
    String deleteUserCost(GirlUserCostVO girlUserCostVO);

    List<GirlUserCost> getListByH5(String source);

}
