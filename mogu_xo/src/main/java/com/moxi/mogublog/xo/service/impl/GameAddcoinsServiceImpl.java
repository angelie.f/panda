package com.moxi.mogublog.xo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.Customers;
import com.moxi.mogublog.commons.entity.GameAddCoins;
import com.moxi.mogublog.utils.RedisUtil;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.mapper.GameAddCoinsMapper;
import com.moxi.mogublog.xo.mapper.GameCreditLogsMapper;
import com.moxi.mogublog.xo.mapper.GameCustomersBalanceMapper;
import com.moxi.mogublog.xo.service.CoinsChangeService;
import com.moxi.mogublog.xo.service.CustomersService;
import com.moxi.mogublog.xo.service.GameAddcoinsService;
import com.moxi.mogublog.xo.vo.GameAddCoinsVo;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.ObjectUtils;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class GameAddcoinsServiceImpl extends SuperServiceImpl<GameAddCoinsMapper, GameAddCoins> implements GameAddcoinsService {
    @Autowired
    private GameAddCoinsMapper gameAddCoinsMapper;
    @Autowired
    private CustomersService customersService;
    @Autowired
    private GameCustomersBalanceMapper gameCustomersBalanceMapper;
    @Autowired
    private GameCreditLogsMapper gameCreditLogsMapper;

    @Autowired
    private CoinsChangeService coinsChangeService;
    @Autowired
    RedisUtil redisUtil;

    @Override
    public IPage<GameAddCoins> getPageList(GameAddCoinsVo gameAddCoinsVo) {
        QueryWrapper<GameAddCoins> queryWrapper = new QueryWrapper<>();
        Page<GameAddCoins> page = new Page<>();
        page.setCurrent(gameAddCoinsVo.getCurrentPage());
        page.setSize(gameAddCoinsVo.getPageSize());
        if(StringUtils.isNotEmpty(gameAddCoinsVo.getBeginCreateTime())){
            queryWrapper.ge(SQLConf.CREATE_TIME,gameAddCoinsVo.getBeginCreateTime());
        }
        if(StringUtils.isNotEmpty(gameAddCoinsVo.getEndCreateTime())){
            queryWrapper.le(SQLConf.CREATE_TIME,gameAddCoinsVo.getEndCreateTime());
        }
        if(StringUtils.isNotEmpty(gameAddCoinsVo.getCusUid())){
            queryWrapper.eq("cus_uid",gameAddCoinsVo.getCusUid());
        }
        queryWrapper.orderByDesc(SQLConf.CREATE_TIME);
        IPage<GameAddCoins> pageList = gameAddCoinsMapper.selectPage(page, queryWrapper);
        return pageList;
    }


    @Override
    public String addConis(GameAddCoinsVo gameAddCoinsVo) {
        try{
            QueryWrapper<Customers> customersQueryWrapper = new QueryWrapper<>();
            customersQueryWrapper.eq(SQLConf.UID,gameAddCoinsVo.getOpenId());
            Customers customers = customersService.getOne(customersQueryWrapper);
            if(ObjectUtils.isEmpty(customers)){
                return ResultUtil.result(SysConf.ERROR, "瓦力ID:"+gameAddCoinsVo.getOpenId()+"未匹配!");
            }

            String key = "gameBalance_"+customers.getCustomerId();
            boolean isLock = redisUtil.setIfAbsent(key,"",30*1000, TimeUnit.MILLISECONDS);

            if (isLock) {
                try {
                    coinsChangeService.addCoins(gameAddCoinsVo,customers);
                } finally {
                    if (redisUtil.hasKey(key))
                        redisUtil.delete(key);
                }
            }else {
                return ResultUtil.result(SysConf.ERROR, MessageConf.OPERATION_FAIL);
            }
            return ResultUtil.result(SysConf.SUCCESS, MessageConf.OPERATION_SUCCESS);
        }catch(Exception e){
            log.debug("添加游戏币异常:{}",e.getMessage());
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return ResultUtil.result(SysConf.ERROR, MessageConf.OPERATION_FAIL);
        }
    }
}
