package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.Girl;
import com.moxi.mogublog.commons.entity.GirlSortItem;
import com.moxi.mogublog.xo.vo.GirlBySortVO;
import com.moxi.mogublog.xo.vo.GirlVO;
import com.moxi.mogublog.xo.vo.LocalGirlVO;
import com.moxi.mougblog.base.service.SuperService;

import java.util.List;

/**
 * 妹子表 服务类
 *
 * @author 陌溪
 * @date 2018-09-04
 */
public interface GirlService extends SuperService<Girl> {

    /**
     * 前端获取妹子列表
     *
     * @param localGirlVO
     * @param girlUids
     * @return
     */
    IPage<Girl> getGirlList(LocalGirlVO localGirlVO, List<Integer> girlUids) throws Exception;

    /**
     * 获取妹子资源列表
     *
     * @param girlVO
     * @return
     */
    IPage<Girl> getPageList(GirlVO girlVO);

    /**
     * 新增妹子资源
     *
     * @param girlVO
     */
    String addGirl(GirlVO girlVO);

    /**
     * 编辑妹子资源
     *
     * @param girlVO
     */
    String editGirl(GirlVO girlVO);

    /**
     * 删除妹子资源
     *
     * @param girlVO
     */
    String deleteGirl(GirlVO girlVO);

    /**
     * 改变妹子可约状态
     *
     * @param girlVO
     */
    String changeAvailable(GirlVO girlVO);

    /**
     * 获取妹子列表
     *
     * @param girlBySortVO
     * @return
     */
    IPage<Girl> getGirlListBySort(GirlBySortVO girlBySortVO);

    /**
     * 妹子上下架
     *
     * @param list
     */
     String upOrOut(List<GirlSortItem> list);

}
