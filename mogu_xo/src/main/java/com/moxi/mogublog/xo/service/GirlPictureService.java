package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.GirlPicture;
import com.moxi.mogublog.xo.vo.GirlPictureVO;
import com.moxi.mougblog.base.service.SuperService;

/**
 * 妹子图片表 服务类
 *
 * @author 陌溪
 * @date 2021-12-29
 */
public interface GirlPictureService extends SuperService<GirlPicture> {

    /**
     * 妹子图片列表
     *
     * @param girlPictureVO
     * @return
     */
    IPage<GirlPicture> getPageList(GirlPictureVO girlPictureVO);

    /**
     * 编辑妹子信息时获取图片列表
     *
     * @param girlPictureVO
     * @return
     */
    IPage<GirlPicture> editGirlPicList(GirlPictureVO girlPictureVO);
}
