package com.moxi.mogublog.xo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.BooksUserCost;
import com.moxi.mogublog.commons.entity.BooksUserLevel;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.mapper.BooksUserCostMapper;
import com.moxi.mogublog.xo.mapper.BooksUserLevelMapper;
import com.moxi.mogublog.xo.service.BooksUserCostService;
import com.moxi.mogublog.xo.vo.BooksUserCostVO;
import com.moxi.mogublog.xo.vo.LocalBooksVO;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class BooksUserCostServiceImpl extends SuperServiceImpl<BooksUserCostMapper, BooksUserCost> implements BooksUserCostService {

    @Autowired
    private BooksUserLevelMapper booksUserLevelMapper;

    @Override
    public String getList(LocalBooksVO localBooksVO) {
        List<BooksUserCost> list = baseMapper.getListBySource(localBooksVO.getSource());
        return ResultUtil.result(SysConf.SUCCESS,list);
    }
    @Override
    public IPage<BooksUserCost> getPageList(BooksUserCostVO booksUserCostVO) {
        QueryWrapper<BooksUserCost> queryWrapper = new QueryWrapper<>();
        log.info("booksUserCostVO:{}",booksUserCostVO.toString());
        if (booksUserCostVO.getProjectUid()!=null) {
            queryWrapper.eq("uc.project_uid", booksUserCostVO.getProjectUid());
        }
        Page<BooksUserCost> page = new Page<>();
        page.setCurrent(booksUserCostVO.getCurrentPage());
        page.setSize(booksUserCostVO.getPageSize());
        queryWrapper.orderByAsc("uc.uid");
        IPage<BooksUserCost> iPage= baseMapper.getPage(page, queryWrapper);
        log.info("page list result{}",iPage.getRecords().toString());
        return iPage;
    }


    @Override
    public String add(BooksUserCostVO booksUserCostVO) {
        if(checkRepeat(booksUserCostVO)){
            return ResultUtil.errorWithMessage(MessageConf.LEVEL_REPEAT);
        }
        BooksUserCost booksUserCost = new BooksUserCost();
        BeanUtils.copyProperties(booksUserCostVO, booksUserCost);
        boolean isSuccess = booksUserCost.insert();
        log.info("insert success:{},data:{}",isSuccess,booksUserCost.toString());
        return ResultUtil.successWithMessage(MessageConf.INSERT_SUCCESS);
    }
    private boolean checkRepeat(BooksUserCostVO booksUserCostVO){
        QueryWrapper<BooksUserCost> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("level_uid",booksUserCostVO.getLevelUid());
        queryWrapper.eq("project_uid",booksUserCostVO.getProjectUid());
        if(booksUserCostVO.getUid()!=null){
            queryWrapper.ne("uid",booksUserCostVO.getUid());
        }
        int count = baseMapper.selectCount(queryWrapper);
        return count>0;
    }

    @Override
    public String edit(BooksUserCostVO booksUserCostVO) {
        if(checkRepeat(booksUserCostVO)){
            return ResultUtil.errorWithMessage(MessageConf.LEVEL_REPEAT);
        }
        BooksUserCost booksUserCost = new BooksUserCost();
        BeanUtils.copyProperties(booksUserCostVO, booksUserCost);
        booksUserCost.setUpdateTime(new Date());
        UpdateWrapper<BooksUserCost> updateWrapper = new UpdateWrapper();
        updateWrapper.eq("uid",booksUserCost.getUid());
        Integer rows =baseMapper.update(booksUserCost,updateWrapper);
        log.info("update success:{},data:{}",rows>0,booksUserCost.toString());
        return ResultUtil.successWithMessage(MessageConf.UPDATE_SUCCESS);
    }
    @Override
    public List<BooksUserLevel> levels(BooksUserCostVO booksUserCostVO) {
        return booksUserLevelMapper.selectListByProjectUid(booksUserCostVO.getProjectUid());
    }
}
