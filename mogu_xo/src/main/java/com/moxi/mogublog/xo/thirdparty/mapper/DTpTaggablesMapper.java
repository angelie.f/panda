package com.moxi.mogublog.xo.thirdparty.mapper;


import com.moxi.mogublog.commons.entity.TpTaggables;
import com.moxi.mougblog.base.mapper.SuperMapper;


public interface DTpTaggablesMapper extends SuperMapper<TpTaggables> {

}
