package com.moxi.mogublog.xo.vo;

import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;

/**
 * TpBooksVO
 * @author Sid
 * @since 2022-05-31
 */
@Data
public class TpBooksVO extends BaseVO<TpBooksVO> {

    /**
     * 排序字段
     */
    private Integer sort;

    /**
     * OrderBy排序字段（desc: 降序）
     */
    private String orderByDescColumn;

    /**
     * OrderBy排序字段（asc: 升序）
     */
    private String orderByAscColumn;

    private String bookName;

    private String author;

    private String topic;

    private String tag;
    /**
     * 创建时间-开始
     */
    private String beginCreateTime;

    /**
     * 创建时间-结束
     */
    private String endCreateTime;
    /**
     * 更改时间-开始
     */
    private String beginUpdateTime;

    /**
     * 更改时间-结束
     */
    private String endUpdateTime;

}
