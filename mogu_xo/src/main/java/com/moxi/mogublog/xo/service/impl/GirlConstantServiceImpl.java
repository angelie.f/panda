package com.moxi.mogublog.xo.service.impl;

import com.moxi.mogublog.commons.entity.GirlConstant;
import com.moxi.mogublog.xo.mapper.GirlConstantMapper;
import com.moxi.mogublog.xo.service.GirlConstantService;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 妹子订单评论表 服务实现类
 *
 * @author 陌溪
 * @since 2021-12-29
 */
@Service
@Slf4j
public class GirlConstantServiceImpl extends SuperServiceImpl<GirlConstantMapper, GirlConstant> implements GirlConstantService {

}
