package com.moxi.mogublog.xo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.moxi.mogublog.commons.entity.GameActReport;
import com.moxi.mogublog.commons.entity.GameAppReport;
import com.moxi.mogublog.utils.ActEnum;
import com.moxi.mogublog.utils.DateUtils;
import com.moxi.mogublog.xo.mapper.CustomersMapper;
import com.moxi.mogublog.xo.mapper.GameActRecordMapper;
import com.moxi.mogublog.xo.mapper.GameActReportMapper;
import com.moxi.mogublog.xo.mapper.GameAppReportMapper;
import com.moxi.mogublog.xo.service.GameReportService;
import com.moxi.mogublog.xo.vo.DateTimeVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class GameReportServiceImpl implements GameReportService {

      private final static String TIME_FORMAT ="yyyy-MM-dd";
   @Autowired
   private CustomersMapper customersMapper;
   @Autowired
   private GameActReportMapper gameActReportMapper;
   @Autowired
   private GameAppReportMapper gameAppReportMapper;
   @Autowired
   private GameActRecordMapper gameActRecordMapper;

    @Override
    public GameActReport addActReport(String source,DateTime time) {
        try{
            Date yestodayStart = DateUtils.getYestodayStart();
            Date yestodayEnd = DateUtils.getYestodayEnd();
            DateTimeVo dateTimeVo = new DateTimeVo(new DateTime(yestodayStart),new DateTime(yestodayEnd));
            log.info(":{}开始执行活动概况报表!",source);
            QueryWrapper<GameActReport> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("period", dateTimeVo.getStartTime().toString(TIME_FORMAT));
            queryWrapper.eq("source",source);
            List<GameActReport> gameActReports = gameActReportMapper.selectList(queryWrapper);
            if(CollectionUtils.isNotEmpty(gameActReports)){
                return gameActReports.get(0);
            }
            GameActReport gameActReport = new GameActReport();
            gameActReport.setSource(source);
            gameActReport.setStatus(1);
            int firstLoginNum =  gameActRecordMapper.queryGameActTypeNum(dateTimeVo.getStartTime().toString(TIME_FORMAT),dateTimeVo.getEndTime().toString(TIME_FORMAT),source, ActEnum.FIRSTLOGINPRE.name());
            int bindNum =  gameActRecordMapper.queryGameActTypeNum(dateTimeVo.getStartTime().toString(TIME_FORMAT),dateTimeVo.getEndTime().toString(TIME_FORMAT),source, ActEnum.BINDPHONEPRE.name());
            int vipNum =  gameActRecordMapper.queryGameActTypeNum(dateTimeVo.getStartTime().toString(TIME_FORMAT),dateTimeVo.getEndTime().toString(TIME_FORMAT),source, ActEnum.VIPLEVELPRE.name());
            int totalActNum = gameActRecordMapper.queryAllGameActTypeNum(dateTimeVo.getStartTime().toString(TIME_FORMAT),dateTimeVo.getEndTime().toString(TIME_FORMAT),source,Arrays.asList(ActEnum.FIRSTLOGINPRE.name(),ActEnum.BINDPHONEPRE.name(),ActEnum.VIPLEVELPRE.name()));
            gameActReport.setLoginFirstNum(firstLoginNum);
            gameActReport.setBindNum(bindNum);
            gameActReport.setVipNum(vipNum);
            gameActReport.setGiftCoinsNum(totalActNum);
            BigDecimal totalAmount = BigDecimal.ZERO;
            BigDecimal firstLoginAmount = BigDecimal.ZERO;
            BigDecimal bindAmount = BigDecimal.ZERO;
            BigDecimal vipAmount = BigDecimal.ZERO;
            BigDecimal addCoinsAmount = BigDecimal.ZERO;
            firstLoginAmount =  gameActRecordMapper.queryGameActTypeAmount(dateTimeVo.getStartTime().toString(TIME_FORMAT),dateTimeVo.getEndTime().toString(TIME_FORMAT),source, ActEnum.FIRSTLOGINPRE.name());
            bindAmount =  gameActRecordMapper.queryGameActTypeAmount(dateTimeVo.getStartTime().toString(TIME_FORMAT),dateTimeVo.getEndTime().toString(TIME_FORMAT),source, ActEnum.BINDPHONEPRE.name());
            vipAmount=  gameActRecordMapper.queryGameActTypeAmount(dateTimeVo.getStartTime().toString(TIME_FORMAT),dateTimeVo.getEndTime().toString(TIME_FORMAT),source, ActEnum.VIPLEVELPRE.name());
            addCoinsAmount = gameActRecordMapper.queryAddCoinsAmount(dateTimeVo.getStartTime().toString(TIME_FORMAT),dateTimeVo.getEndTime().toString(TIME_FORMAT),source);
            totalAmount = totalAmount.add(firstLoginAmount==null?BigDecimal.ZERO:firstLoginAmount).add(bindAmount==null?BigDecimal.ZERO:bindAmount).add(vipAmount==null?BigDecimal.ZERO:vipAmount).add(addCoinsAmount==null?BigDecimal.ZERO:addCoinsAmount);
            gameActReport.setLoginFirstAmount(firstLoginAmount==null?BigDecimal.ZERO:firstLoginAmount);
            gameActReport.setBindAmount(bindAmount==null?BigDecimal.ZERO:bindAmount);
            gameActReport.setVipAmount(vipAmount==null?BigDecimal.ZERO:vipAmount);
            gameActReport.setGiftCoinsAmount(totalAmount);
            gameActReport.setGameCoinsAmount(addCoinsAmount==null?BigDecimal.ZERO:addCoinsAmount);
            gameActReport.setBetAmount(BigDecimal.ZERO);
            gameActReport.setBetDepNum(0);
            gameActReport.setBetNoDepNum(0);
            gameActReport.setPeriod(dateTimeVo.getStartTime().toString(TIME_FORMAT));
            gameActReportMapper.insert(gameActReport);
            log.info(":{}活动概况报表执行结束:{}!",source,gameActReport);
            return gameActReport;
        }catch(Exception e){
            log.error("执行活动概况报表任务异常:{}",e.getMessage());
            return null;
        }
    }

    @Override
    public GameAppReport addAppReport(String source,DateTime time) {
        try{
            String sYestodayStart = DateUtils.getYestodayStartTime();
            String sYestodayEnd = DateUtils.getYestodayEndTime();
            String sYestoday = DateUtils.getYestoday();
            log.info(":{}开始执行平台概况报表!",source);
            QueryWrapper<GameAppReport> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("period", sYestoday);
            queryWrapper.eq("source",source);
            List<GameAppReport> gameAppReports = gameAppReportMapper.selectList(queryWrapper);
            if(CollectionUtils.isNotEmpty(gameAppReports)){
                return gameAppReports.get(0);
            }
            GameAppReport gameAppReport = new GameAppReport();
            gameAppReport.setStatus(1);
            gameAppReport.setSource(source);
            int onlineNum = customersMapper.queryDayOnlineNum(sYestodayStart,sYestodayEnd,source);
            int cusNewNum = customersMapper.queryNewCusNum(sYestodayStart,sYestodayEnd,source);
            int totalOrders = customersMapper.queryTotalOrders(sYestodayStart,sYestodayEnd,source);;
            int totalOrdersSucess = customersMapper.queryTotalOrdersSucess(sYestodayStart,sYestodayEnd,source, Arrays.asList("1","2"));
            int firstDepTotalOrders = customersMapper.queryFirstDepTotalOrders(sYestodayStart,sYestodayEnd,source, Arrays.asList("1","2"),"1");
            int vipTotalOrders  = customersMapper.queryVipTotalOrders(sYestodayStart,sYestodayEnd,source,"1");
            int depNum = customersMapper.queryDepTotalNum(sYestodayStart,sYestodayEnd,source, Arrays.asList("1","2"));
            int totalDepNum = customersMapper.queryDepALLNum(sYestodayStart,sYestodayEnd,source);
            int wdlNum = customersMapper.queryWithdrawalTotalNum(sYestodayStart,sYestodayEnd,source ,Arrays.asList("2"));
            BigDecimal totalAmountOrdersSucess = customersMapper.queryTotalAmountOrdersSucess(sYestodayStart,sYestodayEnd,source, Arrays.asList("1","2"));
            BigDecimal firstDepTotalAmountOrders = customersMapper.queryFirstDepTotalAmountOrders(sYestodayStart,sYestodayEnd,source, Arrays.asList("1","2"),"1");
            BigDecimal withdrawalTotalAmountOrders = customersMapper.queryWithdrawalTotalAmountOrders(sYestodayStart,sYestodayEnd,source, Arrays.asList("2"));
            BigDecimal withdrawalTotalTaxAmountOrders = customersMapper.queryWithdrawalTotalTaxAmountOrders(sYestodayStart,sYestodayEnd,source, Arrays.asList("2"));
            gameAppReport.setOnlineNum(onlineNum);
            gameAppReport.setRegisterNum(cusNewNum);
            gameAppReport.setTotalOrder(totalOrders);
            gameAppReport.setTotalFinshOrder(totalOrdersSucess);
            gameAppReport.setTotalFirstDep(firstDepTotalOrders);
            gameAppReport.setTotalVipOrder(vipTotalOrders);
            gameAppReport.setTotalEtcOrder(totalOrders==0?0:totalOrders-vipTotalOrders);
            gameAppReport.setFinshOrderAmount(totalAmountOrdersSucess==null?BigDecimal.ZERO:totalAmountOrdersSucess);
            gameAppReport.setFirstDepAmount(firstDepTotalAmountOrders==null?BigDecimal.ZERO:firstDepTotalAmountOrders);
            gameAppReport.setWdlAmount(withdrawalTotalAmountOrders==null?BigDecimal.ZERO:withdrawalTotalAmountOrders);
            gameAppReport.setWdlTaxAmount(withdrawalTotalTaxAmountOrders==null?BigDecimal.ZERO:withdrawalTotalTaxAmountOrders);
            gameAppReport.setPeriod(sYestoday);
            gameAppReport.setDepNum(depNum);
            gameAppReport.setWdlNum(wdlNum);
            gameAppReport.setTotalDepNum(totalDepNum);
            gameAppReport.setDepWdlSub(gameAppReport.getFinshOrderAmount().subtract(gameAppReport.getWdlAmount()));
            gameAppReportMapper.insert(gameAppReport);
            log.info(":{}平台概况报表执行结束:{}!",source,gameAppReport);
            return gameAppReport;
        }catch(Exception e){
            log.error("执行平台概况报表任务异常:{}",e.getMessage());
            return null;
        }
    }


//    public static DateTime todayStartTimeJoda = DateTime.now().millisOfDay().withMinimumValue();
//    public static DateTime todayEndTimeJoda = DateTime.now().millisOfDay().withMaximumValue();

    private DateTimeVo getLastDayTime(DateTime nowTime){
       DateTime startTimeJoda = nowTime.millisOfDay().withMinimumValue().plusDays(-1);
       DateTime endTimeJoda = nowTime.millisOfDay().withMaximumValue().plusDays(-1);
       return new DateTimeVo(startTimeJoda,endTimeJoda);
    }
}
