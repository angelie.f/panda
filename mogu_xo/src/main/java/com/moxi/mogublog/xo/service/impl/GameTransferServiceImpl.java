package com.moxi.mogublog.xo.service.impl;


import com.moxi.mogublog.commons.entity.GameTransfer;
import com.moxi.mogublog.xo.mapper.GameTransferMapper;
import com.moxi.mogublog.xo.service.GameTransferService;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Slf4j
@Service
public class GameTransferServiceImpl extends SuperServiceImpl<GameTransferMapper, GameTransfer> implements GameTransferService {
}
