package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.Customers;
import com.moxi.mogublog.commons.entity.GameActRecord;
import com.moxi.mogublog.xo.vo.ActRecordRequest;
import com.moxi.mogublog.xo.vo.ActRecordResponse;
import com.moxi.mogublog.xo.vo.GameActRecordVo;
import com.moxi.mougblog.base.service.SuperService;

public interface GameActRecordService extends SuperService<GameActRecord> {
    Boolean addActRecord(Customers customersResult);

    ActRecordResponse searchActUserInfo(ActRecordRequest actRecordRequest);

    Boolean addLoginActRecord(Customers customersResult);

    Boolean addVipActRecord(Customers customersResult);

    Boolean addBindActRecord(Customers customersResult);

    IPage<GameActRecord> getPageList(GameActRecordVo gameActRecordVo);
}
