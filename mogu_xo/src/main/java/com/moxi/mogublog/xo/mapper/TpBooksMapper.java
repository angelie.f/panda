package com.moxi.mogublog.xo.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.TpBooks;
import com.moxi.mogublog.commons.entity.TpPayLog;
import com.moxi.mougblog.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;


public interface TpBooksMapper extends SuperMapper<TpBooks> {

    @Select(" select tb.id,tb.title,tb.author,tb.description, " +
            "  tb.end,tb.view_counts,tb.vertical_cover,tb.horizontal_cover,tb.created_at " +
            " from tp_books tb " +
            " left join tp_topic_items tti on tb.id=tti.item_id " +
            " where tb.status =1 and tti.topic_id=${topicId} " +
            " order by tti.sort_num desc,id")
    IPage<TpBooks> getPageByTopicId(Page<TpBooks> page,@Param("topicId") Integer topicId );

    @Select("select" +
            "  uid,source,(select name from t_books_project where source =rlog.source) as source_name," +
            "  plog.source,(select name from t_books_project where source =plog.source) as source_name," +
            "  plog.uid, " +
            "  plog.book_id," +
            "  tpb.title as book_name," +
            "  plog.episode," +
            "  tpbc.title as chapter_name," +
            "  (select customer_id from t_customers where open_id= plog.open_id) as customer_id," +
            "  price,create_time,update_time " +
            " from tp_pay_log plog " +
            " left join tp_books tpb on plog.book_id =tpb.id " +
            " left join tp_book_chapters tpbc on plog.book_id =tpbc.book_id  and plog.episode =tpbc.episode " +
            "${ew.customSqlSegment}")
    IPage<TpBooks> getPage(Page<TpBooks> page, @Param(Constants.WRAPPER) QueryWrapper queryWrapper);

    @Insert("<script>" +
            " INSERT INTO tp_books" +
            " (id,title,author,description,end,vertical_cover," +
            " horizontal_cover,type,visits,status,review," +
            " operating,created_at,updated_at,deleted_at,collect_counts," +
            " view_counts,topic_sort) VALUES " +
            " <foreach collection='list' item='books' separator=','>" +
                " (" +
                "#{books.id},#{books.title},#{books.author},#{books.description},#{books.end},#{books.verticalCover}," +
                "#{books.horizontalCover},#{books.type},#{books.visits},#{books.status},#{books.review}," +
                "#{books.operating},#{books.createdAt},#{books.updatedAt},#{books.deletedAt},#{books.collectCounts}," +
                "#{books.viewCounts},#{books.topicSort}" +
                " )" +
            "</foreach> " +
            " ON DUPLICATE KEY UPDATE " +
            " title=VALUES(title),author=VALUES(author),description=VALUES(description),end=VALUES(end),vertical_cover=VALUES(vertical_cover), " +
            " horizontal_cover=VALUES(horizontal_cover),type=VALUES(type),visits=VALUES(visits),status=VALUES(status),review=VALUES(review), " +
            " operating=VALUES(operating),created_at=VALUES(created_at),updated_at=VALUES(updated_at),deleted_at=VALUES(deleted_at),collect_counts=VALUES(collect_counts), " +
            " view_counts=VALUES(view_counts),topic_sort=VALUES(topic_sort) " +
            "</script>"
    )
    int insertOrUpdateBatch(@Param("list") List<TpBooks> list);

}
