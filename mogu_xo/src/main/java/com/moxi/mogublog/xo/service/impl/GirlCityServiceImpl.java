package com.moxi.mogublog.xo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.GirlCity;
import com.moxi.mogublog.utils.RedisUtil;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.mapper.GirlCityMapper;
import com.moxi.mogublog.xo.service.GirlCityService;
import com.moxi.mogublog.xo.vo.GirlCityVO;
import com.moxi.mougblog.base.enums.EStatus;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 妹子城市表 服务实现类
 *
 * @author Vergift
 * @date 2021-12-29
 */
@Service
public class GirlCityServiceImpl extends SuperServiceImpl<GirlCityMapper, GirlCity> implements GirlCityService {

    @Autowired
    private GirlCityService girlCityService;

    @Autowired
    private RedisUtil redisUtil;

    @Override
    public IPage<GirlCity> getPageList(GirlCityVO girlCityVO) {
        QueryWrapper<GirlCity> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(girlCityVO.getName()) && !StringUtils.isEmpty(girlCityVO.getName())) {
            queryWrapper.like(SQLConf.NAME, girlCityVO.getName().trim());
        }

        Page<GirlCity> page = new Page<>();
        page.setCurrent(girlCityVO.getCurrentPage());
        page.setSize(girlCityVO.getPageSize());
        if(StringUtils.isNotEmpty(girlCityVO.getOrderByAscColumn())) {
            // 将驼峰转换成下划线
            String column = StringUtils.underLine(new StringBuffer(girlCityVO.getOrderByAscColumn())).toString();
            queryWrapper.orderByAsc(column);
        } else if(StringUtils.isNotEmpty(girlCityVO.getOrderByDescColumn())) {
            // 将驼峰转换成下划线
            String column = StringUtils.underLine(new StringBuffer(girlCityVO.getOrderByDescColumn())).toString();
            queryWrapper.orderByDesc(column);
        }
        IPage<GirlCity> pageList = girlCityService.page(page, queryWrapper);
        return pageList;
    }

    @Override
    public String addCity(GirlCityVO girlCityVO) {
        QueryWrapper<GirlCity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SQLConf.NAME, girlCityVO.getName());
        if (girlCityService.getOne(queryWrapper) != null) {
            return ResultUtil.errorWithMessage(MessageConf.ENTITY_EXIST);
        }
        GirlCity girlCity = new GirlCity();
        girlCity.setName(girlCityVO.getName());
        girlCity.setStatus(EStatus.ENABLE);
        girlCity.insert();
        redisUtil.delete("city_list");
        return ResultUtil.successWithMessage(MessageConf.INSERT_SUCCESS);
    }

    @Override
    public String editCity(GirlCityVO girlCityVO) {
        GirlCity girlCity = girlCityService.getById(girlCityVO.getUid());

        if (girlCity != null && !girlCity.getName().equals(girlCityVO.getName())) {
            QueryWrapper<GirlCity> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq(SQLConf.NAME, girlCityVO.getName());
            queryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
            GirlCity tempTag = girlCityService.getOne(queryWrapper);
            if (tempTag != null) {
                return ResultUtil.errorWithMessage(MessageConf.ENTITY_EXIST);
            }
        }

        girlCity.setName(girlCityVO.getName());
        girlCity.setUpdateTime(new Date());
        girlCity.updateById();
        redisUtil.delete("city_list");
        return ResultUtil.successWithMessage(MessageConf.UPDATE_SUCCESS);
    }

}
