package com.moxi.mogublog.xo.vo;

import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;

/**
 * ProjectVO
 *
 * @author Vergift
 * @since 2021-11-25
 */
@Data
public class ProjectVO extends BaseVO<ProjectVO> {

    /**
     * OrderBy排序字段（desc: 降序）
     */
    private String orderByDescColumn;

    /**
     * OrderBy排序字段（asc: 升序）
     */
    private String orderByAscColumn;

    /**
     * 项目名称
     */
    private String name;

    /**
     * 来源:1.青青草；2.小黄鸭
     */
    private Integer source;
}
