package com.moxi.mogublog.xo.service;

import com.moxi.mogublog.commons.entity.GameActReport;
import com.moxi.mogublog.commons.entity.GameAppReport;
import org.joda.time.DateTime;

public interface GameReportService {
    GameActReport addActReport(String source, DateTime time);
    GameAppReport addAppReport(String source,DateTime time);
}
