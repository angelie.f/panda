package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.PictureAtlas;
import com.moxi.mogublog.commons.entity.PictureSort;
import com.moxi.mougblog.base.mapper.SuperMapper;

/**
 * 图集表 Mapper 接口
 *
 * @author Vergift
 * @date 2021-12-02
 */
public interface PictureSortMapper extends SuperMapper<PictureSort> {

}
