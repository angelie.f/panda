package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.UsePower;
import com.moxi.mougblog.base.mapper.SuperMapper;

/**
 * 图片表 Mapper 接口
 *
 * @author Vergift
 * @since 2021-12-03
 */
public interface UsePowerMapper extends SuperMapper<UsePower> {

}
