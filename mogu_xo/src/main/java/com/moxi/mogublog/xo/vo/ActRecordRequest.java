package com.moxi.mogublog.xo.vo;

import lombok.Data;

@Data
public class ActRecordRequest{

    /**
     * 用户id
     */
    private String customerId;

    private String openId;

    private String loginName;
}
