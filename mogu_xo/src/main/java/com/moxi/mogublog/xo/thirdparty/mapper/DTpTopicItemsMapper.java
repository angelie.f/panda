package com.moxi.mogublog.xo.thirdparty.mapper;

import com.moxi.mogublog.commons.entity.TpTopicItems;
import com.moxi.mougblog.base.mapper.SuperMapper;


public interface DTpTopicItemsMapper extends SuperMapper<TpTopicItems> {

}
