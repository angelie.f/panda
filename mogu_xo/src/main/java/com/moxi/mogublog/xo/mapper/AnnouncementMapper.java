package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.Announcement;
import com.moxi.mougblog.base.mapper.SuperMapper;

public interface AnnouncementMapper extends SuperMapper<Announcement> {

}
