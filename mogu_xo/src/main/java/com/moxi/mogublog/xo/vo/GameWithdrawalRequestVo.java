package com.moxi.mogublog.xo.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

@ToString
@Data
public class GameWithdrawalRequestVo extends BaseVO<GameWithdrawalRequestVo> {

    /**
     * 用户id
     */
    private String openId;

    private String customerId;

    /**
     * 订单号
     */
    private String requestId;
    /**
     * 订单状态
     */
    private String orderStatus;


    private String isVip;


    /**
     * 创建时间-开始
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH")
    @JsonFormat(pattern = "yyyy-MM-dd HH")
    private String beginCreateTime;

    /**
     * 创建时间-结束
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH")
    @JsonFormat(pattern = "yyyy-MM-dd HH")
    private String endCreateTime;

    /**
     * 创建时间-开始
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH")
    @JsonFormat(pattern = "yyyy-MM-dd HH")
    private String beginUpdateTime;

    /**
     * 创建时间-结束
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH")
    @JsonFormat(pattern = "yyyy-MM-dd HH")
    private String endUpdateTime;

    private String source;
}
