package com.moxi.mogublog.xo.vo;

import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

@ToString
@Data
public class ConfigUrlVO {

    private String key;

    private String value;

}
