package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.GameActConfig;
import com.moxi.mougblog.base.mapper.SuperMapper;


public interface GameActConfigMapper extends SuperMapper<GameActConfig> {

}
