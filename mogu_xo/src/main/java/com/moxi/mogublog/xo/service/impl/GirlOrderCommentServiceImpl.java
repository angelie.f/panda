package com.moxi.mogublog.xo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.*;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.mapper.*;
import com.moxi.mogublog.xo.service.*;
import com.moxi.mogublog.xo.vo.*;
import com.moxi.mougblog.base.enums.EStatus;
import com.moxi.mougblog.base.global.BaseMessageConf;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 妹子订单评论表 服务实现类
 *
 * @author 陌溪
 * @since 2021-12-29
 */
@Service
@Slf4j
public class GirlOrderCommentServiceImpl extends SuperServiceImpl<GirlOrderCommentMapper, GirlOrderComment> implements GirlOrderCommentService {


    @Autowired
    private GirlOrderCommentService girlOrderCommentService;
    @Autowired
    private GirlConstantService girlConstantService;
    @Autowired
    private GirlService girlService;
    @Override
    public IPage<GirlOrderComment> getGirlOrderCommentList(LocalGirlVO localGirlVO) {

        QueryWrapper<GirlOrderComment> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(localGirlVO.getOrderId())) {
            queryWrapper.eq(SQLConf.ORDER_ID, localGirlVO.getOrderId());
        }
        if (localGirlVO.getGirlId()!=null){
            queryWrapper.eq(SQLConf.GIRL_ID, localGirlVO.getGirlId());
        }
        queryWrapper.lt(SQLConf.FIRST_COMMENT_UID,1);
        queryWrapper.orderByDesc(SQLConf.UPDATE_TIME);

        Page<GirlOrderComment> page = new Page<>();
        page.setCurrent(localGirlVO.getCurrentPage());
        page.setSize(localGirlVO.getPageSize());
        IPage<GirlOrderComment> iPage =  girlOrderCommentService.page(page, queryWrapper);
        List<GirlOrderComment>list = iPage.getRecords();
        log.info("评论数:{}",list.size());
        for (GirlOrderComment girlOrderComment : list) {
            if (isToday(girlOrderComment.getCreateTime().getTime())) {
                girlOrderComment.setCreateTimeStr("今天评价");
            } else {
                int days = differentDays(girlOrderComment.getCreateTime(), new Date());
                if (days < 30) {
                    girlOrderComment.setCreateTimeStr(days + "天前评价");
                } else if (days < 365) {
                    girlOrderComment.setCreateTimeStr(days / 30 + "月前评价");
                } else {
                    girlOrderComment.setCreateTimeStr(days / 365 + "年前评价");
                }
            }
            QueryWrapper<GirlOrderComment> wrapper = new QueryWrapper<>();
            Page<GirlOrderComment> page1 = new Page<>();
            page1.setCurrent(1);
            page1.setSize(1000);
            wrapper.eq(SQLConf.FIRST_COMMENT_UID,Integer.valueOf(girlOrderComment.getUid()));
            wrapper.orderByDesc(SQLConf.UPDATE_TIME);
            IPage<GirlOrderComment> iPage1 = girlOrderCommentService.page(page1,wrapper);
            List<GirlOrderComment>list1 = iPage1.getRecords();
            for (GirlOrderComment comment:list1){
                if (isToday(comment.getCreateTime().getTime())) {
                    comment.setCreateTimeStr("今天评价");
                } else {
                    int days = differentDays(comment.getCreateTime(), new Date());
                    if (days < 30) {
                        comment.setCreateTimeStr(days + "天前评价");
                    } else if (days < 365) {
                        comment.setCreateTimeStr(days / 30 + "月前评价");
                    } else {
                        comment.setCreateTimeStr(days / 365 + "年前评价");
                    }
                }
            }
            log.info("回复条数:{}",iPage1.getRecords().size());
            girlOrderComment.setCommentList(list1);
        }
        iPage.setRecords(list);
        return iPage;
    }

    @Override
    public Map<String,Object> getScoreByGirl(int girlId) {
        QueryWrapper<GirlOrderComment> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SQLConf.GIRL_ID, girlId);
        queryWrapper.lt(SQLConf.FIRST_COMMENT_UID,1);
        queryWrapper.orderByDesc(SQLConf.UPDATE_TIME);
        Map<String,Object>map = new HashMap<>();

        Page<GirlOrderComment> page = new Page<>();
        page.setCurrent(1l);
        page.setSize(1000l);
        IPage<GirlOrderComment> ipage = girlOrderCommentService.page(page, queryWrapper);
        List<GirlOrderComment>list = ipage.getRecords();
        if (list.size()>0) {
            float result = 0f;
            float resultSc = 0f;
            float resultYz = 0f;
            float resultFw = 0f;
            for (GirlOrderComment girlOrderComment : list) {
                result = result + girlOrderComment.getScore() + girlOrderComment.getScore1() + girlOrderComment.getScore2();
                resultSc = resultSc + girlOrderComment.getScore();
                resultYz = resultYz + girlOrderComment.getScore1();
                resultFw = resultFw + girlOrderComment.getScore2();
            }
            BigDecimal decimal = new BigDecimal(result);
            BigDecimal newResult = decimal.divide(new BigDecimal(list.size()*3),1, BigDecimal.ROUND_HALF_UP);
            map.put("score",newResult.floatValue());
            map.put("scoreSc",new BigDecimal(resultSc).divide(new BigDecimal(list.size()),1,BigDecimal.ROUND_HALF_UP).floatValue());
            map.put("scoreYz",new BigDecimal(resultYz).divide(new BigDecimal(list.size()),1,BigDecimal.ROUND_HALF_UP).floatValue());
            map.put("scoreFw",new BigDecimal(resultFw).divide(new BigDecimal(list.size()),1,BigDecimal.ROUND_HALF_UP).floatValue());
            return map;
        }else {
            map.put("score",0f);
            map.put("scoreSc",0f);
            map.put("scoreYz",0f);
            map.put("scoreFw",0f);
            return map;
        }
    }

    public static boolean isToday(long time) {
        Date date = new Date(time);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String param = sdf.format(date);//参数时间
        String now = sdf.format(new Date());//当前时间
        if (param.equals(now)) {
            return true;
        }
        return false;
    }

    public static int differentDays(Date date1,Date date2)
    {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        int day1= cal1.get(Calendar.DAY_OF_YEAR);
        int day2 = cal2.get(Calendar.DAY_OF_YEAR);

        int year1 = cal1.get(Calendar.YEAR);
        int year2 = cal2.get(Calendar.YEAR);
        if(year1 != year2)   //同一年
        {
            int timeDistance = 0 ;
            for(int i = year1 ; i < year2 ; i ++)
            {
                if(i%4==0 && i%100!=0 || i%400==0)    //闰年
                {
                    timeDistance += 366;
                }
                else    //不是闰年
                {
                    timeDistance += 365;
                }
            }

            return timeDistance + (day2-day1) ;
        }
        else    //不同年
        {
            System.out.println("判断day2 - day1 : " + (day2-day1));
            return day2-day1;
        }
    }



    @Override
    public IPage<GirlOrderComment> getPageList(GirlOrderCommentVO girlOrderCommentVO) {
        QueryWrapper<GirlOrderComment> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(girlOrderCommentVO.getUserId()) && !StringUtils.isEmpty(girlOrderCommentVO.getUserId())) {
            queryWrapper.eq(SQLConf.USER_ID, girlOrderCommentVO.getUserId().trim());
        }
        if (girlOrderCommentVO.getGirlId()!=null) {
            queryWrapper.eq(SQLConf.GIRL_ID, girlOrderCommentVO.getGirlId());
        }
        if (girlOrderCommentVO.getCommentType()!=null) {
            //评论
            if(girlOrderCommentVO.getCommentType().compareTo(1)==0){
                queryWrapper.eq(SQLConf.FIRST_COMMENT_UID, 0);
            }
            //回復
            if(girlOrderCommentVO.getCommentType().compareTo(2)==0){
                queryWrapper.ne(SQLConf.FIRST_COMMENT_UID, 0);
            }
        }
        queryWrapper.orderByDesc(SQLConf.UID);
        Page<GirlOrderComment> page = new Page<>();
        page.setCurrent(girlOrderCommentVO.getCurrentPage());
        page.setSize(girlOrderCommentVO.getPageSize());
        IPage<GirlOrderComment> pageList = girlOrderCommentService.page(page, queryWrapper);
        List<GirlOrderComment>list = pageList.getRecords();
        for (GirlOrderComment girlOrderComment:list){
            QueryWrapper<Girl> wrapper = new QueryWrapper<>();
            wrapper.eq(SQLConf.UID,girlOrderComment.getGirlId());
            Girl girl = girlService.getOne(wrapper);
            if (girl!=null) {
                girlOrderComment.setGirlName(girl.getName());
            }
        }
        pageList.setRecords(list);
        return pageList;
    }


    @Override
    public String createComment(LocalGirlVO localGirlVO) {
        QueryWrapper<GirlOrderComment> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SQLConf.ORDER_ID, localGirlVO.getOrderId());
        queryWrapper.lt(SQLConf.FIRST_COMMENT_UID,1);
        queryWrapper.eq(SQLConf.USER_ID,localGirlVO.getUserId());
        int count = girlOrderCommentService.count(queryWrapper);
        if (count>0){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.ORDER_ALREADY_CREATE);
        }
        GirlOrderComment girlOrderComment = new GirlOrderComment();
        girlOrderComment.setOrderId(String.valueOf(StringUtils.getSnowflakeId()));
        girlOrderComment.setGirlId(localGirlVO.getGirlId());
        girlOrderComment.setScore(localGirlVO.getScore());
        girlOrderComment.setScore1(localGirlVO.getScore1());
        girlOrderComment.setScore2(localGirlVO.getScore2());
        girlOrderComment.setUserId(localGirlVO.getUserId());
        girlOrderComment.setContent(localGirlVO.getContent());
        girlOrderComment.setToUid(localGirlVO.getToUid()==null?0:localGirlVO.getToUid());
        girlOrderComment.setFirstCommentUid(localGirlVO.getFirstCommentUid()==null?0:localGirlVO.getFirstCommentUid());
        girlOrderComment.setStatus(1);
        girlOrderCommentService.save(girlOrderComment);
        Map<String,String> map = new HashMap<>();
        QueryWrapper<GirlConstant> wrapper = new QueryWrapper<>();
        wrapper.eq("param", "is_gift");
        List<GirlConstant>constantList = girlConstantService.list(wrapper);
        if (constantList.size()>0){
            map.put("status",String.valueOf(constantList.get(0).getStatus()));
            map.put("content",constantList.get(0).getValue());
        }else {
            map.put("status","0");
        }
        return ResultUtil.result(SysConf.SUCCESS,map);
    }

    @Override
    public String edit(GirlOrderCommentVO girlOrderCommentVO) {
        GirlOrderComment girlOrderComment = girlOrderCommentService.getById(girlOrderCommentVO.getUid());
        girlOrderComment.setScore(girlOrderCommentVO.getScore());
        girlOrderComment.setScore1(girlOrderCommentVO.getScore1());
        girlOrderComment.setScore2(girlOrderCommentVO.getScore2());
        girlOrderComment.setContent(girlOrderCommentVO.getContent());
        girlOrderComment.setStatus(girlOrderCommentVO.getStatus());
        girlOrderComment.updateById();
        return ResultUtil.successWithMessage(MessageConf.UPDATE_SUCCESS);
    }

    @Override
    public String createCommentByOffice(GirlOrderCommentVO girlOrderCommentVO) {
        GirlOrderComment girlOrderComment = new GirlOrderComment();
        girlOrderComment.setOrderId(girlOrderCommentVO.getOrderId());
        girlOrderComment.setGirlId(girlOrderCommentVO.getGirlId());
        girlOrderComment.setScore(girlOrderCommentVO.getScore());
        girlOrderComment.setScore1(girlOrderCommentVO.getScore1());
        girlOrderComment.setScore2(girlOrderCommentVO.getScore2());
        girlOrderComment.setUserId(girlOrderCommentVO.getUserId());
        girlOrderComment.setContent(girlOrderCommentVO.getContent());
        girlOrderComment.setToUid(girlOrderCommentVO.getToUid());
        girlOrderComment.setFirstCommentUid(girlOrderCommentVO.getFirstCommentUid());
        girlOrderComment.setIsOfficial(girlOrderCommentVO.getIsOfficial());
        girlOrderComment.setStatus(1);
        girlOrderCommentService.save(girlOrderComment);
        return ResultUtil.successWithMessage(MessageConf.INSERT_SUCCESS);
    }
}
