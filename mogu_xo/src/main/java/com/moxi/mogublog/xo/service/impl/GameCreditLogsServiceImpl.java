package com.moxi.mogublog.xo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.GameConfig;
import com.moxi.mogublog.commons.entity.GameCreditLogs;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.mapper.GameCreditLogsMapper;
import com.moxi.mogublog.xo.service.GameCreditLogsService;
import com.moxi.mogublog.xo.vo.GameCreditLogsVO;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;


@Service
@Slf4j
public class GameCreditLogsServiceImpl extends SuperServiceImpl<GameCreditLogsMapper, GameCreditLogs> implements GameCreditLogsService {

    @Resource
    private GameCreditLogsMapper gameCreditLogsMapper;

    @Override
    public IPage<GameCreditLogs> getPageList(GameCreditLogsVO gameCreditLogsVO) {
        Page<GameCreditLogs> page = new Page<>();
        page.setCurrent(gameCreditLogsVO.getCurrentPage());
        page.setSize(gameCreditLogsVO.getPageSize());
        QueryWrapper<GameCreditLogs> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("customer_id",gameCreditLogsVO.getCustomerId());
        if (gameCreditLogsVO.getType()!=null){
            if (gameCreditLogsVO.getType()!=3) {
                queryWrapper.eq("type", gameCreditLogsVO.getType());
            }else {
                List<Integer>list = new ArrayList<>();
                list.add(3);
                list.add(4);
                queryWrapper.in("type",list);
            }
        }
        if (StringUtils.isNotEmpty(gameCreditLogsVO.getCreateTimeEnd())) {
            queryWrapper.lt("create_time", gameCreditLogsVO.getCreateTimeEnd());
        }
        if (StringUtils.isNotEmpty(gameCreditLogsVO.getCreateTimeBegin())) {
            queryWrapper.gt("create_time", gameCreditLogsVO.getCreateTimeBegin());
        }
        queryWrapper.orderByDesc(SQLConf.CREATE_TIME);
        return gameCreditLogsMapper.selectPage(page,queryWrapper);
    }
}
