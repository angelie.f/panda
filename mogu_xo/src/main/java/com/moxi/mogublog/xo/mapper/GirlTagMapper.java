package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.GirlTag;
import com.moxi.mougblog.base.mapper.SuperMapper;

/**
 * 妹子标签表 Mapper 接口
 *
 * @author Vergift
 * @since 2021-12-29
 */
public interface GirlTagMapper extends SuperMapper<GirlTag> {

}
