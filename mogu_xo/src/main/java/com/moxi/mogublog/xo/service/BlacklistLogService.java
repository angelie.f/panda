package com.moxi.mogublog.xo.service;

import com.moxi.mogublog.commons.entity.BlacklistLog;
import com.moxi.mogublog.xo.vo.BlacklistLogVO;
import com.moxi.mougblog.base.service.SuperService;

import java.util.List;

public interface BlacklistLogService extends SuperService<BlacklistLog> {

    List<BlacklistLog> getList(BlacklistLogVO blacklistLogVO);

}
