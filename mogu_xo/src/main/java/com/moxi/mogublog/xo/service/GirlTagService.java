package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.GirlTag;
import com.moxi.mogublog.commons.entity.PictureTag;
import com.moxi.mogublog.xo.vo.CustomersVO;
import com.moxi.mogublog.xo.vo.GirlTagVO;
import com.moxi.mogublog.xo.vo.PictureTagVO;
import com.moxi.mougblog.base.service.SuperService;

import java.util.List;

/**
 * 妹子标签表 服务类
 *
 * @author Vergift
 * @date 2022-01-05
 */
public interface GirlTagService extends SuperService<GirlTag> {
    /**
     * 获取妹子标签列表
     *
     * @param girlTagVO
     * @return
     */
    IPage<GirlTag> getPageList(GirlTagVO girlTagVO);

    /**
     * 新增妹子标签
     *
     * @param girlTagVO
     */
    String addTag(GirlTagVO girlTagVO);

    /**
     * 编辑妹子标签
     *
     * @param girlTagVO
     */
    String editTag(GirlTagVO girlTagVO);

}
