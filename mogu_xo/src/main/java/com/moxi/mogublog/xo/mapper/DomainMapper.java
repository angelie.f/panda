package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.Domain;
import com.moxi.mougblog.base.mapper.SuperMapper;


public interface DomainMapper extends SuperMapper<Domain> {

}
