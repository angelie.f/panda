package com.moxi.mogublog.xo.thirdparty.mapper;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.TpTags;
import com.moxi.mogublog.commons.entity.TpTopics;
import com.moxi.mougblog.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface DTpTopicsMapper extends SuperMapper<TpTopics> {
    @Select(" select id," +
            " title, sort, spotlight, row, causer, " +
            " convert(JSON_UNQUOTE(json_extract(properties , '$.\"tag\"[0]')) using utf8) as tag, " +
            " convert(JSON_UNQUOTE(json_extract(properties , '$.\"icon\"[0]')) using utf8) as icon," +
            " status, created_at, updated_at" +
            " from tp_topics " +
            " ${ew.customSqlSegment}")
    IPage<TpTopics> selectPageForUpdate(Page<TpTopics> page, @Param(Constants.WRAPPER) QueryWrapper queryWrapper);

}
