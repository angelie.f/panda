package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.PandaProject;
import com.moxi.mougblog.base.mapper.SuperMapper;


public interface PandaProjectMapper extends SuperMapper<PandaProject> {

}
