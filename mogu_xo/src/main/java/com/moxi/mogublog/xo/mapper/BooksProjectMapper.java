package com.moxi.mogublog.xo.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.BooksProject;
import com.moxi.mogublog.commons.entity.TpBooks;
import com.moxi.mougblog.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;


public interface BooksProjectMapper extends SuperMapper<BooksProject> {
    @Select("select" +
            "  uid,source,(select name from t_books_project where source =rlog.source) as source_name," +
            "  plog.source,(select name from t_books_project where source =plog.source) as source_name," +
            "  plog.uid, " +
            "  plog.book_id," +
            "  tpb.title as book_name," +
            "  plog.episode," +
            "  tpbc.title as chapter_name," +
            "  (select customer_id from t_customers where open_id= plog.open_id) as customer_id," +
            "  price,create_time,update_time " +
            " from tp_pay_log plog " +
            " left join tp_books tpb on plog.book_id =tpb.id " +
            " left join tp_book_chapters tpbc on plog.book_id =tpbc.book_id  and plog.episode =tpbc.episode " +
            "${ew.customSqlSegment}")
    IPage<BooksProject> getPage(Page<BooksProject> page, @Param(Constants.WRAPPER) QueryWrapper queryWrapper);




}
