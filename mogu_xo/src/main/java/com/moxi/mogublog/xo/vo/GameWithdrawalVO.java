package com.moxi.mogublog.xo.vo;

import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

@ToString
@Data
public class GameWithdrawalVO {

    /**
     * 用户id
     */
    private String customerId;

    private String openId;

    private String loginName;

    /**
     * 注册来源：1.青青草 2.小黄鸭
     */
    private Integer source;

    //提现金额
    private BigDecimal amount;

    //扣除手续费后的到账金额
    private BigDecimal disAmount;



}
