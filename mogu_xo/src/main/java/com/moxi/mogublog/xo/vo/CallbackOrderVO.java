package com.moxi.mogublog.xo.vo;

import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;

import java.math.BigDecimal;

/**
 * CallbackOrderVO
 *
 * @author Vergift
 * @since 2022-01-07
 */
@Data
public class CallbackOrderVO extends BaseVO<CallbackOrderVO> {

    private String customerId;

    private String referenceId;

    private String gameId;

    private BigDecimal amount;

    private Integer opType;

    private String timestamp;
}
