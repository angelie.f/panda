package com.moxi.mogublog.xo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.GameActReport;
import com.moxi.mogublog.commons.entity.GameAppReport;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.mapper.GameActReportMapper;
import com.moxi.mogublog.xo.service.GameActReportService;
import com.moxi.mogublog.xo.vo.GameActReportVo;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class GameActReportServiceImpl extends SuperServiceImpl<GameActReportMapper, GameActReport> implements GameActReportService {

    @Autowired
    private GameActReportMapper gameActReportMapper;

    @Override
    public IPage<GameActReport> getPageList(GameActReportVo gameActReportVo) {
        QueryWrapper<GameActReport> queryWrapper = new QueryWrapper<>();

        if (StringUtils.isNotBlank(gameActReportVo.getSource())) {
            queryWrapper.eq(SQLConf.SOURCE, gameActReportVo.getSource().trim());
        }
        if (StringUtils.isNotBlank(gameActReportVo.getBeginCreateTime()) && gameActReportVo.getBeginCreateTime().equals(gameActReportVo.getEndCreateTime())){
            queryWrapper.like(SQLConf.PERIOD, gameActReportVo.getEndCreateTime().trim());
        }else {
            if (StringUtils.isNotEmpty(gameActReportVo.getBeginCreateTime())) {
                queryWrapper.ge(SQLConf.PERIOD, gameActReportVo.getBeginCreateTime().trim());
            }
            if (StringUtils.isNotEmpty(gameActReportVo.getEndCreateTime())) {
                queryWrapper.le(SQLConf.PERIOD, gameActReportVo.getEndCreateTime().trim());
            }
        }
        Page<GameActReport> page = new Page<>();
        page.setCurrent(gameActReportVo.getCurrentPage());
        page.setSize(gameActReportVo.getPageSize());
        queryWrapper.orderByDesc(SQLConf.UID);
        IPage<GameActReport> pageList = gameActReportMapper.getPage(page, queryWrapper);
        return pageList;
    }
}
