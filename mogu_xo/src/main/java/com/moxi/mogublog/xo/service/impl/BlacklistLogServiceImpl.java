package com.moxi.mogublog.xo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.moxi.mogublog.commons.config.security.SecurityUser;
import com.moxi.mogublog.commons.entity.Blacklist;
import com.moxi.mogublog.commons.entity.BlacklistLog;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.mapper.BlacklistLogMapper;
import com.moxi.mogublog.xo.mapper.BlacklistMapper;
import com.moxi.mogublog.xo.service.BlacklistLogService;
import com.moxi.mogublog.xo.service.BlacklistService;
import com.moxi.mogublog.xo.vo.BlacklistLogVO;
import com.moxi.mogublog.xo.vo.BlacklistVO;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class BlacklistLogServiceImpl extends SuperServiceImpl<BlacklistLogMapper, BlacklistLog> implements BlacklistLogService {


    @Override
    public List<BlacklistLog> getList(BlacklistLogVO blacklistLogVO){
        List<BlacklistLog> list =baseMapper.queryList(blacklistLogVO.getValiUid());
        return list;
    }

}
