package com.moxi.mogublog.xo.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.BooksUserCost;
import com.moxi.mogublog.commons.entity.BooksUserLevel;
import com.moxi.mogublog.commons.entity.GameConfig;
import com.moxi.mougblog.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface GameConfigMapper extends SuperMapper<GameConfig> {
    @Select(" SELECT " +
            "   tgc.*,tgt.name as game_type_name " +
            " FROM t_game_config tgc " +
            " left join t_game_type tgt " +
            " on tgc.game_type_uid =tgt.uid " +
            " ${ew.customSqlSegment}")
    IPage<GameConfig> getPage(Page<GameConfig> page, @Param(Constants.WRAPPER) QueryWrapper queryWrapper);

    @Select(" SELECT " +
            "   tgc.*,tgt.name as game_type_name " +
            " FROM t_game_config tgc " +
            " left join t_game_type tgt " +
            " on tgc.game_type_uid =tgt.uid " +
            " ${ew.customSqlSegment}")
    List<GameConfig> getList(@Param(Constants.WRAPPER) QueryWrapper queryWrapper);

}
