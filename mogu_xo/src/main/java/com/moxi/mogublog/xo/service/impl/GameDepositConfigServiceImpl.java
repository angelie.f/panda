package com.moxi.mogublog.xo.service.impl;


import cn.hutool.core.lang.Assert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.GameDepositConfig;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.mapper.GameDepositConfigMapper;
import com.moxi.mogublog.xo.mapper.GameDepositRequestMapper;
import com.moxi.mogublog.xo.service.GameDepositConfigService;
import com.moxi.mogublog.xo.vo.GameDepositConfigVo;
import com.moxi.mogublog.xo.vo.GameDepositVO;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Slf4j
@Service
public class GameDepositConfigServiceImpl extends SuperServiceImpl<GameDepositConfigMapper, GameDepositConfig> implements GameDepositConfigService {
    @Autowired
    private GameDepositConfigMapper gameDepositConfigMapper;

    @Autowired
    private GameDepositRequestMapper gameDepositRequestMapper;

    @Override
    public List<GameDepositConfig> getDepositConfigList(GameDepositVO gameDepositVO) {
        int countDeposit = gameDepositRequestMapper.isUserDeposit(gameDepositVO.getOpenId());
        QueryWrapper<GameDepositConfig> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status",1);
        if(countDeposit>0){
            //续充
            queryWrapper.eq("deposit_type",2).or().eq("deposit_type",3);
        }
        else{
            //首充
            queryWrapper.eq("deposit_type",1).or().eq("deposit_type",3);
        }
        return gameDepositConfigMapper.selectList(queryWrapper);
    }

    @Override
    public String addDeposit(GameDepositConfigVo gameDepositConfigVo) {
            GameDepositConfig gameDepositConfig = new GameDepositConfig();
            BeanUtils.copyProperties(gameDepositConfigVo,gameDepositConfig);
            gameDepositConfig.setPromotionCoins(gameDepositConfig.getCoins().intValue()-gameDepositConfig.getPrice().intValue()>0?gameDepositConfig.getCoins().intValue()-gameDepositConfig.getPrice().intValue():0);
            gameDepositConfigMapper.insert(gameDepositConfig);
            return ResultUtil.successWithMessage(MessageConf.INSERT_SUCCESS);
    }

    @Override
    public String editDeposit(GameDepositConfigVo gameDepositConfigVo) {
            GameDepositConfig gameDepositConfig = gameDepositConfigMapper.selectById(gameDepositConfigVo.getUid());
            Assert.notNull(gameDepositConfig, MessageConf.PARAM_INCORRECT);
            gameDepositConfig.setPrice(gameDepositConfigVo.getPrice());
            gameDepositConfig.setCoins(gameDepositConfigVo.getCoins());
            gameDepositConfig.setPromotionCoins(gameDepositConfig.getCoins().intValue()-gameDepositConfig.getPrice().intValue()>0?gameDepositConfig.getCoins().intValue()-gameDepositConfig.getPrice().intValue():0);
            gameDepositConfig.setDescription(gameDepositConfigVo.getDescription());
            gameDepositConfig.setDepositType(gameDepositConfigVo.getDepositType());
            gameDepositConfig.setStatus(gameDepositConfigVo.getStatus());
            gameDepositConfig.updateById();
            return ResultUtil.successWithMessage(MessageConf.OPERATION_SUCCESS);
    }

    @Override
    public IPage<GameDepositConfig> getPageList(GameDepositConfigVo gameDepositConfigVo) {
        QueryWrapper<GameDepositConfig> queryWrapper = new QueryWrapper<>();
        Page<GameDepositConfig> page = new Page<>();
        page.setCurrent(gameDepositConfigVo.getCurrentPage());
        page.setSize(gameDepositConfigVo.getPageSize());
        //queryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
        queryWrapper.orderByDesc(SQLConf.CREATE_TIME);
        IPage<GameDepositConfig> pageList = gameDepositConfigMapper.selectPage(page, queryWrapper);
        return pageList;
    }

    @Override
    public String deleteDepConfig(GameDepositConfigVo gameDepositConfigVo) {
        if(StringUtils.isNotBlank(gameDepositConfigVo.getUid())){
            gameDepositConfigMapper.deleteById(gameDepositConfigVo.getUid());
            return ResultUtil.successWithMessage(MessageConf.OPERATION_SUCCESS);
        }

        return ResultUtil.successWithMessage(MessageConf.OPERATION_FAIL);
    }
}
