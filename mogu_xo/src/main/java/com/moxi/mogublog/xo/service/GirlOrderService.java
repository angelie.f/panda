package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.Customers;
import com.moxi.mogublog.commons.entity.GirlOrder;
import com.moxi.mogublog.commons.entity.GirlProject;
import com.moxi.mogublog.xo.vo.CallbackOrderVO;
import com.moxi.mogublog.xo.vo.GirlOrderVO;
import com.moxi.mogublog.xo.vo.LocalGirlVO;
import com.moxi.mougblog.base.service.SuperService;

import java.util.Map;

/**
 * 妹子订单表 服务类
 *
 * @author 陌溪
 * @date 2021-12-29
 */
public interface GirlOrderService extends SuperService<GirlOrder> {

    /**
     * 前端获取妹子订单列表
     *
     * @param localGirlVO
     * @return
     */
    IPage<GirlOrder> getGirlOrderList(LocalGirlVO localGirlVO);

    /**
     * 获取妹子订单列表
     *
     * @param girlOrderVO
     * @return
     */
    IPage<GirlOrder> getPageList(GirlOrderVO girlOrderVO);

    Map<String,Object> getGirlOrderCountSum(GirlOrderVO GirlOrderVO);

    /**
     * 获取订单金額列表
     *
     * @param localGirlVO
     * @return
     */
    IPage<GirlOrder> amounts(LocalGirlVO localGirlVO);

    /**
     * 获取會員等級列表
     *
     * @param localGirlVO
     * @return
     */
    IPage<Customers> vipLevels(LocalGirlVO localGirlVO);

    /**
     * 获取來源渠道列表
     *
     * @param localGirlVO
     * @return
     */
    IPage<GirlProject> sources(LocalGirlVO localGirlVO);

    /**
     * 编辑订单标签
     *
     * @param girlOrderVO
     */
    String editOrder(GirlOrderVO girlOrderVO);

    /**
     * 生成订单
     */
    String createOrder(LocalGirlVO localGirlVO);

    Map<String,Object> callbackOrder(CallbackOrderVO callbackOrderVO);

    Map<String,String> checkOrderIfComment(LocalGirlVO localGirlVO);



    String getRecentServiceTime(String girlUid);
}


