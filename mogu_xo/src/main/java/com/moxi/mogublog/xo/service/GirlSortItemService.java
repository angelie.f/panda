package com.moxi.mogublog.xo.service;

import com.moxi.mogublog.commons.entity.GirlSortItem;
import com.moxi.mougblog.base.service.SuperService;

/**
 * 妹子上下架 服务类
 *
 * @author Vergift
 * @date 2021-12-29
 */
public interface GirlSortItemService extends SuperService<GirlSortItem> {

}
