package com.moxi.mogublog.xo.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.GirlOrder;
import com.moxi.mogublog.commons.entity.ReadingLog;
import com.moxi.mougblog.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;


public interface ReadingLogMapper extends SuperMapper<ReadingLog> {

    @Select("select source,customer_id,book_id,episode,rlog.update_time," +
            " to_days(update_time) = to_days(now()) as is_today, " +
            " tpb.end as end, " +
            " tpb.title as book_name,tpb.vertical_cover as image_url," +
            " (select max(episode) from tp_book_chapters where book_id =rlog.book_id) as max_episode " +
            " from t_reading_log rlog left join tp_books tpb on rlog.book_id =tpb.id  " +
            " ${ew.customSqlSegment}")
    List<ReadingLog> getList(@Param(Constants.WRAPPER) QueryWrapper queryWrapper);

    @Select("select" +
            " rlog.uid,rlog.source,(select name from t_books_project where source =rlog.source) as source_name," +
            " cust.open_id as customer_id," +
            " book_id,tpb.title as book_name," +
            " episode,(select title from tp_book_chapters where book_id= rlog.book_id and episode =rlog.episode limit 1 ) as capter_name," +
            " DATE_FORMAT(rlog.update_time,'%Y-%m-%d') as update_time,rlog.create_time " +
            " from t_reading_log rlog " +
            " left join tp_books tpb on rlog.book_id =tpb.id " +
            " left join t_customers cust on rlog.customer_id=cust.customer_id " +
            " ${ew.customSqlSegment}")
    IPage<ReadingLog> getPage(Page<ReadingLog> page, @Param(Constants.WRAPPER) QueryWrapper queryWrapper);

}
