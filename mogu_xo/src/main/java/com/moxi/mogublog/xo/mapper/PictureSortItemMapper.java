package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.PictureSortItem;
import com.moxi.mougblog.base.mapper.SuperMapper;

/**
 * 上下架 Mapper 接口
 *
 * @author Vergift
 * @date 2021-12-03
 */
public interface PictureSortItemMapper extends SuperMapper<PictureSortItem> {

}
