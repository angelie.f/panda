package com.moxi.mogublog.xo.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.ReadingLog;
import com.moxi.mogublog.commons.entity.TpPayLog;
import com.moxi.mougblog.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;


public interface TpPayLogMapper extends SuperMapper<TpPayLog> {

    @Select("select" +
            "  plog.source,(select name from t_books_project where source =plog.source) as source_name," +
            "  plog.uid,plog.platform," +
            "  plog.book_id," +
            "  tpb.title as book_name," +
            "  plog.episode,plog.open_id," +
            "  (select title from tp_book_chapters where book_id = plog.book_id and episode =plog.episode limit 1) as chapter_name," +
            "  price,plog.create_time,plog.update_time " +
            " from tp_pay_log plog " +
            " left join tp_books tpb on plog.book_id =tpb.id " +
            "${ew.customSqlSegment}")
    IPage<TpPayLog> getPage(Page<TpPayLog> page, @Param(Constants.WRAPPER) QueryWrapper queryWrapper);
}
