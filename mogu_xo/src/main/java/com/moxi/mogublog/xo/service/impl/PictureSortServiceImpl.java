package com.moxi.mogublog.xo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.*;
import com.moxi.mogublog.commons.feign.PictureFeignClient;
import com.moxi.mogublog.utils.RedisUtil;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.mapper.PictureSortMapper;
import com.moxi.mogublog.xo.service.*;
import com.moxi.mogublog.xo.utils.WebUtil;
import com.moxi.mogublog.xo.vo.CustomersVO;
import com.moxi.mogublog.xo.vo.PictureSortVO;
import com.moxi.mogublog.xo.vo.ProjectVO;
import com.moxi.mougblog.base.enums.EStatus;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 图片分类表 服务实现类
 *
 * @author Vergift
 * @date 2021-12-02
 */
@Service
public class PictureSortServiceImpl extends SuperServiceImpl<PictureSortMapper, PictureSort> implements PictureSortService {

    @Autowired
    private WebUtil webUtil;
    @Autowired
    RedisUtil redisUtil;
    @Autowired
    private PictureSortService pictureSortService;
    @Resource
    private PictureFeignClient pictureFeignClient;
    @Autowired
    private PictureTagService pictureTagService;
    @Resource
    private PictureService pictureService;
    @Resource
    private ProjectService projectService;
    @Resource
    private PictureAtlasService pictureAtlasService;
    @Resource
    private PictureSortItemService pictureSortItemService;
    @Resource
    private UsePowerService usePowerService;
    @Resource
    private AdminService adminService;

    @Override
    public IPage<PictureSort> getPageList(PictureSortVO pictureSortVO) {
        QueryWrapper<PictureSort> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(pictureSortVO.getProjectUid()) && !StringUtils.isEmpty(pictureSortVO.getProjectUid().trim())){
            queryWrapper.eq(SQLConf.PROJECT_UID,pictureSortVO.getProjectUid());//只取当前用户所属项目的图集分类
        }else {
            Project project = initPorject();
            if(project != null){
                queryWrapper.eq(SQLConf.PROJECT_UID,project.getUid());//只取当前用户所属项目的图集分类
            }
        }

        if (StringUtils.isNotEmpty(pictureSortVO.getKeyword()) && !StringUtils.isEmpty(pictureSortVO.getKeyword().trim())) {
            queryWrapper.like(SQLConf.NAME, pictureSortVO.getKeyword().trim());
        }
        if (!StringUtils.isEmpty(pictureSortVO.getCreateTime())) {
            queryWrapper.like(SQLConf.CREATE_TIME, pictureSortVO.getCreateTime());
        }
        Page<PictureSort> page = new Page<>();
        page.setCurrent(pictureSortVO.getCurrentPage());
        page.setSize(pictureSortVO.getPageSize());
        queryWrapper.orderByDesc(SQLConf.CREATE_TIME);
        IPage<PictureSort> pageList = pictureSortService.page(page, queryWrapper);
        List<PictureSort> list = pageList.getRecords();
        List<String> tagUids = new ArrayList<>();
        List<String> projectUids = new ArrayList<>();
        final StringBuffer fileUids = new StringBuffer();
        list.forEach(item -> {
            if (StringUtils.isNotEmpty(item.getFileUid())) {
                fileUids.append(item.getFileUid() + SysConf.FILE_SEGMENTATION);
            }
            if (StringUtils.isNotEmpty(item.getTagUid())) {
                List<String> tagUidsTemp = StringUtils.changeStringToString(item.getTagUid(), SysConf.FILE_SEGMENTATION);
                for (String itemTagUid : tagUidsTemp) {
                    tagUids.add(itemTagUid);
                }
            }
            if (StringUtils.isNotEmpty(item.getProjectUid())) {
                List<String> projectUidsTemp = StringUtils.changeStringToString(item.getProjectUid(), SysConf.FILE_SEGMENTATION);
                for (String itemProjectUid : projectUidsTemp) {
                    projectUids.add(itemProjectUid);
                }
            }
        });
        String pictureResult = null;
        Map<String, String> pictureMap = new HashMap<>();
        if (fileUids != null) {
            pictureResult = this.pictureFeignClient.getPicture(fileUids.toString(), SysConf.FILE_SEGMENTATION);
        }
        List<Map<String, Object>> picList = webUtil.getPictureMap(pictureResult);

        picList.forEach(item -> {
            pictureMap.put(item.get(SysConf.UID).toString(), item.get(SysConf.URL).toString());
        });

        Collection<PictureTag> tagList = new ArrayList<>();
        if (tagUids.size() > 0) {
            tagList = pictureTagService.listByIds(tagUids);
        }

        Collection<Project> projectList = new ArrayList<>();
        if (tagUids.size() > 0) {
            projectList = projectService.listByIds(projectUids);
        }

        Map<String, PictureTag> tagMap = new HashMap<>();

        tagList.forEach(item -> {
            tagMap.put(item.getUid(), item);
        });

        Map<String, Project> projectMap = new HashMap<>();

        projectList.forEach(item -> {
            projectMap.put(item.getUid(), item);
        });

        List<UsePower> usePowerList = usePowerService.list();
        for (PictureSort item : list) {

            QueryWrapper<PictureSortItem> upCountQueryWrapper = new QueryWrapper<>();
            upCountQueryWrapper.eq(SQLConf.SORT_UID,item.getUid());
            upCountQueryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
            item.setUpCount(pictureSortItemService.list(upCountQueryWrapper).size());

            QueryWrapper<PictureSortItem> outCountQueryWrapper = new QueryWrapper<>();
            outCountQueryWrapper.eq(SQLConf.SORT_UID,item.getUid());
            outCountQueryWrapper.eq(SQLConf.STATUS, EStatus.DISABLED);
            item.setOutCount(pictureSortItemService.list(outCountQueryWrapper).size());

            //获取标签
            if (StringUtils.isNotEmpty(item.getTagUid())) {
                List<String> tagUidsTemp = StringUtils.changeStringToString(item.getTagUid(), SysConf.FILE_SEGMENTATION);
                List<PictureTag> tagListTemp = new ArrayList<>();

                tagUidsTemp.forEach(tag -> {
                    tagListTemp.add(tagMap.get(tag));
                });
                item.setTagList(tagListTemp);
            }

            //获取项目
            if (StringUtils.isNotEmpty(item.getProjectUid())) {

                item.setProject(projectMap.get(item.getProjectUid()));

                //获取使用权限
                for (UsePower usePower:usePowerList) {
                    if (usePower.getProjectUid().equals(item.getProjectUid())){
                        if (usePower.getUserAstrict() == 1){
                            item.setUerPower("会员");
                        }else {
                            item.setUerPower("全部用户");
                        }
                        continue;
                    }
                }
            }

            //获取图片
            if (StringUtils.isNotEmpty(item.getFileUid())) {
                List<String> pictureUidsTemp = StringUtils.changeStringToString(item.getFileUid(), SysConf.FILE_SEGMENTATION);
                List<String> pictureListTemp = new ArrayList<>();
                pictureUidsTemp.forEach(picture -> {
                    pictureListTemp.add(pictureMap.get(picture));
                });
                item.setPhotoList(pictureListTemp);
            }
        }
        pageList.setRecords(list);
        return pageList;
    }

    @Override
    public List<PictureSort> getResourceList() {
        QueryWrapper<PictureSort> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("DISTINCT "+SQLConf.RESOURCE);
        queryWrapper.isNotNull(SQLConf.RESOURCE);
        List<PictureSort> pageList = pictureSortService.list(queryWrapper);
        return pageList;
    }

    @Override
    public String addPictureSort(PictureSort pictureSort) {
        if ( initPorject()!=null){
            pictureSort.setProjectUid(initPorject().getUid());
        }
        pictureSort.insert();
        initSortItem(pictureSort);
        return ResultUtil.successWithMessage(MessageConf.INSERT_SUCCESS);
    }

    @Override
    public String editPictureSort(PictureSort pictureSort) {
        pictureSort.setUpdateTime(new Date());
        pictureSort.updateById();
        initSortItem(pictureSort);
        return ResultUtil.successWithMessage(MessageConf.UPDATE_SUCCESS);
    }

    //获取当前用户账号和所属项目
    public Project initPorject(){
        ServletRequestAttributes attribute = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attribute.getRequest();
        // 解析出请求者的ID和用户名
        String adminUid = request.getAttribute(SysConf.ADMIN_UID).toString();
        Admin admin = adminService.getById(adminUid);
        return projectService.getById(admin.getProjectUid());
    }

    //新增和编辑时需要对应的更新上下架信息
    public void initSortItem(PictureSort pictureSort){
        String[] tagUidList = pictureSort.getTagUid().split(",");
        List<PictureSortItem> pictureSortItemList = new ArrayList<>();
        Map<String,PictureSortItem> map = new HashMap();
        for (String tagUid:tagUidList) {
            QueryWrapper<PictureAtlas> queryWrapper = new QueryWrapper<>();
            queryWrapper.orderByDesc(SQLConf.CREATE_TIME);
            if (!StringUtils.isEmpty(tagUid)) {
                queryWrapper.and(wrapper ->wrapper.eq(SQLConf.TAG_UID, tagUid)
                        .or().likeRight(SQLConf.TAG_UID, tagUid + SysConf.FILE_SEGMENTATION)
                        .or().like(SQLConf.TAG_UID, SysConf.FILE_SEGMENTATION + tagUid + SysConf.FILE_SEGMENTATION)
                        .or().likeLeft(SQLConf.TAG_UID, SysConf.FILE_SEGMENTATION + tagUid));
                List<PictureAtlas> pageList = pictureAtlasService.list(queryWrapper);

                for (PictureAtlas pictureAtlas:pageList) {
                    //新的上下架信息
                    PictureSortItem pictureSortItem = new PictureSortItem();
                    pictureSortItem.setUid(null);
                    pictureSortItem.setAtlasUid(Integer.valueOf(pictureAtlas.getUid()));
                    pictureSortItem.setSortUid(pictureSort.getUid());
                    pictureSortItem.setStatus(EStatus.DISABLED);//默认为下架中

                    //去掉重复的图集
                    map.put(pictureAtlas.getUid(),pictureSortItem);

                }
            }

        }

        //获取老的上下架信息
        QueryWrapper<PictureSortItem> sortItemQueryWrapper = new QueryWrapper<>();
        sortItemQueryWrapper.eq(SQLConf.SORT_UID,pictureSort.getUid());
        List<PictureSortItem> sortItemList = pictureSortItemService.list(sortItemQueryWrapper);
        List<String> sortItemUids = new ArrayList<>();
        //删除原因的上下架信息
        if (sortItemList.size() > 0){
            for (PictureSortItem sortItem:sortItemList) {
                if (map.get(String.valueOf(sortItem.getAtlasUid())) != null){
                    ////去掉已有的分类图集上下信息
                    map.remove(String.valueOf(sortItem.getAtlasUid()));
                }else {
                    sortItemUids.add(sortItem.getUid());
                }
            }

            //去掉不需要的分类图集上下信息
            if (sortItemUids.size() > 0){
                pictureSortItemService.removeByIds(sortItemUids);
            }
        }

        for(String atlasUid:map.keySet()){
            pictureSortItemList.add(map.get(atlasUid));
        }

        if (pictureSortItemList.size() > 0){
            //批量添加新的上下架信息
            pictureSortItemService.saveBatch(pictureSortItemList);
        }

    }

    @Override
    public IPage<PictureSort> getCustomersSortList(CustomersVO customersVO) {
        ProjectVO projectVO = new ProjectVO();
        projectVO.setSource(customersVO.getSource());
        List<Project> projectList = projectService.getList(projectVO);

        QueryWrapper<PictureSort> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SQLConf.PROJECT_UID,projectList.get(0).getUid());//只取当前用户所属项目的图集分类
        queryWrapper.orderByDesc(SQLConf.SORT);

        Page<PictureSort> page = new Page<>();
        if (customersVO.getCurrentPage() == null){
            page.setCurrent(1);
        }else {
            page.setCurrent(customersVO.getCurrentPage());
        }
        if (customersVO.getPageSize() == null){
            page.setSize(10000);
        }else {
            page.setSize(customersVO.getPageSize());
        }

        IPage<PictureSort> pageList = pictureSortService.page(page, queryWrapper);
        List<PictureSort> list = pageList.getRecords();
        final StringBuffer fileUids = new StringBuffer();
        list.forEach(item -> {
            if (StringUtils.isNotEmpty(item.getFileUid())) {
                fileUids.append(item.getFileUid() + SysConf.FILE_SEGMENTATION);
            }
        });
        String pictureResult = null;
        Map<String, String> pictureMap = new HashMap<>();
        if (fileUids != null) {
            pictureResult = this.pictureFeignClient.getPicture(fileUids.toString(), SysConf.FILE_SEGMENTATION);
        }
        List<Map<String, Object>> picList = webUtil.getPictureMap(pictureResult);

        picList.forEach(item -> {
            pictureMap.put(item.get(SysConf.UID).toString(), item.get(SysConf.URL).toString());
        });


        for (PictureSort item : list) {
            //获取图片
            if (StringUtils.isNotEmpty(item.getFileUid())) {
                List<String> pictureUidsTemp = StringUtils.changeStringToString(item.getFileUid(), SysConf.FILE_SEGMENTATION);
                List<String> pictureListTemp = new ArrayList<>();
                pictureUidsTemp.forEach(picture -> {
                    pictureListTemp.add(pictureMap.get(picture));
                });
                item.setPhotoList(pictureListTemp);
            }
        }
        pageList.setRecords(list);
        return pageList;
    }
}
