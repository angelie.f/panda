package com.moxi.mogublog.xo.service.impl;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.*;
import com.moxi.mogublog.utils.ActEnum;
import com.moxi.mogublog.utils.GameActEnum;
import com.moxi.mogublog.utils.RedisUtil;
import com.moxi.mogublog.xo.mapper.*;
import com.moxi.mogublog.xo.service.GameActRecordService;
import com.moxi.mogublog.xo.vo.ActPreInfo;
import com.moxi.mogublog.xo.vo.ActRecordRequest;
import com.moxi.mogublog.xo.vo.ActRecordResponse;
import com.moxi.mogublog.xo.vo.GameActRecordVo;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
@Slf4j
public class GameActRecordServiceImpl extends SuperServiceImpl<GameActRecordMapper, GameActRecord> implements GameActRecordService {
    @Autowired
    private GameActRecordMapper gameActRecordMapper;
    @Autowired
    private GameCustomersBalanceMapper gameCustomersBalanceMapper;
    @Autowired
    private GameCreditLogsMapper gameCreditLogsMapper;
    @Autowired
    RedisUtil redisUtil;
    @Autowired
    private GameActConfigMapper gameActConfigMapper;
    @Autowired
    private CustomersMapper customersMapper;

    @Transactional(rollbackFor = {Exception.class})
    @Override
     public Boolean addLoginActRecord(Customers customersResult){
         try{
             QueryWrapper<GameActConfig> queryWrapper = new QueryWrapper<>();
             queryWrapper.eq("status",1);
             queryWrapper.eq("act_type",ActEnum.FIRSTLOGINPRE.name());
             queryWrapper.le("act_begin_time",new Date());
             queryWrapper.ge("act_end_time",new Date());
             GameActConfig configList = gameActConfigMapper.selectOne(queryWrapper);
             if(ObjectUtils.isEmpty(configList)){
                 return false;
             }
             Optional<GameActEnum> findActSource = GameActEnum.findActSource(customersResult.getSource().toString());
             if(ObjectUtils.isEmpty(findActSource)){
                 return false;
             }
             BigDecimal actPreAmount =  new BigDecimal(String.valueOf(JSONObject.parseObject(configList.getActInfo()).get(findActSource.get().getKey())));
             QueryWrapper<GameActRecord> gameActRecordQueryWrapper = new QueryWrapper<>();
             gameActRecordQueryWrapper.eq("customer_id",customersResult.getCustomerId());
             gameActRecordQueryWrapper.in("act_type", configList.getActType());
             gameActRecordQueryWrapper.eq("flag",1);
             GameActRecord gameActRecord = gameActRecordMapper.selectOne(gameActRecordQueryWrapper);
             if(ObjectUtils.isEmpty(gameActRecord) && configList.getActChannel().contains(customersResult.getSource().toString()) && !ObjectUtils.isEmpty(actPreAmount) && actPreAmount.compareTo(BigDecimal.ZERO)==1){
                 GameActRecord gameAddCoins = new GameActRecord();
                 gameAddCoins.setActType(configList.getActType());
                 gameAddCoins.setCustomerId(customersResult.getCustomerId());
                 gameAddCoins.setOpenId(customersResult.getOpenId());
                 gameAddCoins.setActPreAmount(actPreAmount);
                 gameAddCoins.setOpenId(customersResult.getOpenId());
                 gameAddCoins.setCustomerId(customersResult.getCustomerId());
                 //gameAddCoins.setActDesc(configList.getActTitle());
                 gameAddCoins.setFlag(1);
                 gameAddCoins.setSource(customersResult.getSource()==null?"":customersResult.getSource().toString());
                 gameAddCoins.setRequestId("D"+ com.moxi.mogublog.utils.StringUtils.getSnowflakeId());
                 gameActRecordMapper.insert(gameAddCoins);
                 GameCreditLogs gameCreditLogs = new GameCreditLogs();
                 String key = "gameBalance_"+customersResult.getCustomerId();
                 boolean isLock = redisUtil.setIfAbsent(key,"",30*1000, TimeUnit.MILLISECONDS);
                 if (isLock) {
                     try {
                         QueryWrapper<GameCustomersBalance> query = new QueryWrapper<>();
                         query.eq("customer_id",customersResult.getCustomerId());
                         GameCustomersBalance gameCustomersBalance = gameCustomersBalanceMapper.selectOne(query);
                         if(ObjectUtils.isEmpty(gameCustomersBalance)){
                             GameCustomersBalance balance = new GameCustomersBalance();
                             balance.setCustomerId(customersResult.getCustomerId());
                             balance.setCoins(gameAddCoins.getActPreAmount());
                             balance.setFrozenCoins(new BigDecimal(0));
                             gameCustomersBalanceMapper.insert(balance);
                             gameCreditLogs.setCoinsBefore(new BigDecimal(0));
                             gameCreditLogs.setCoinsAfter(gameCustomersBalance.getCoins());
                         }else{
                             gameCreditLogs.setCoinsBefore(gameCustomersBalance.getCoins());
                             gameCustomersBalance.setCoins(gameCustomersBalance.getCoins().add(gameAddCoins.getActPreAmount()));
                             gameCreditLogs.setCoinsAfter(gameCustomersBalance.getCoins());
                             gameCustomersBalance.setMeVersion(null);
                             gameCustomersBalanceMapper.updateById(gameCustomersBalance);
                         }
                     } finally {
                         if (redisUtil.hasKey(key))
                             redisUtil.delete(key);
                     }
                 }else {
                     throw new Exception("系统繁忙");
                 }
                 gameCreditLogs.setCustomerId(customersResult.getCustomerId().trim());
                 gameCreditLogs.setType(1);
                 gameCreditLogs.setSource(customersResult.getSource());
                 gameCreditLogs.setReferenceId(gameAddCoins.getRequestId());
                 gameCreditLogsMapper.insert(gameCreditLogs);
             }
         }catch(Exception e){
             TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
             log.error("用户ID:{}登录赠币执行异常:{}",customersResult.getOpenId(),e.getMessage());
         }
         return true;
     }

    @Transactional(rollbackFor = {Exception.class})
    @Override
    public Boolean addVipActRecord(Customers customersResult) {
        try{
            QueryWrapper<GameActConfig> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("status",1);
            queryWrapper.eq("act_type",ActEnum.VIPLEVELPRE.name());
            queryWrapper.le("act_begin_time",new Date());
            queryWrapper.ge("act_end_time",new Date());
            GameActConfig configList = gameActConfigMapper.selectOne(queryWrapper);
            if(ObjectUtils.isEmpty(configList)){
                return false;
            }
            Optional<GameActEnum> findActSource = GameActEnum.findActSource(customersResult.getSource().toString());
            if(ObjectUtils.isEmpty(findActSource)){
                return false;
            }
            BigDecimal actPreAmount =  new BigDecimal(String.valueOf(JSONObject.parseObject(configList.getActInfo()).get(findActSource.get().getKey())));
            QueryWrapper<GameActRecord> vipPreQuery = new QueryWrapper<>();
            vipPreQuery.eq("customer_id",customersResult.getCustomerId());
            vipPreQuery.in("act_type", configList.getActType());
            vipPreQuery.eq("flag",1);
            GameActRecord gameActRecord = gameActRecordMapper.selectOne(vipPreQuery);
            if(ObjectUtils.isEmpty(gameActRecord) && !ObjectUtils.isEmpty(customersResult.getIsVip()) && customersResult.getIsVip()==1 && configList.getActChannel().contains(customersResult.getSource().toString()) && !ObjectUtils.isEmpty(actPreAmount) && actPreAmount.compareTo(BigDecimal.ZERO)==1){
                GameActRecord gameAddCoins = new GameActRecord();
                gameAddCoins.setActType(configList.getActType());
                gameAddCoins.setCustomerId(customersResult.getCustomerId());
                gameAddCoins.setOpenId(customersResult.getOpenId());
                gameAddCoins.setActPreAmount(actPreAmount);
                gameAddCoins.setOpenId(customersResult.getOpenId());
                gameAddCoins.setCustomerId(customersResult.getCustomerId());
                //gameAddCoins.setActDesc(configList.getActTitle());
                gameAddCoins.setFlag(1);
                gameAddCoins.setSource(customersResult.getSource()==null?"":customersResult.getSource().toString());
                gameAddCoins.setRequestId("D"+ com.moxi.mogublog.utils.StringUtils.getSnowflakeId());
                gameActRecordMapper.insert(gameAddCoins);
                GameCreditLogs gameCreditLogs = new GameCreditLogs();
                String key = "gameBalance_"+customersResult.getCustomerId();
                boolean isLock = redisUtil.setIfAbsent(key,"",30*1000, TimeUnit.MILLISECONDS);
                if (isLock) {
                    try {
                        QueryWrapper<GameCustomersBalance> query = new QueryWrapper<>();
                        query.eq("customer_id",customersResult.getCustomerId());
                        GameCustomersBalance gameCustomersBalance = gameCustomersBalanceMapper.selectOne(query);
                        if(ObjectUtils.isEmpty(gameCustomersBalance)){
                            GameCustomersBalance balance = new GameCustomersBalance();
                            balance.setCustomerId(customersResult.getCustomerId());
                            balance.setCoins(gameAddCoins.getActPreAmount());
                            balance.setFrozenCoins(new BigDecimal(0));
                            gameCustomersBalanceMapper.insert(balance);
                            gameCreditLogs.setCoinsBefore(new BigDecimal(0));
                            gameCreditLogs.setCoinsAfter(gameCustomersBalance.getCoins());
                        }else{
                            gameCreditLogs.setCoinsBefore(gameCustomersBalance.getCoins());
                            gameCustomersBalance.setCoins(gameCustomersBalance.getCoins().add(gameAddCoins.getActPreAmount()));
                            gameCreditLogs.setCoinsAfter(gameCustomersBalance.getCoins());
                            gameCustomersBalance.setMeVersion(null);
                            gameCustomersBalanceMapper.updateById(gameCustomersBalance);
                        }
                    } finally {
                        if (redisUtil.hasKey(key))
                            redisUtil.delete(key);
                    }
                }else {
                    throw new Exception("系统繁忙");
                }
                gameCreditLogs.setCustomerId(customersResult.getCustomerId().trim());
                gameCreditLogs.setType(1);
                gameCreditLogs.setSource(customersResult.getSource());
                gameCreditLogs.setReferenceId(gameAddCoins.getRequestId());
                gameCreditLogsMapper.insert(gameCreditLogs);
            }
        }catch(Exception e){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            log.error("用户ID:{}会员赠币执行异常:{}",customersResult.getOpenId(),e.getMessage());
        }
        return true;
    }

    @Transactional(rollbackFor = {Exception.class})
    @Override
    public Boolean addBindActRecord(Customers customersResult) {
        try{
            QueryWrapper<GameActConfig> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("status",1);
            queryWrapper.eq("act_type", ActEnum.BINDPHONEPRE.name());
            queryWrapper.le("act_begin_time",new Date());
            queryWrapper.ge("act_end_time",new Date());
            GameActConfig configList = gameActConfigMapper.selectOne(queryWrapper);
            if(ObjectUtils.isEmpty(configList)){
                return false;
            }
            Optional<GameActEnum> findActSource = GameActEnum.findActSource(customersResult.getSource().toString());
            if(ObjectUtils.isEmpty(findActSource)){
                return false;
            }
            BigDecimal actPreAmount =  new BigDecimal(String.valueOf(JSONObject.parseObject(configList.getActInfo()).get(findActSource.get().getKey())));
            QueryWrapper<GameActRecord> vipPreQuery = new QueryWrapper<>();
            vipPreQuery.eq("customer_id",customersResult.getCustomerId());
            vipPreQuery.in("act_type", configList.getActType());
            vipPreQuery.eq("flag",1);
            GameActRecord gameActRecord = gameActRecordMapper.selectOne(vipPreQuery);
            if(ObjectUtils.isEmpty(gameActRecord) && org.apache.commons.lang.StringUtils.isNotBlank(customersResult.getPhone()) && configList.getActChannel().contains(customersResult.getSource().toString()) && !ObjectUtils.isEmpty(actPreAmount) && actPreAmount.compareTo(BigDecimal.ZERO)==1){
                GameActRecord gameAddCoins = new GameActRecord();
                gameAddCoins.setActType(configList.getActType());
                gameAddCoins.setCustomerId(customersResult.getCustomerId());
                gameAddCoins.setOpenId(customersResult.getOpenId());
                gameAddCoins.setActPreAmount(actPreAmount);
                gameAddCoins.setOpenId(customersResult.getOpenId());
                gameAddCoins.setCustomerId(customersResult.getCustomerId());
                //gameAddCoins.setActDesc(configList.getActTitle());
                gameAddCoins.setFlag(1);
                gameAddCoins.setSource(customersResult.getSource()==null?"":customersResult.getSource().toString());
                gameAddCoins.setRequestId("D"+ com.moxi.mogublog.utils.StringUtils.getSnowflakeId());
                gameActRecordMapper.insert(gameAddCoins);
                GameCreditLogs gameCreditLogs = new GameCreditLogs();
                String key = "gameBalance_"+customersResult.getCustomerId();
                boolean isLock = redisUtil.setIfAbsent(key,"",30*1000, TimeUnit.MILLISECONDS);
                if (isLock) {
                    try {
                        QueryWrapper<GameCustomersBalance> query = new QueryWrapper<>();
                        query.eq("customer_id",customersResult.getCustomerId());
                        GameCustomersBalance gameCustomersBalance = gameCustomersBalanceMapper.selectOne(query);
                        if(ObjectUtils.isEmpty(gameCustomersBalance)){
                            GameCustomersBalance balance = new GameCustomersBalance();
                            balance.setCustomerId(customersResult.getCustomerId());
                            balance.setCoins(gameAddCoins.getActPreAmount());
                            balance.setFrozenCoins(new BigDecimal(0));
                            gameCustomersBalanceMapper.insert(balance);
                            gameCreditLogs.setCoinsBefore(new BigDecimal(0));
                            gameCreditLogs.setCoinsAfter(gameCustomersBalance.getCoins());
                        }else{
                            gameCreditLogs.setCoinsBefore(gameCustomersBalance.getCoins());
                            gameCustomersBalance.setCoins(gameCustomersBalance.getCoins().add(gameAddCoins.getActPreAmount()));
                            gameCreditLogs.setCoinsAfter(gameCustomersBalance.getCoins());
                            gameCustomersBalance.setMeVersion(null);
                            gameCustomersBalanceMapper.updateById(gameCustomersBalance);
                        }
                    } finally {
                        if (redisUtil.hasKey(key))
                            redisUtil.delete(key);
                    }
                }else {
                    throw new Exception("系统繁忙");
                }
                gameCreditLogs.setCustomerId(customersResult.getCustomerId().trim());
                gameCreditLogs.setType(1);
                gameCreditLogs.setSource(customersResult.getSource());
                gameCreditLogs.setReferenceId(gameAddCoins.getRequestId());
                gameCreditLogsMapper.insert(gameCreditLogs);
            }
        }catch(Exception e){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            log.error("用户ID:{}会员赠币执行异常:{}",customersResult.getOpenId(),e.getMessage());
        }
        return true;
    }

    @Override
    public IPage<GameActRecord> getPageList(GameActRecordVo gameActRecordVo) {
        Page<GameActRecord> page = new Page<>();
        page.setCurrent(gameActRecordVo.getCurrentPage());
        page.setSize(gameActRecordVo.getPageSize());
        IPage<GameActRecord> pageList = gameActRecordMapper.queryGameActRecord(page, gameActRecordVo);
        return pageList;
    }


    @Override
    public Boolean addActRecord(Customers customersResult) {
        addLoginActRecord(customersResult);
        addVipActRecord(customersResult);
        addBindActRecord(customersResult);
        return true;
    }

    @Override
    public ActRecordResponse searchActUserInfo(ActRecordRequest actRecordRequest) {
        QueryWrapper<Customers> query = new QueryWrapper<>();
        query.eq("customer_id",actRecordRequest.getCustomerId());
        List<Customers> customers = customersMapper.selectList(query);
        ActRecordResponse actRecordResponse = new ActRecordResponse();
        actRecordResponse.setCustomerId(actRecordRequest.getCustomerId());
        actRecordResponse.setTotalAmount(BigDecimal.ZERO);
        actRecordResponse.setShowPop(true);
        List<ActPreInfo> actPreInfoList = new ArrayList<>();
        actRecordResponse.setActPreInfoList(actPreInfoList);
        QueryWrapper<GameActConfig> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status",1);
        queryWrapper.le("act_begin_time",new Date());
        queryWrapper.ge("act_end_time",new Date());
        List<GameActConfig> configList = gameActConfigMapper.selectList(queryWrapper);
        if(CollectionUtils.isEmpty(configList) || CollectionUtils.isEmpty(customers)){
            actRecordResponse.setShowPop(false);
            return actRecordResponse;
        }
        Customers customersResult = customers.get(0);
        List<GameActConfig> singleConfigList = configList.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(()->new TreeSet<>(Comparator.comparing(GameActConfig::getActType))),ArrayList::new));
        log.info("singleConfigList size:{}",singleConfigList.size());
        log.info("singleConfigList:{}",singleConfigList);
        singleConfigList.forEach(item->{
            GameActConfig gameActConfig = item;
            Optional<GameActEnum> findActSource = GameActEnum.findActSource(customersResult.getSource().toString());
            if(!ObjectUtils.isEmpty(findActSource) && gameActConfig.getActChannel().contains(customersResult.getSource().toString())){
                BigDecimal actPreAmount =  new BigDecimal(String.valueOf(JSONObject.parseObject(gameActConfig.getActInfo()).get(findActSource.get().getKey())));
                if(!ObjectUtils.isEmpty(actPreAmount) && actPreAmount.compareTo(BigDecimal.ZERO)==1){
                    ActPreInfo actPreInfo = new ActPreInfo();
                    actPreInfo.setAmount(actPreAmount);
                    actPreInfo.setActType(gameActConfig.getActType());
                    actPreInfo.setActTitle(gameActConfig.getActTitle());
                    QueryWrapper<GameActRecord> recordQueryWrapper = new QueryWrapper<>();
                    recordQueryWrapper.eq("customer_id",actRecordRequest.getCustomerId());
                    recordQueryWrapper.in("act_type", gameActConfig.getActType());
                    recordQueryWrapper.eq("flag",1);
                    GameActRecord gameActRecord = gameActRecordMapper.selectOne(recordQueryWrapper);
                    actPreInfo.setFalg(0);
                    actPreInfo.setPop(false);
                    if(!ObjectUtils.isEmpty(gameActRecord)) {
                        actPreInfo.setFalg(gameActRecord.getFlag());
                        actRecordResponse.setTotalAmount(actRecordResponse.getTotalAmount().add(gameActRecord.getActPreAmount()));
                        if (!ObjectUtils.isEmpty(gameActRecord.getPopTimes()) && gameActRecord.getPopTimes() == 1) {
                            actPreInfo.setPop(true);
                        } else {
                            gameActRecord.setPopTimes(1);
                            gameActRecordMapper.updateById(gameActRecord);
                        }
                    }
                    actPreInfoList.add(actPreInfo);
                }
            }
        });
        log.info("actPreInfoList size:{}",actPreInfoList.size());
        if(!CollectionUtils.isEmpty(actPreInfoList)){
            actRecordResponse.setActPreInfoList(actPreInfoList);
          List<ActPreInfo> gameActConfigList = actPreInfoList.stream().filter(item-> item.getPop()==false).collect(Collectors.toList());
          if(CollectionUtils.isEmpty(gameActConfigList)){
              actRecordResponse.setShowPop(false);
          }
        }else{
            actRecordResponse.setShowPop(false);
        }
        return actRecordResponse;
    }
}
