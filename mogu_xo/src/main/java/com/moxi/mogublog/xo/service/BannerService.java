package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.Banner;
import com.moxi.mogublog.xo.vo.BannerVo;
import com.moxi.mougblog.base.service.SuperService;

import java.util.List;

public interface BannerService extends SuperService<Banner> {

    /**
     * 添加
     *
     * @param bannerVo
     * @return
     */
   String addBanner(BannerVo bannerVo);

    /**
     * 编辑
     *
     * @param bannerVo
     * @return
     */
    String editBanner(BannerVo bannerVo);


    IPage<Banner> getPageList(BannerVo bannerVo);
}
