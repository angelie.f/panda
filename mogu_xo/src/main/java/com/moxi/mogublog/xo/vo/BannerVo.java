package com.moxi.mogublog.xo.vo;

import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class BannerVo extends BaseVO<BannerVo> {

    private String imageUrl;
    private String jumpUrl;
    private Integer sort;
    private Integer openFlag;
    private String title;
    private Integer fileUid;
    private String showSource;
    private String bannerType;

}
