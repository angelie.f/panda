package com.moxi.mogublog.xo.vo;

import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class GameManagerVo extends BaseVO<GameManagerVo> {

    private String name;
    private Integer sort;
    private String imageUrl;
    private Integer fileUid;
    private Integer gameSource;
    private String orderByDescColumn;
    private String orderByAscColumn;
    private String gameTypeUid;

}
