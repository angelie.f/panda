package com.moxi.mogublog.xo.mapper;


import com.moxi.mogublog.commons.entity.UserDailyRead;
import com.moxi.mougblog.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface UserDailyReadMapper extends SuperMapper<UserDailyRead> {

    @Select(" select count(1) from t_user_daily_read where read_date=#{today}  and customer_id= #{customerId} ")
    Integer queryReadedCount(@Param("today") String today,@Param("customerId") String customerId);

    @Select(" select count(1) from t_user_daily_read where read_date=#{today}  and customer_id= #{customerId} and book_id=#{bookId} and episode=#{episode}")
    Integer queryRepeat(@Param("today") String today,@Param("customerId") String customerId,@Param("bookId") Integer bookId,@Param("episode") Integer episode);

}
