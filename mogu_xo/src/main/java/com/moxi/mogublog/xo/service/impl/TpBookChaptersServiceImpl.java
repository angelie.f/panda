package com.moxi.mogublog.xo.service.impl;

import com.alibaba.csp.sentinel.util.StringUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.ReadingLog;
import com.moxi.mogublog.commons.entity.TpBooks;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.mapper.BooksSettingMapper;
import com.moxi.mogublog.xo.mapper.BooksUserCostMapper;
import com.moxi.mogublog.xo.mapper.CustomersMapper;
import com.moxi.mogublog.xo.service.ReadingLogService;
import com.moxi.mogublog.xo.vo.LocalBooksVO;
import com.moxi.mogublog.xo.vo.TpBookChaptersVO;
import com.moxi.mogublog.xo.vo.TpBooksVO;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.moxi.mogublog.xo.mapper.TpBookChaptersMapper;
import com.moxi.mogublog.xo.service.TpBookChaptersService;
import com.moxi.mogublog.commons.entity.TpBookChapters;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TpBookChaptersServiceImpl extends SuperServiceImpl<TpBookChaptersMapper, TpBookChapters> implements TpBookChaptersService {

    @Autowired
    private CustomersMapper customersMapper;

    @Autowired
    private BooksUserCostMapper booksUserCostMapper;

    @Autowired
    private ReadingLogService readingLogService;

    @Autowired
    BooksSettingMapper booksSettingMapper;

    @Value("${books.domain}")
    private String imageDomain;

    @Override
    public String getBookChapters(LocalBooksVO localBooksVO) {
        QueryWrapper<TpBookChapters> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("book_id","episode","title","content","json_images","charge","created_at", "if(DATE_ADD(created_at , INTERVAL 1 MONTH)>current_timestamp(),1,0) as is_new");
        queryWrapper.eq("status",1);
        queryWrapper.eq("book_id",localBooksVO.getBookId());
        if(localBooksVO.getEpisode()!=null){
            queryWrapper.eq("episode",localBooksVO.getEpisode());
        }
        if(StringUtils.isNotEmpty(localBooksVO.getOrderType())&&"desc".equals(localBooksVO.getOrderType())){
            queryWrapper.orderByDesc("episode");
        }
        else{
            queryWrapper.orderByAsc("episode");
        }
        Page<TpBookChapters> page = new Page<>();
        if (localBooksVO.getCurrentPage() == null){
            page.setCurrent(1);
        }else {
            page.setCurrent(localBooksVO.getCurrentPage());
        }
        if (localBooksVO.getPageSize() == null){
            page.setSize(10000);
        }else {
            page.setSize(localBooksVO.getPageSize());
        }
        IPage<TpBookChapters> pageList = page(page, queryWrapper);
        int readAbleNum = getReadAbleCount(localBooksVO.getCustomerId(),localBooksVO.getSource());

        for(TpBookChapters chapters:pageList.getRecords()){
            Integer isPay = baseMapper.isPay(chapters.getBookId(),chapters.getEpisode(),localBooksVO.getCustomerId());
            //会员可看集数设定为免费
            if(chapters.getCharge()>0&&chapters.getEpisode()<=readAbleNum){
                chapters.setCharge(-1);
            }
            chapters.setIsPay(isPay);
            chapters.setContent(convertImageUrl(chapters.getContent()));
            chapters.setImageUrls(convertJsonImage(chapters.getJsonImages()));
            chapters.setJsonImages("");
        }
        Map map =new HashMap<>();
        map.put("list",pageList.getRecords());
        ReadingLog log =readingLogService.getReadingLog(localBooksVO);

        if(log==null){
            map.put("readingEpisode",0);
        }
        else{
            map.put("readingEpisode",log.getEpisode());
        }
        Integer maxEpisode = baseMapper.getMaxEpisode(localBooksVO.getBookId());
        map.put("maxEpisode",maxEpisode==null?0:maxEpisode);
        Integer chargeCount= baseMapper.getChargeCount(localBooksVO.getBookId());
        map.put("isAllFree",chargeCount>0?0:1);
        Integer perFee = booksSettingMapper.queryFee("perFee");
        map.put("perFee",perFee);
        return ResultUtil.result(SysConf.SUCCESS,map);
    }
    private int getReadAbleCount(String customerId, String source) {
        Integer vipLevel = customersMapper.queryVipLevel(customerId);
        Integer enableEpisode = booksUserCostMapper.queryEnableEpisode(source, vipLevel);
        return enableEpisode == null ? 0 : enableEpisode;
    }
    private List<String> convertJsonImage(String jsonImage){
        JSONArray jsonArray = JSONObject.parseArray(jsonImage);
        List<String> images = new ArrayList<>();
        for (int i = 0 ; i < jsonArray.size(); i++) {
            String s = jsonArray.getString(i);
            images.add(convertImageUrl(s));
            jsonArray.set(i, convertImageUrl(s));
        }
        return images;
    }
    private  String convertImageUrl(String url){
        if(StringUtils.isNotEmpty(url)){
            url = url.replaceAll("\\\\", "");
            //UAT
            url=imageDomain.concat(url);
        }
        return url;
    }
    @Override
    public IPage<TpBookChapters> getPageList(TpBookChaptersVO tpBookChaptersVO) {
        QueryWrapper<TpBookChapters> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("book_id","episode","title","json_images","charge","created_at","updated_at");
        queryWrapper.eq("status",1);
        if(StringUtils.isNotEmpty(tpBookChaptersVO.getBookId())){
            queryWrapper.eq("book_id",tpBookChaptersVO.getBookId());
        }

        Page<TpBookChapters> page = new Page<>();
        page.setCurrent(tpBookChaptersVO.getCurrentPage());
        page.setSize(tpBookChaptersVO.getPageSize());
        queryWrapper.orderByAsc("book_id","episode");
        IPage<TpBookChapters> pageList = page(page, queryWrapper);
        List<TpBookChapters> list = pageList.getRecords();
        for(TpBookChapters chapters:list){
            chapters.setImageUrls(convertJsonImage(chapters.getJsonImages()));
            chapters.setImageCount(chapters.getImageUrls().size());
        }
        return pageList;
    }
}

