package com.moxi.mogublog.xo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.Girl;
import com.moxi.mogublog.commons.entity.GirlPicture;
import com.moxi.mogublog.commons.feign.PictureFeignClient;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.mapper.GirlPictureMapper;
import com.moxi.mogublog.xo.service.GirlPictureService;
import com.moxi.mogublog.xo.service.GirlService;
import com.moxi.mogublog.xo.utils.WebUtil;
import com.moxi.mogublog.xo.vo.GirlPictureVO;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 妹子图片表 服务实现类
 *
 * @author 陌溪
 * @since 2021-12-29
 */
@Service
public class GirlPictureServiceImpl extends SuperServiceImpl<GirlPictureMapper, GirlPicture> implements GirlPictureService {

    @Autowired
    private WebUtil webUtil;

    @Resource
    private PictureFeignClient pictureFeignClient;

    @Autowired
    private GirlPictureService girlPictureService;

    @Autowired
    private GirlService girlService;

    @Override
    public IPage<GirlPicture> getPageList(GirlPictureVO girlPictureVO) {
        QueryWrapper<GirlPicture> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SQLConf.GIRL_UID, girlPictureVO.getGirlUid());
        queryWrapper.orderByDesc(SQLConf.UPDATE_TIME);

        Page<GirlPicture> page = new Page<>();
        page.setCurrent(girlPictureVO.getCurrentPage());
        page.setSize(girlPictureVO.getPageSize());
        IPage<GirlPicture> pageList = girlPictureService.page(page, queryWrapper);
        List<GirlPicture> pictureList = pageList.getRecords();

        //获取图集封面消息。
        QueryWrapper<Girl> girlQueryWrapper = new QueryWrapper<>();
        girlQueryWrapper.eq(SQLConf.UID, girlPictureVO.getGirlUid());
        Girl girl =  girlService.getOne(girlQueryWrapper);
        if (StringUtils.isNotEmpty(girl.getVideoUid())){
            GirlPicture video = new GirlPicture();
            video.setFileUid(girl.getVideoUid());
            video.setUpdateTime(girl.getUpdateTime());
            if (pictureList.size() > 0){
                pictureList.add(0,video);
            }else {
                pictureList = new ArrayList<>();
                pictureList.add(video);
            }
        }

        final StringBuffer fileUids = new StringBuffer();
        pictureList.forEach(item -> {
            if (StringUtils.isNotEmpty(item.getFileUid())) {
                fileUids.append(item.getFileUid() + SysConf.FILE_SEGMENTATION);
            }
        });

        String pictureResult = null;
        Map<String, String> pictureMap = new HashMap<>();

        if (fileUids != null) {
            pictureResult = this.pictureFeignClient.getPicture(fileUids.toString(), SysConf.FILE_SEGMENTATION);
        }
        List<Map<String, Object>> picList = webUtil.getPictureMap(pictureResult);

        picList.forEach(item -> {
            pictureMap.put(item.get(SysConf.UID).toString(), item.get(SysConf.URL).toString());
        });

        for (GirlPicture item : pictureList) {
            if (StringUtils.isNotEmpty(item.getFileUid())) {
                item.setPictureUrl(pictureMap.get(item.getFileUid()));
            }
        }
        pageList.setRecords(pictureList);
        return pageList;
    }

    @Override
    public IPage<GirlPicture> editGirlPicList(GirlPictureVO girlPictureVO) {
        QueryWrapper<GirlPicture> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SQLConf.GIRL_UID, girlPictureVO.getGirlUid());
        queryWrapper.orderByDesc(SQLConf.UPDATE_TIME);

        Page<GirlPicture> page = new Page<>();
        page.setCurrent(girlPictureVO.getCurrentPage());
        page.setSize(girlPictureVO.getPageSize());
        IPage<GirlPicture> pageList = girlPictureService.page(page, queryWrapper);
        List<GirlPicture> pictureList = pageList.getRecords();

        final StringBuffer fileUids = new StringBuffer();
        pictureList.forEach(item -> {
            if (StringUtils.isNotEmpty(item.getFileUid())) {
                fileUids.append(item.getFileUid() + SysConf.FILE_SEGMENTATION);
            }
        });

        String pictureResult = null;
        Map<String, String> pictureMap = new HashMap<>();

        if (fileUids != null) {
            pictureResult = this.pictureFeignClient.getPicture(fileUids.toString(), SysConf.FILE_SEGMENTATION);
        }
        List<Map<String, Object>> picList = webUtil.getPictureMap(pictureResult);

        picList.forEach(item -> {
            pictureMap.put(item.get(SysConf.UID).toString(), item.get(SysConf.URL).toString());
        });

        for (GirlPicture item : pictureList) {
            if (StringUtils.isNotEmpty(item.getFileUid())) {
                item.setPictureUrl(pictureMap.get(item.getFileUid()));
            }
        }
        pageList.setRecords(pictureList);
        return pageList;
    }
}
