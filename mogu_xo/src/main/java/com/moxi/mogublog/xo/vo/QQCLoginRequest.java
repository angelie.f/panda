package com.moxi.mogublog.xo.vo;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class QQCLoginRequest {

    private String app;

    private String deviceUuid;

    private String deviceId;

    private String deviceIp;

    private String deviceOs;


    private String userHeadimg;

    private String userId;

    private String userNickname;

    private String userPhone;


    private String timestamp;


    private String sign;

    private String screen;

    private String ipAddress;

    private String domain;

    private String terminal;

    private String userToken;


    private Integer isVip;

    private String vipLevel;

    private String vipType;


    private String platform;


    private String userInfo;

    private String gameId;
    private Integer source;
    private Integer isCash;

//    private Integer isH5;


}
