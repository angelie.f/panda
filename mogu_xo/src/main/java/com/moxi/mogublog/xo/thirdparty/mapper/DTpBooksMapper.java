package com.moxi.mogublog.xo.thirdparty.mapper;


import com.moxi.mogublog.commons.entity.TpBooks;
import com.moxi.mougblog.base.mapper.SuperMapper;


public interface DTpBooksMapper extends SuperMapper<TpBooks> {
}
