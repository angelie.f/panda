package com.moxi.mogublog.xo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.GirlProject;
import com.moxi.mogublog.commons.entity.GirlUserCost;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.mapper.GirlUserCostMapper;
import com.moxi.mogublog.xo.service.GirlProjectService;
import com.moxi.mogublog.xo.service.GirlUserCostService;
import com.moxi.mogublog.xo.vo.GirlUserCostVO;
import com.moxi.mougblog.base.enums.EStatus;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 妹子项目表 服务实现类
 *
 * @author Vergift
 * @date 2021-11-29
 */
@Service
public class GirlUserCostServiceImpl extends SuperServiceImpl<GirlUserCostMapper, GirlUserCost> implements GirlUserCostService {

    @Autowired
    private GirlUserCostService girlUserCostService;

    @Autowired
    private GirlProjectService girlProjectService;

    @Override
    public IPage<GirlUserCost> getPageList(GirlUserCostVO girlUserCostVO) {
        QueryWrapper<GirlUserCost> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SQLConf.PROJECT_UID, girlUserCostVO.getProjectUid());

        Page<GirlUserCost> page = new Page<>();
        page.setCurrent(girlUserCostVO.getCurrentPage());
        page.setSize(girlUserCostVO.getPageSize());
        queryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
        if(StringUtils.isNotEmpty(girlUserCostVO.getOrderByAscColumn())) {
            // 将驼峰转换成下划线
            String column = StringUtils.underLine(new StringBuffer(girlUserCostVO.getOrderByAscColumn())).toString();
            queryWrapper.orderByAsc(column);
        } else if(StringUtils.isNotEmpty(girlUserCostVO.getOrderByDescColumn())) {
            // 将驼峰转换成下划线
            String column = StringUtils.underLine(new StringBuffer(girlUserCostVO.getOrderByDescColumn())).toString();
            queryWrapper.orderByDesc(column);
        }

        return girlUserCostService.page(page, queryWrapper);
    }

    @Override
    public String addUserCost(GirlUserCostVO girlUserCostVO) {
        QueryWrapper<GirlUserCost> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SQLConf.LEVEL_NAME, girlUserCostVO.getLevelName());
        queryWrapper.eq(SQLConf.PROJECT_UID, girlUserCostVO.getProjectUid());
        if (girlUserCostService.getOne(queryWrapper) != null) {
            return ResultUtil.errorWithMessage(MessageConf.ENTITY_EXIST);
        }
        GirlUserCost project = new GirlUserCost();
        project.setLevelName(girlUserCostVO.getLevelName());
        project.setLevelId(girlUserCostVO.getLevelId());
        project.setProjectUid(girlUserCostVO.getProjectUid());
        project.setCost(girlUserCostVO.getCost());
        project.setStatus(EStatus.ENABLE);
        project.insert();
        return ResultUtil.successWithMessage(MessageConf.INSERT_SUCCESS);
    }

    @Override
    public String editUserCost(GirlUserCostVO girlUserCostVO) {
        GirlUserCost project = girlUserCostService.getById(girlUserCostVO.getUid());

        if (project != null && !project.getLevelName().equals(girlUserCostVO.getLevelName())) {
            QueryWrapper<GirlUserCost> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq(SQLConf.LEVEL_NAME, girlUserCostVO.getLevelName());
            queryWrapper.eq(SQLConf.PROJECT_UID, girlUserCostVO.getProjectUid());
            GirlUserCost tempTag = girlUserCostService.getOne(queryWrapper);
            if (tempTag != null) {
                return ResultUtil.errorWithMessage(MessageConf.ENTITY_EXIST);
            }
        }

        project.setLevelName(girlUserCostVO.getLevelName());
        project.setLevelId(girlUserCostVO.getLevelId());
        project.setCost(girlUserCostVO.getCost());
        project.setUpdateTime(new Date());
        project.updateById();
        return ResultUtil.successWithMessage(MessageConf.UPDATE_SUCCESS);
    }
    @Override
    public String deleteUserCost(GirlUserCostVO girlUserCostVO) {
        GirlUserCost project = new GirlUserCost();
        project.setUid(girlUserCostVO.getUid());
        project.deleteById();
        return ResultUtil.successWithMessage(MessageConf.DELETE_SUCCESS);
    }

    @Override
    public List<GirlUserCost> getListByH5(String source) {
        QueryWrapper<GirlProject> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SQLConf.SOURCE, source);
        GirlProject girlProject = girlProjectService.getOne(queryWrapper);

        QueryWrapper<GirlUserCost> costQueryWrapper = new QueryWrapper<>();
        costQueryWrapper.eq(SQLConf.PROJECT_UID, girlProject.getUid());
        return girlUserCostService.list(costQueryWrapper);
    }
}
