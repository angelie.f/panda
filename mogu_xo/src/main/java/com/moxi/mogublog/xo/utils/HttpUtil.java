package com.moxi.mogublog.xo.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Alexander.C
 * @date 2019/5/8 16:21
 **/
@Slf4j
public class HttpUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(HttpUtil.class);

    public static String get(String url, String app, String platform, String token) throws Exception {
        // 创建Httpclient对象
        CloseableHttpClient httpclient = HttpClients.createDefault();
        // 创建http GET请求
        HttpGet get = new HttpGet(url);
        if (app != null && !"".equals(app)) {
            get.setHeader("app", app);
        }
        if (platform != null && !"".equals(platform)) {
            get.setHeader("platform", platform);
        }
        if (token != null && !"".equals(token)) {
            get.setHeader("token", token);
        }
        String content = null;
        CloseableHttpResponse response = null;
        try {
            // 执行请求
            response = httpclient.execute(get);
            // 判断返回状态是否为200
            if (response.getStatusLine().getStatusCode() == 200) {
                //请求体内容
                content = EntityUtils.toString(response.getEntity(), "UTF-8");
                //内容写入文件
            }
        } finally {
            if (response != null) {
                response.close();
            }
            //相当于关闭浏览器
            httpclient.close();
        }
        return content;
    }

}
