package com.moxi.mogublog.xo.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.BooksSetting;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.mapper.BooksSettingMapper;
import com.moxi.mogublog.xo.service.BooksSettingService;
import com.moxi.mogublog.xo.vo.BooksSettingVO;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class BooksSettingServiceImpl extends SuperServiceImpl<BooksSettingMapper, BooksSetting> implements BooksSettingService {

    @Override
    public IPage<BooksSetting> getPageList(BooksSettingVO booksSettingVO) {
        QueryWrapper<BooksSetting> queryWrapper = new QueryWrapper<>();
        Page<BooksSetting> page = new Page<>();
        page.setCurrent(booksSettingVO.getCurrentPage());
        page.setSize(booksSettingVO.getPageSize());
        queryWrapper.orderByDesc(SQLConf.UPDATE_TIME);
        IPage<BooksSetting> pageList = page(page, queryWrapper);
        return pageList;
    }
    @Override
    public String editSetting(BooksSettingVO booksSettingVO) {
        BooksSetting setting =getById(booksSettingVO.getUid());
        setting.setTitle(booksSettingVO.getTitle());
        setting.setValue(booksSettingVO.getValue());
        setting.setContent(booksSettingVO.getContent());
        setting.setUpdateTime(new Date());
        setting.updateById();
        return ResultUtil.successWithMessage(MessageConf.UPDATE_SUCCESS);
    }
}
