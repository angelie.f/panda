package com.moxi.mogublog.xo.service.impl;

import cn.hutool.core.lang.Assert;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.GameActConfig;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.mapper.GameActConfigMapper;
import com.moxi.mogublog.xo.service.GameActConfigService;
import com.moxi.mogublog.xo.vo.ActConfigVo;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

@Service
@Slf4j
public class GameActConfigServiceImpl extends SuperServiceImpl<GameActConfigMapper, GameActConfig> implements GameActConfigService {
    @Autowired
    private GameActConfigMapper gameActConfigMapper;
    @Override
    public IPage<ActConfigVo> getPageList(ActConfigVo actConfigVo) {
        IPage<ActConfigVo> actConfigVoIPage = new Page<>();
        QueryWrapper<GameActConfig> queryWrapper = new QueryWrapper<>();
        Page<GameActConfig> page = new Page<>();
        page.setCurrent(actConfigVo.getCurrentPage());
        page.setSize(actConfigVo.getPageSize());
        queryWrapper.orderByDesc(SQLConf.CREATE_TIME);
        IPage<GameActConfig> pageList = gameActConfigMapper.selectPage(page, queryWrapper);
        List<ActConfigVo> actConfigVos = new ArrayList<>();
        if(!ObjectUtils.isEmpty(pageList) && CollectionUtils.isNotEmpty(pageList.getRecords())){
            List<GameActConfig> gameActConfigs = pageList.getRecords();
            actConfigVos = gameActConfigs.stream().map(item->{
                ActConfigVo actConfigVo1 = JSONObject.parseObject(item.getActInfo(),ActConfigVo.class);
                actConfigVo1.setUid(item.getUid());
                actConfigVo1.setActPreAmount(Arrays.asList(actConfigVo1.toString().split(",")).stream().filter(mp-> StringUtils.isNotEmpty(mp) && !mp.equals("0")).collect(Collectors.joining(",")));
                return actConfigVo1;
            }).collect(Collectors.toList());
        }
        actConfigVoIPage.setRecords(actConfigVos);
        actConfigVoIPage.setCurrent(pageList.getCurrent());
        actConfigVoIPage.setTotal(pageList.getTotal());
        actConfigVoIPage.setPages(pageList.getPages());
        actConfigVoIPage.setSize(pageList.getSize());
        return actConfigVoIPage;
    }

    @Override
    public String addBanner(ActConfigVo actConfigVo) {
        QueryWrapper<GameActConfig> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("act_type",actConfigVo.getActType());
        queryWrapper.eq("status",1);
        List<GameActConfig> gameActConfigs = gameActConfigMapper.selectList(queryWrapper);
        if(CollectionUtils.isNotEmpty(gameActConfigs)){
            return ResultUtil.result(SysConf.ERROR, "存在同类型的活动配置!");
        }
        GameActConfig gameActConfig = new GameActConfig();
        BeanUtils.copyProperties(actConfigVo,gameActConfig);
        gameActConfig.setActInfo(JSON.toJSONString(actConfigVo));
        gameActConfigMapper.insert(gameActConfig);
        return ResultUtil.result(SysConf.SUCCESS, MessageConf.OPERATION_SUCCESS);
    }

    @Override
    public String editBanner(ActConfigVo actConfigVo) {
        GameActConfig gameActConfig = gameActConfigMapper.selectById(actConfigVo.getUid());
        Assert.notNull(gameActConfig, MessageConf.PARAM_INCORRECT);
        QueryWrapper<GameActConfig> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("act_type",actConfigVo.getActType());
        queryWrapper.eq("status",1);
        queryWrapper.ne("uid",actConfigVo.getUid());
        List<GameActConfig> gameActConfigs = gameActConfigMapper.selectList(queryWrapper);
        if(CollectionUtils.isNotEmpty(gameActConfigs)){
            return ResultUtil.result(SysConf.ERROR, "存在同类型的活动配置!");
        }
        gameActConfig.setActType(actConfigVo.getActType());
        gameActConfig.setActTitle(actConfigVo.getActTitle());
        gameActConfig.setActDesc(actConfigVo.getActDesc());
        gameActConfig.setActChannel(actConfigVo.getActChannel());
        gameActConfig.setActPreAmount(actConfigVo.getActPreAmount());
        gameActConfig.setActInfo(JSON.toJSONString(actConfigVo));
        gameActConfig.setActBeginTime(actConfigVo.getActBeginTime());
        gameActConfig.setActEndTime(actConfigVo.getActEndTime());
        gameActConfig.setStatus(actConfigVo.getStatus());
        gameActConfig.setActInfo(JSON.toJSONString(actConfigVo));
        gameActConfigMapper.updateById(gameActConfig);
        return ResultUtil.result(SysConf.SUCCESS, MessageConf.OPERATION_SUCCESS);
    }

    private Map beanToMap(ActConfigVo actConfigVo){
        return JSONObject.parseObject(JSON.toJSONString(actConfigVo),Map.class);
    }

}
