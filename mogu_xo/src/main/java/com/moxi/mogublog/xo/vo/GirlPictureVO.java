package com.moxi.mogublog.xo.vo;

import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;
import lombok.ToString;

/**
 * 妹子图片实体类
 *
 * @author vergift
 * @date 2022-01-13
 */
@ToString
@Data
public class GirlPictureVO extends BaseVO<GirlPictureVO> {

    /**
     * 妹子UID
     */
    private String girlUid;
}
