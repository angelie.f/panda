package com.moxi.mogublog.xo.vo;

import com.moxi.mougblog.base.validator.annotion.IntegerNotNull;
import com.moxi.mougblog.base.validator.annotion.NotBlank;
import com.moxi.mougblog.base.validator.group.Insert;
import com.moxi.mougblog.base.validator.group.Update;
import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;
import lombok.ToString;

/**
 * 图集实体查询参数类
 *
 * @author 陌溪
 * @date 2018年9月17日16:10:38
 */
@ToString
@Data
public class PictureAtlasVO extends BaseVO<PictureAtlasVO> {

    /**
     * 父UID
     */
    private String parentUid;

    /**
     * 图集名
     */
    private String name;

    /**
     * 图集封面图片Uid
     */
    private String fileUid;

    /**
     * 排序字段，数值越大，越靠前
     */
    private int sort;

    /**
     * 是否显示  1: 是  0: 否
     */
    @IntegerNotNull(groups = {Insert.class, Update.class})
    private Integer isShow;

    /**
     * 图集标签uid
     */
    private String tagUid;

    /**
     * m模特
     */
    private String model;

    /**
     * 出品商
     */
    private String producer;

    /**
     * 图集分类uid
     */
    private String sortUid;

    /**
     * 资源图集id
     */
    private String resourceId;

    /**
     * 图集创建时间
     */
    private String createTime;

    /**
     * 图片最新入库时间时间
     */
    private String storageTime;

    /**
     * 图片来源
     */
    private String resource;
}
