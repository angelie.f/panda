package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.TpTaggables;
import com.moxi.mogublog.commons.entity.TpTags;
import com.moxi.mougblog.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;


public interface TpTagsMapper extends SuperMapper<TpTags> {

    @Select("select name from tp_tags where id in " +
            " (select tag_id from tp_taggables where taggable_type ='Book' and taggable_id =#{bookId}) ")
    List<String>getTags(@Param("bookId") Integer bookId);

    @Insert("<script>" +
            " INSERT INTO tp_tags" +
            "  (id, name, type, suggest, queries, " +
            "   order_column, created_at, updated_at" +
            "  ) " +
            " VALUES " +
            " <foreach collection='list' item='tags' separator=','>" +
            " (" +
            "  #{tags.id},#{tags.name},#{tags.type},#{tags.suggest},#{tags.queries}," +
            "  #{tags.orderColumn},#{tags.createdAt},#{tags.updatedAt}" +
            " )" +
            "</foreach> " +
            " ON DUPLICATE KEY UPDATE " +
            " name=VALUES(name),type=VALUES(type),suggest=VALUES(suggest),queries=VALUES(queries)," +
            " order_column=VALUES(order_column),created_at=VALUES(created_at),updated_at=VALUES(updated_at) " +
            "</script>"
    )
    int insertOrUpdateBatch(@Param("list") List<TpTags> list);
}
