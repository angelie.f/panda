package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.PictureAtlas;
import com.moxi.mogublog.commons.entity.PictureSortItem;
import com.moxi.mogublog.commons.entity.UsePower;
import com.moxi.mogublog.xo.vo.CustomersVO;
import com.moxi.mogublog.xo.vo.PictureAtlasBySortVO;
import com.moxi.mogublog.xo.vo.PictureAtlasVO;
import com.moxi.mougblog.base.service.SuperService;

import java.util.List;

/**
 * 图片分类表 服务类
 *
 * @author 陌溪
 * @date 2018-09-04
 */
public interface PictureAtlasService extends SuperService<PictureAtlas> {

    /**
     * 获取图片分类列表
     *
     * @param pictureAtlasVO
     * @return
     */
    public IPage<PictureAtlas> getPageList(PictureAtlasVO pictureAtlasVO);

    /**
     * 返回前端分类列表
     *
     * @param customersVO
     * @return
     */
    public IPage<PictureAtlas> getCustomersAtlasList(CustomersVO customersVO, List<Integer> atlasUids, UsePower usePower);

    /**
     * 获取图片分类列表
     *
     * @param pictureAtlasBySortVO
     * @return
     */
    IPage<PictureAtlas> getAtlasListBySort(PictureAtlasBySortVO pictureAtlasBySortVO);

    /**
     * 获取图片来源列表
     *
     * @return
     */
    public List<PictureAtlas> getResourceList();

    /**
     * 新增图片分类
     *
     * @param pictureAtlasVO
     */
    public String addPictureAtlas(PictureAtlasVO pictureAtlasVO);

    /**
     * 编辑图片分类
     *
     * @param pictureAtlasVO
     */
    public String editPictureAtlas(PictureAtlasVO pictureAtlasVO);

    /**
     * 删除图片分类
     *
     * @param pictureAtlasVO
     */
    public String deletePictureAtlas(PictureAtlasVO pictureAtlasVO);

    /**
     * 置顶图片分类
     *
     * @param pictureAtlasVO
     */
    public String stickPictureAtlas(PictureAtlasVO pictureAtlasVO);

    /**
     * 删除和博客分类有关的Redis缓存
     */
    public void deleteRedisByPictureTag();

    /**
     * 删除和博客有关的Redis缓存
     */
    public void deleteRedisByPictureAtlas();

    /**
     * 修改标签
     *
     * @param list
     */
    public String updateTag(List<PictureAtlasVO> list);

    /**
     * 图集上下架
     *
     * @param list
     */
    public String upOrOut(List<PictureSortItem> list);

}
