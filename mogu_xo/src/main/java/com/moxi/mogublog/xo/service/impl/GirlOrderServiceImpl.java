package com.moxi.mogublog.xo.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.*;
import com.moxi.mogublog.commons.feign.PictureFeignClient;
import com.moxi.mogublog.utils.RedisUtil;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.mapper.CustomersMapper;
import com.moxi.mogublog.xo.mapper.GirlOrderMapper;
import com.moxi.mogublog.xo.mapper.GirlProjectMapper;
import com.moxi.mogublog.xo.mapper.GirlUserCostMapper;
import com.moxi.mogublog.xo.service.*;
import com.moxi.mogublog.xo.utils.WebUtil;
import com.moxi.mogublog.xo.vo.CallbackOrderVO;
import com.moxi.mogublog.xo.vo.GirlOrderVO;
import com.moxi.mogublog.xo.vo.LocalGirlVO;
import com.moxi.mougblog.base.enums.EStatus;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 妹子订单表 服务实现类
 *
 * @author 陌溪
 * @since 2021-12-29
 */
@Service
@Slf4j
public class GirlOrderServiceImpl extends SuperServiceImpl<GirlOrderMapper, GirlOrder> implements GirlOrderService {

    @Autowired
    private GirlOrderService girlOrderService;

    @Autowired
    private GirlPictureService girlPictureService;

    @Autowired
    private GirlService girlService;

    @Autowired
    private GirlOrderCommentService girlOrderCommentService;

    @Autowired
    private CustomersMapper customersMapper;

    @Autowired
    private GirlUserCostMapper girlUserCostMapper;

    @Autowired
    private GirlProjectMapper girlProjectMapper;

    @Autowired
    private CustomersService customersService;
    @Autowired
    private WebUtil webUtil;

    @Resource
    private PictureFeignClient pictureFeignClient;

    @Autowired
    private GirlCityService girCityService;

    @Autowired
    private GirlProjectService girlProjectService;

    @Autowired
    private RedisUtil redisUtil;

    @Override
    public IPage<GirlOrder> getGirlOrderList(LocalGirlVO localGirlVO) {

        QueryWrapper<GirlOrder> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(localGirlVO.getCustomerId()) && !StringUtils.isEmpty(localGirlVO.getCustomerId())) {
            queryWrapper.eq(SQLConf.CUSTOMER_ID, localGirlVO.getCustomerId());
        }
        queryWrapper.orderByDesc(SQLConf.UPDATE_TIME);

        Page<GirlOrder> page = new Page<>();
        page.setCurrent(localGirlVO.getCurrentPage());
        page.setSize(localGirlVO.getPageSize());
        IPage<GirlOrder> iPage = girlOrderService.page(page, queryWrapper);
        List<GirlOrder> list = iPage.getRecords();
        if (list.size() > 0) {
            for (GirlOrder girlOrder : list) {
                QueryWrapper<Girl> wrapper = new QueryWrapper<>();
                wrapper.eq(SQLConf.UID, girlOrder.getGirlUid());
                wrapper.orderByDesc(SQLConf.UPDATE_TIME);
                List<Girl> girlList = girlService.list(wrapper);
                if (girlList.size() > 0) {
                    String pictureResult = null;
                    Map<String, String> pictureMap = new HashMap<>();
                    final StringBuffer fileUids = new StringBuffer();
                    List<String> pictureListTemp = new ArrayList<>();
                    QueryWrapper<GirlPicture> picQueryWrapper = new QueryWrapper<>();
                    picQueryWrapper.eq(SQLConf.GIRL_UID, girlList.get(0).getUid());
                    List<GirlPicture> pics = girlPictureService.list(picQueryWrapper);
                    List<String> picUids = new ArrayList<>();
                    for (GirlPicture pic : pics) {
                        fileUids.append(pic.getFileUid() + SysConf.FILE_SEGMENTATION);
                        picUids.add(pic.getFileUid());
                    }
                    girlList.get(0).setPicUids(picUids);
                    if (fileUids != null) {
                        pictureResult = this.pictureFeignClient.getPicture(fileUids.toString(), SysConf.FILE_SEGMENTATION);
                    }
                    List<Map<String, Object>> picList = webUtil.getPictureMap(pictureResult);

                    picList.forEach(item -> {
                        pictureMap.put(item.get(SysConf.UID).toString(), item.get(SysConf.URL).toString());
                    });
                    //获取图片
                    if (girlList.get(0).getPicUids().size() > 0) {
                        for (String picUid : girlList.get(0).getPicUids()) {
                            List<String> pictureUidsTemp = StringUtils.changeStringToString(picUid, SysConf.FILE_SEGMENTATION);
                            pictureUidsTemp.forEach(picture -> {
                                pictureListTemp.add(pictureMap.get(picture));
                            });
                        }
                    }
                    girlOrder.setPhotoList(pictureListTemp);
                    girlOrder.setTotalAmount(girlList.get(0).getPriceOne());
                    girlOrder.setExtraAmount(girlList.get(0).getPriceOne().subtract(girlOrder.getAmount()));
                    QueryWrapper<GirlProject> girlProjectQueryWrapper = new QueryWrapper<>();
                    girlProjectQueryWrapper.eq(SQLConf.SOURCE, localGirlVO.getSource());
                    List<GirlProject> girlProjectList = girlProjectService.list(girlProjectQueryWrapper);
                    girlOrder.setAuthentication(girlProjectList.get(0).getAuthentication());
                    GirlCity girlCity = girCityService.getById(girlList.get(0).getCityUid());
                    if (girlCity != null) {
                        girlOrder.setCityName(girlCity.getName());
                    }
                }
                QueryWrapper<GirlOrderComment> commentWrapper = new QueryWrapper<>();
                commentWrapper.eq(SQLConf.ORDER_ID, girlOrder.getOrderId());
                int count = girlOrderCommentService.count(commentWrapper);
                if (count > 0) {
                    girlOrder.setCommentStatus("1");
                } else {
                    girlOrder.setCommentStatus("0");
                }
            }
        }
        iPage.setRecords(list);
        return iPage;
    }

    @Override
    public IPage<GirlOrder> amounts(LocalGirlVO localGirlVO) {
        Page<GirlOrder> page = new Page<>();
        page.setCurrent(localGirlVO.getCurrentPage());
        page.setSize(localGirlVO.getPageSize());
        QueryWrapper<GirlOrder> queryWrapper = new QueryWrapper<>();
        queryWrapper.select(SQLConf.AMOUNT.concat(" as uid,").concat(SQLConf.AMOUNT));
        queryWrapper.groupBy(SQLConf.AMOUNT);
        queryWrapper.orderByAsc(SQLConf.AMOUNT);
        IPage<GirlOrder> iPage = girlOrderService.page(page, queryWrapper);
        return iPage;
    }

    @Override
    public IPage<Customers> vipLevels(LocalGirlVO localGirlVO) {
        Page<Customers> page = new Page<>();
        page.setCurrent(localGirlVO.getCurrentPage());
        page.setSize(localGirlVO.getPageSize());
        QueryWrapper<Customers> queryWrapper = new QueryWrapper<>();
        queryWrapper.select(SQLConf.VIP_LEVEL.concat(" as uid,").concat(SQLConf.VIP_LEVEL));
        queryWrapper.isNotNull(SQLConf.VIP_LEVEL);
        queryWrapper.groupBy(SQLConf.VIP_LEVEL);
        queryWrapper.orderByAsc(SQLConf.VIP_LEVEL);
        IPage<Customers> iPage = customersMapper.selectPage(page, queryWrapper);
        return iPage;
    }

    @Override
    public IPage<GirlProject> sources(LocalGirlVO localGirlVO) {
        Page<GirlProject> page = new Page<>();
        page.setCurrent(localGirlVO.getCurrentPage());
        page.setSize(localGirlVO.getPageSize());
        QueryWrapper<GirlProject> queryWrapper = new QueryWrapper<>();
        queryWrapper.select(SQLConf.SOURCE,SQLConf.NAME);
        queryWrapper.orderByAsc(SQLConf.SOURCE);
        IPage<GirlProject> iPage = girlProjectMapper.selectPage(page, queryWrapper);
        return iPage;
    }

    @Override
    public IPage<GirlOrder> getPageList(GirlOrderVO girlOrderVO) {
        QueryWrapper<GirlOrder> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(girlOrderVO.getOrderId()) && !StringUtils.isEmpty(girlOrderVO.getOrderId())) {
            queryWrapper.like("g."+SQLConf.ORDER_ID, girlOrderVO.getOrderId().trim());
        }
        if (StringUtils.isNotEmpty(girlOrderVO.getUserId()) && !StringUtils.isEmpty(girlOrderVO.getUserId())) {
            queryWrapper.like("g."+SQLConf.USER_ID, girlOrderVO.getUserId().trim());
        }
        if (StringUtils.isNotEmpty(girlOrderVO.getGirlName()) && !StringUtils.isEmpty(girlOrderVO.getGirlName())) {
            queryWrapper.like("g."+SQLConf.GIRL_NAME, girlOrderVO.getGirlName().trim());
        }
        if (StringUtils.isNotEmpty(girlOrderVO.getGirlUid()) && !StringUtils.isEmpty(girlOrderVO.getGirlUid())) {
            queryWrapper.like("g."+SQLConf.GIRL_UID, girlOrderVO.getGirlUid().trim());
        }
        if (StringUtils.isNotEmpty(girlOrderVO.getPayCount1())) {
            if ("lt".equals(girlOrderVO.getPayCount1())) {
                queryWrapper.lt("g."+SQLConf.PAY_COUNT, girlOrderVO.getPayCount2());
            }
            if ("le".equals(girlOrderVO.getPayCount1())) {
                queryWrapper.le("g."+SQLConf.PAY_COUNT, girlOrderVO.getPayCount2());
            }
            if ("eq".equals(girlOrderVO.getPayCount1())) {
                queryWrapper.eq("g."+SQLConf.PAY_COUNT, girlOrderVO.getPayCount2());
            }
            if ("gt".equals(girlOrderVO.getPayCount1())) {
                queryWrapper.gt("g."+SQLConf.PAY_COUNT, girlOrderVO.getPayCount2());
            }
            if ("ge".equals(girlOrderVO.getPayCount1())) {
                queryWrapper.ge("g."+SQLConf.PAY_COUNT, girlOrderVO.getPayCount2());
            }
        }
        if (girlOrderVO.getPayStatus() != null) {
            queryWrapper.eq("g."+SQLConf.PAY_STATUS, girlOrderVO.getPayStatus());
        }
        if (girlOrderVO.getStatus() != null) {
            queryWrapper.eq("g."+SQLConf.STATUS, girlOrderVO.getStatus());
        }
        if (girlOrderVO.getAmount().compareTo(BigDecimal.ZERO) > 0) {
            queryWrapper.eq("g."+SQLConf.AMOUNT, girlOrderVO.getAmount());
        }
        if (StringUtils.isNotEmpty(girlOrderVO.getGirlUid()) && !StringUtils.isEmpty(girlOrderVO.getGirlUid())) {
            queryWrapper.like("g."+SQLConf.GIRL_UID, girlOrderVO.getGirlUid().trim());
        }

        if (StringUtils.isNotEmpty(girlOrderVO.getBeginCreateTime()) && girlOrderVO.getBeginCreateTime().equals(girlOrderVO.getEndCreateTime())) {
            queryWrapper.like("g."+SQLConf.CREATE_TIME, girlOrderVO.getEndCreateTime().trim());
        } else {
            if (StringUtils.isNotEmpty(girlOrderVO.getBeginCreateTime())) {
                queryWrapper.ge("g."+SQLConf.CREATE_TIME, girlOrderVO.getBeginCreateTime().trim());
            }
            if (StringUtils.isNotEmpty(girlOrderVO.getEndCreateTime())) {
                queryWrapper.le("g."+SQLConf.CREATE_TIME, girlOrderVO.getEndCreateTime().trim());
            }
        }
        if (StringUtils.isNotEmpty(girlOrderVO.getBeginUpdateTime()) && girlOrderVO.getBeginUpdateTime().equals(girlOrderVO.getEndUpdateTime())) {
            queryWrapper.like("g."+SQLConf.UPDATE_TIME, girlOrderVO.getBeginUpdateTime().trim());
        } else {
            if (StringUtils.isNotEmpty(girlOrderVO.getBeginUpdateTime())) {
                queryWrapper.ge("g."+SQLConf.UPDATE_TIME, girlOrderVO.getBeginUpdateTime().trim());
            }
            if (StringUtils.isNotEmpty(girlOrderVO.getEndUpdateTime())) {
                queryWrapper.le("g."+SQLConf.UPDATE_TIME, girlOrderVO.getEndUpdateTime().trim());
            }
        }
        if (girlOrderVO.getVipLevel().intValue() > 0) {
            queryWrapper.eq("c.vip_level",girlOrderVO.getVipLevel());
        }

        if (StringUtils.isNotEmpty(girlOrderVO.getSource())) {
            queryWrapper.eq("c.source",girlOrderVO.getSource());
        }

        Page<GirlOrder> page = new Page<>();
        page.setCurrent(girlOrderVO.getCurrentPage());
        page.setSize(girlOrderVO.getPageSize());
        if (StringUtils.isNotEmpty(girlOrderVO.getOrderByAscColumn())) {
            // 将驼峰转换成下划线
            String column = StringUtils.underLine(new StringBuffer(girlOrderVO.getOrderByAscColumn())).toString();
            queryWrapper.orderByAsc(column);
        } else if (StringUtils.isNotEmpty(girlOrderVO.getOrderByDescColumn())) {
            // 将驼峰转换成下划线
            String column = StringUtils.underLine(new StringBuffer(girlOrderVO.getOrderByDescColumn())).toString();
            queryWrapper.orderByDesc(column);
        }
        else{
            queryWrapper.orderByDesc("g."+SQLConf.UPDATE_TIME);
        }
        IPage<GirlOrder> pageList = this.getBaseMapper().getPage(page, queryWrapper);
        List<GirlOrder> list = pageList.getRecords();
        Set<Integer> girlUidSet = new HashSet<>();
        Collection<Girl> girlList = new ArrayList<>();
        list.forEach(item -> {
            if (item.getGirlUid() != null) {
                girlUidSet.add(item.getGirlUid());
            }
        });
        if (girlUidSet.size() > 0) {
            QueryWrapper<Girl> girlQueryWrapper = new QueryWrapper<>();
            girlQueryWrapper.in(SQLConf.UID, girlUidSet);
            girlList = girlService.list(girlQueryWrapper);
        }
        Map<String, Girl> girlMap = new HashMap<>();
        girlList.forEach(item -> {
            girlMap.put(item.getUid(), item);
        });
        for (GirlOrder item : list) {
            if (item.getGirlUid() != null && girlMap.containsKey(item.getGirlUid().toString())) {
                Girl girl = girlMap.get(item.getGirlUid().toString());
                item.setCityName(getCityName(girl.getCityUid()));
            }
        }
        pageList.setRecords(list);
        return pageList;
    }
    private String getCityName(String cityUid) {
        String jsonList = redisUtil.get("city_list");
        List<GirlCity> list = JSONArray.parseArray(jsonList, GirlCity.class);
        for (GirlCity item : list) {
            if (item.getUid().equals(cityUid)) {
                return item.getName();
            }
        }
        return "";
    }

    @Override
    public String editOrder(GirlOrderVO girlOrderVO) {
        GirlOrder order = girlOrderService.getById(girlOrderVO.getUid());
        QueryWrapper<Girl> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SQLConf.UID, girlOrderVO.getGirlUid());

        Girl girl = girlService.getOne(queryWrapper);
        if (girl == null) {
            return ResultUtil.errorWithMessage(MessageConf.GIRL_NOT_EXIST);
        }

        order.setGirlUid(Integer.valueOf(girlOrderVO.getGirlUid()));
        if (girlOrderVO.getStatus() != null && girlOrderVO.getStatus() == EStatus.ENABLE) {
            order.setStatus(EStatus.ENABLE);
        } else {
            order.setStatus(EStatus.DISABLED);
        }
        order.setUpdateTime(new Date());
        order.setGirlName(girl.getName());
        order.updateById();

        return ResultUtil.successWithMessage(MessageConf.DELETE_SUCCESS);
    }

    @Override
    public String createOrder(LocalGirlVO localGirlVO) {
        BigDecimal amount = null;
        QueryWrapper<Customers> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("customer_id", localGirlVO.getCustomerId());
        Customers customers = customersMapper.selectOne(queryWrapper);


        QueryWrapper<GirlProject> projectQueryWrapper = new QueryWrapper<>();
        projectQueryWrapper.eq(SQLConf.SOURCE, customers.getSource());
        GirlProject project = girlProjectMapper.selectOne(projectQueryWrapper);

        QueryWrapper<GirlUserCost> userCostQueryWrapper = new QueryWrapper<>();
        userCostQueryWrapper.eq(SQLConf.PROJECT_UID, project.getUid());
        userCostQueryWrapper.eq(SQLConf.LEVEL_ID, customers.getVipLevel());
        GirlUserCost userCost = girlUserCostMapper.selectOne(userCostQueryWrapper);
        if (userCost != null) {
            amount = userCost.getCost();
        } else {
            QueryWrapper<GirlUserCost> ptUserCostQueryWrapper = new QueryWrapper<>();
            ptUserCostQueryWrapper.eq(SQLConf.PROJECT_UID, project.getUid());
            ptUserCostQueryWrapper.eq(SQLConf.LEVEL_NAME, MessageConf.ORDINARY_VIP);
            GirlUserCost ptuserCost = girlUserCostMapper.selectOne(ptUserCostQueryWrapper);
            amount = ptuserCost.getCost();
        }

        GirlOrder girlOrder = new GirlOrder();
        if (StringUtils.isNotEmpty(localGirlVO.getOrderId())) {
            QueryWrapper<GirlOrder> orderQueryWrapper = new QueryWrapper<>();
            orderQueryWrapper.eq(SQLConf.ORDER_ID, localGirlVO.getOrderId().trim());
            List<GirlOrder> orderList = girlOrderService.list(orderQueryWrapper);
            if (orderList.size() > 0) {
                girlOrder = orderList.get(0);
            }
        } else {
            girlOrder.setOrderId(String.valueOf(StringUtils.getSnowflakeId()));
            girlOrder.setAmount(amount);
            girlOrder.setUserId(customers.getOpenId());
            girlOrder.setGirlName(localGirlVO.getGirlName());
            girlOrder.setGirlUid(Integer.valueOf(localGirlVO.getGirlUid()));
            girlOrder.setUserNick(customers.getLoginName());
            girlOrder.setCustomerId(localGirlVO.getCustomerId());
            girlOrder.setPayStatus(EStatus.DISABLED);
            girlOrder.setStatus(EStatus.DISABLED);
            girlOrder.setThirdOrderId("0");
            girlOrder.setServiceTime(localGirlVO.getServiceTime());
            girlOrder.setServiceType(localGirlVO.getServiceType());
            girlOrder.setOffPrice(localGirlVO.getOffPrice());
            girlOrderService.save(girlOrder);
        }
        String url = "/newPay?name=" + customers.getLoginName() + "&userId=" + customers.getOpenId() + "&price=" + girlOrder.getAmount() + "&desc=官方担保包赔" + "&gameId=G2042&orderId=" + girlOrder.getOrderId() + "&userAgent=h5";
        return url;
    }

    @Override
    public Map<String, Object> callbackOrder(CallbackOrderVO callbackOrderVO) {
        QueryWrapper<GirlOrder> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SQLConf.ORDER_ID, callbackOrderVO.getReferenceId().trim());
        queryWrapper.eq(SQLConf.PAY_STATUS, EStatus.DISABLED);
        Page<GirlOrder> page = new Page<>();
        page.setCurrent(1);
        page.setSize(20);
        Map<String, Object> resutMap = new HashMap<>();
        Map<String, Object> resutData = new HashMap<>();
        resutMap.put("code", "200");
        List<GirlOrder> orderList = girlOrderService.list(queryWrapper);
        if (orderList.size() > 0) {
            GirlOrder girlOrder = orderList.get(0);
            girlOrder.setPayStatus(EStatus.ENABLE);
            girlOrder.setPayTime(new Date());

            QueryWrapper<GirlOrder> countQueryWrapper = new QueryWrapper<>();
            countQueryWrapper.eq(SQLConf.CUSTOMER_ID, orderList.get(0).getCustomerId().trim());
            countQueryWrapper.eq(SQLConf.PAY_STATUS, EStatus.ENABLE);
            List<GirlOrder> countList = girlOrderService.list(countQueryWrapper);

            girlOrder.setPayCount(countList.size() + 1);
            girlOrderService.saveOrUpdate(girlOrder);

            resutData.put("customerId", girlOrder.getUserId());
            resutData.put("balance", girlOrder.getAmount());

            resutMap.put("msg", "成功");
            resutMap.put("data", resutData);
        } else {
            resutMap.put("msg", "查无此单或已经上分成功");
        }
        return resutMap;
    }

    @Override
    public Map<String, String> checkOrderIfComment(LocalGirlVO localGirlVO) {
        Map<String, String> map = new HashMap<>();
        String result = "0";
        QueryWrapper<GirlOrder> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
        queryWrapper.eq(SQLConf.CUSTOMER_ID, localGirlVO.getCustomerId());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Calendar c = Calendar.getInstance();

        c.add(Calendar.DATE, -7);

        Date monday = c.getTime();

        String preMonday = sdf.format(monday);
        queryWrapper.ge(SQLConf.CREATE_TIME, preMonday);
        List<GirlOrder> orderList = girlOrderService.list(queryWrapper);
        if (orderList.size() > 0) {
            for (GirlOrder girlOrder : orderList) {
                QueryWrapper<GirlOrderComment> countWrapper = new QueryWrapper<>();
                countWrapper.eq(SQLConf.ORDER_ID, girlOrder.getOrderId());
                int count = girlOrderCommentService.count(countWrapper);
                if (count == 0) {
                    result = "1";
                    break;
                }
            }
        }
        map.put("orderCommentStatus", result);
        Calendar c1 = Calendar.getInstance();

        c1.add(Calendar.MINUTE, -1);

        Date minuteBefore = c1.getTime();
        QueryWrapper<GirlOrder> query = new QueryWrapper<>();
        query.eq(SQLConf.CUSTOMER_ID, localGirlVO.getCustomerId());
        query.eq(SQLConf.PAY_STATUS, EStatus.DISABLED);
        query.lt(SQLConf.CREATE_TIME, sdf.format(minuteBefore));
        List<GirlOrder> orders = girlOrderService.list(query);
        if (orders.size() > 0) {
            map.put("orderStatus", "1");
        } else {
            map.put("orderStatus", "0");
        }
        return map;
    }

    @Override
    public String getRecentServiceTime(String girlUid) {
        QueryWrapper<GirlOrder> queryWrapper = new QueryWrapper<>();
        queryWrapper.select(" IFNULL( max(service_time),'') as service_time");
        queryWrapper.eq(SQLConf.GIRL_UID, girlUid);
        queryWrapper.eq(SQLConf.PAY_STATUS,1);
        GirlOrder order = girlOrderService.getOne(queryWrapper);
        return order.getServiceTime();
    }

    @Override
    public Map<String, Object> getGirlOrderCountSum(GirlOrderVO girlOrderVO) {
        QueryWrapper<GirlOrder> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(girlOrderVO.getOrderId()) && !StringUtils.isEmpty(girlOrderVO.getOrderId())) {
            queryWrapper.like("g."+SQLConf.ORDER_ID, girlOrderVO.getOrderId().trim());
        }
        if (StringUtils.isNotEmpty(girlOrderVO.getUserId()) && !StringUtils.isEmpty(girlOrderVO.getUserId())) {
            queryWrapper.like("g."+SQLConf.USER_ID, girlOrderVO.getUserId().trim());
        }
        if (StringUtils.isNotEmpty(girlOrderVO.getGirlName()) && !StringUtils.isEmpty(girlOrderVO.getGirlName())) {
            queryWrapper.like("g."+SQLConf.GIRL_NAME, girlOrderVO.getGirlName().trim());
        }
        if (StringUtils.isNotEmpty(girlOrderVO.getGirlUid()) && !StringUtils.isEmpty(girlOrderVO.getGirlUid())) {
            queryWrapper.like("g."+SQLConf.GIRL_UID, girlOrderVO.getGirlUid().trim());
        }
        if (StringUtils.isNotEmpty(girlOrderVO.getPayCount1())) {
            if ("lt".equals(girlOrderVO.getPayCount1())) {
                queryWrapper.lt("g."+SQLConf.PAY_COUNT, girlOrderVO.getPayCount2());
            }
            if ("le".equals(girlOrderVO.getPayCount1())) {
                queryWrapper.le("g."+SQLConf.PAY_COUNT, girlOrderVO.getPayCount2());
            }
            if ("eq".equals(girlOrderVO.getPayCount1())) {
                queryWrapper.eq("g."+SQLConf.PAY_COUNT, girlOrderVO.getPayCount2());
            }
            if ("gt".equals(girlOrderVO.getPayCount1())) {
                queryWrapper.gt("g."+SQLConf.PAY_COUNT, girlOrderVO.getPayCount2());
            }
            if ("ge".equals(girlOrderVO.getPayCount1())) {
                queryWrapper.ge("g."+SQLConf.PAY_COUNT, girlOrderVO.getPayCount2());
            }
        }
        if (girlOrderVO.getPayStatus() != null) {
            queryWrapper.eq("g."+SQLConf.PAY_STATUS, girlOrderVO.getPayStatus());
        }
        if (girlOrderVO.getStatus() != null) {
            queryWrapper.eq("g."+SQLConf.STATUS, girlOrderVO.getStatus());
        }
        if (girlOrderVO.getAmount().compareTo(BigDecimal.ZERO) > 0) {
            queryWrapper.eq("g."+SQLConf.AMOUNT, girlOrderVO.getAmount());
        }
        if (StringUtils.isNotEmpty(girlOrderVO.getGirlUid()) && !StringUtils.isEmpty(girlOrderVO.getGirlUid())) {
            queryWrapper.like("g."+SQLConf.GIRL_UID, girlOrderVO.getGirlUid().trim());
        }

        if (StringUtils.isNotEmpty(girlOrderVO.getBeginCreateTime()) && girlOrderVO.getBeginCreateTime().equals(girlOrderVO.getEndCreateTime())) {
            queryWrapper.like("g."+SQLConf.CREATE_TIME, girlOrderVO.getEndCreateTime().trim());
        } else {
            if (StringUtils.isNotEmpty(girlOrderVO.getBeginCreateTime())) {
                queryWrapper.ge("g."+SQLConf.CREATE_TIME, girlOrderVO.getBeginCreateTime().trim());
            }
            if (StringUtils.isNotEmpty(girlOrderVO.getEndCreateTime())) {
                queryWrapper.le("g."+SQLConf.CREATE_TIME, girlOrderVO.getEndCreateTime().trim());
            }
        }
        if (StringUtils.isNotEmpty(girlOrderVO.getBeginUpdateTime()) && girlOrderVO.getBeginUpdateTime().equals(girlOrderVO.getEndUpdateTime())) {
            queryWrapper.like("g."+SQLConf.UPDATE_TIME, girlOrderVO.getBeginUpdateTime().trim());
        } else {
            if (StringUtils.isNotEmpty(girlOrderVO.getBeginUpdateTime())) {
                queryWrapper.ge("g."+SQLConf.UPDATE_TIME, girlOrderVO.getBeginUpdateTime().trim());
            }
            if (StringUtils.isNotEmpty(girlOrderVO.getEndUpdateTime())) {
                queryWrapper.le("g."+SQLConf.UPDATE_TIME, girlOrderVO.getEndUpdateTime().trim());
            }
        }
        if (girlOrderVO.getVipLevel().intValue() > 0) {
            queryWrapper.eq("c.vip_level",girlOrderVO.getVipLevel());
        }

        if (StringUtils.isNotEmpty(girlOrderVO.getSource())) {
            queryWrapper.eq("c.source",girlOrderVO.getSource());
        }
        return this.getBaseMapper().getGirlOrderCountSum(queryWrapper);
    }
}