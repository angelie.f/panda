package com.moxi.mogublog.xo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.Domain;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.mapper.DomainMapper;
import com.moxi.mogublog.xo.service.DomainService;
import com.moxi.mogublog.xo.vo.DomainVO;
import com.moxi.mougblog.base.enums.EStatus;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
public class DomainServiceImpl extends SuperServiceImpl<DomainMapper, Domain> implements DomainService {


    @Override
    public IPage<Domain> getPageList(DomainVO domainVO) {
        QueryWrapper<Domain> queryWrapper = new QueryWrapper<>();

        Page<Domain> page = new Page<>();
        page.setCurrent(domainVO.getCurrentPage());
        page.setSize(domainVO.getPageSize());
        queryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
        queryWrapper.orderByAsc("uid");
        IPage<Domain> pageList = page(page, queryWrapper);
        return pageList;
    }

    @Override
    public List<String> getList(int domainType) {
        QueryWrapper<Domain> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("domain_name");
        queryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
        if(domainType!=-1){
            queryWrapper.eq("domain_type", domainType);
        }
        queryWrapper.orderByAsc("uid");
        List<Domain> domainList = list(queryWrapper);
        List<String> list = new ArrayList<>();
        for (Domain domain:domainList){
            list.add(domain.getDomainName());
        }
        return list;
    }

    @Override
    public String add(DomainVO domainVO) {
        QueryWrapper<Domain> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("domain_name", domainVO.getDomainName());
        if (getOne(queryWrapper) != null) {
            return ResultUtil.errorWithMessage(MessageConf.ENTITY_EXIST);
        }
        Domain domain = new Domain();
        domain.setDomainName(domainVO.getDomainName());
        domain.setDomainType(domainVO.getDomainType());
        domain.insert();
        return ResultUtil.successWithMessage(MessageConf.INSERT_SUCCESS);
    }

    @Override
    public String edit(DomainVO domainVO) {
        Domain domain = getById(domainVO.getUid());

        if (domain != null && !domain.getDomainName().equals(domainVO.getDomainName())) {
            QueryWrapper<Domain> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("domain_name", domainVO.getDomainName());
            queryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
            Domain tempG = getOne(queryWrapper);
            if (tempG != null) {
                return ResultUtil.errorWithMessage(MessageConf.ENTITY_EXIST);
            }
        }

        domain.setDomainName(domainVO.getDomainName());
        domain.setDomainType(domainVO.getDomainType());
        domain.setStatus(EStatus.ENABLE);
        domain.setUpdateTime(new Date());
        domain.updateById();
        return ResultUtil.successWithMessage(MessageConf.UPDATE_SUCCESS);
    }

}
