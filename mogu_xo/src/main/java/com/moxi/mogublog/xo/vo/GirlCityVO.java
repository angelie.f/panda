package com.moxi.mogublog.xo.vo;

import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;

/**
 * ProjectVO
 *
 * @author Vergift
 * @since 2021-11-25
 */
@Data
public class GirlCityVO extends BaseVO<GirlCityVO> {

    /**
     * OrderBy排序字段（desc: 降序）
     */
    private String orderByDescColumn;

    /**
     * OrderBy排序字段（asc: 升序）
     */
    private String orderByAscColumn;

    /**
     * 城市名称
     */
    private String name;
}
