package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.GameActConfig;
import com.moxi.mogublog.xo.vo.ActConfigVo;
import com.moxi.mougblog.base.service.SuperService;

public interface GameActConfigService extends SuperService<GameActConfig> {

    IPage<ActConfigVo> getPageList(ActConfigVo actConfigVo);

    String addBanner(ActConfigVo actConfigVo);

    String editBanner(ActConfigVo actConfigVo);
}
