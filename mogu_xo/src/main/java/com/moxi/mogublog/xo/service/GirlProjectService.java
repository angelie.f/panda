package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.GirlProject;
import com.moxi.mogublog.xo.vo.GirlProjectVO;
import com.moxi.mougblog.base.service.SuperService;

import java.util.List;

/**
 * 妹子项目列表 服务类
 *
 * @author Vergift
 * @date 2021-11-29
 */
public interface GirlProjectService extends SuperService<GirlProject> {

    /**
     * 获取项目列表
     *
     * @param girlProjectVO
     * @return
     */
    IPage<GirlProject> getPageList(GirlProjectVO girlProjectVO);

    /**
     * 获取全部项目列表
     *
     * @return
     */
    List<GirlProject> getList(GirlProjectVO girlProjectVO);

    /**
     * 新增项目
     *
     * @param girlProjectVO
     */
    String addGirlProject(GirlProjectVO girlProjectVO);

    /**
     * 编辑项目
     *
     * @param girlProjectVO
     */
    String editGirlProject(GirlProjectVO girlProjectVO);

    /**
     * 初始化项目
     *
     * @return
     */
    GirlProject initPorject();

}
