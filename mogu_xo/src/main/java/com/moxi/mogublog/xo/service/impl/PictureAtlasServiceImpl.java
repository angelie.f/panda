package com.moxi.mogublog.xo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.*;
import com.moxi.mogublog.commons.feign.PictureFeignClient;
import com.moxi.mogublog.utils.RedisUtil;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.global.RedisConf;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.mapper.PictureAtlasMapper;
import com.moxi.mogublog.xo.service.*;
import com.moxi.mogublog.xo.utils.WebUtil;
import com.moxi.mogublog.xo.vo.CustomersVO;
import com.moxi.mogublog.xo.vo.PictureAtlasBySortVO;
import com.moxi.mogublog.xo.vo.PictureAtlasVO;
import com.moxi.mougblog.base.enums.EStatus;
import com.moxi.mougblog.base.global.Constants;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * 图片分类表 服务实现类
 *
 * @author 陌溪
 * @since 2018-09-04
 */
@Service
public class PictureAtlasServiceImpl extends SuperServiceImpl<PictureAtlasMapper, PictureAtlas> implements PictureAtlasService {

    @Autowired
    private WebUtil webUtil;
    @Autowired
    private PictureSortService pictureSortService;
    @Autowired
    private PictureAtlasService pictureAtlasService;
    @Autowired
    private PictureTagService pictureTagService;
    @Autowired
    private PictureService pictureService;
    @Autowired
    private PictureSortItemService pictureSortItemService;
    @Resource
    private PictureFeignClient pictureFeignClient;
    @Autowired
    private RedisUtil redisUtil;

    @Override
    public IPage<PictureAtlas> getPageList(PictureAtlasVO pictureAtlasVO) {
        QueryWrapper<PictureAtlas> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(pictureAtlasVO.getSortUid())){//分类uid获取图集
            QueryWrapper<PictureSortItem> sortItemQueryWrapper = new QueryWrapper<>();
            sortItemQueryWrapper.eq(SQLConf.SORT_UID, pictureAtlasVO.getSortUid());

            Page<PictureSortItem> sortItemPage = new Page<>();
            sortItemPage.setCurrent(1);
            sortItemPage.setSize(10000);
            IPage<PictureSortItem> sortItemPageList =  pictureSortItemService.page(sortItemPage,sortItemQueryWrapper);
            List<PictureSortItem> sortItemList = sortItemPageList.getRecords();
            List<Integer> atlasUids = new ArrayList<>();
            for (PictureSortItem pictureSortItem:sortItemList) {
                atlasUids.add(pictureSortItem.getAtlasUid());
            }
            if (atlasUids.size() >0 ){
                queryWrapper.in(SQLConf.UID,atlasUids);
            }
        }
        if (StringUtils.isNotEmpty(pictureAtlasVO.getUid())) {
            queryWrapper.eq(SQLConf.UID, pictureAtlasVO.getUid().trim());
        }
        if (StringUtils.isNotEmpty(pictureAtlasVO.getKeyword()) && !StringUtils.isEmpty(pictureAtlasVO.getKeyword().trim())) {
            queryWrapper.like(SQLConf.NAME, pictureAtlasVO.getKeyword().trim());
        }
        if (!StringUtils.isEmpty(pictureAtlasVO.getTagUid())) {
            queryWrapper.and(wrapper ->wrapper.eq(SQLConf.TAG_UID, pictureAtlasVO.getTagUid())
                    .or().likeRight(SQLConf.TAG_UID, pictureAtlasVO.getTagUid() + SysConf.FILE_SEGMENTATION)
                    .or().like(SQLConf.TAG_UID, SysConf.FILE_SEGMENTATION + pictureAtlasVO.getTagUid() + SysConf.FILE_SEGMENTATION)
                    .or().likeLeft(SQLConf.TAG_UID, SysConf.FILE_SEGMENTATION + pictureAtlasVO.getTagUid()));
        }
        if (!StringUtils.isEmpty(pictureAtlasVO.getCreateTime())) {
            queryWrapper.like(SQLConf.CREATE_TIME, pictureAtlasVO.getCreateTime());
        }
        if (!StringUtils.isEmpty(pictureAtlasVO.getStorageTime())) {
            queryWrapper.like(SQLConf.STORAGE_TIME, pictureAtlasVO.getStorageTime());
        }
        if (!StringUtils.isEmpty(pictureAtlasVO.getResource())) {
            queryWrapper.eq(SQLConf.RESOURCE, pictureAtlasVO.getResource());
        }
        if (pictureAtlasVO.getStatus()!=null) {
            queryWrapper.eq(SQLConf.STATUS, pictureAtlasVO.getStatus());
        }
        Page<PictureAtlas> page = new Page<>();
        page.setCurrent(pictureAtlasVO.getCurrentPage());
        page.setSize(pictureAtlasVO.getPageSize());
        queryWrapper.orderByDesc(SQLConf.CREATE_TIME);
        IPage<PictureAtlas> pageList = pictureAtlasService.page(page, queryWrapper);
        List<PictureAtlas> list = pageList.getRecords();
        List<String> tagUids = new ArrayList<>();
        final StringBuffer fileUids = new StringBuffer();
        list.forEach(item -> {
            if (StringUtils.isNotEmpty(item.getFileUid())) {
                fileUids.append(item.getFileUid() + SysConf.FILE_SEGMENTATION);
            }
            if (StringUtils.isNotEmpty(item.getTagUid())) {
                List<String> tagUidsTemp = StringUtils.changeStringToString(item.getTagUid(), SysConf.FILE_SEGMENTATION);
                for (String itemTagUid : tagUidsTemp) {
                    tagUids.add(itemTagUid);
                }
            }
        });
        String pictureResult = null;
        Map<String, String> pictureMap = new HashMap<>();
        if (fileUids != null) {
            pictureResult = this.pictureFeignClient.getPicture(fileUids.toString(), SysConf.FILE_SEGMENTATION);
        }
        List<Map<String, Object>> picList = webUtil.getPictureMap(pictureResult);

        picList.forEach(item -> {
            pictureMap.put(item.get(SysConf.UID).toString(), item.get(SysConf.URL).toString());
        });

        Collection<PictureTag> tagList = new ArrayList<>();
        if (tagUids.size() > 0) {
            tagList = pictureTagService.listByIds(tagUids);
        }

        Map<String, PictureTag> tagMap = new HashMap<>();

        tagList.forEach(item -> {
            tagMap.put(item.getUid(), item);
        });

        for (PictureAtlas item : list) {
            //获取图集下面图片数量
            /*QueryWrapper<Picture> pictureQueryWrapper = new QueryWrapper<>();
            pictureQueryWrapper.eq(SQLConf.PICTURE_ATLAS_UID,item.getUid());
            item.setCount(pictureService.count(pictureQueryWrapper));*/

            //获取标签
            if (StringUtils.isNotEmpty(item.getTagUid())) {
                List<String> tagUidsTemp = StringUtils.changeStringToString(item.getTagUid(), SysConf.FILE_SEGMENTATION);
                List<PictureTag> tagListTemp = new ArrayList<>();

                tagUidsTemp.forEach(tag -> {
                    tagListTemp.add(tagMap.get(tag));
                });
                item.setTagList(tagListTemp);
            }

            //获取图片
            if (StringUtils.isNotEmpty(item.getFileUid())) {
                List<String> pictureUidsTemp = StringUtils.changeStringToString(item.getFileUid(), SysConf.FILE_SEGMENTATION);
                List<String> pictureListTemp = new ArrayList<>();
                pictureUidsTemp.forEach(picture -> {
                    pictureListTemp.add(pictureMap.get(picture));
                });
                item.setPhotoList(pictureListTemp);
            }
        }
        pageList.setRecords(list);
        return pageList;
    }

    @Override
    public IPage<PictureAtlas> getAtlasListBySort(PictureAtlasBySortVO pictureAtlasBySortVO) {
        //获取上下架里面的图集uid
        QueryWrapper<PictureSortItem> sortItemQueryWrapper = new QueryWrapper<>();
        sortItemQueryWrapper.eq(SQLConf.SORT_UID, pictureAtlasBySortVO.getSortUid());
        sortItemQueryWrapper.eq(SQLConf.STATUS, pictureAtlasBySortVO.getStatus());

        Page<PictureSortItem> sortItemPage = new Page<>();
        sortItemPage.setCurrent(1);
        sortItemPage.setSize(10000);
        IPage<PictureSortItem> sortItemPageList =  pictureSortItemService.page(sortItemPage,sortItemQueryWrapper);
        List<PictureSortItem> sortItemList = sortItemPageList.getRecords();
        List<Integer> atlasUids = new ArrayList<>();
        for (PictureSortItem pictureSortItem:sortItemList) {
            atlasUids.add(pictureSortItem.getAtlasUid());
        }

        //根据上下架里面的图集uid获取图集消息
        QueryWrapper<PictureAtlas> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(pictureAtlasBySortVO.getName())){
            queryWrapper.like(SQLConf.NAME,pictureAtlasBySortVO.getName());
        }
        queryWrapper.in(SQLConf.UID,atlasUids);

        Page<PictureAtlas> page = new Page<>();
        page.setCurrent(pictureAtlasBySortVO.getCurrentPage());
        page.setSize(pictureAtlasBySortVO.getPageSize());
        queryWrapper.orderByDesc(SQLConf.CREATE_TIME);
        IPage<PictureAtlas> pageList = pictureAtlasService.page(page, queryWrapper);
        List<PictureAtlas> list = pageList.getRecords();
        List<String> tagUids = new ArrayList<>();
        final StringBuffer fileUids = new StringBuffer();
        list.forEach(item -> {
            if (StringUtils.isNotEmpty(item.getFileUid())) {
                fileUids.append(item.getFileUid() + SysConf.FILE_SEGMENTATION);
            }
            if (StringUtils.isNotEmpty(item.getTagUid())) {
                List<String> tagUidsTemp = StringUtils.changeStringToString(item.getTagUid(), SysConf.FILE_SEGMENTATION);
                for (String itemTagUid : tagUidsTemp) {
                    tagUids.add(itemTagUid);
                }
            }
        });
        String pictureResult = null;
        Map<String, String> pictureMap = new HashMap<>();
        if (fileUids != null) {
            pictureResult = this.pictureFeignClient.getPicture(fileUids.toString(), SysConf.FILE_SEGMENTATION);
        }
        List<Map<String, Object>> picList = webUtil.getPictureMap(pictureResult);

        picList.forEach(item -> {
            pictureMap.put(item.get(SysConf.UID).toString(), item.get(SysConf.URL).toString());
        });

        Collection<PictureTag> tagList = new ArrayList<>();
        if (tagUids.size() > 0) {
            tagList = pictureTagService.listByIds(tagUids);
        }

        Map<String, PictureTag> tagMap = new HashMap<>();

        tagList.forEach(item -> {
            tagMap.put(item.getUid(), item);
        });

        for (PictureAtlas item : list) {

            item.setStatus( pictureAtlasBySortVO.getStatus());
            //获取图集下面图片数量
            if (item.getCount() == null){
                QueryWrapper<Picture> pictureQueryWrapper = new QueryWrapper<>();
                pictureQueryWrapper.eq(SQLConf.PICTURE_ATLAS_UID,item.getUid());
                item.setCount(pictureService.count(pictureQueryWrapper));
            }

            //获取标签
            if (StringUtils.isNotEmpty(item.getTagUid())) {
                List<String> tagUidsTemp = StringUtils.changeStringToString(item.getTagUid(), SysConf.FILE_SEGMENTATION);
                List<PictureTag> tagListTemp = new ArrayList<>();

                tagUidsTemp.forEach(tag -> {
                    tagListTemp.add(tagMap.get(tag));
                });
                item.setTagList(tagListTemp);
            }

            //获取图集里面的图片
            if (StringUtils.isNotEmpty(item.getFileUid())) {
                List<String> pictureUidsTemp = StringUtils.changeStringToString(item.getFileUid(), SysConf.FILE_SEGMENTATION);
                List<String> pictureListTemp = new ArrayList<>();
                pictureUidsTemp.forEach(picture -> {
                    pictureListTemp.add(pictureMap.get(picture));
                });
                item.setPhotoList(pictureListTemp);

            }
        }
        pageList.setRecords(list);
        return pageList;
    }

    @Override
    public List<PictureAtlas> getResourceList() {
        QueryWrapper<PictureAtlas> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("DISTINCT "+SQLConf.RESOURCE);
        queryWrapper.isNotNull(SQLConf.RESOURCE);
        List<PictureAtlas> pageList = pictureAtlasService.list(queryWrapper);
        return pageList;
    }

    @Override
    public String addPictureAtlas(PictureAtlasVO pictureAtlasVO) {
        PictureAtlas pictureAtlas = new PictureAtlas();
        pictureAtlas.setName(pictureAtlasVO.getName());
        pictureAtlas.setParentUid(pictureAtlasVO.getParentUid());
        pictureAtlas.setSort(pictureAtlasVO.getSort());
        pictureAtlas.setFileUid(pictureAtlasVO.getFileUid());
        pictureAtlas.setStatus(EStatus.ENABLE);
        pictureAtlas.setIsShow(pictureAtlasVO.getIsShow());
        pictureAtlas.setUpdateTime(new Date());
        pictureAtlas.setTagUid(pictureAtlasVO.getTagUid());
        pictureAtlas.insert();
        return ResultUtil.successWithMessage(MessageConf.INSERT_SUCCESS);
    }

    @Override
    public String editPictureAtlas(PictureAtlasVO pictureAtlasVO) {
        PictureAtlas pictureAtlas = pictureAtlasService.getById(pictureAtlasVO.getUid());
        pictureAtlas.setName(pictureAtlasVO.getName());
        pictureAtlas.setModel(pictureAtlasVO.getModel());
        pictureAtlas.setProducer(pictureAtlasVO.getProducer());
        pictureAtlas.setTagUid(pictureAtlasVO.getTagUid());
        pictureAtlas.setUpdateTime(new Date());
        pictureAtlas.updateById();
        return ResultUtil.successWithMessage(MessageConf.UPDATE_SUCCESS);
    }

    @Override
    public String deletePictureAtlas(PictureAtlasVO pictureAtlasVO) {
        // 判断要删除的分类，是否有图片
        QueryWrapper<Picture> pictureQueryWrapper = new QueryWrapper<>();
        pictureQueryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
        pictureQueryWrapper.eq(SQLConf.PICTURE_ATLAS_UID, pictureAtlasVO.getUid());
        Integer pictureCount = pictureService.count(pictureQueryWrapper);
        if (pictureCount > 0) {
            return ResultUtil.errorWithMessage(MessageConf.PICTURE_UNDER_THIS_SORT);
        }

        PictureAtlas pictureAtlas = pictureAtlasService.getById(pictureAtlasVO.getUid());
        pictureAtlas.setStatus(EStatus.DISABLED);
        pictureAtlas.setUpdateTime(new Date());
        pictureAtlas.updateById();
        return ResultUtil.successWithMessage(MessageConf.DELETE_SUCCESS);
    }

    @Override
    public String stickPictureAtlas(PictureAtlasVO pictureAtlasVO) {
        PictureAtlas pictureAtlas = pictureAtlasService.getById(pictureAtlasVO.getUid());
        //查找出最大的那一个
        QueryWrapper<PictureAtlas> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc(SQLConf.SORT);
        Page<PictureAtlas> page = new Page<>();
        page.setCurrent(0);
        page.setSize(1);
        IPage<PictureAtlas> pageList = pictureAtlasService.page(page, queryWrapper);
        List<PictureAtlas> list = pageList.getRecords();
        PictureAtlas maxSort = list.get(0);
        if (StringUtils.isEmpty(maxSort.getUid())) {
            return ResultUtil.errorWithMessage(MessageConf.PARAM_INCORRECT);
        }
        if (maxSort.getUid().equals(pictureAtlas.getUid())) {
            return ResultUtil.errorWithMessage(MessageConf.THIS_SORT_IS_TOP);
        }
        Integer sortCount = maxSort.getSort() + 1;
        pictureAtlas.setSort(sortCount);
        pictureAtlas.setUpdateTime(new Date());
        pictureAtlas.updateById();
        return ResultUtil.successWithMessage(MessageConf.OPERATION_SUCCESS);
    }

    @Override
    public String updateTag(List<PictureAtlasVO> list) {
        List<PictureAtlas> lsit = new ArrayList<>();
        for (PictureAtlasVO pictureAtlasVO:list) {
            PictureAtlas pictureAtlas = new PictureAtlas();
            pictureAtlas.setUid(pictureAtlasVO.getUid());
            pictureAtlas.setTagUid(pictureAtlasVO.getTagUid());
            lsit.add(pictureAtlas);
            pictureAtlasService.updateById(pictureAtlas);
        }
//        pictureAtlasService.updateBatchById(lsit);
        return ResultUtil.successWithMessage(MessageConf.OPERATION_SUCCESS);
    }

    @Override
    public String upOrOut(List<PictureSortItem> list) {
        List<Integer> atlasUid = new ArrayList<>();
        for (PictureSortItem pictureSortItem:list) {
            atlasUid.add(pictureSortItem.getAtlasUid());
        }
        QueryWrapper<PictureSortItem> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SQLConf.SORT_UID, list.get(0).getSortUid());
        queryWrapper.in(SQLConf.ATLAS_UID,atlasUid);
        List<PictureSortItem> sortItemList =  pictureSortItemService.list(queryWrapper);

        for (PictureSortItem pictureSortItem:sortItemList) {
            pictureSortItem.setStatus(list.get(0).getStatus());
            pictureSortItem.setUpdateTime(new Date());
        }
        pictureSortItemService.updateBatchById(sortItemList);
        return ResultUtil.successWithMessage(MessageConf.OPERATION_SUCCESS);
    }

    @Override
    public void deleteRedisByPictureTag() {
        // 删除Redis中博客分类下的博客数量
        redisUtil.delete(RedisConf.DASHBOARD + Constants.SYMBOL_COLON + RedisConf.PICTURE_COUNT_BY_TAG);
        // 删除博客相关缓存
        deleteRedisByPictureAtlas();
    }

    @Override
    public void deleteRedisByPictureAtlas() {
        // 删除博客相关缓存
        redisUtil.delete(RedisConf.HOT_PICTURE_SORT);
        redisUtil.delete(RedisConf.HOT_PICTURE_SORT);
    }

    @Override
    public IPage<PictureAtlas> getCustomersAtlasList(CustomersVO customersVO, List<Integer> atlasUids ,UsePower usePower) {
        QueryWrapper<PictureAtlas> queryWrapper = new QueryWrapper<>();
        queryWrapper.in(SQLConf.UID, atlasUids);
        if (StringUtils.isNotEmpty(customersVO.getTagUid())){
            queryWrapper.and(wrapper ->wrapper.eq(SQLConf.TAG_UID, customersVO.getTagUid())
                    .or().likeRight(SQLConf.TAG_UID, customersVO.getTagUid() + SysConf.FILE_SEGMENTATION)
                    .or().like(SQLConf.TAG_UID, SysConf.FILE_SEGMENTATION + customersVO.getTagUid() + SysConf.FILE_SEGMENTATION)
                    .or().likeLeft(SQLConf.TAG_UID, SysConf.FILE_SEGMENTATION + customersVO.getTagUid()));
        }

        Page<PictureAtlas> page = new Page<>();
        page.setCurrent(customersVO.getCurrentPage());
        page.setSize(customersVO.getPageSize());
        queryWrapper.orderByDesc(SQLConf.CREATE_TIME);
        IPage<PictureAtlas> pageList = pictureAtlasService.page(page, queryWrapper);

        List<PictureAtlas> list = pageList.getRecords();

        String pictureResult = null;
        Map<String, String> pictureMap = new HashMap<>();
        final StringBuffer fileUids = new StringBuffer();
        list.forEach(item -> {
            //当该图集没有封面时默认第一张图片为封面
            if (StringUtils.isEmpty(item.getFileUid()) || "0".equals(item.getFileUid())) {
                QueryWrapper<Picture> pictureQueryWrapper = new QueryWrapper<>();
                pictureQueryWrapper.eq(SQLConf.PICTURE_ATLAS_UID, item.getUid());
                List<Picture> pictureList = pictureService.list(pictureQueryWrapper);

                if(pictureList.size() > 0){
                    Picture picture = pictureList.get(0);
                    item.setFileUid(picture.getFileUid());
                    picture.setUpdateTime(new Date());
                    picture.updateById();
                }

            }
            fileUids.append(item.getFileUid() + SysConf.FILE_SEGMENTATION);
        });
        if (fileUids != null) {
            pictureResult = this.pictureFeignClient.getPicture(fileUids.toString(), SysConf.FILE_SEGMENTATION);
        }
        List<Map<String, Object>> picList = webUtil.getPictureMap(pictureResult);

        picList.forEach(item -> {
            pictureMap.put(item.get(SysConf.UID).toString(), item.get(SysConf.URL).toString());
        });

        for (PictureAtlas item : list) {

            //设置图集的普通用户限制的图片数量
            if (usePower != null && usePower.getUserAstrict() == 1){
                item.setUserLimit(usePower.getUserLimit());
            }else {
                item.setUserLimit(item.getCount());
            }
            //获取图片
            if (StringUtils.isNotEmpty(item.getFileUid())) {
                List<String> pictureUidsTemp = StringUtils.changeStringToString(item.getFileUid(), SysConf.FILE_SEGMENTATION);
                List<String> pictureListTemp = new ArrayList<>();
                pictureUidsTemp.forEach(picture -> {
                    pictureListTemp.add(pictureMap.get(picture));
                });
                item.setPhotoList(pictureListTemp);
            }
        }
        pageList.setRecords(list);
        return pageList;
    }

}
