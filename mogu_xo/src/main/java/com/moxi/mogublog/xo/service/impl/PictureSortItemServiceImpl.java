package com.moxi.mogublog.xo.service.impl;

import com.moxi.mogublog.commons.entity.PictureSortItem;
import com.moxi.mogublog.xo.mapper.PictureSortItemMapper;
import com.moxi.mogublog.xo.service.PictureSortItemService;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 上下架表 服务实现类
 *
 * @author Vergift
 * @date 2021-12-03
 */
@Service
public class PictureSortItemServiceImpl extends SuperServiceImpl<PictureSortItemMapper, PictureSortItem> implements PictureSortItemService {

}
