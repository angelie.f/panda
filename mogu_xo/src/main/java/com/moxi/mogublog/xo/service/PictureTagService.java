package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.PictureTag;
import com.moxi.mogublog.xo.vo.CustomersVO;
import com.moxi.mogublog.xo.vo.PictureTagVO;
import com.moxi.mougblog.base.service.SuperService;

import java.util.List;

/**
 * 图片标签表 服务类
 *
 * @author Vergift
 * @date 2021-11-22
 */
public interface PictureTagService extends SuperService<PictureTag> {
    /**
     * 获取图集标签列表
     *
     * @param tictureTagVO
     * @return
     */
    public IPage<PictureTag> getPageList(PictureTagVO tictureTagVO);
    /**
     * 获取图集标签列表
     *
     * @param customersVO
     * @return
     */
    IPage<PictureTag> getCustomersTagList(CustomersVO customersVO);

    /**
     * 获取全部图集标签列表
     *
     * @return
     */
    public List<PictureTag> getList();

    /**
     * 新增图集标签
     *
     * @param tictureTagVO
     */
    public String addTag(PictureTagVO tictureTagVO);

    /**
     * 编辑图集标签
     *
     * @param tictureTagVO
     */
    public String editTag(PictureTagVO tictureTagVO);

    /**
     * 批量删除图集标签
     *
     * @param pictureTagVOList
     */
    public String deleteBatchTag(List<PictureTagVO> pictureTagVOList);

    /**
     * 置顶图集标签
     *
     * @param tictureTagVO
     */
    public String stickTag(PictureTagVO tictureTagVO);

    /**
     * 通过点击量排序博客
     *
     * @return
     */
    public String tagSortByClickCount();

    /**
     * 通过引用量排序博客
     *
     * @return
     */
    public String tagSortByCite();

    /**
     * 获取热门标签
     *
     * @return
     */
    public List<PictureTag> getHotTag(Integer hotTagCount);

    /**
     * 获取一个排序最高的标签
     *
     * @return
     */
    public PictureTag getTopTag();

    /**
     * 删除和图集有关的Redis缓存
     */
    public void deleteRedisByPictureTag();

    /**
     * 删除和图集有关的Redis缓存
     */
    public void deleteRedisByPictureAtlas();
}
