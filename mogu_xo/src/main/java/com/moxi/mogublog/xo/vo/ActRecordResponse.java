package com.moxi.mogublog.xo.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class ActRecordResponse implements Serializable {
    private static final long serialVersionUID = 3568309452729779871L;
    private String customerId;
    private BigDecimal totalAmount;
    private Boolean showPop;
    List<ActPreInfo> actPreInfoList;
}
