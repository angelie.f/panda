package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.GirlOrderComment;
import com.moxi.mougblog.base.mapper.SuperMapper;

/**
 * 妹子订单表 Mapper 接口
 *
 * @author Vergift
 * @since 2021-12-29
 */
public interface GirlOrderCommentMapper extends SuperMapper<GirlOrderComment> {

}
