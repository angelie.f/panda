package com.moxi.mogublog.xo.vo;

import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;

import java.math.BigDecimal;

/**
 * GirlOrderCommentVO
 *
 * @author Vergift
 * @since 2022-01-07
 */
@Data
public class GirlOrderCommentVO extends BaseVO<GirlOrderCommentVO> {

    /**
     * OrderBy排序字段（desc: 降序）
     */
    private String orderByDescColumn;

    /**
     * OrderBy排序字段（asc: 升序）
     */
    private String orderByAscColumn;

    private Float score;
    private Float score1;
    private Float score2;

    /**
     * 订单ID
     */
    private String orderId;

    private Integer girlId;

    /**
     * 评论内容
     */
    private String content;

    /**
     * 回复某条评论的uid
     */
    private Integer toUid;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 一级评论uid
     */
    private Integer firstCommentUid;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 更新时间
     */
    private String updateTime;

    /**
     * 评论类型
     */
    private Integer commentType;

    /**
     * 是否為官方ID
     */
    private Integer isOfficial;
}
