package com.moxi.mogublog.xo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.moxi.mogublog.commons.config.security.SecurityUser;
import com.moxi.mogublog.commons.entity.Blacklist;
import com.moxi.mogublog.commons.entity.BlacklistLog;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.mapper.BlacklistLogMapper;
import com.moxi.mogublog.xo.mapper.BlacklistMapper;
import com.moxi.mogublog.xo.service.BlacklistService;
import com.moxi.mogublog.xo.vo.BlacklistVO;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class BlacklistServiceImpl extends SuperServiceImpl<BlacklistMapper, Blacklist> implements BlacklistService{

    @Autowired
    private BlacklistLogMapper blacklistLogMapper;

    @Override
    public Blacklist getBlacklist(BlacklistVO blacklistVO){
        QueryWrapper<Blacklist> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status",1);
        queryWrapper.eq("vali_uid",blacklistVO.getValiUid());
        Blacklist blacklist = getOne(queryWrapper);
        if(blacklist==null){
            blacklist = new Blacklist();
            blacklist.setIsBlack(0);
            blacklist.setReason("");
        }
        return blacklist;
    }
    @Override
    public String edit(BlacklistVO blacklistVO) {
        QueryWrapper<Blacklist> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("vali_uid",blacklistVO.getValiUid());
        queryWrapper.eq("status",1);
        Blacklist blacklist =baseMapper.selectOne(queryWrapper);
        SecurityUser securityUser = (SecurityUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        //insert
        if(blacklist==null){
            blacklist = new Blacklist();
            blacklist.setValiUid(blacklistVO.getValiUid());
            blacklist.setReason(blacklistVO.getReason());
            blacklist.setIsBlack(blacklistVO.getIsBlack());
            blacklist.setPhone(blacklistVO.getPhone());
            Integer rows = baseMapper.insert(blacklist);
            BlacklistLog blog = new BlacklistLog();
            blog.setUserName(securityUser.getUsername());
            blog.setValiUid(blacklistVO.getValiUid());
            StringBuilder sbOperation = new StringBuilder();
            sbOperation.append("1;");
            if(StringUtils.isNotEmpty(blacklist.getReason())){
                sbOperation.append("3;");
            }
            blog.setOperation(sbOperation.toString());
            blacklistLogMapper.insert(blog);
            log.info("insert success:{},data:{}",rows>0,blacklist.toString());
            return ResultUtil.successWithMessage(MessageConf.INSERT_SUCCESS);
        }
        else {
            Blacklist updateBl= new Blacklist();
            updateBl.setValiUid(blacklistVO.getValiUid());
            updateBl.setReason(blacklistVO.getReason());
            updateBl.setIsBlack(blacklistVO.getIsBlack());
            updateBl.setPhone(blacklistVO.getPhone());
            UpdateWrapper<Blacklist> updateWrapper = new UpdateWrapper();
            updateWrapper.eq("vali_uid",blacklistVO.getValiUid());
            updateWrapper.eq("status",1);
            Integer rows =baseMapper.update(updateBl,updateWrapper);
            BlacklistLog blog = new BlacklistLog();
            blog.setUserName(securityUser.getUsername());
            blog.setValiUid(blacklistVO.getValiUid());
            StringBuilder sbOperation = new StringBuilder();
            if(!updateBl.getIsBlack().equals(blacklist.getIsBlack())){
                if(updateBl.getIsBlack()==1){
                    sbOperation.append("1;");
                }
                else{
                    sbOperation.append("2;");
                }
            }
            if(!updateBl.getReason().equals(blacklist.getReason())){
                sbOperation.append("3;");
            }
            blog.setOperation(sbOperation.toString());
            blacklistLogMapper.insert(blog);
            log.info("update success:{},data:{}",rows>0,updateBl.toString());
            return ResultUtil.successWithMessage(MessageConf.UPDATE_SUCCESS);

        }
    }
    @Override
    public boolean checkInBlackByCustomerId(String customerId){
        int count =baseMapper.countBlackByCustomerId(customerId);
        return count>0;
    }
}
