package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.Banner;
import com.moxi.mougblog.base.mapper.SuperMapper;

public interface BannerMapper extends SuperMapper<Banner> {

}
