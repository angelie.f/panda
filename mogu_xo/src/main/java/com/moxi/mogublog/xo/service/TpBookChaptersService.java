package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.xo.vo.LocalBooksVO;
import com.moxi.mogublog.xo.vo.TpBookChaptersVO;
import com.moxi.mougblog.base.service.SuperService;
import com.moxi.mogublog.commons.entity.TpBookChapters;

public interface TpBookChaptersService extends SuperService<TpBookChapters> {

    String getBookChapters(LocalBooksVO localBooksVO);
    IPage<TpBookChapters> getPageList(TpBookChaptersVO tpBookChaptersVO);
}

