package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.PictureAtlas;
import com.moxi.mogublog.commons.entity.PictureSort;
import com.moxi.mogublog.commons.entity.Project;
import com.moxi.mogublog.xo.vo.CustomersVO;
import com.moxi.mogublog.xo.vo.PictureAtlasVO;
import com.moxi.mogublog.xo.vo.PictureSortVO;
import com.moxi.mougblog.base.service.SuperService;

import java.util.List;

/**
 * 图片分类表 服务类
 *
 * @author Vergift
 * @date 2021-12-02
 */
public interface PictureSortService extends SuperService<PictureSort> {
    /**
     * 获取图片分类列表
     *
     * @return
     */
    Project initPorject();

    /**
     * 获取图片分类列表
     *
     * @param pictureSortVO
     * @return
     */
    IPage<PictureSort> getPageList(PictureSortVO pictureSortVO);

    /**
     * 获取图片分类列表
     *
     * @param customersVO
     * @return
     */
    IPage<PictureSort> getCustomersSortList(CustomersVO customersVO);

    /**
     * 获取图片来源列表
     *
     * @return
     */
    List<PictureSort> getResourceList();

    /**
     * 新增图片分类
     *
     * @param pictureSort
     */
    String addPictureSort(PictureSort pictureSort);

    /**
     * 编辑图片分类
     *
     * @param pictureSort
     */
    String editPictureSort(PictureSort pictureSort);

}
