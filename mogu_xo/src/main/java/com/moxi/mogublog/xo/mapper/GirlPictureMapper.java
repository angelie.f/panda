package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.GirlPicture;
import com.moxi.mougblog.base.mapper.SuperMapper;

/**
 * 妹子图片表 Mapper 接口
 *
 * @author Vergift
 * @since 2021-12-29
 */
public interface GirlPictureMapper extends SuperMapper<GirlPicture> {

}
