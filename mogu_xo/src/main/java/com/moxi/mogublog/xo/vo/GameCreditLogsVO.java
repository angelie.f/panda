package com.moxi.mogublog.xo.vo;

import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;
import lombok.ToString;


@ToString
@Data
public class GameCreditLogsVO extends BaseVO<GameCreditLogsVO> {

    /**
     * 用户id
     */
    private String customerId;

    private Integer type;

    private String createTimeBegin;

    private String createTimeEnd;



}
