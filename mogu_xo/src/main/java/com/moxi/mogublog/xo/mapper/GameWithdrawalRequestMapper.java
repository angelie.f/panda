package com.moxi.mogublog.xo.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.GameWithdrawalRequest;
import com.moxi.mogublog.xo.vo.GameWithdrawalRequestVo;
import com.moxi.mougblog.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

public interface GameWithdrawalRequestMapper extends SuperMapper<GameWithdrawalRequest> {

    IPage<GameWithdrawalRequest> queryWithdrawalList(IPage<GameWithdrawalRequest> page, @Param("gameWithdrawalRequestVo") GameWithdrawalRequestVo gameWithdrawalRequestVo);

    BigDecimal queryWithdrawalSunAmout(@Param("gameWithdrawalRequestVo") GameWithdrawalRequestVo gameWithdrawalRequestVo);

    int queryWithdrawalTimesTotal(@Param("gameWithdrawalRequestVo") GameWithdrawalRequestVo gameWithdrawalRequestVo);
}
