package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.PictureTag;
import com.moxi.mogublog.commons.entity.Tag;
import com.moxi.mougblog.base.mapper.SuperMapper;

/**
 * 标签表 Mapper 接口
 *
 * @author Vergift
 * @since 2021-11-22
 */
public interface PictureTagMapper extends SuperMapper<PictureTag> {

}
