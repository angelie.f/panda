package com.moxi.mogublog.xo.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class ActPreInfo implements Serializable {
    private static final long serialVersionUID = 3823142303287944504L;
    private BigDecimal amount;
    private Integer falg;
    private String actType;
    private String actTitle;
    private Boolean pop;

}
