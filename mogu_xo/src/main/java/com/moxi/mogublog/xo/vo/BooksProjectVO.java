package com.moxi.mogublog.xo.vo;

import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;

/**
 * BooksProjectVO
 * @author Sid
 * @since 2022-05-31
 */
@Data
public class BooksProjectVO extends BaseVO<BooksProjectVO> {

    /**
     * 排序字段
     */
    private Integer sort;

    /**
     * OrderBy排序字段（desc: 降序）
     */
    private String orderByDescColumn;

    /**
     * OrderBy排序字段（asc: 升序）
     */
    private String orderByAscColumn;

    private Integer source;
    //标题
    private String name;
    //普通用戶是否限制:1.限制；0.不限制
    private Integer ordmemberLimit;
    //币别名称
    private String currency;
    //会员升级提示文字
    private String upgradeMemo;
    //会员升级跳转URL
    private String upgradeUrl;
    //会员升级提示图档uid
    private Integer fileUid;

}
