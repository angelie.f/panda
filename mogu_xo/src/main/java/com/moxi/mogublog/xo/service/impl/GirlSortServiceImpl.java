package com.moxi.mogublog.xo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.*;
import com.moxi.mogublog.commons.feign.PictureFeignClient;
import com.moxi.mogublog.utils.RedisUtil;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.mapper.GirlSortMapper;
import com.moxi.mogublog.xo.service.*;
import com.moxi.mogublog.xo.utils.WebUtil;
import com.moxi.mogublog.xo.vo.GirlSortVO;
import com.moxi.mogublog.xo.vo.LocalGirlVO;
import com.moxi.mougblog.base.enums.EStatus;
import com.moxi.mougblog.base.global.BaseSQLConf;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * 妹子分类表 服务实现类
 *
 * @author Vergift
 * @date 2021-12-29
 */
@Service
public class GirlSortServiceImpl extends SuperServiceImpl<GirlSortMapper, GirlSort> implements GirlSortService {

    @Autowired
    private WebUtil webUtil;
    @Autowired
    RedisUtil redisUtil;
    @Autowired
    private GirlSortService girlSortService;
    @Resource
    private PictureFeignClient pictureFeignClient;
    @Resource
    private GirlProjectService girlProjectService;
    @Resource
    private GirlTagService girlTagService;
    @Resource
    private GirlSortItemService girlSortItemService;
    @Resource
    private GirlService girlService;
    @Resource
    private UsePowerService usePowerService;

    @Override
    public IPage<GirlSort> getPageList(GirlSortVO girlSortVO) {
        QueryWrapper<GirlSort> queryWrapper = new QueryWrapper<>();
        //GirlProject girlProject = girlProjectService.initPorject();
        //if (girlProject != null){
            //queryWrapper.eq(SQLConf.PROJECT_UID,girlProject.getUid());//只取当前用户所属项目的图集分类
        //}

        if (StringUtils.isNotEmpty(girlSortVO.getName()) && !StringUtils.isEmpty(girlSortVO.getName().trim())) {
            queryWrapper.like(SQLConf.NAME, girlSortVO.getName().trim());
        }
        if (!StringUtils.isEmpty(girlSortVO.getCreateTime())) {
            queryWrapper.like(SQLConf.CREATE_TIME, girlSortVO.getCreateTime());
        }
        Page<GirlSort> page = new Page<>();
        page.setCurrent(girlSortVO.getCurrentPage());
        page.setSize(girlSortVO.getPageSize());
        queryWrapper.orderByDesc(SQLConf.CREATE_TIME);
        IPage<GirlSort> pageList = girlSortService.page(page, queryWrapper);
        List<GirlSort> list = pageList.getRecords();
        List<String> tagUids = new ArrayList<>();
        //List<String> projectUids = new ArrayList<>();
        final StringBuffer fileUids = new StringBuffer();
        list.forEach(item -> {
            if (StringUtils.isNotEmpty(item.getFileUid())) {
                fileUids.append(item.getFileUid() + SysConf.FILE_SEGMENTATION);
            }
            if (StringUtils.isNotEmpty(item.getTagUid())) {
                List<String> tagUidsTemp = StringUtils.changeStringToString(item.getTagUid(), SysConf.FILE_SEGMENTATION);
                for (String itemTagUid : tagUidsTemp) {
                    tagUids.add(itemTagUid);
                }
            }
          /*
            if (StringUtils.isNotEmpty(item.getProjectUid())) {
                List<String> projectUidsTemp = StringUtils.changeStringToString(item.getProjectUid(), SysConf.FILE_SEGMENTATION);
                for (String itemProjectUid : projectUidsTemp) {
                    projectUids.add(itemProjectUid);
                }
            }
            */

        });
        String pictureResult = null;
        Map<String, String> pictureMap = new HashMap<>();
        if (fileUids != null) {
            pictureResult = this.pictureFeignClient.getPicture(fileUids.toString(), SysConf.FILE_SEGMENTATION);
        }
        List<Map<String, Object>> picList = webUtil.getPictureMap(pictureResult);

        picList.forEach(item -> {
            pictureMap.put(item.get(SysConf.UID).toString(), item.get(SysConf.URL).toString());
        });

        Collection<GirlTag> tagList = new ArrayList<>();
        if (tagUids.size() > 0) {
            tagList = girlTagService.listByIds(tagUids);
        }

        //Collection<GirlProject> projectList = new ArrayList<>();
        //if (tagUids.size() > 0) {
           // projectList = girlProjectService.listByIds(projectUids);
        //}

        Map<String, GirlTag> tagMap = new HashMap<>();

        tagList.forEach(item -> {
            tagMap.put(item.getUid(), item);
        });

        Map<String, GirlProject> projectMap = new HashMap<>();

       // projectList.forEach(item -> {
       //     projectMap.put(item.getUid(), item);
        //});

        List<UsePower> usePowerList = usePowerService.list();
        for (GirlSort item : list) {

            QueryWrapper<GirlSortItem> upCountQueryWrapper = new QueryWrapper<>();
            upCountQueryWrapper.eq(SQLConf.SORT_UID,item.getUid());
            upCountQueryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
            item.setUpCount(girlSortItemService.list(upCountQueryWrapper).size());

            QueryWrapper<GirlSortItem> outCountQueryWrapper = new QueryWrapper<>();
            outCountQueryWrapper.eq(SQLConf.SORT_UID,item.getUid());
            outCountQueryWrapper.eq(SQLConf.STATUS, EStatus.DISABLED);
            item.setOutCount(girlSortItemService.list(outCountQueryWrapper).size());

            //获取标签
            if (StringUtils.isNotEmpty(item.getTagUid())) {
                List<String> tagUidsTemp = StringUtils.changeStringToString(item.getTagUid(), SysConf.FILE_SEGMENTATION);
                List<GirlTag> tagListTemp = new ArrayList<>();

                tagUidsTemp.forEach(tag -> {
                    tagListTemp.add(tagMap.get(tag));
                });
                item.setTagList(tagListTemp);
            }

            //获取项目
            if (StringUtils.isNotEmpty(item.getProjectUid())) {
                item.setProject(projectMap.get(item.getProjectUid()));
            }

            //获取图片
            if (StringUtils.isNotEmpty(item.getFileUid())) {
                List<String> pictureUidsTemp = StringUtils.changeStringToString(item.getFileUid(), SysConf.FILE_SEGMENTATION);
                List<String> pictureListTemp = new ArrayList<>();
                pictureUidsTemp.forEach(picture -> {
                    pictureListTemp.add(pictureMap.get(picture));
                });
                item.setPhotoList(pictureListTemp);
            }
        }
        pageList.setRecords(list);
        return pageList;
    }

    @Override
    public String addGirlSort(GirlSortVO girlSortVO) {
        QueryWrapper<GirlSort> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SQLConf.NAME, girlSortVO.getName());
        /*if (girlSortService.getOne(queryWrapper) != null) {
            return ResultUtil.errorWithMessage(MessageConf.ENTITY_EXIST);
        }*/

        girlSortVO.setStatus(EStatus.ENABLE);
        GirlSort girlSort = new GirlSort();
        BeanUtils.copyProperties(girlSortVO, girlSort);
        girlSort.setProjectUid("1");
        girlSort.insert();

        initSortItem(girlSort);

        return ResultUtil.successWithMessage(MessageConf.INSERT_SUCCESS);
    }

    @Override
    public String editGirlSort(GirlSortVO girlSortVO) {
        GirlSort girlSort = new GirlSort();
        BeanUtils.copyProperties(girlSortVO, girlSort);
        girlSort.setUpdateTime(new Date());
        girlSort.setProjectUid("1");
        girlSort.updateById();

        initSortItem(girlSort);

        return ResultUtil.successWithMessage(MessageConf.UPDATE_SUCCESS);
    }

    @Override
    public String deleteGirlSort(GirlSortVO girlSortVO) {
        //删除分类上下架妹子
        QueryWrapper<GirlSortItem> sortItemQueryWrapper = new QueryWrapper<>();
        sortItemQueryWrapper.eq(SQLConf.SORT_UID,girlSortVO.getUid());
        girlSortItemService.remove(sortItemQueryWrapper);

        //删除分类
        girlSortService.removeById(girlSortVO.getUid());

        return ResultUtil.successWithMessage(MessageConf.UPDATE_SUCCESS);
    }

    //新增和编辑时需要对应的更新上下架信息
    public void initSortItem(GirlSort girlSort){
        String[] tagUidList = girlSort.getTagUid().split(",");
        List<GirlSortItem> girlSortItemList = new ArrayList<>();
        Map<String,GirlSortItem> map = new HashMap();
        for (String tagUid:tagUidList) {
            QueryWrapper<Girl> queryWrapper = new QueryWrapper<>();
            queryWrapper.orderByDesc(SQLConf.CREATE_TIME);
            if (!StringUtils.isEmpty(tagUid)) {
                queryWrapper.and(wrapper ->wrapper.eq(SQLConf.TAG_UID, tagUid)
                        .or().likeRight(SQLConf.TAG_UID, tagUid + SysConf.FILE_SEGMENTATION)
                        .or().like(SQLConf.TAG_UID, SysConf.FILE_SEGMENTATION + tagUid + SysConf.FILE_SEGMENTATION)
                        .or().likeLeft(SQLConf.TAG_UID, SysConf.FILE_SEGMENTATION + tagUid));
                List<Girl> pageList = girlService.list(queryWrapper);

                for (Girl girl:pageList) {
                    //新的上下架信息
                    GirlSortItem girlSortItem = new GirlSortItem();
                    girlSortItem.setUid(null);
                    girlSortItem.setGirlUid(Integer.valueOf(girl.getUid()));
                    girlSortItem.setSortUid(Integer.valueOf(girlSort.getUid()));
                    girlSortItem.setStatus(EStatus.DISABLED);//默认为下架中

                    //去掉重复的图集
                    map.put(girl.getUid(),girlSortItem);

                }
            }

        }

        //获取老的上下架信息
        QueryWrapper<GirlSortItem> sortItemQueryWrapper = new QueryWrapper<>();
        sortItemQueryWrapper.eq(SQLConf.SORT_UID,girlSort.getUid());
        List<GirlSortItem> sortItemList = girlSortItemService.list(sortItemQueryWrapper);
        List<String> sortItemUids = new ArrayList<>();
        //删除原因的上下架信息
        if (sortItemList.size() > 0){
            for (GirlSortItem sortItem:sortItemList) {
                if (map.get(String.valueOf(sortItem.getGirlUid())) != null){
                    ////去掉已有的分类图集上下信息
                    map.remove(String.valueOf(sortItem.getGirlUid()));
                }else {
                    sortItemUids.add(sortItem.getUid());
                }
            }

            //去掉不需要的分类图集上下信息
            if (sortItemUids.size() > 0){
                girlSortItemService.removeByIds(sortItemUids);
            }
        }

        for(String atlasUid:map.keySet()){
            girlSortItemList.add(map.get(atlasUid));
        }

        if (girlSortItemList.size() > 0){
            //批量添加新的上下架信息
            girlSortItemService.saveBatch(girlSortItemList);
        }

    }

    @Override
    public String getGirlSortList(LocalGirlVO localGirlVO) {
//        QueryWrapper<GirlProject> girlProjectQueryWrapper = new QueryWrapper<>();

//        girlProjectQueryWrapper.eq(SQLConf.SOURCE, localGirlVO.getSource());
//        List<GirlProject> girlProjectList = girlProjectService.list(girlProjectQueryWrapper);

        QueryWrapper<GirlSort> queryWrapper = new QueryWrapper<>();
        //queryWrapper.eq(SQLConf.PROJECT_UID,girlProjectList.get(0).getUid());//只取当前用户所属项目的图集分类
        queryWrapper.orderByDesc(BaseSQLConf.SORT);

        Page<GirlSort> page = new Page<>();
        if (localGirlVO.getCurrentPage() == null){
            page.setCurrent(1);
        }else {
            page.setCurrent(localGirlVO.getCurrentPage());
        }
        if (localGirlVO.getPageSize() == null){
            page.setSize(10000);
        }else {
            page.setSize(localGirlVO.getPageSize());
        }

        IPage<GirlSort> pageList = girlSortService.page(page, queryWrapper);
        List<GirlSort> list = pageList.getRecords();
        final StringBuffer fileUids = new StringBuffer();
        list.forEach(item -> {
            if (StringUtils.isNotEmpty(item.getFileUid())) {
                fileUids.append(item.getFileUid() + SysConf.FILE_SEGMENTATION);
            }
        });
        String pictureResult = null;
        Map<String, String> pictureMap = new HashMap<>();
        if (fileUids != null) {
            pictureResult = this.pictureFeignClient.getPicture(fileUids.toString(), SysConf.FILE_SEGMENTATION);
        }
        List<Map<String, Object>> picList = webUtil.getPictureMap(pictureResult);

        picList.forEach(item -> {
            pictureMap.put(item.get(SysConf.UID).toString(), item.get(SysConf.URL).toString());
        });

        GirlSort girlSort = new GirlSort();
        girlSort.setName("推荐");
        girlSort.setUid("");
        list.add(0,girlSort);
        for (GirlSort item : list) {
            //获取图片
            if (StringUtils.isNotEmpty(item.getFileUid())) {
                List<String> pictureUidsTemp = StringUtils.changeStringToString(item.getFileUid(), SysConf.FILE_SEGMENTATION);
                List<String> pictureListTemp = new ArrayList<>();
                pictureUidsTemp.forEach(picture -> {
                    pictureListTemp.add(pictureMap.get(picture));
                });
                item.setPhotoList(pictureListTemp);
            }
        }
        pageList.setRecords(list);
        return ResultUtil.result(SysConf.SUCCESS,pageList);
    }

}
