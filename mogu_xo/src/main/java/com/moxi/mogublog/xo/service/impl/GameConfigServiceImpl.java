package com.moxi.mogublog.xo.service.impl;


import cn.hutool.core.lang.Assert;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.*;
import com.moxi.mogublog.utils.*;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.mapper.*;
import com.moxi.mogublog.xo.service.CoinsChangeService;
import com.moxi.mogublog.xo.service.GameConfigService;
import com.moxi.mogublog.xo.service.GameCreditLogsService;
import com.moxi.mogublog.xo.utils.HttpUtil;
import com.moxi.mogublog.xo.utils.HttpsUtil;
import com.moxi.mogublog.xo.vo.GameConfigTrialVO;
import com.moxi.mogublog.xo.vo.GameConfigVO;
import com.moxi.mogublog.xo.vo.GameManagerVo;
import com.moxi.mougblog.base.global.BaseMessageConf;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import sun.misc.BASE64Encoder;

import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class GameConfigServiceImpl extends SuperServiceImpl<GameConfigMapper, GameConfig> implements GameConfigService {
    @Autowired
    private GameConfigMapper gameConfigMapper;
    @Autowired
    private GameTypeMapper gameTypeMapper;
    @Autowired
    private GameConfigTrialMapper gameConfigTrialMapper;
    @Autowired
    private CustomersMapper customersMapper;
    @Autowired
    private GameTransferMapper gameTransferMapper;
    @Autowired
    private GameCustomersBalanceMapper gameCustomersBalanceMapper;
    @Autowired
    private GameCreditLogsService gameCreditLogsService;
    @Autowired
    RedisUtil redisUtil;
    @Value(value = "${game.enterGame.url}")
    private String enterGameUrl;
    @Value(value = "${game.transfer.url}")
    private String transferUrl;
    @Value(value = "${game.getBalance.url}")
    private String getBalanceUrl;
    @Value(value = "${game.aes.key}")
    private String aesKey;
    @Value(value = "${game.user.account}")
    private String account;
    @Value(value = "${game.sign.account}")
    private String signKey;
    @Value(value = "${game.agent.id}")
    private String agentId;

    @Autowired
    private CoinsChangeService coinsChangeService;

    @Override
    public Map<String, Object> getConfigAndType() {
        QueryWrapper<GameConfig> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("tgc.status", 1);
        queryWrapper.orderByDesc("tgc.sort");
        List<GameConfig> configs = gameConfigMapper.getList(queryWrapper);
        QueryWrapper<GameType> queryTypeWrapper = new QueryWrapper<>();
        queryTypeWrapper.eq("status", 1);
        queryTypeWrapper.orderByAsc("uid");
        List<GameType> types = gameTypeMapper.selectList(queryTypeWrapper);
        Map<String, Object> map = new HashMap();
        map.put("configs", configs);
        map.put("types", types);
        return map;
    }

    @Override
    public String getGameTrail(GameConfigTrialVO gameConfigTrialVO) {
        QueryWrapper<GameConfigTrial> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("game_source", gameConfigTrialVO.getSource());
        GameConfigTrial gameConfigTrial = gameConfigTrialMapper.selectOne(queryWrapper);
        if (gameConfigTrial == null) {
            gameConfigTrial = new GameConfigTrial();
            gameConfigTrial.setStatus(0);
            gameConfigTrial.setGameSource(Integer.parseInt(gameConfigTrialVO.getSource()));
            gameConfigTrialMapper.insert(gameConfigTrial);
        }
        return ResultUtil.result(SysConf.SUCCESS, gameConfigTrial);
    }

    @Transactional(rollbackFor = {Exception.class})
    @Override
    public String enterGame(GameConfigVO gameConfigVO) {
        Customers customers = customersMapper.queryBalanceByCustomerId(gameConfigVO.getCustomerId());
        if (customers == null) {
            return ResultUtil.result(com.moxi.mogublog.xo.global.SysConf.ERROR, BaseMessageConf.ENTITY_NOT_EXIST);
        }
        BigDecimal coins = customers.getCoins();


        QueryWrapper<GameCustomersBalance> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("customer_id", gameConfigVO.getCustomerId());
        GameCustomersBalance gameCustomersBalance = gameCustomersBalanceMapper.selectOne(queryWrapper);
        gameCustomersBalance.setCoins(new BigDecimal(0));
        gameCustomersBalance.setMeVersion(null);
        gameCustomersBalance.setFrozenCoins(gameCustomersBalance.getFrozenCoins().add(gameCustomersBalance.getCoins()));
        gameCustomersBalanceMapper.updateById(gameCustomersBalance);


        String p = "";
        String orderId = agentId + "_" + DateUtils.formateDate(new Date(), "yyyyMMddHHmmssSSS") + "_" + customers.getUid();
        if (coins.compareTo(new BigDecimal(0)) == 1) {
            p = "uid=" + customers.getUid() + "&game=" + gameConfigVO.getThirdNo() + "&orderId=" + orderId + "&credit=" + coins;
        } else {
            p = "uid=" + customers.getUid() + "&game=" + gameConfigVO.getThirdNo();
        }
        String result = "";
        try {
            p = AESUtils.encryptForGame(p, aesKey);
            p = p.replaceAll("[\\s*\t\n\r]", "");
            String t = String.valueOf(new Date().getTime() / 1000);
            String a = account;
            String k = MD5Utils.md5Sign(p + t + signKey);
            log.info("p参数urlEncode前的值：{}", p);
            p = URLEncoder.encode(p, "utf-8");
            log.info("游戏上分传参url:{}", enterGameUrl + "?" + "a=" + a + "&t=" + t + "&p=" + p + "&k=" + k);
            result = HttpsUtil.get(enterGameUrl + "?" + "a=" + a + "&t=" + t + "&p=" + p + "&k=" + k);
            log.info("游戏上分返回结果result: {}", result);
        } catch (Exception e) {
            log.info("游戏上分失败,{}", e.getMessage());
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return ResultUtil.result(com.moxi.mogublog.xo.global.SysConf.ERROR, BaseMessageConf.TRANSFER_GAME);
        }
        JSONObject jsonObject = JSONObject.parseObject(result);
        Integer code = jsonObject.getInteger("code");

        if (code == 0) {
            String data = jsonObject.getString("data");
            JSONObject dataJson = JSONObject.parseObject(data);
            Integer orderStatus = dataJson.getInteger("orderStatus");
            String orderReason = dataJson.getString("orderReason");
            if (orderStatus!=null&&orderStatus!=1){
                log.info("游戏上分失败,{}", orderReason);
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return ResultUtil.result(com.moxi.mogublog.xo.global.SysConf.ERROR, BaseMessageConf.TRANSFER_GAME);
            }
            if (coins.compareTo(new BigDecimal(0)) == 1 ) {
                GameCreditLogs gameCreditLogs = new GameCreditLogs();
                gameCreditLogs.setCustomerId(customers.getCustomerId());
                gameCreditLogs.setType(3);
                gameCreditLogs.setSource(customers.getSource());
                gameCreditLogs.setCoinsBefore(coins);
                gameCreditLogs.setCoinsAfter(new BigDecimal(0));
                gameCreditLogs.setReferenceId(orderId);
                gameCreditLogsService.save(gameCreditLogs);
            }

            String gameUrl = dataJson.getString("gameUrl");
            return ResultUtil.result(SysConf.SUCCESS, gameUrl);
        } else {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return ResultUtil.result(com.moxi.mogublog.xo.global.SysConf.ERROR, BaseMessageConf.TRANSFER_GAME);
        }
    }


    @Override
    public String transferOut(GameConfigVO gameConfigVO) {
        Customers customers = customersMapper.queryBalanceByCustomerId(gameConfigVO.getCustomerId());
        String p1 = "";
        p1 = "uid=" + customers.getUid();
        p1 = AESUtils.encryptForGame(p1, aesKey);
        p1 = p1.replaceAll("[\\s*\t\n\r]", "");
        String t1 = String.valueOf(new Date().getTime() / 1000);
        String a1 = account;
        String k1 = MD5Utils.md5Sign(p1 + t1 + signKey);
        String result1 = "";
        try {
            p1 = URLEncoder.encode(p1, "utf-8");
            log.info("查询游戏可下分余额传参url:{},a:{},t:{},p:{},k:{}", getBalanceUrl, a1, t1, p1, k1);
            result1 = HttpsUtil.get(getBalanceUrl + "?" + "a=" + a1 + "&t=" + t1 + "&p=" + p1 + "&k=" + k1);
            log.info("查询游戏可下分余额返回结果result: {}", result1);
            JSONObject jsonObject1 = JSONObject.parseObject(result1);
            Integer code1 = jsonObject1.getInteger("code");
            if (code1 == 0) {
                String data1 = jsonObject1.getString("data");
                JSONObject dataJson1 = JSONObject.parseObject(data1);
                Integer status1 = dataJson1.getInteger("status");
                String transferable = dataJson1.getString("transferable");
                String orderId = agentId + "_" + DateUtils.formateDate(new Date(), "yyyyMMddHHmmssSSS") + "_" + customers.getUid();
                if (status1 == 1 && new BigDecimal(transferable).compareTo(new BigDecimal(0)) == 1) {
                    String p = "";
                    p = "uid=" + customers.getUid() + "&orderId=" + orderId + "&credit=" + (new BigDecimal(transferable).negate());
                    p = AESUtils.encryptForGame(p, aesKey);
                    p = p.replaceAll("[\\s*\t\n\r]", "");
                    String t = String.valueOf(new Date().getTime() / 1000);
                    String a = account;
                    String k = MD5Utils.md5Sign(p + t + signKey);
                    String result = "";
                    p = URLEncoder.encode(p, "utf-8");

                    //扩大锁的范围 如果没有拿到锁就退出游戏 这里三方是没办法回滚的 金额会减少
                    String key = "gameBalance_" + gameConfigVO.getCustomerId();
                    boolean isLock = redisUtil.setIfAbsent(key, "", 30 * 1000, TimeUnit.MILLISECONDS);
                    if (isLock) {
                        try {
                            log.info("游戏下分传参url:{},a:{},t:{},p:{},k:{}", transferUrl, a, t, p, k);
                            result = HttpsUtil.get(transferUrl + "?" + "a=" + a + "&t=" + t + "&p=" + p + "&k=" + k);
                            log.info("游戏下分返回结果result: {}", result);
                            JSONObject jsonObject = JSONObject.parseObject(result);
                            Integer code = jsonObject.getInteger("code");
                            if (code == 0) {
                                String data = jsonObject.getString("data");
                                JSONObject dataJson = JSONObject.parseObject(data);
                                Integer status = dataJson.getInteger("status");
                                if (status == 1) {
                                    coinsChangeService.transferOut(transferable, customers, gameConfigVO, orderId);
                                    return ResultUtil.result(SysConf.SUCCESS, "下分成功");
                                } else {
                                    return ResultUtil.result(com.moxi.mogublog.xo.global.SysConf.ERROR, dataJson.getString("reason"));
                                }
                            } else {
                                return ResultUtil.result(com.moxi.mogublog.xo.global.SysConf.ERROR, BaseMessageConf.TRANSFER_GAME);
                            }
                        } finally {
                            if (redisUtil.hasKey(key))
                                redisUtil.delete(key);
                        }
                    } else {
                        return ResultUtil.result(SysConf.ERROR, BaseMessageConf.TRANSFER_GAME);
                    }
                }
            }
        } catch (Exception e) {
            log.info("游戏下分失败,{}", e.getMessage());
            return ResultUtil.result(com.moxi.mogublog.xo.global.SysConf.ERROR, BaseMessageConf.TRANSFER_GAME);
        }
        return ResultUtil.result(SysConf.SUCCESS, "下分成功");
    }

    @Override
    public String editGameConfig(GameManagerVo gameManagerVo) {
        GameConfig gameConfig = gameConfigMapper.selectById(gameManagerVo.getUid());
        Assert.notNull(gameConfig, MessageConf.PARAM_INCORRECT);
        gameConfig.setName(gameManagerVo.getName());
        gameConfig.setImageUrl(gameManagerVo.getImageUrl());
        gameConfig.setSort(gameManagerVo.getSort());
        gameConfig.setFileUid(gameManagerVo.getFileUid());
        gameConfig.setStatus(gameManagerVo.getStatus());
        gameConfig.setGameTypeUid(gameManagerVo.getGameTypeUid());
        gameConfig.updateById();
        return ResultUtil.successWithMessage(MessageConf.OPERATION_SUCCESS);
    }

    @Override
    public String changeAvailable(GameConfigTrialVO gameConfigTrialVO) {
        log.info("changeAvailable gameConfigTrial:{}", gameConfigTrialVO.toString());
        gameConfigTrialMapper.updateAvailable(gameConfigTrialVO.getSource(), gameConfigTrialVO.getStatus());
        return ResultUtil.successWithMessage(MessageConf.UPDATE_SUCCESS);
    }

    @Override
    public IPage<GameConfig> getPageList(GameManagerVo gameManagerVo) {
        QueryWrapper<GameConfig> queryWrapper = new QueryWrapper<>();
        Page<GameConfig> page = new Page<>();
        page.setCurrent(gameManagerVo.getCurrentPage());
        page.setSize(gameManagerVo.getPageSize());
        //queryWrapper.eq("game_source", gameManagerVo.getGameSource());
        if (StringUtils.isNotEmpty(gameManagerVo.getOrderByAscColumn())) {
            // 将驼峰转换成下划线
            String column = StringUtils.underLine(new StringBuffer(gameManagerVo.getOrderByAscColumn())).toString();
            queryWrapper.orderByAsc("tgc.".concat(column));
        } else if (StringUtils.isNotEmpty(gameManagerVo.getOrderByDescColumn())) {
            // 将驼峰转换成下划线
            String column = StringUtils.underLine(new StringBuffer(gameManagerVo.getOrderByDescColumn())).toString();
            queryWrapper.orderByDesc("tgc.".concat(column));
        } else {
            queryWrapper.orderByDesc("tgc.".concat(SQLConf.CREATE_TIME));
        }
        IPage<GameConfig> pageList = gameConfigMapper.getPage(page, queryWrapper);
        return pageList;
    }
}
