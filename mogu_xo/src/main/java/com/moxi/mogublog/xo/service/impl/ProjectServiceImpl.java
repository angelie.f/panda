package com.moxi.mogublog.xo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.Project;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.mapper.ProjectMapper;
import com.moxi.mogublog.xo.service.ProjectService;
import com.moxi.mogublog.xo.vo.ProjectVO;
import com.moxi.mougblog.base.enums.EStatus;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * 图片标签表 服务实现类
 *
 * @author Vergift
 * @date 2021-11-22
 */
@Service
public class ProjectServiceImpl extends SuperServiceImpl<ProjectMapper, Project> implements ProjectService {

    @Autowired
    private ProjectService projectService;

    @Override
    public IPage<Project> getPageList(ProjectVO projectVO) {
        QueryWrapper<Project> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(projectVO.getName()) && !StringUtils.isEmpty(projectVO.getName())) {
            queryWrapper.like(SQLConf.NAME, projectVO.getName().trim());
        }

        Page<Project> page = new Page<>();
        page.setCurrent(projectVO.getCurrentPage());
        page.setSize(projectVO.getPageSize());
        queryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
        if(StringUtils.isNotEmpty(projectVO.getOrderByAscColumn())) {
            // 将驼峰转换成下划线
            String column = StringUtils.underLine(new StringBuffer(projectVO.getOrderByAscColumn())).toString();
            queryWrapper.orderByAsc(column);
        } else if(StringUtils.isNotEmpty(projectVO.getOrderByDescColumn())) {
            // 将驼峰转换成下划线
            String column = StringUtils.underLine(new StringBuffer(projectVO.getOrderByDescColumn())).toString();
            queryWrapper.orderByDesc(column);
        }
        IPage<Project> pageList = projectService.page(page, queryWrapper);
        return pageList;
    }

    @Override
    public List<Project> getList(ProjectVO projectVO) {
        QueryWrapper<Project> queryWrapper = new QueryWrapper<>();

        if (projectVO.getSource() != null) {
            queryWrapper.eq(SQLConf.SOURCE, projectVO.getSource());
        }
        List<Project> tagList = projectService.list(queryWrapper);
        return tagList;
    }

    @Override
    public String addTag(ProjectVO projectVO) {
        QueryWrapper<Project> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SQLConf.NAME, projectVO.getName());
        if (projectService.getOne(queryWrapper) != null) {
            return ResultUtil.errorWithMessage(MessageConf.ENTITY_EXIST);
        }
        Project project = new Project();
        project.setName(projectVO.getName());
        project.setSource(projectVO.getSource());
        project.insert();
        return ResultUtil.successWithMessage(MessageConf.INSERT_SUCCESS);
    }

    @Override
    public String editTag(ProjectVO projectVO) {
        Project project = projectService.getById(projectVO.getUid());

        if (project != null && !project.getName().equals(projectVO.getName())) {
            QueryWrapper<Project> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq(SQLConf.NAME, projectVO.getName());
            queryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
            Project tempTag = projectService.getOne(queryWrapper);
            if (tempTag != null) {
                return ResultUtil.errorWithMessage(MessageConf.ENTITY_EXIST);
            }
        }

        project.setName(projectVO.getName());
        project.setSource(projectVO.getSource());
        project.setStatus(EStatus.ENABLE);
        project.setUpdateTime(new Date());
        project.updateById();
        return ResultUtil.successWithMessage(MessageConf.UPDATE_SUCCESS);
    }

    @Override
    public String deleteBatchTag(List<ProjectVO> projectVOList) {
        if (projectVOList.size() <= 0) {
            return ResultUtil.errorWithMessage(MessageConf.PARAM_INCORRECT);
        }
        List<String> uids = new ArrayList<>();
        projectVOList.forEach(item -> {
            uids.add(item.getUid());
        });

        Collection<Project> tagList = projectService.listByIds(uids);
        tagList.forEach(item -> {
            item.setUpdateTime(new Date());
            item.setStatus(EStatus.DISABLED);
        });

        Boolean save = projectService.updateBatchById(tagList);

        if (save) {
            return ResultUtil.successWithMessage(MessageConf.DELETE_SUCCESS);
        } else {
            return ResultUtil.errorWithMessage(MessageConf.DELETE_FAIL);
        }
    }

}
