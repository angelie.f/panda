package com.moxi.mogublog.xo.vo;

import lombok.Data;
import lombok.ToString;

/**
 * 同城约妹参数类
 *
 * @author Vergift
 * @date 2021-12-29
 */
@ToString
@Data
public class LocalGirlGiftVO {

    private Integer uid;

    /**
     * 注册来源：1.青青草 2.小黄鸭
     */
    private String source;

    private String customerId;

    /**
     * 订单id
     */
    private String orderId;

    private String receiveName;

    private String receiveNumber;

    private String receiveAddress;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 更新时间
     */
    private String updateTime;

    /**
     * 签名
     */
    private String sign;

    /**
     * 访问请求终端 H5
     */
    private String endPoint;

    /**
     * 当前页
     */
    private Long currentPage;

    /**
     * 页大小
     */
    private Long pageSize;

}
