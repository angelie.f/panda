package com.moxi.mogublog.xo.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class LoginGameResponse {

    private String url;

    private String userId;

    private String token;

    private String customersId;

    private Integer source;

    private String productId;

    private String platform;

    private String appToken;

    private String route;

    private String openCode;

    private String screen;

    private Integer isVip;

    private Integer vipLevel;

    private Integer onOff;

    private Integer vipAstrict;

    private String authentication;

    private BigDecimal coins;

    private BigDecimal withdrawalCoins;

    private Integer isBindPhone;

    private String mobile;

    private Integer isBlacklist;

    private Integer isOneMember;
    private String oneSecret;
//    private Integer isH5;

}
