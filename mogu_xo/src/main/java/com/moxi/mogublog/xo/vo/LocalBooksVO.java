package com.moxi.mogublog.xo.vo;

import lombok.Data;
import lombok.ToString;


@ToString
@Data
public class LocalBooksVO {

    /**
     * 用户id
     */
    private String customerId;

    /**
     * 第三方用户id
     */
    private String productId;

    /**
     * 注册来源：1.青青草 2.小黄鸭
     */
    private String source;

    /**
     * 关键字
     */
    private String keyWord;

    /**
     * 当前页
     */
    private Long currentPage;

    /**
     * 页大小
     */
    private Long pageSize;

    /**
     * 主题Id
     */
    private Integer topicId;

    /**
     * 漫画Id
     */
    private Integer bookId;

    /**
     * 漫画集数
     */
    private Integer episode;

    /**
     * 升序:asc;降序:desc
     */
    private String orderType;

    /**
     * 平台
     */
    private String platform;

}
