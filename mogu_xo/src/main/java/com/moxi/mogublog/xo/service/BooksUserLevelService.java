package com.moxi.mogublog.xo.service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.BooksUserLevel;
import com.moxi.mogublog.xo.vo.BooksUserLevelVO;
import com.moxi.mougblog.base.service.SuperService;



public interface BooksUserLevelService extends SuperService<BooksUserLevel> {

    IPage<BooksUserLevel> getPageList(BooksUserLevelVO booksUserLevelVO);

    String add(BooksUserLevelVO booksUserLevelVO);

    String edit(BooksUserLevelVO booksUserLevelVO);

    String delete(BooksUserLevelVO booksUserLevelVO);

}

