package com.moxi.mogublog.xo.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CoinsResponse {

    private BigDecimal coins;

    private String version;


}
