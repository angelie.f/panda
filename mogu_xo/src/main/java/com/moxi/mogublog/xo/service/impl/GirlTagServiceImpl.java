package com.moxi.mogublog.xo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.GirlTag;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.mapper.GirlTagMapper;
import com.moxi.mogublog.xo.service.GirlTagService;
import com.moxi.mogublog.xo.vo.GirlTagVO;
import com.moxi.mougblog.base.enums.EStatus;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 妹子标签表 服务实现类
 *
 * @author Vergift
 * @date 2022-01-05
 */
@Service
public class GirlTagServiceImpl extends SuperServiceImpl<GirlTagMapper, GirlTag> implements GirlTagService {

    @Autowired
    private GirlTagService girlTagService;

    @Override
    public IPage<GirlTag> getPageList(GirlTagVO girlTagVO) {
        QueryWrapper<GirlTag> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(girlTagVO.getKeyword()) && !StringUtils.isEmpty(girlTagVO.getKeyword())) {
            queryWrapper.like(SQLConf.NAME, girlTagVO.getKeyword().trim());
        }

        Page<GirlTag> page = new Page<>();
        page.setCurrent(girlTagVO.getCurrentPage());
        page.setSize(girlTagVO.getPageSize());
        if(StringUtils.isNotEmpty(girlTagVO.getOrderByAscColumn())) {
            // 将驼峰转换成下划线
            String column = StringUtils.underLine(new StringBuffer(girlTagVO.getOrderByAscColumn())).toString();
            queryWrapper.orderByAsc(column);
        } else if(StringUtils.isNotEmpty(girlTagVO.getOrderByDescColumn())) {
            // 将驼峰转换成下划线
            String column = StringUtils.underLine(new StringBuffer(girlTagVO.getOrderByDescColumn())).toString();
            queryWrapper.orderByDesc(column);
        }
        IPage<GirlTag> pageList = girlTagService.page(page, queryWrapper);
        return pageList;
    }

    @Override
    public String addTag(GirlTagVO girlTagVO) {
        QueryWrapper<GirlTag> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SQLConf.NAME, girlTagVO.getName());
        queryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
        GirlTag tempTag = girlTagService.getOne(queryWrapper);
        if (tempTag != null) {
            return ResultUtil.errorWithMessage(MessageConf.ENTITY_EXIST);
        }
        GirlTag tag = new GirlTag();
        tag.setName(girlTagVO.getName());
        tag.setStatus(EStatus.ENABLE);
        tag.setIntroduce(girlTagVO.getIntroduce());
        tag.insert();

        return ResultUtil.successWithMessage(MessageConf.INSERT_SUCCESS);
    }

    @Override
    public String editTag(GirlTagVO girlTagVO) {
        GirlTag tag = girlTagService.getById(girlTagVO.getUid());

        if (tag != null && !tag.getName().equals(girlTagVO.getName())) {
            QueryWrapper<GirlTag> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq(SQLConf.NAME, girlTagVO.getName());
            queryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
            GirlTag tempTag = girlTagService.getOne(queryWrapper);
            if (tempTag != null) {
                return ResultUtil.errorWithMessage(MessageConf.ENTITY_EXIST);
            }
        }

        tag.setName(girlTagVO.getName());
        tag.setIntroduce(girlTagVO.getIntroduce());
        tag.setUpdateTime(new Date());
        tag.updateById();

        return ResultUtil.successWithMessage(MessageConf.UPDATE_SUCCESS);
    }

}
