package com.moxi.mogublog.xo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.GameDepositAward;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.mapper.GameDepositAwardMapper;
import com.moxi.mogublog.xo.service.GameDepositAwardService;
import com.moxi.mogublog.xo.vo.GameDepositAwardVo;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class GameDepositAwardServiceImpl extends SuperServiceImpl<GameDepositAwardMapper, GameDepositAward> implements GameDepositAwardService {

    @Override
    public IPage<GameDepositAward> getPageList(GameDepositAwardVo gameDepositAwardVo) {
        QueryWrapper<GameDepositAwardVo> queryWrapper = new QueryWrapper<>();

        if (StringUtils.isNotBlank(gameDepositAwardVo.getSource())) {
            queryWrapper.eq("a.source", gameDepositAwardVo.getSource().trim());
        }
        if (StringUtils.isNotBlank(gameDepositAwardVo.getBeginCreateTime()) && gameDepositAwardVo.getBeginCreateTime().equals(gameDepositAwardVo.getEndCreateTime())){
            queryWrapper.like("a.period", gameDepositAwardVo.getEndCreateTime().trim());
        }else {
            if (StringUtils.isNotEmpty(gameDepositAwardVo.getBeginCreateTime())) {
                queryWrapper.ge("a.period", gameDepositAwardVo.getBeginCreateTime().trim());
            }
            if (StringUtils.isNotEmpty(gameDepositAwardVo.getEndCreateTime())) {
                queryWrapper.le("a.period", gameDepositAwardVo.getEndCreateTime().trim());
            }
        }
        if (StringUtils.isNotBlank(gameDepositAwardVo.getWuid())) {
            queryWrapper.eq("c.uid", gameDepositAwardVo.getWuid().trim());
        }
        Page<GameDepositAward> page = new Page<>();
        page.setCurrent(gameDepositAwardVo.getCurrentPage());
        page.setSize(gameDepositAwardVo.getPageSize());
        queryWrapper.orderByDesc("a.uid");
        IPage<GameDepositAward> pageList = baseMapper.getPage(page, queryWrapper);
        return pageList;
    }
    public GameDepositAward queryAward(GameDepositAwardVo gameDepositAwardVo) {
        return baseMapper.queryAward(gameDepositAwardVo.getAwardDate());
    }
    public List<GameDepositAward> queryRecentAwards() {
        return baseMapper.queryRecent7Award();
    }
}
