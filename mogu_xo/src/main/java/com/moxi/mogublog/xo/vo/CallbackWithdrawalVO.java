package com.moxi.mogublog.xo.vo;

import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class CallbackWithdrawalVO {

    private String userId;

    private String requestId;

    private String status;

    private String timestamp;

    private String remarks;
}
