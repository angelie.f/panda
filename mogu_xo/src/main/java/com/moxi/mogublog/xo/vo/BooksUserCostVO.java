package com.moxi.mogublog.xo.vo;

import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;

/**
 * BooksUserCostVO
 * @author Sid
 * @since 2022-05-31
 */
@Data
public class BooksUserCostVO extends BaseVO<BooksUserCostVO> {

    /**
     * 排序字段
     */
    private Integer sort;

    /**
     * OrderBy排序字段（desc: 降序）
     */
    private String orderByDescColumn;

    /**
     * OrderBy排序字段（asc: 升序）
     */
    private String orderByAscColumn;

    /**
     * 会员等级UID
     */
    private Integer levelUid;

    /**
     * 会员等级ID
     */
    private Integer levelId;

    /**
     * 会员等级名称
     */
    private String levelName;

    /**
     * 每日可看话数或集数
     */
    private Integer enableEpisode;

    /**
     * 漫画项目uid
     */
    private Integer projectUid;
}
