package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.GameConfigTrial;
import com.moxi.mougblog.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

public interface GameConfigTrialMapper extends SuperMapper<GameConfigTrial> {

    @Update("update t_game_config_trial set status =${status} where game_source=${source}")
    int updateAvailable(@Param("source") String source, @Param("status") int status);
}
