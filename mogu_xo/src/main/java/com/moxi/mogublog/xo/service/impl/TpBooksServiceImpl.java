package com.moxi.mogublog.xo.service.impl;

import com.alibaba.csp.sentinel.util.StringUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.TpTopics;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.mapper.TpTaggablesMapper;
import com.moxi.mogublog.xo.mapper.TpTagsMapper;
import com.moxi.mogublog.xo.mapper.TpTopicsMapper;
import com.moxi.mogublog.xo.service.TpTopicsService;
import com.moxi.mogublog.xo.vo.LocalBooksVO;
import com.moxi.mogublog.xo.vo.TpBooksVO;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.moxi.mogublog.xo.mapper.TpBooksMapper;
import com.moxi.mogublog.xo.service.TpBooksService;
import com.moxi.mogublog.commons.entity.TpBooks;

import java.util.ArrayList;
import java.util.List;

@Service
public class TpBooksServiceImpl extends SuperServiceImpl<TpBooksMapper, TpBooks> implements TpBooksService {

    @Autowired
    private TpTopicsMapper tpTopicsMapper;

    @Autowired
    private TpTopicsService tpTopicsService;

    @Autowired
    private TpTagsMapper tpTagsMapper;

    @Autowired
    private TpTaggablesMapper tpTaggablesMapper;

    @Value("${books.domain}")
    private String imageDomain;

    @Override
    public String getBooksList(LocalBooksVO localBooksVO) {
        Page<TpBooks> page = new Page<>();
        if (localBooksVO.getCurrentPage() == null){
            page.setCurrent(1);
        }else {
            page.setCurrent(localBooksVO.getCurrentPage());
        }
        if (localBooksVO.getPageSize() == null){
            page.setSize(20);
        }else {
            page.setSize(localBooksVO.getPageSize());
        }
        if(localBooksVO.getTopicId()!=null){
            IPage<TpBooks> pageList = baseMapper.getPageByTopicId(page,localBooksVO.getTopicId());
            for(TpBooks books:pageList.getRecords()){
                books.setHorizontalCover(convertImageUrl(books.getHorizontalCover()));
                books.setVerticalCover(convertImageUrl(books.getVerticalCover()));
            }
            addTags(pageList.getRecords());
            return ResultUtil.result(SysConf.SUCCESS,pageList.getRecords());
        }
        else{
            QueryWrapper<TpBooks> queryWrapper = new QueryWrapper<>();
            queryWrapper.select("id","title","author","description","end","view_counts","vertical_cover","horizontal_cover","created_at");
            queryWrapper.eq("status",1);
            queryWrapper.like("title",localBooksVO.getKeyWord());
            List<TpTopics> topicsList = tpTopicsService.getTopicList(localBooksVO);
            List<Integer> topicsIds = new ArrayList<>();
            if(topicsList!=null){
                for (TpTopics topics:topicsList) {
                    topicsIds.add(topics.getId());
                }
            }
            if(topicsIds.size()>0){
                List<Integer> bookIds = tpTopicsMapper.getBooksIdsByTopicIds(topicsIds);
                if(bookIds!=null&&bookIds.size()>0){
                    queryWrapper.or().in("id",bookIds);
                }
            }
            IPage<TpBooks> pageList = page(page, queryWrapper);
            addTags(pageList.getRecords());
            for(TpBooks books:pageList.getRecords()){
                books.setHorizontalCover(convertImageUrl(books.getHorizontalCover()));
                books.setVerticalCover(convertImageUrl(books.getVerticalCover()));
            }
            return ResultUtil.result(SysConf.SUCCESS,pageList.getRecords());
        }
    }
    public String getRelatedBooksList(LocalBooksVO localBooksVO) {
        QueryWrapper<TpBooks> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("id","title","author","description","end","view_counts","vertical_cover","horizontal_cover","created_at");
        queryWrapper.eq("status",1);
        List<Integer> booksIds= tpTaggablesMapper.getRelatedBookIds(localBooksVO.getBookId());
        if(booksIds.isEmpty()){
                return ResultUtil.result(SysConf.SUCCESS,"");
        }
        queryWrapper.in("id",booksIds);
        Page<TpBooks> page = new Page<>();
        if (localBooksVO.getCurrentPage() == null){
            page.setCurrent(1);
        }else {
            page.setCurrent(localBooksVO.getCurrentPage());
        }
        if (localBooksVO.getPageSize() == null){
            page.setSize(10000);
        }else {
            page.setSize(localBooksVO.getPageSize());
        }
        IPage<TpBooks> pageList = page(page, queryWrapper);
        addTags(pageList.getRecords());
        for(TpBooks books:pageList.getRecords()){
            books.setHorizontalCover(convertImageUrl(books.getHorizontalCover()));
            books.setVerticalCover(convertImageUrl(books.getVerticalCover()));
        }
        return ResultUtil.result(SysConf.SUCCESS,pageList.getRecords());
    }

    @Override
    public IPage<TpBooks> getPageList(TpBooksVO tpBooksVO) {
        QueryWrapper<TpBooks> queryWrapper = new QueryWrapper<>();
        if (StringUtil.isNotEmpty(tpBooksVO.getBookName())) {
            queryWrapper.like("title", tpBooksVO.getBookName());
        }
        if (StringUtil.isNotEmpty(tpBooksVO.getAuthor())) {
            queryWrapper.like("author", tpBooksVO.getAuthor());
        }
        if (StringUtil.isNotEmpty(tpBooksVO.getTag())) {
            QueryWrapper wrapper = new QueryWrapper<>();
            wrapper.like("tt.name ",tpBooksVO.getTag());
            List<Integer> bookIds = tpTaggablesMapper.queryBookIdsByTagName(wrapper);
            if(bookIds==null||bookIds.isEmpty()){
                queryWrapper.eq("id",-1);
            }
            else{
                queryWrapper.in("id",bookIds);
            }
        }
        if (StringUtil.isNotEmpty(tpBooksVO.getTopic())) {
            QueryWrapper wrapper = new QueryWrapper<>();
            wrapper.like("ttp.title  ",tpBooksVO.getTopic());
            List<Integer> bookIds = tpTaggablesMapper.queryBookIdsByTopics(wrapper);
            if(bookIds==null||bookIds.isEmpty()){
                queryWrapper.eq("id",-1);
            }
            else{
                queryWrapper.in("id",bookIds);
            }
        }
        if (StringUtil.isNotEmpty(tpBooksVO.getBeginCreateTime())) {
            queryWrapper.ge("created_at", tpBooksVO.getBeginCreateTime().trim());
        }
        if (com.moxi.mogublog.utils.StringUtils.isNotEmpty(tpBooksVO.getEndCreateTime())) {
            queryWrapper.le("created_at", tpBooksVO.getEndCreateTime().trim());
        }
        if (StringUtil.isNotEmpty(tpBooksVO.getBeginUpdateTime())) {
            queryWrapper.ge("updated_at", tpBooksVO.getBeginUpdateTime().trim());
        }
        if (StringUtils.isNotEmpty(tpBooksVO.getEndUpdateTime())) {
            queryWrapper.le("updated_at", tpBooksVO.getEndUpdateTime().trim());
        }

        Page<TpBooks> page = new Page<>();
        page.setCurrent(tpBooksVO.getCurrentPage());
        page.setSize(tpBooksVO.getPageSize());
        if (StringUtils.isNotEmpty(tpBooksVO.getOrderByAscColumn())) {
            // 将驼峰转换成下划线
            String column = com.moxi.mogublog.utils.StringUtils.underLine(new StringBuffer(tpBooksVO.getOrderByAscColumn())).toString();
            queryWrapper.orderByAsc(column);
        } else if (StringUtils.isNotEmpty(tpBooksVO.getOrderByDescColumn())) {
            // 将驼峰转换成下划线
            String column = com.moxi.mogublog.utils.StringUtils.underLine(new StringBuffer(tpBooksVO.getOrderByDescColumn())).toString();
            queryWrapper.orderByDesc(column);
        }
        else{
            queryWrapper.orderByDesc("updated_at");
        }
        IPage<TpBooks> pageList = page(page, queryWrapper);
        List<TpBooks> list = pageList.getRecords();
        for(TpBooks books:list){
            List<String> tags =tpTagsMapper.getTags(books.getId());
            books.setTags(tags);
            List<String> topics  = tpTopicsMapper.queryTopicsByBooksId(books.getId());
            books.setTopics(topics);
        }
        return pageList;
    }

    private String convertImageUrl(String url){
        if(StringUtils.isNotEmpty(url)){
            url = url.replaceAll("\\\\", "");
            //UAT
            url=imageDomain.concat(url);
        }
        return url;
    }
    private void addTags(List<TpBooks> list){
        for(TpBooks books:list){
            books.setTags(tpTagsMapper.getTags(books.getId()));
        }
    }
}

