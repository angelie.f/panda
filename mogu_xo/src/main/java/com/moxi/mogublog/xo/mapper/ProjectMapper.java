package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.Project;
import com.moxi.mougblog.base.mapper.SuperMapper;

/**
 * 项目表 Mapper 接口
 *
 * @author Vergift
 * @since 2021-11-25
 */
public interface ProjectMapper extends SuperMapper<Project> {

}
