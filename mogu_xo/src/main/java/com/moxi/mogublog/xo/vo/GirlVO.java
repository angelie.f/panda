package com.moxi.mogublog.xo.vo;

import com.moxi.mogublog.commons.entity.GirlPicture;
import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * GirlVO
 *
 * @author Vergift
 * @since 2022-01-10
 */
@Data
public class GirlVO extends BaseVO<GirlVO> {

    /**
     * OrderBy排序字段（desc: 降序）
     */
    private String orderByDescColumn;

    /**
     * OrderBy排序字段（asc: 升序）
     */
    private String orderByAscColumn;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 更新时间
     */
    private String updateTime;

    /**
     * 妹子uid
     */
    private String girlUid;

    /**
     * 小视频uid
     */
    private String videoUid;

    /**
     * 姓名
     */
    private String name;

    /**
     * 简介
     */
    private String introduce;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 价格（次）
     */
    private BigDecimal priceOne;

    /**
     * 价格（包夜）
     */
    private BigDecimal priceNight;

    /**
     * 城市uid
     */
    private String cityUid;

    /**
     * 身高（cm）
     */
    private Integer height;

    /**
     * 体重（kg）
     */
    private Integer weight;

    /**
     * 罩杯
     */
    private String cup;

    /**
     * 职业
     */
    private String occupation;

    /**
     * 妹子标签uid:1,2,...
     */
    private String tagUid;

    private List<GirlPicture> picList;

    private String dateable;

    private Integer available;

}
