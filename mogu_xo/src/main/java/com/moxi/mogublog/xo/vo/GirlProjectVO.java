package com.moxi.mogublog.xo.vo;

import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;

/**
 * ProjectVO
 *
 * @author Vergift
 * @since 2021-11-29
 */
@Data
public class GirlProjectVO extends BaseVO<GirlProjectVO> {

    /**
     * OrderBy排序字段（desc: 降序）
     */
    private String orderByDescColumn;

    /**
     * OrderBy排序字段（asc: 升序）
     */
    private String orderByAscColumn;

    /**
     * 项目名称
     */
    private String name;

    /**
     * KEY
     */
    private String proKey;

    /**
     * 来源:1.青青草；2.小黄鸭
     */
    private Integer source;

    /**
     * 是否会员:1.会员；0.普通用户
     */
    private Integer isVip;

    /**
     * 项目开关:1.开启；0.关闭
     */
    private Integer onOff;

    /**
     * 认证信息
     */
    private String authentication;
}
