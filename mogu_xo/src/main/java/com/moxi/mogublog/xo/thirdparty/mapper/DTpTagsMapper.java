package com.moxi.mogublog.xo.thirdparty.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.TpBooks;
import com.moxi.mogublog.commons.entity.TpTags;
import com.moxi.mougblog.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface DTpTagsMapper extends SuperMapper<TpTags> {


    @Select(" select id," +
            " convert(JSON_UNQUOTE(json_extract(name , '$.\"zh-CN\"')) using utf8) as name," +
            " type,suggest, queries, order_column, created_at, updated_at  " +
            " from tp_tags " +
            " ${ew.customSqlSegment}")
    IPage<TpTags> selectPageForUpdate(Page<TpTags> page, @Param(Constants.WRAPPER) QueryWrapper queryWrapper);




}
