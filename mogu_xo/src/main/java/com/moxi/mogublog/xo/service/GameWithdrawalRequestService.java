package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.GameWithdrawalRequest;
import com.moxi.mogublog.xo.vo.*;
import com.moxi.mougblog.base.service.SuperService;

import java.util.Map;

public interface GameWithdrawalRequestService extends SuperService<GameWithdrawalRequest> {

    IPage<GameWithdrawalRequest> getPageList(GameWithdrawalRequestVo gameWithdrawalRequestVo);

    String createOrder(GameWithdrawalVO gameWithdrawalVO);

    Map<String,Object> callbackOrder(CallbackWithdrawalVO callbackWithdrawalVO);

    IPage<GameWithdrawalRequest> queryWithdrawalList(GameWithdrawalRequestVo gameWithdrawalRequestVo);

    GameDepositTotalVo getWithdrawalTotal(GameWithdrawalRequestVo gameWithdrawalRequestVo);

    String editWithdrawalInfo(GameWithdrawalBackVo gameWithdrawalBackVo);
}
