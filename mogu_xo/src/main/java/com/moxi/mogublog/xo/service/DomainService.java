package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.Domain;
import com.moxi.mogublog.xo.vo.DomainVO;
import com.moxi.mougblog.base.service.SuperService;

import java.util.List;


public interface DomainService extends SuperService<Domain> {
    IPage<Domain> getPageList(DomainVO domainVO);
    List<String> getList(int domainType);
    String add(DomainVO domainVO);
    String edit(DomainVO domainVO);
}
