package com.moxi.mogublog.xo.vo;

import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;

/**
 * TpBookChaptersVO
 * @author Sid
 * @since 2022-05-31
 */
@Data
public class TpBookChaptersVO extends BaseVO<TpBookChaptersVO> {

    /**
     * 排序字段
     */
    private Integer sort;

    /**
     * OrderBy排序字段（desc: 降序）
     */
    private String orderByDescColumn;

    /**
     * OrderBy排序字段（asc: 升序）
     */
    private String orderByAscColumn;

    private String bookId;


}
