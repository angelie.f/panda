package com.moxi.mogublog.xo.vo;

import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;

import java.math.BigDecimal;

/**
 * GirlUserCostVO
 *
 * @author Vergift
 * @since 2022-01-05
 */
@Data
public class GirlUserCostVO extends BaseVO<GirlUserCostVO> {

    /**
     * OrderBy排序字段（desc: 降序）
     */
    private String orderByDescColumn;

    /**
     * OrderBy排序字段（asc: 升序）
     */
    private String orderByAscColumn;

    /**
     * 会员等级名称
     */
    private String levelName;

    /**
     * 会员等级ID
     */
    private String levelId;

    /**
     * 妹子项目uid
     */
    private Integer projectUid;

    /**
     * 平台费用
     */
    private BigDecimal cost;

}
