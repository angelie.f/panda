package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.BooksProject;
import com.moxi.mogublog.xo.vo.BooksProjectVO;
import com.moxi.mougblog.base.service.SuperService;

public interface BooksProjectService extends SuperService<BooksProject> {

    /**
     * 获取漫画项目
     *
     * @param booksProjectVO
     * @return
     */
    IPage<BooksProject> getPageList(BooksProjectVO booksProjectVO);
    /**
     * 编辑漫画项目
     *
     * @param booksProjectVO
     */
    String edit(BooksProjectVO booksProjectVO);

    String add(BooksProjectVO booksProjectVO);

    IPage<BooksProject> sources(BooksProjectVO booksProjectVO);

}

