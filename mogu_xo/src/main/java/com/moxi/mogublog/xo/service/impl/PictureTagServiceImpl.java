package com.moxi.mogublog.xo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.*;
import com.moxi.mogublog.utils.RedisUtil;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.global.RedisConf;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.mapper.PictureTagMapper;
import com.moxi.mogublog.xo.service.*;
import com.moxi.mogublog.xo.vo.CustomersVO;
import com.moxi.mogublog.xo.vo.PictureSortVO;
import com.moxi.mogublog.xo.vo.PictureTagVO;
import com.moxi.mogublog.xo.vo.ProjectVO;
import com.moxi.mougblog.base.enums.EPublish;
import com.moxi.mougblog.base.enums.EStatus;
import com.moxi.mougblog.base.global.Constants;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 图片标签表 服务实现类
 *
 * @author Vergift
 * @date 2021-11-22
 */
@Service
public class PictureTagServiceImpl extends SuperServiceImpl<PictureTagMapper, PictureTag> implements PictureTagService {

    @Autowired
    private ProjectService projectService;
    @Autowired
    private PictureSortService pictureSortService;
    @Autowired
    private PictureTagService pictureTagService;
    @Autowired
    private PictureAtlasService pictureAtlasService;
    @Autowired
    private PictureSortItemService pictureSortItemService;
    @Autowired
    private RedisUtil redisUtil;

    @Override
    public IPage<PictureTag> getPageList(PictureTagVO tictureTagVO) {
        QueryWrapper<PictureTag> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(tictureTagVO.getKeyword()) && !StringUtils.isEmpty(tictureTagVO.getKeyword())) {
            queryWrapper.like(SQLConf.CONTENT, tictureTagVO.getKeyword().trim());
        }

        Page<PictureTag> page = new Page<>();
        page.setCurrent(tictureTagVO.getCurrentPage());
        page.setSize(tictureTagVO.getPageSize());
        if(StringUtils.isNotEmpty(tictureTagVO.getOrderByAscColumn())) {
            // 将驼峰转换成下划线
            String column = StringUtils.underLine(new StringBuffer(tictureTagVO.getOrderByAscColumn())).toString();
            queryWrapper.orderByAsc(column);
        } else if(StringUtils.isNotEmpty(tictureTagVO.getOrderByDescColumn())) {
            // 将驼峰转换成下划线
            String column = StringUtils.underLine(new StringBuffer(tictureTagVO.getOrderByDescColumn())).toString();
            queryWrapper.orderByDesc(column);
        } else {
            queryWrapper.orderByDesc(SQLConf.SORT);
        }
        IPage<PictureTag> pageList = pictureTagService.page(page, queryWrapper);
        return pageList;
    }

    @Override
    public List<PictureTag> getList() {
        QueryWrapper<PictureTag> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SysConf.STATUS, EStatus.ENABLE);
        queryWrapper.orderByDesc(SQLConf.SORT);
        List<PictureTag> tagList = pictureTagService.list(queryWrapper);
        return tagList;
    }

    @Override
    public String addTag(PictureTagVO tictureTagVO) {
        QueryWrapper<PictureTag> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SQLConf.CONTENT, tictureTagVO.getContent());
        queryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
        PictureTag tempTag = pictureTagService.getOne(queryWrapper);
        if (tempTag != null) {
            return ResultUtil.errorWithMessage(MessageConf.ENTITY_EXIST);
        }
        PictureTag pictureTag = new PictureTag();
        pictureTag.setContent(tictureTagVO.getContent());
        pictureTag.setClickCount(0);
        pictureTag.setStatus(EStatus.ENABLE);
        pictureTag.setSort(tictureTagVO.getSort());
        pictureTag.setIntroduce(tictureTagVO.getIntroduce());
        pictureTag.insert();
        // 删除Redis中的BLOG_TAG
        deleteRedisBlogTagList();
        return ResultUtil.successWithMessage(MessageConf.INSERT_SUCCESS);
    }

    @Override
    public String editTag(PictureTagVO tictureTagVO) {
        PictureTag pictureTag = pictureTagService.getById(tictureTagVO.getUid());

        if (pictureTag != null && !pictureTag.getContent().equals(tictureTagVO.getContent())) {
            QueryWrapper<PictureTag> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq(SQLConf.CONTENT, tictureTagVO.getContent());
            queryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
            PictureTag tempTag = pictureTagService.getOne(queryWrapper);
            if (tempTag != null) {
                return ResultUtil.errorWithMessage(MessageConf.ENTITY_EXIST);
            }
        }

        pictureTag.setContent(tictureTagVO.getContent());
        pictureTag.setStatus(EStatus.ENABLE);
        pictureTag.setSort(tictureTagVO.getSort());
        pictureTag.setUpdateTime(new Date());
        pictureTag.setIntroduce(tictureTagVO.getIntroduce());
        pictureTag.updateById();
        // 删除和标签相关的博客缓存
        pictureAtlasService.deleteRedisByPictureTag();
        // 删除Redis中的BLOG_TAG
        deleteRedisBlogTagList();
        return ResultUtil.successWithMessage(MessageConf.UPDATE_SUCCESS);
    }

    @Override
    public String deleteBatchTag(List<PictureTagVO> pictureTagVOList) {
        if (pictureTagVOList.size() <= 0) {
            return ResultUtil.errorWithMessage(MessageConf.PARAM_INCORRECT);
        }
        List<String> uids = new ArrayList<>();
        pictureTagVOList.forEach(item -> {
            uids.add(item.getUid());
        });

        // 判断要删除的标签，是否有图集
        QueryWrapper<PictureAtlas> blogQueryWrapper = new QueryWrapper<>();
        blogQueryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
        blogQueryWrapper.in(SQLConf.TAG_UID, uids);
        Integer blogCount = pictureAtlasService.count(blogQueryWrapper);
        if (blogCount > 0) {
            return ResultUtil.errorWithMessage(MessageConf.PICTURE_UNDER_THIS_TAG);
        }

        Collection<PictureTag> tagList = pictureTagService.listByIds(uids);

        tagList.forEach(item -> {
            item.setUpdateTime(new Date());
            item.setStatus(EStatus.DISABLED);
        });

        Boolean save = pictureTagService.updateBatchById(tagList);
        // 删除和标签相关的博客缓存
        pictureAtlasService.deleteRedisByPictureTag();
        // 删除Redis中的BLOG_TAG
        deleteRedisBlogTagList();
        if (save) {
            return ResultUtil.successWithMessage(MessageConf.DELETE_SUCCESS);
        } else {
            return ResultUtil.errorWithMessage(MessageConf.DELETE_FAIL);
        }
    }

    @Override
    public String stickTag(PictureTagVO tictureTagVO) {
        PictureTag pictureTag = pictureTagService.getById(tictureTagVO.getUid());

        //查找出最大的那一个
        QueryWrapper<PictureTag> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc(SQLConf.SORT);
        Page<PictureTag> page = new Page<>();
        page.setCurrent(0);
        page.setSize(1);
        IPage<PictureTag> pageList = pictureTagService.page(page, queryWrapper);
        List<PictureTag> list = pageList.getRecords();
        PictureTag maxTag = list.get(0);

        if (StringUtils.isEmpty(maxTag.getUid())) {
            return ResultUtil.errorWithMessage(MessageConf.PARAM_INCORRECT);
        }
        if (maxTag.getUid().equals(pictureTag.getUid())) {
            return ResultUtil.errorWithMessage(MessageConf.THIS_TAG_IS_TOP);
        }

        Integer sortCount = maxTag.getSort() + 1;

        pictureTag.setSort(sortCount);
        pictureTag.setUpdateTime(new Date());
        pictureTag.updateById();
        // 删除Redis中的BLOG_TAG
        deleteRedisBlogTagList();
        return ResultUtil.successWithMessage(MessageConf.OPERATION_SUCCESS);
    }

    @Override
    public String tagSortByClickCount() {
        QueryWrapper<PictureTag> queryWrapper = new QueryWrapper();
        queryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
        // 按点击从高到低排序
        queryWrapper.orderByDesc(SQLConf.CLICK_COUNT);
        List<PictureTag> tagList = pictureTagService.list(queryWrapper);
        // 设置初始化最大的sort值
        Integer maxSort = tagList.size();
        for (PictureTag item : tagList) {
            item.setSort(item.getClickCount());
            item.setCreateTime(new Date());
        }
        pictureTagService.updateBatchById(tagList);
        // 删除Redis中的BLOG_TAG
        deleteRedisBlogTagList();
        return ResultUtil.successWithMessage(MessageConf.OPERATION_SUCCESS);
    }

    @Override
    public String tagSortByCite() {
        // 定义Map   key：tagUid,  value: 引用量
        Map<String, Integer> map = new HashMap<>();
        QueryWrapper<PictureTag> tagQueryWrapper = new QueryWrapper<>();
        tagQueryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
        List<PictureTag> tagList = pictureTagService.list(tagQueryWrapper);
        // 初始化所有标签的引用量
        tagList.forEach(item -> {
            map.put(item.getUid(), 0);
        });

        QueryWrapper<PictureAtlas> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
        queryWrapper.eq(SQLConf.IS_PUBLISH, EPublish.PUBLISH);
        // 过滤content字段
        queryWrapper.select(PictureAtlas.class, i -> !i.getProperty().equals(SQLConf.CONTENT));
        List<PictureAtlas> pictureAtlasList = pictureAtlasService.list(queryWrapper);

        pictureAtlasList.forEach(item -> {
            String tagUids = item.getTagUid();
            List<String> tagUidList = StringUtils.changeStringToString(tagUids, SysConf.FILE_SEGMENTATION);
            for (String tagUid : tagUidList) {
                if (map.get(tagUid) != null) {
                    Integer count = map.get(tagUid) + 1;
                    map.put(tagUid, count);
                } else {
                    map.put(tagUid, 0);
                }
            }
        });

        tagList.forEach(item -> {
            item.setSort(map.get(item.getUid()));
            item.setUpdateTime(new Date());
        });
        pictureTagService.updateBatchById(tagList);
        // 删除Redis中的BLOG_TAG
        deleteRedisBlogTagList();
        return ResultUtil.successWithMessage(MessageConf.OPERATION_SUCCESS);
    }

    @Override
    public List<PictureTag> getHotTag(Integer hotTagCount) {
        QueryWrapper<PictureTag> queryWrapper = new QueryWrapper<>();
        Page<PictureTag> page = new Page<>();
        page.setCurrent(1);
        page.setSize(hotTagCount);
        queryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
        queryWrapper.orderByDesc(SQLConf.SORT);
        queryWrapper.orderByDesc(SQLConf.CLICK_COUNT);
        IPage<PictureTag> pageList = pictureTagService.page(page, queryWrapper);
        return pageList.getRecords();
    }

    @Override
    public PictureTag getTopTag() {
        QueryWrapper<PictureTag> tagQueryWrapper = new QueryWrapper<>();
        tagQueryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
        tagQueryWrapper.last(SysConf.LIMIT_ONE);
        tagQueryWrapper.orderByDesc(SQLConf.SORT);
        PictureTag pictureTag = pictureTagService.getOne(tagQueryWrapper);
        return pictureTag;
    }

    @Override
    public void deleteRedisByPictureTag() {
        // 删除Redis中博客分类下的博客数量
        redisUtil.delete(RedisConf.DASHBOARD + Constants.SYMBOL_COLON + RedisConf.PICTURE_COUNT_BY_TAG);
        // 删除博客相关缓存
        deleteRedisByPictureAtlas();
    }

    @Override
    public void deleteRedisByPictureAtlas() {
        // 删除博客相关缓存
        redisUtil.delete(RedisConf.HOT_PICTURE_SORT);
        redisUtil.delete(RedisConf.HOT_PICTURE_SORT);
    }

    /**
     * 删除Redis中的友链列表
     */
    private void deleteRedisBlogTagList() {
        // 删除Redis中的PICTURE_LINK
        Set<String> keys = redisUtil.keys(RedisConf.PICTURE_TAG + Constants.SYMBOL_COLON + "*");
        redisUtil.delete(keys);
    }

    @Override
    public IPage<PictureTag> getCustomersTagList(CustomersVO customersVO) {
        LinkedHashSet<String> tagUids = new LinkedHashSet<>();
        QueryWrapper<PictureSort> queryWrapper = new QueryWrapper<>();
        /*ProjectVO projectVO =new ProjectVO();
        projectVO.setSource(customersVO.getSource());
        List<Project> projectList = projectService.getList(projectVO);

        queryWrapper.eq(SQLConf.PROJECT_UID, projectList.get(0).getUid());
        List<PictureSort> pictureSortList = pictureSortService.list(queryWrapper);

        for (PictureSort pictureSort:pictureSortList) {
            if (StringUtils.isNotEmpty(pictureSort.getTagUid())){
                tagUids.addAll(Arrays.asList(pictureSort.getTagUid().split(Constants.SYMBOL_COMMA)));
            }
        }*/
        queryWrapper.eq(SQLConf.UID, customersVO.getSortUid());
        PictureSort pictureSort = pictureSortService.getOne(queryWrapper);
        if (StringUtils.isNotEmpty(pictureSort.getTagUid())){
            tagUids.addAll(Arrays.asList(pictureSort.getTagUid().split(Constants.SYMBOL_COMMA)));
        }
        Page<PictureTag> page = new Page<>();
        if (customersVO.getCurrentPage() == null){
            page.setCurrent(1);
        }else {
            page.setCurrent(customersVO.getCurrentPage());
        }
        if (customersVO.getPageSize() == null){
            page.setSize(10000);
        }else {
            page.setSize(customersVO.getPageSize());
        }

        QueryWrapper<PictureTag> tagQueryWrapper = new QueryWrapper<>();
        tagQueryWrapper.in(SQLConf.UID, tagUids);
         queryWrapper.orderByDesc(SQLConf.UPDATE_TIME);

        IPage<PictureTag> pageList = pictureTagService.page(page, tagQueryWrapper);
        List<PictureTag> list = pageList.getRecords();
        Integer sumCount = 0;
        Integer count = 0;
        for (PictureTag pictureTag:list) {

            List<Integer> atlasUids = upAtlasCount(customersVO.getSortUid());
            if(atlasUids.size() > 0) {
                QueryWrapper<PictureAtlas> atlasQueryWrapper = new QueryWrapper<>();
                atlasQueryWrapper.in(SQLConf.UID, atlasUids);
                atlasQueryWrapper.and(wrapper ->wrapper.eq(SQLConf.TAG_UID, pictureTag.getUid())
                            .or().likeRight(SQLConf.TAG_UID, pictureTag.getUid() + SysConf.FILE_SEGMENTATION)
                            .or().like(SQLConf.TAG_UID, SysConf.FILE_SEGMENTATION + pictureTag.getUid() + SysConf.FILE_SEGMENTATION)
                            .or().likeLeft(SQLConf.TAG_UID, SysConf.FILE_SEGMENTATION + pictureTag.getUid()));
                count = pictureAtlasService.count(atlasQueryWrapper);
                pictureTag.setCount(count);
                sumCount += count;
            }else {
                pictureTag.setCount(0);
            }
        }
        PictureTag sumTag = new PictureTag ();
        sumTag.setUid(customersVO.getSortUid());
        sumTag.setContent("全部");
        sumTag.setCount(sumCount);
        list.add(0,sumTag);
        pageList.setRecords(list);
        return pageList;
    }

    public List<Integer> upAtlasCount(String sortUid){
        List<Integer> atlasUids = new ArrayList<>();

        QueryWrapper<PictureSortItem> sortItemQueryWrapper = new QueryWrapper<>();

        //根据分类获取图集
        sortItemQueryWrapper.eq(SQLConf.SORT_UID, sortUid);
        sortItemQueryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);

        List<PictureSortItem> sortItemList =  pictureSortItemService.list(sortItemQueryWrapper);

        for (PictureSortItem pictureSortItem:sortItemList) {
            atlasUids.add(pictureSortItem.getAtlasUid());
        }
        return atlasUids;
    }

}
