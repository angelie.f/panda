package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.UsePower;
import com.moxi.mogublog.xo.vo.UsePowerVO;
import com.moxi.mougblog.base.service.SuperService;

import java.util.List;

/**
 * 使用权限 服务类
 *
 * @author Vergift
 * @date 2021-12-04
 */
public interface UsePowerService extends SuperService<UsePower> {

    /**
     * 获取使用权限消息
     *
     * @return
     */
    public IPage<UsePower> getPageList(UsePowerVO usePowerVO);

    /**
     * 获取使用权限消息
     *
     * @return
     */
    public UsePower getUsePower(UsePowerVO usePowerVO);

    /**
     * 编辑用权限消息
     *
     * @param usePowerVO
     */
    public String editUsePower(UsePowerVO usePowerVO);

    /**
     * 添加用权限消息
     *
     * @param usePower
     */
    public String addUsePower(UsePower usePower);


}
