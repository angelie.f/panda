package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.GirlCity;
import com.moxi.mogublog.xo.vo.GirlCityVO;
import com.moxi.mougblog.base.service.SuperService;

/**
 * 妹子城市表 服务类
 *
 * @author Vergift
 * @date 2021-12-29
 */
public interface GirlCityService extends SuperService<GirlCity> {
    /**
     * 获取妹子城市列表
     *
     * @param girlCityVO
     * @return
     */
    IPage<GirlCity> getPageList(GirlCityVO girlCityVO);

    /**
     * 新增妹子城市
     *
     * @param girlCityVO
     */
    String addCity(GirlCityVO girlCityVO);

    /**
     * 编辑妹子城市
     *
     * @param girlCityVO
     */
    String editCity(GirlCityVO girlCityVO);

}
