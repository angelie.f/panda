package com.moxi.mogublog.xo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.GameType;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.mapper.GameTypeMapper;
import com.moxi.mogublog.xo.service.GameTypeService;
import com.moxi.mogublog.xo.vo.GameTypeVO;
import com.moxi.mougblog.base.enums.EStatus;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.List;


@Service
public class GameTypeServiceImpl extends SuperServiceImpl<GameTypeMapper, GameType> implements GameTypeService {


    @Override
    public IPage<GameType> getPageList(GameTypeVO gameTypeVO) {
        QueryWrapper<GameType> queryWrapper = new QueryWrapper<>();

        Page<GameType> page = new Page<>();
        page.setCurrent(gameTypeVO.getCurrentPage());
        page.setSize(gameTypeVO.getPageSize());
        queryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
        if(StringUtils.isNotEmpty(gameTypeVO.getOrderByAscColumn())) {
            // 将驼峰转换成下划线
            String column = StringUtils.underLine(new StringBuffer(gameTypeVO.getOrderByAscColumn())).toString();
            queryWrapper.orderByAsc(column);
        } else if(StringUtils.isNotEmpty(gameTypeVO.getOrderByDescColumn())) {
            // 将驼峰转换成下划线
            String column = StringUtils.underLine(new StringBuffer(gameTypeVO.getOrderByDescColumn())).toString();
            queryWrapper.orderByDesc(column);
        }
        IPage<GameType> pageList = page(page, queryWrapper);
        return pageList;
    }

    @Override
    public List<GameType> getList(GameTypeVO gameTypeVO) {
        QueryWrapper<GameType> queryWrapper = new QueryWrapper<>();
        List<GameType> typeList = list(queryWrapper);
        return typeList;
    }

    @Override
    public String add(GameTypeVO gameTypeVO) {
        QueryWrapper<GameType> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SQLConf.NAME, gameTypeVO.getName());
        if (getOne(queryWrapper) != null) {
            return ResultUtil.errorWithMessage(MessageConf.ENTITY_EXIST);
        }
        GameType gameType = new GameType();
        gameType.setName(gameTypeVO.getName());
        gameType.insert();
        return ResultUtil.successWithMessage(MessageConf.INSERT_SUCCESS);
    }

    @Override
    public String edit(GameTypeVO gameTypeVO) {
        GameType gameType = getById(gameTypeVO.getUid());

        if (gameType != null && !gameType.getName().equals(gameTypeVO.getName())) {
            QueryWrapper<GameType> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq(SQLConf.NAME, gameTypeVO.getName());
            queryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
            GameType tempG = getOne(queryWrapper);
            if (tempG != null) {
                return ResultUtil.errorWithMessage(MessageConf.ENTITY_EXIST);
            }
        }

        gameType.setName(gameTypeVO.getName());
        gameType.setStatus(EStatus.ENABLE);
        gameType.setUpdateTime(new Date());
        gameType.updateById();
        return ResultUtil.successWithMessage(MessageConf.UPDATE_SUCCESS);
    }
    @Override
    public IPage<GameType> gameTypes(GameTypeVO gameTypeVO) {
        Page<GameType> page = new Page<>();
        page.setCurrent(gameTypeVO.getCurrentPage());
        page.setSize(gameTypeVO.getPageSize());
        QueryWrapper<GameType> queryWrapper = new QueryWrapper<>();
        queryWrapper.select(SQLConf.UID,SQLConf.NAME);
        queryWrapper.orderByAsc(SQLConf.UID);
        IPage<GameType> iPage = baseMapper.selectPage(page, queryWrapper);
        return iPage;
    }
}
