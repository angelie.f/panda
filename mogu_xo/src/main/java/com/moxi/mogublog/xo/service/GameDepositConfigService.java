package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.GameDepositConfig;
import com.moxi.mogublog.xo.vo.GameDepositConfigVo;
import com.moxi.mogublog.xo.vo.GameDepositVO;
import com.moxi.mougblog.base.service.SuperService;

import java.util.List;

public interface GameDepositConfigService extends SuperService<GameDepositConfig> {
    List<GameDepositConfig>getDepositConfigList(GameDepositVO gameDepositVO);

    /**
     * 添加
     *
     * @param gameDepositConfigVo
     * @return
     */
    String addDeposit(GameDepositConfigVo gameDepositConfigVo);

    /**
     * 编辑
     *
     * @param gameDepositConfigVo
     * @return
     */
    String editDeposit(GameDepositConfigVo gameDepositConfigVo);


    IPage<GameDepositConfig> getPageList(GameDepositConfigVo gameDepositConfigVo);

    String deleteDepConfig(GameDepositConfigVo gameDepositConfigVo);
}
