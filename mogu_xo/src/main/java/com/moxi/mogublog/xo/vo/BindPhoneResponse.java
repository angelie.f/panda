package com.moxi.mogublog.xo.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class BindPhoneResponse {

    private Integer isBindPhone;

    private String version;

}
