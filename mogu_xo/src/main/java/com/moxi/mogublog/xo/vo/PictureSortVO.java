package com.moxi.mogublog.xo.vo;

import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;
import lombok.ToString;

/**
 * 分类实体查询参数类
 *
 * @author Vergift
 * @date 2021年12月02日17:07:38
 */
@ToString
@Data
public class PictureSortVO extends BaseVO<PictureSortVO> {

    /**
     * 图集创建时间
     */
    private String createTime;


    /**
     * 所属项目uid
     */
    private String projectUid;

}
