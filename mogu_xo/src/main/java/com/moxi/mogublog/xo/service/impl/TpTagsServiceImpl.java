package com.moxi.mogublog.xo.service.impl;

import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import org.springframework.stereotype.Service;
import com.moxi.mogublog.xo.mapper.TpTagsMapper;
import com.moxi.mogublog.xo.service.TpTagsService;
import com.moxi.mogublog.commons.entity.TpTags;

@Service
public class TpTagsServiceImpl extends SuperServiceImpl<TpTagsMapper, TpTags> implements TpTagsService {

}

