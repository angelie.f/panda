package com.moxi.mogublog.xo.vo;

import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;

import javax.validation.Valid;

/**
 * CallbackOrderVO
 *
 * @author Vergift
 * @since 2022-01-07
 */
@Data
public class CallbackOrderRequest extends BaseVO<CallbackOrderVO> {

    private String gameId;

    @Valid
    private CallbackOrderVO param;
}
