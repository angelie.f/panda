package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.TpTaggables;
import com.moxi.mogublog.commons.entity.TpTopicItems;
import com.moxi.mougblog.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface TpTopicItemsMapper extends SuperMapper<TpTopicItems> {
    @Insert("<script>" +
            " INSERT INTO tp_topic_items " +
            " (id, topic_id, topic_causer, item_id, sort_num, created_at, updated_at)" +
            " VALUES " +
            " <foreach collection='list' item='topicItems' separator=','>" +
            " (" +
            "  #{topicItems.id},#{topicItems.topicId},#{topicItems.topicCauser},#{topicItems.itemId}," +
            "  #{topicItems.sortNum},#{topicItems.createdAt},#{topicItems.updatedAt}" +
            " )" +
            "</foreach> " +
            " ON DUPLICATE KEY UPDATE " +
            " topic_id=VALUES(topic_id),topic_causer=VALUES(topic_causer),item_id=VALUES(item_id), " +
            " sort_num=VALUES(sort_num),created_at=VALUES(created_at),updated_at=VALUES(updated_at) " +
            "</script>"
    )
    int insertOrUpdateBatch(@Param("list") List<TpTopicItems> list);
}
