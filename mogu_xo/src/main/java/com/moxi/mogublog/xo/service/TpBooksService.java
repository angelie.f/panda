package com.moxi.mogublog.xo.service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.xo.vo.LocalBooksVO;
import com.moxi.mogublog.xo.vo.TpBooksVO;
import com.moxi.mougblog.base.service.SuperService;
import com.moxi.mogublog.commons.entity.TpBooks;

public interface TpBooksService extends SuperService<TpBooks> {

    String getBooksList(LocalBooksVO localBooksVO);

    String getRelatedBooksList(LocalBooksVO localBooksVO);

    /**
     * 获取漫画列表
     *
     * @param tpBooksVO
     * @return
     */
    IPage<TpBooks> getPageList(TpBooksVO tpBooksVO);
}

