package com.moxi.mogublog.xo.service.impl;

import com.alibaba.csp.sentinel.util.StringUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.ReadingLog;
import com.moxi.mogublog.commons.entity.UserDailyRead;
import com.moxi.mogublog.utils.DateUtils;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.mapper.*;
import com.moxi.mogublog.xo.service.ReadingLogService;
import com.moxi.mogublog.xo.vo.LocalBooksVO;
import com.moxi.mogublog.xo.vo.ReadingLogVO;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class ReadingLogServiceImpl extends SuperServiceImpl<ReadingLogMapper, ReadingLog> implements ReadingLogService {

    @Autowired
    private CustomersMapper customersMapper;

    @Autowired
    private BooksUserCostMapper booksUserCostMapper;

    @Autowired
    private UserDailyReadMapper userDailyReadMapper;

    @Autowired
    private BooksSettingMapper booksSettingMapper;

    @Value("${books.domain}")
    private String imageDomain;

    @Override
    public Map getReadingLogInfo(LocalBooksVO localBooksVO) {
        QueryWrapper<ReadingLog> queryWrapper = new QueryWrapper<>();
        if (StringUtil.isNotEmpty(localBooksVO.getSource())) {
            queryWrapper.eq("source", localBooksVO.getSource());
        }
        if (StringUtil.isNotEmpty(localBooksVO.getCustomerId())) {
            queryWrapper.eq("customer_id", localBooksVO.getCustomerId());
        }
        if (localBooksVO.getBookId() != null) {
            queryWrapper.eq("book_id", localBooksVO.getBookId());
        }
        queryWrapper.orderByDesc("rlog.update_time");
        List<ReadingLog> list = baseMapper.getList(queryWrapper);
        for (ReadingLog log : list) {
            log.setImageUrl(convertImageUrl(log.getImageUrl()));
        }
        Map map = new HashMap();
        map.put("list", list);
        int readAbleCount = getReadAbleCount(localBooksVO.getCustomerId(), localBooksVO.getSource());
        String today = DateUtils.formateDate(new Date(), "yyyy-MM-dd");
        int readedCount = userDailyReadMapper.queryReadedCount(today, localBooksVO.getCustomerId());
        map.put("readAbleCount", readAbleCount);
        map.put("readedCount", readedCount);
        map.put("remainingAble", readAbleCount > readedCount ? readAbleCount - readedCount : 0);
        return map;
    }

    private int getReadAbleCount(String customerId, String source) {
        Integer vipLevel = customersMapper.queryVipLevel(customerId);
        Integer enableEpisode = booksUserCostMapper.queryEnableEpisode(source, vipLevel);
        return enableEpisode == null ? 0 : enableEpisode;
    }

    private String convertImageUrl(String url) {
        if (org.apache.commons.lang.StringUtils.isNotEmpty(url)) {
            url = url.replaceAll("\\\\", "");
            //UAT
            url = imageDomain.concat(url);
        }
        return url;
    }

    public ReadingLog getReadingLog(LocalBooksVO localBooksVO) {
        QueryWrapper<ReadingLog> queryWrapper = new QueryWrapper<>();
        if (StringUtil.isNotEmpty(localBooksVO.getSource())) {
            queryWrapper.eq("source", localBooksVO.getSource());
        }
        if (StringUtil.isNotEmpty(localBooksVO.getCustomerId())) {
            queryWrapper.eq("customer_id", localBooksVO.getCustomerId());
        }
        if (localBooksVO.getBookId() != null) {
            queryWrapper.eq("book_id", localBooksVO.getBookId());
        }
        List<ReadingLog> list = this.baseMapper.getList(queryWrapper);
        if(list!=null&&!list.isEmpty()){
            return list.get(0);
        }
        return null;
    }

    public boolean updateReadingLog(LocalBooksVO localBooksVO) {
        QueryWrapper<ReadingLog> queryWrapper = new QueryWrapper<>();
        if (StringUtil.isNotEmpty(localBooksVO.getSource())) {
            queryWrapper.eq("source", localBooksVO.getSource());
        }
        if (StringUtil.isNotEmpty(localBooksVO.getCustomerId())) {
            queryWrapper.eq("customer_id", localBooksVO.getCustomerId());
        }
        if (localBooksVO.getBookId() != null) {
            queryWrapper.eq("book_id", localBooksVO.getBookId());
        }
        ReadingLog log = getOne(queryWrapper);
        ReadingLog newLog = new ReadingLog();
        newLog.setSource(Integer.parseInt(localBooksVO.getSource()));
        newLog.setBookId(localBooksVO.getBookId());
        newLog.setCustomerId(localBooksVO.getCustomerId());
        newLog.setEpisode(localBooksVO.getEpisode());
        if (log != null) {
            newLog.setUid(log.getUid());
        }
        return saveOrUpdate(newLog);
    }


    @Override
    public IPage<ReadingLog> getPageList(ReadingLogVO readingLogVO) {
        QueryWrapper<ReadingLog> queryWrapper = new QueryWrapper<>();
        if (StringUtil.isNotEmpty(readingLogVO.getSource())) {
            queryWrapper.eq("rlog.source", readingLogVO.getSource());
        }
        if (StringUtil.isNotEmpty(readingLogVO.getCustomerId())) {
            queryWrapper.eq("cust.open_id", readingLogVO.getCustomerId());
        }
        if (StringUtil.isNotEmpty(readingLogVO.getBeginCreateTime())) {
            queryWrapper.ge("rlog.create_time", readingLogVO.getBeginCreateTime().trim());
        }
        if (StringUtils.isNotEmpty(readingLogVO.getEndCreateTime())) {
            queryWrapper.le("rlog.create_time", readingLogVO.getEndCreateTime().trim());
        }
        if (StringUtil.isNotEmpty(readingLogVO.getBookName())) {
            queryWrapper.like("tpb.title", readingLogVO.getBookName().trim());
        }

        Page<ReadingLog> page = new Page<>();
        page.setCurrent(readingLogVO.getCurrentPage());
        page.setSize(readingLogVO.getPageSize());
        if (StringUtils.isNotEmpty(readingLogVO.getOrderByAscColumn())) {
            // 将驼峰转换成下划线
            String column = StringUtils.underLine(new StringBuffer(readingLogVO.getOrderByAscColumn())).toString();
            queryWrapper.orderByAsc(column);
        } else if (StringUtils.isNotEmpty(readingLogVO.getOrderByDescColumn())) {
            // 将驼峰转换成下划线
            String column = StringUtils.underLine(new StringBuffer(readingLogVO.getOrderByDescColumn())).toString();
            queryWrapper.orderByDesc(column);
        } else {
            queryWrapper.orderByDesc("rlog.create_time");
        }
        return getBaseMapper().getPage(page, queryWrapper);
    }

    @Override
    public String logDailyAvailable(LocalBooksVO localBooksVO) {
        boolean success = false;
        String today = DateUtils.formateDate(new Date(), "yyyy-MM-dd");
        int count = userDailyReadMapper.queryRepeat(today, localBooksVO.getCustomerId(), localBooksVO.getBookId(), localBooksVO.getEpisode());
        if (count == 0) {
            UserDailyRead dailyRead = new UserDailyRead();
            dailyRead.setCustomerId(localBooksVO.getCustomerId());
            dailyRead.setBookId(localBooksVO.getBookId());
            dailyRead.setEpisode(localBooksVO.getEpisode());
            dailyRead.setReadDate(today);
            success = userDailyReadMapper.insert(dailyRead) > 0;
        }
        Map map = new HashMap();
        int readAbleCount = getReadAbleCount(localBooksVO.getCustomerId(), localBooksVO.getSource());
        int readedCount = userDailyReadMapper.queryReadedCount(today, localBooksVO.getCustomerId());
        map.put("readAbleCount", readAbleCount);
        map.put("readedCount", readedCount);
        map.put("remainingAble", readAbleCount > readedCount ? readAbleCount - readedCount : 0);
        if (success)
            return ResultUtil.result(SysConf.SUCCESS, map);
        else
            return ResultUtil.result(SysConf.ERROR, map);
    }
    @Override
    public String getDailyAvailable(LocalBooksVO localBooksVO) {
        String today = DateUtils.formateDate(new Date(), "yyyy-MM-dd");
        Map map = new HashMap();
        int readAbleCount = getReadAbleCount(localBooksVO.getCustomerId(), localBooksVO.getSource());
        int readedCount = userDailyReadMapper.queryReadedCount(today, localBooksVO.getCustomerId());
        Integer perFee = booksSettingMapper.queryFee("perFee");
        map.put("readAbleCount", readAbleCount);
        map.put("readedCount", readedCount);
        map.put("remainingAble", readAbleCount > readedCount ? readAbleCount - readedCount : 0);
        map.put("perFee", perFee);
        return ResultUtil.result(SysConf.SUCCESS, map);
    }

}