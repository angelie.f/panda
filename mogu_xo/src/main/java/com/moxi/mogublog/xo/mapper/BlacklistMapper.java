package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.Blacklist;
import com.moxi.mougblog.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;


public interface BlacklistMapper extends SuperMapper<Blacklist> {

    @Select(" select count(1) from t_blacklist where status=1 and is_black=1 and vali_uid =#{valiUid} ")
    int countBlack(@Param("valiUid") String valiUid);

    @Select(" select count(1) from t_blacklist where status=1 and is_black=1 and vali_uid in (select uid from t_customers  where customer_id =#{customerId}) ")
    int countBlackByCustomerId(@Param("customerId") String customerId);

}
