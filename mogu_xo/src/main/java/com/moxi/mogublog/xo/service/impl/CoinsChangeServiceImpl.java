package com.moxi.mogublog.xo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.*;
import com.moxi.mogublog.utils.*;
import com.moxi.mogublog.xo.global.RedisConf;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.mapper.*;
import com.moxi.mogublog.xo.service.*;
import com.moxi.mogublog.xo.vo.CallbackWithdrawalVO;
import com.moxi.mogublog.xo.vo.GameAddCoinsVo;
import com.moxi.mogublog.xo.vo.GameConfigVO;
import com.moxi.mogublog.xo.vo.WebVisitVO;
import com.moxi.mougblog.base.enums.EBehavior;
import com.moxi.mougblog.base.enums.EStatus;
import com.moxi.mougblog.base.global.BaseSysConf;
import com.moxi.mougblog.base.global.Constants;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * 用户访问记录表 服务实现类
 *
 * @author 陌溪
 * @date 2018-09-08
 */
@Slf4j
@Service
public class CoinsChangeServiceImpl  implements CoinsChangeService {

    @Autowired
    private GameWithdrawalRequestMapper gameWithdrawalRequestMapper;

    @Autowired
    private GameCustomersBalanceMapper gameCustomersBalanceMapper;

    @Autowired
    private GameCreditLogsService gameCreditLogsService;

    @Autowired
    private CustomersMapper customersMapper;

    @Autowired
    private GameDepositRequestMapper gameDepositRequestMapper;

    @Autowired
    private GameAddCoinsMapper gameAddCoinsMapper;

    @Autowired
    private GameCreditLogsMapper gameCreditLogsMapper;



    @Transactional(rollbackFor = { Exception.class })
    @Override
    public void withdrawalCallbackOrderSuccess(GameWithdrawalRequest gameWithdrawalRequest,CallbackWithdrawalVO callbackWithdrawalVO) {
        log.info("withdrawalCallbackOrderSuccess事务");
        gameWithdrawalRequest.setFlag(2);
        gameWithdrawalRequest.setRemarks(callbackWithdrawalVO.getRemarks());
        gameWithdrawalRequestMapper.updateById(gameWithdrawalRequest);

        QueryWrapper<GameCustomersBalance> query = new QueryWrapper<>();
        query.eq("customer_id", gameWithdrawalRequest.getCustomerId());
        GameCustomersBalance gameCustomersBalance = gameCustomersBalanceMapper.selectOne(query);
        gameCustomersBalance.setFrozenCoins(gameCustomersBalance.getFrozenCoins().subtract(gameWithdrawalRequest.getAmount()));
        gameCustomersBalance.setMeVersion(null);
        gameCustomersBalanceMapper.updateById(gameCustomersBalance);

    }
    @Transactional(rollbackFor = { Exception.class })
    @Override
    public void withdrawalCallbackOrderFail(GameWithdrawalRequest gameWithdrawalRequest, CallbackWithdrawalVO callbackWithdrawalVO) {
        log.info("withdrawalCallbackOrderFail事务");
        gameWithdrawalRequest.setFlag(3);
        gameWithdrawalRequest.setRemarks(callbackWithdrawalVO.getRemarks());
        gameWithdrawalRequestMapper.updateById(gameWithdrawalRequest);

        QueryWrapper<GameCustomersBalance> query = new QueryWrapper<>();
        query.eq("customer_id", gameWithdrawalRequest.getCustomerId());
        GameCustomersBalance gameCustomersBalance = gameCustomersBalanceMapper.selectOne(query);
        gameCustomersBalance.setCoins(gameCustomersBalance.getCoins().add(gameWithdrawalRequest.getAmount()));
        gameCustomersBalance.setFrozenCoins(gameCustomersBalance.getFrozenCoins().subtract(gameWithdrawalRequest.getAmount()));
        gameCustomersBalance.setMeVersion(null);
        gameCustomersBalanceMapper.updateById(gameCustomersBalance);
    }
    @Transactional(rollbackFor = { Exception.class })
    @Override
    public void transferOut(String transferable, Customers customers, GameConfigVO gameConfigVO,String orderId) {
        log.info("transferOut事务");
        GameCreditLogs gameCreditLogs = new GameCreditLogs();
        gameCreditLogs.setCustomerId(customers.getCustomerId());
        gameCreditLogs.setType(4);
        gameCreditLogs.setSource(customers.getSource());
        gameCreditLogs.setCoinsBefore(customers.getCoins());
        gameCreditLogs.setCoinsAfter(customers.getCoins().add(new BigDecimal(transferable)));
        gameCreditLogs.setReferenceId(orderId);
        gameCreditLogsService.save(gameCreditLogs);

        QueryWrapper<GameCustomersBalance> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("customer_id", gameConfigVO.getCustomerId());
        GameCustomersBalance gameCustomersBalance = gameCustomersBalanceMapper.selectOne(queryWrapper);
        gameCustomersBalance.setCoins(gameCustomersBalance.getCoins().add(new BigDecimal(transferable)));
        gameCustomersBalance.setFrozenCoins(gameCustomersBalance.getFrozenCoins().subtract(new BigDecimal(transferable)));
        gameCustomersBalance.setMeVersion(null);
        gameCustomersBalanceMapper.updateById(gameCustomersBalance);
    }
    @Transactional(rollbackFor = { Exception.class })
    @Override
    public void depositCallbackOrder(GameDepositRequest gameDepositRequest ) {
        log.info("depositCallbackOrder事务");
        QueryWrapper<Customers> customersQueryWrapper = new QueryWrapper<>();
        customersQueryWrapper.eq("customer_id", gameDepositRequest.getCustomerId().trim());
        Customers customers = customersMapper.selectOne(customersQueryWrapper);
        //首存判断
        gameDepositRequest.setIsFirstDep(2);
        QueryWrapper<GameDepositRequest> depositRequestQueryWrapper = new QueryWrapper<>();
        depositRequestQueryWrapper.eq("customer_id",customers.getCustomerId());
        depositRequestQueryWrapper.in("flag", Arrays.asList("1","2"));
        depositRequestQueryWrapper.eq("is_first_dep",1);
        List<GameDepositRequest> gameDepositRequests = gameDepositRequestMapper.selectList(depositRequestQueryWrapper);
        if(CollectionUtils.isEmpty(gameDepositRequests)){
            gameDepositRequest.setIsFirstDep(1);
        }
        gameDepositRequest.setFlag(EStatus.ENABLE);
        gameDepositRequest.setPayTime(new Date());
        QueryWrapper<GameDepositRequest> countQueryWrapper = new QueryWrapper<>();
        countQueryWrapper.eq(SQLConf.CUSTOMER_ID, gameDepositRequest.getCustomerId().trim());
        countQueryWrapper.eq(SQLConf.FLAG, EStatus.ENABLE);
        Integer countList = gameDepositRequestMapper.selectCount(countQueryWrapper);
        gameDepositRequest.setPayCount(countList + 1);
        gameDepositRequestMapper.updateById(gameDepositRequest);
        GameCreditLogs gameCreditLogs = new GameCreditLogs();
        QueryWrapper<GameCustomersBalance> wrapper = new QueryWrapper<>();
        wrapper.eq("customer_id", gameDepositRequest.getCustomerId().trim());
        GameCustomersBalance gameCustomersBalance = gameCustomersBalanceMapper.selectOne(wrapper);
//                if(org.apache.commons.lang.StringUtils.isNotBlank(gameCustomersBalance.getMeVersion()) && !org.apache.commons.lang.StringUtils.equalsIgnoreCase(gameCustomersBalance.getMeVersion(), MD5Utils.md5Sign(customers.getOpenId()+gameCustomersBalance.getCoins().toPlainString()))){
//                   throw new RuntimeException(String.format("用户{0}充值回调接口金额版本异常",customers.getOpenId()));
//                }
        gameCreditLogs.setCustomerId(gameDepositRequest.getCustomerId().trim());
        gameCreditLogs.setType(1);
        gameCreditLogs.setSource(customers.getSource());
        gameCreditLogs.setCoinsBefore(gameCustomersBalance.getCoins());
        gameCreditLogs.setCoinsAfter(gameCustomersBalance.getCoins().add(new BigDecimal(gameDepositRequest.getCoins())));
        gameCreditLogs.setReferenceId(gameDepositRequest.getRequestId());
        gameCreditLogsService.save(gameCreditLogs);
        gameCustomersBalance.setCoins(gameCustomersBalance.getCoins().add(new BigDecimal(gameDepositRequest.getCoins())));
        gameCustomersBalance.setMeVersion(null);
//                if(gameCustomersBalance.getCoins().compareTo(BigDecimal.ZERO)==1){
//                    gameCustomersBalance.setMeVersion(MD5Utils.md5Sign(customers.getOpenId()+gameCustomersBalance.getCoins().toPlainString()));
//                }
        gameCustomersBalanceMapper.updateById(gameCustomersBalance);
    }
    @Transactional(rollbackFor = { Exception.class })
    @Override
    public void withdrawOrder(GameWithdrawalRequest gameWithdrawalRequest,Customers customers) {
        log.info("withdrawOrder事务");
        gameWithdrawalRequestMapper.insert(gameWithdrawalRequest);
        QueryWrapper<GameCustomersBalance> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("customer_id", customers.getCustomerId());
        GameCustomersBalance gameCustomersBalance = gameCustomersBalanceMapper.selectOne(queryWrapper);
//            if(org.apache.commons.lang.StringUtils.isNotBlank(gameCustomersBalance.getMeVersion()) && !org.apache.commons.lang.StringUtils.equalsIgnoreCase(gameCustomersBalance.getMeVersion(), MD5Utils.md5Sign(customers.getOpenId()+gameCustomersBalance.getCoins().toPlainString()))){
//                throw new RuntimeException(String.format("用户{0}充值回调接口金额版本异常",customers.getOpenId()));
//            }
        gameCustomersBalance.setCoins(gameCustomersBalance.getCoins().subtract(gameWithdrawalRequest.getAmount()));
        gameCustomersBalance.setFrozenCoins(gameCustomersBalance.getFrozenCoins().add(gameWithdrawalRequest.getAmount()));
        gameCustomersBalance.setMeVersion(null);
//            if(gameCustomersBalance.getCoins().compareTo(BigDecimal.ZERO)==1){
//                gameCustomersBalance.setMeVersion(MD5Utils.md5Sign(customers.getOpenId()+gameCustomersBalance.getCoins().toPlainString()));
//            }
        gameCustomersBalanceMapper.updateById(gameCustomersBalance);
        GameCreditLogs gameCreditLogs = new GameCreditLogs();
        gameCreditLogs.setCustomerId(gameWithdrawalRequest.getCustomerId().trim());
        gameCreditLogs.setType(2);
        gameCreditLogs.setSource(customers.getSource());
        gameCreditLogs.setCoinsBefore(gameCustomersBalance.getCoins());
        gameCreditLogs.setCoinsAfter(gameCustomersBalance.getCoins().subtract(gameWithdrawalRequest.getAmount()));
        gameCreditLogs.setReferenceId(gameWithdrawalRequest.getRequestId());
        gameCreditLogsService.save(gameCreditLogs);
    }
    @Transactional(rollbackFor = { Exception.class })
    @Override
    public void addCoins(GameAddCoinsVo gameAddCoinsVo,Customers customers) {
        log.info("addCoins事务");
        GameCreditLogs gameCreditLogs = new GameCreditLogs();
        GameAddCoins gameAddCoins = new GameAddCoins();
        gameAddCoins.setCoins(new BigDecimal(String.valueOf(gameAddCoinsVo.getCoins())));
        gameAddCoins.setOpenId(customers.getOpenId());
        gameAddCoins.setCustomerId(customers.getCustomerId());
        gameAddCoins.setRemark(gameAddCoinsVo.getRemark().trim());
        gameAddCoins.setCusUid(customers.getUid());
        gameAddCoins.setOperName(gameAddCoinsVo.getOperName());
        gameAddCoins.setSource(customers.getSource().toString());
        gameAddCoins.setRequestId("D"+ StringUtils.getSnowflakeId());
        gameAddCoinsMapper.insert(gameAddCoins);

        QueryWrapper<GameCustomersBalance> query = new QueryWrapper<>();
        query.eq("customer_id",customers.getCustomerId());
        GameCustomersBalance gameCustomersBalance = gameCustomersBalanceMapper.selectOne(query);
        if(ObjectUtils.isEmpty(gameCustomersBalance)){
            gameCreditLogs.setCoinsBefore(new BigDecimal(0));
            GameCustomersBalance balance = new GameCustomersBalance();
            balance.setCustomerId(customers.getCustomerId());
            balance.setCoins(new BigDecimal(String.valueOf(gameAddCoinsVo.getCoins())));
            balance.setFrozenCoins(new BigDecimal(0));
            gameCustomersBalanceMapper.insert(balance);
            gameCreditLogs.setCoinsAfter(balance.getCoins());
        }else{
            gameCreditLogs.setCoinsBefore(gameCustomersBalance.getCoins());
            gameCustomersBalance.setCoins(gameCustomersBalance.getCoins().add(new BigDecimal(String.valueOf(gameAddCoinsVo.getCoins()))));
            gameCustomersBalanceMapper.updateById(gameCustomersBalance);
            gameCreditLogs.setCoinsAfter(gameCustomersBalance.getCoins());
        }
        gameCreditLogs.setCustomerId(customers.getCustomerId().trim());
        gameCreditLogs.setType(5);
        gameCreditLogs.setSource(customers.getSource());
        gameCreditLogs.setReferenceId(gameAddCoins.getRequestId());
        gameCreditLogsMapper.insert(gameCreditLogs);
    }
}
