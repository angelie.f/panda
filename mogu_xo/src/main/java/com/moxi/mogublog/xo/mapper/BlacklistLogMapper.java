package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.BlacklistLog;
import com.moxi.mougblog.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;


public interface BlacklistLogMapper extends SuperMapper<BlacklistLog> {

    @Select(" select vali_uid,user_name,operation,update_time,replace(replace(replace(operation,'1','开启拉黑'),'2','关闭拉黑'),'3','修改原因') as content from t_blacklist_log  where vali_uid=${valiUid}")
    List<BlacklistLog> queryList(@Param("valiUid") Integer valiUid);
}
