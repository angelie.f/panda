package com.moxi.mogublog.xo.service.impl;

import com.alibaba.csp.sentinel.util.StringUtil;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.Customers;
import com.moxi.mogublog.commons.entity.TpPayLog;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.mapper.BooksSettingMapper;
import com.moxi.mogublog.xo.mapper.CustomersMapper;
import com.moxi.mogublog.xo.mapper.TpPayLogMapper;
import com.moxi.mogublog.xo.service.TpPayService;
import com.moxi.mogublog.xo.utils.HttpsUtil;
import com.moxi.mogublog.xo.utils.sign.Signature;
import com.moxi.mogublog.xo.vo.LocalBooksVO;
import com.moxi.mogublog.xo.vo.TpPayLogVO;
import com.moxi.mougblog.base.global.BaseMessageConf;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class TpPayServiceImpl extends SuperServiceImpl<TpPayLogMapper, TpPayLog> implements TpPayService {

    @Autowired
    private CustomersMapper customersMapper;

    @Autowired
    private BooksSettingMapper booksSettingMapper;

    @Value(value = "${T08.url:}")
    private String t08Url;
    @Value("${T08.sign.key:}")
    private String T08Sign;

    @Override
    public String pay(LocalBooksVO localBooksVO) {
        QueryWrapper<Customers> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("customer_id",localBooksVO.getCustomerId());
        Customers customers = customersMapper.selectOne(queryWrapper);
        QueryWrapper<TpPayLog>payWrapper = new QueryWrapper<>();
        payWrapper.eq("source",localBooksVO.getSource());
        payWrapper.eq("open_id",customers.getOpenId());
        payWrapper.eq("book_id",localBooksVO.getBookId());
        payWrapper.eq("episode",localBooksVO.getEpisode());
        TpPayLog tpPayLog = this.getOne(payWrapper);
        if (tpPayLog!=null){
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.ALREADY_PAY);
        }

        String url = t08Url+"/minigame_api/gi/query_pay_grass_url";
        Integer fee =booksSettingMapper.queryFee("perFee");
        Map<String, Object> map = new HashMap<>();
        Map<String, Object> param = new HashMap<>();
        param.put("userId", customers.getOpenId());
        param.put("amount",fee);
        param.put("transCode",3);
        Calendar calendar = Calendar.getInstance();
        param.put("timestamp", String.valueOf(calendar.getTime().getTime()));
        map.put("param", param);
        map.put("gameId","G1005");
        String resign = null;
        String result = "";
        try {
            resign = Signature.getLowerSign(chinaToUnicode(JSONObject.toJSONString(param, SerializerFeature.WriteSlashAsSpecial, SerializerFeature.MapSortField)));
            String signKey=T08Sign;
            resign = Signature.getLowerSign(resign + signKey);
            map.put("sign", resign);
            result = HttpsUtil.post(url,JSONObject.toJSONString(map));
        } catch (Exception e) {
            return ResultUtil.result(SysConf.ERROR, BaseMessageConf.VERIFY_ERROR);
        }

        log.info("扣草币接口,请求地址:{},查询参数:{},返回结果:{}", url, param, result);
        JSONObject jsonObject = JSONObject.parseObject(result);
        String code = jsonObject.getString("code");
        if (!"200".equals(code)) {
            return ResultUtil.result(SysConf.ERROR, "草币额度不足");
        }
        tpPayLog = new TpPayLog();
        tpPayLog.setSource(Integer.valueOf(localBooksVO.getSource()));
        tpPayLog.setOpenId(customers.getOpenId());
        tpPayLog.setBookId(localBooksVO.getBookId());
        tpPayLog.setEpisode(localBooksVO.getEpisode());
        tpPayLog.setPlatform(localBooksVO.getPlatform());
        tpPayLog.setPrice(BigDecimal.valueOf(fee));
        this.save(tpPayLog);
        return ResultUtil.result(SysConf.SUCCESS, "扣除草币成功");
    }



    public String chinaToUnicode(String str) {
        String result = "";
        for (int i = 0; i < str.length(); i++) {
            int char1 = (char) str.charAt(i);
            if (char1 > 127) {
                result = result + "\\u" + Integer.toHexString(char1);
            } else {
                result = result + str.charAt(i);
            }
        }
        return result;
    }
    @Override
    public IPage<TpPayLog> getPageList(TpPayLogVO tpPayLogVO) {
        QueryWrapper<TpPayLog> queryWrapper = new QueryWrapper<>();
        if (StringUtil.isNotEmpty(tpPayLogVO.getSource())) {
            queryWrapper.eq("plog.source", tpPayLogVO.getSource());
        }
        if (StringUtil.isNotEmpty(tpPayLogVO.getOpenId())) {
            queryWrapper.eq("plog.open_id", tpPayLogVO.getOpenId());
        }
        if (StringUtil.isNotEmpty(tpPayLogVO.getBeginCreateTime())) {
            queryWrapper.ge("plog.create_time", tpPayLogVO.getBeginCreateTime().trim());
        }
        if (StringUtils.isNotEmpty(tpPayLogVO.getEndCreateTime())) {
            queryWrapper.le("plog.create_time", tpPayLogVO.getEndCreateTime().trim());
        }
        Page<TpPayLog> page = new Page<>();
        page.setCurrent(tpPayLogVO.getCurrentPage());
        page.setSize(tpPayLogVO.getPageSize());
        if (StringUtils.isNotEmpty(tpPayLogVO.getOrderByAscColumn())) {
            // 将驼峰转换成下划线
            String column = StringUtils.underLine(new StringBuffer(tpPayLogVO.getOrderByAscColumn())).toString();
            queryWrapper.orderByAsc(column);
        } else if (StringUtils.isNotEmpty(tpPayLogVO.getOrderByDescColumn())) {
            // 将驼峰转换成下划线
            String column = StringUtils.underLine(new StringBuffer(tpPayLogVO.getOrderByDescColumn())).toString();
            queryWrapper.orderByDesc(column);
        }
        else{
            queryWrapper.orderByDesc(SQLConf.UPDATE_TIME);
        }
        return getBaseMapper().getPage(page, queryWrapper);
    }
}
