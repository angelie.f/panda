package com.moxi.mogublog.xo.service;

import com.moxi.mogublog.commons.entity.Announcement;
import com.moxi.mogublog.xo.vo.AnnouncementVo;
import com.moxi.mougblog.base.service.SuperService;

import java.util.List;

public interface AnnouncementService extends SuperService<Announcement> {
    Announcement getAdvConfig();

    String editAdvConfig(AnnouncementVo announcementVo);
}
