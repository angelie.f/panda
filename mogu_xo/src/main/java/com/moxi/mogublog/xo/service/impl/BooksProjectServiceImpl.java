package com.moxi.mogublog.xo.service.impl;


import com.alibaba.csp.sentinel.util.StringUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.BooksProject;
import com.moxi.mogublog.commons.entity.BooksSetting;
import com.moxi.mogublog.commons.entity.GirlSort;
import com.moxi.mogublog.commons.feign.PictureFeignClient;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.mapper.BooksProjectMapper;
import com.moxi.mogublog.xo.service.BooksProjectService;
import com.moxi.mogublog.xo.utils.WebUtil;
import com.moxi.mogublog.xo.vo.BooksProjectVO;
import com.moxi.mogublog.xo.vo.BooksSettingVO;
import com.moxi.mogublog.xo.vo.GirlSortVO;
import com.moxi.mougblog.base.enums.EStatus;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

@Service
@Slf4j
public class BooksProjectServiceImpl extends SuperServiceImpl<BooksProjectMapper, BooksProject> implements BooksProjectService {

    @Autowired
    private WebUtil webUtil;

    @Resource
    private PictureFeignClient pictureFeignClient;

    @Override
    public IPage<BooksProject> getPageList(BooksProjectVO booksProjectVO) {
        QueryWrapper<BooksProject> queryWrapper = new QueryWrapper<>();
        if (booksProjectVO.getSource()!=null) {
            queryWrapper.eq(SQLConf.SOURCE, booksProjectVO.getSource());
        }
        Page<BooksProject> page = new Page<>();
        page.setCurrent(booksProjectVO.getCurrentPage());
        page.setSize(booksProjectVO.getPageSize());
        queryWrapper.orderByAsc(SQLConf.UID);
        IPage<BooksProject> pageList = page(page, queryWrapper);
        List<BooksProject> projects = pageList.getRecords();
        final StringBuffer fileUids = new StringBuffer();
        projects.forEach(item -> {
            if (item.getFileUid()!=null) {
                fileUids.append(item.getFileUid() + SysConf.FILE_SEGMENTATION);
            }

        });
        String pictureResult = null;
        Map<String, String> pictureMap = new HashMap<>();
        if (fileUids != null) {
            pictureResult = this.pictureFeignClient.getPicture(fileUids.toString(), SysConf.FILE_SEGMENTATION);
        }
        List<Map<String, Object>> picList = webUtil.getPictureMap(pictureResult);

        picList.forEach(item -> {
            pictureMap.put(item.get(SysConf.UID).toString(), item.get(SysConf.URL).toString());
        });
        projects.forEach(item -> {
            if (item.getFileUid()!=null) {
                item.setPhoto(pictureMap.get(item.getFileUid().toString()));
            }
        });
        return pageList;
    }

    @Override
    public String add(BooksProjectVO booksProjectVO) {
        if(checkRepeat(booksProjectVO)){
            return ResultUtil.errorWithMessage(MessageConf.SOURCE_REPEAT);
        }
        booksProjectVO.setStatus(EStatus.ENABLE);
        BooksProject booksProject = new BooksProject();
        BeanUtils.copyProperties(booksProjectVO, booksProject);
        boolean isSuccess = booksProject.insert();
        log.info("insert success:{},data:{}",isSuccess,booksProject.toString());
        return ResultUtil.successWithMessage(MessageConf.INSERT_SUCCESS);
    }

    @Override
    public String edit(BooksProjectVO booksProjectVO) {
        if(checkRepeat(booksProjectVO)){
            return ResultUtil.errorWithMessage(MessageConf.SOURCE_REPEAT);
        }
        BooksProject booksProject = new BooksProject();
        BeanUtils.copyProperties(booksProjectVO, booksProject);
        booksProject.setUpdateTime(new Date());
        boolean isSuccess = booksProject.updateById();
        log.info("update success:{},data:{}",isSuccess,booksProject.toString());
        return ResultUtil.successWithMessage(MessageConf.UPDATE_SUCCESS);
    }
    private boolean checkRepeat(BooksProjectVO booksProjectVO){
        QueryWrapper<BooksProject> wrapper = new QueryWrapper<>();
        wrapper.eq(SQLConf.SOURCE,booksProjectVO.getSource());
        if(StringUtils.isNotEmpty(booksProjectVO.getUid())){
            wrapper.ne(SQLConf.UID,booksProjectVO.getUid());
        }
        Integer count = baseMapper.selectCount(wrapper);
        return count>0;
    }

    @Override
    public IPage<BooksProject> sources(BooksProjectVO booksProjectVO) {
        Page<BooksProject> page = new Page<>();
        page.setCurrent(booksProjectVO.getCurrentPage());
        page.setSize(booksProjectVO.getPageSize());
        QueryWrapper<BooksProject> queryWrapper = new QueryWrapper<>();
        queryWrapper.select(SQLConf.SOURCE,SQLConf.NAME);
        queryWrapper.orderByAsc(SQLConf.SOURCE);
        IPage<BooksProject> iPage = baseMapper.selectPage(page, queryWrapper);
        return iPage;
    }
}
