package com.moxi.mogublog.xo.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.BooksUserCost;
import com.moxi.mougblog.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;


public interface BooksUserCostMapper extends SuperMapper<BooksUserCost> {
    @Select("select uc.level_uid,ul.level_id ,ul.level_name,enable_episode  " +
            " from t_books_user_cost uc " +
            " left join t_books_user_level ul on uc.level_uid =ul.uid " +
            "where uc.status=1 and ul.status=1 and project_uid = " +
            " (select uid from t_books_project where status=1 and source = #{source}) order by ul.level_id")
    List<BooksUserCost> getListBySource(@Param("source") String source);

    @Select("select uc.uid,uc.level_uid,ul.level_id, " +
            " ul.level_name , uc.enable_episode,uc.create_time ,uc.update_time,project_uid,uc.status " +
            " from t_books_user_cost uc " +
            " left join t_books_user_level ul on uc.level_uid =ul.uid " +
            " ${ew.customSqlSegment}")
    IPage<BooksUserCost> getPage(Page<BooksUserCost> page, @Param(Constants.WRAPPER) QueryWrapper queryWrapper);

    @Select(" select enable_episode from t_books_user_cost  " +
            " where status=1 and level_uid = (select uid from t_books_user_level where status=1 and source =#{source} and level_id =#{levelId}) " +
            " and project_uid = " +
            " (select uid from t_books_project where status=1 and source = #{source}) ")
    Integer queryEnableEpisode(@Param("source") String source,@Param("levelId") Integer levelId );


}
