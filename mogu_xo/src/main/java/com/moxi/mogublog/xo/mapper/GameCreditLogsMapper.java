package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.GameCreditLogs;
import com.moxi.mougblog.base.mapper.SuperMapper;

public interface GameCreditLogsMapper extends SuperMapper<GameCreditLogs> {

}
