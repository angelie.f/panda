package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.PictureTag;
import com.moxi.mogublog.commons.entity.Project;
import com.moxi.mogublog.xo.vo.PictureTagVO;
import com.moxi.mogublog.xo.vo.ProjectVO;
import com.moxi.mougblog.base.service.SuperService;

import java.util.List;

/**
 * 项目列表 服务类
 *
 * @author Vergift
 * @date 2021-11-25
 */
public interface ProjectService extends SuperService<Project> {
    /**
     * 获取项目列表
     *
     * @param projectVO
     * @return
     */
    public IPage<Project> getPageList(ProjectVO projectVO);

    /**
     * 获取全部项目列表
     *
     * @return
     */
    public List<Project> getList(ProjectVO projectVO);

    /**
     * 新增项目
     *
     * @param projectVO
     */
    public String addTag(ProjectVO projectVO);

    /**
     * 编辑项目
     *
     * @param projectVO
     */
    public String editTag(ProjectVO projectVO);

    /**
     * 批量删除项目
     *
     * @param projectVOList
     */
    public String deleteBatchTag(List<ProjectVO> projectVOList);

}
