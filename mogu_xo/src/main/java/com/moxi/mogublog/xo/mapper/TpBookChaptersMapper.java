package com.moxi.mogublog.xo.mapper;

import com.moxi.mougblog.base.mapper.SuperMapper;
import com.moxi.mogublog.commons.entity.TpBookChapters;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;


public interface TpBookChaptersMapper extends SuperMapper<TpBookChapters> {
    @Select("select max(episode)" +
            " from tp_book_chapters where book_id =#{bookId} ")
    Integer getMaxEpisode(@Param("bookId") Integer bookId);

    @Select("select count(1) " +
            " from tp_book_chapters where book_id =#{bookId} and charge=1 ")
    Integer getChargeCount(@Param("bookId") Integer bookId);

    @Select("select " +
            "   exists " +
            "       (select 1 from tp_pay_log log " +
            "           left join t_customers c " +
            "           on log.open_id =c.open_id and log.source = c.source " +
            "           where log.book_id =#{bookId} and log.episode =#{episode} " +
            "               and log.status =1 and c.customer_id =#{customerId} )")
    Integer isPay(@Param("bookId") Integer bookId,@Param("episode") Integer episode, @Param("customerId") String customerId);

    @Insert("<script>" +
            " INSERT INTO tp_book_chapters" +
            " (id,book_id, episode, title, content, json_images, " +
            "  status, charge, review, operating, created_at, " +
            "  updated_at, deleted_at, views, collect_counts)" +
            " VALUES " +
            " <foreach collection='list' item='bookChapters' separator=','>" +
            " (" +
            "  #{bookChapters.id},#{bookChapters.bookId},#{bookChapters.episode},#{bookChapters.title},#{bookChapters.content},#{bookChapters.jsonImages}, " +
            "  #{bookChapters.status},#{bookChapters.charge},#{bookChapters.review},#{bookChapters.operating},#{bookChapters.createdAt}," +
            "  #{bookChapters.updatedAt},#{bookChapters.deletedAt},#{bookChapters.views},#{bookChapters.collectCounts}" +
            " )" +
            "</foreach> " +
            " ON DUPLICATE KEY UPDATE " +
            " book_id=VALUES(book_id),episode=VALUES(episode),title=VALUES(title),content=VALUES(content),json_images=VALUES(json_images), " +
            " status=VALUES(status),charge=VALUES(charge),review=VALUES(review),operating=VALUES(operating),created_at=VALUES(created_at), " +
            " updated_at=VALUES(updated_at),deleted_at=VALUES(deleted_at),views=VALUES(views),collect_counts=VALUES(collect_counts) " +
            "</script>"
    )
    int insertOrUpdateBatch(@Param("list") List<TpBookChapters> list);



}
