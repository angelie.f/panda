package com.moxi.mogublog.xo.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.GameActReport;
import com.moxi.mogublog.commons.entity.GameAppReport;
import com.moxi.mougblog.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface GameActReportMapper extends SuperMapper<GameActReport> {
    @Select("select gar.*," +
            "  (select name from t_panda_project where source =gar.source) as source_name" +
            " from t_game_act_report gar " +
            "${ew.customSqlSegment}")
    IPage<GameActReport> getPage(Page<GameActReport> page, @Param(Constants.WRAPPER) QueryWrapper queryWrapper);

}
