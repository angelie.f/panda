package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.Customers;
import com.moxi.mogublog.xo.vo.*;
import com.moxi.mougblog.base.service.SuperService;

/**
 * 第三方用户 服务类
 *
 * @author Vergift
 * @since 2021-12-07
 */
public interface CustomersService extends SuperService<Customers> {

    LoginGameResponse registerCustomer(Customers customers, QQCLoginRequest request) throws Exception;

    LoginGameResponse registerCustomerForGame(Customers customers, QQCLoginRequest request) throws Exception;

    LoginGameResponse refreshCustomer(CustomersRefreshVO customersRefreshVO);

    /**
     * 获取用户列表
     *
     * @param
     * @return
     */
    Integer editOne(PandaUserVO pandaUserVO);

    IPage<Customers> getPageList(PandaUserVO pandaUserVO);

    CoinsResponse queryCoins(UserRequest request) throws Exception;

    String queryCoinsByUserId(UserRequest request);


    BindPhoneResponse queryBindPhone(UserRequest request) throws Exception;

    Customers queryIsOneMemberCustomerId(String CustomerId);

}
