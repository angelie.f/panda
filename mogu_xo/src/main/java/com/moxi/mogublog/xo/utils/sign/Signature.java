package com.moxi.mogublog.xo.utils.sign;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import static java.util.stream.Collectors.joining;


public class Signature {
    /**
     * 签名算法:
     *  签名,将筛选的参数按照第一个字符的键值ASCII码递增排序（字母升序排序），
     *  如果遇到相同字符则按照第二个字符的键值ASCII码递增排序，
     *  以此类推,形成key=value& * skey的字符串MD5加密; (必填)
     * @param o 要参与签名的数据对象
     * @return 签名
     * @throws IllegalAccessException
     */
    public static String getSign(Object o) throws IllegalAccessException, UnsupportedEncodingException, NoSuchAlgorithmException {
        if(null == o ){
            return null;
        }
        ArrayList<String> list = new ArrayList<String>();
        Class cls = o.getClass();
        Field[] fields = cls.getDeclaredFields();
        for (Field f : fields) {
            f.setAccessible(true);
            if (f.get(o) != null && f.get(o) != "") {
                list.add(f.getName() + "=" + f.get(o) + "&");
            }
        }
        int size = list.size();
        String[] arrayToSort = list.toArray(new String[size]);
        Arrays.sort(arrayToSort, String.CASE_INSENSITIVE_ORDER);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; i++) {
            sb.append(arrayToSort[i]);
        }
        String result = sb.toString();
        result = MD5Util.getMD5UpperString(result);
        return result;
    }

    public static String getSign(Map<String, Object> map, String privateKey) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        if(null == map ){
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(map.entrySet().stream().map(Object::toString).collect(joining("&"))).append("&").append(privateKey);
        return MD5Util.getMD5UpperString(sb.toString());
    }

    public static String getSign(String jsonString) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        return MD5Util.getMD5UpperString(jsonString);
    }
    public static String getLowerSign(String jsonString)throws Exception{
        return MD5Util.getMD5String(jsonString);
    }
}
