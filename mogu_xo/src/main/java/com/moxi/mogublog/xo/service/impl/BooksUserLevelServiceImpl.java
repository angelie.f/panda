package com.moxi.mogublog.xo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.BooksUserLevel;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.mapper.BooksUserLevelMapper;
import com.moxi.mogublog.xo.service.BooksUserLevelService;
import com.moxi.mogublog.xo.vo.BooksUserLevelVO;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import java.util.Date;

@Service
@Slf4j
public class BooksUserLevelServiceImpl extends SuperServiceImpl<BooksUserLevelMapper, BooksUserLevel> implements BooksUserLevelService {

    @Override
    public IPage<BooksUserLevel> getPageList(BooksUserLevelVO booksUserLevelVO) {
        log.info("booksUserLevelVO:{}",booksUserLevelVO.toString());
        Page<BooksUserLevel> page = new Page<>();
        page.setCurrent(booksUserLevelVO.getCurrentPage());
        page.setSize(booksUserLevelVO.getPageSize());
        IPage<BooksUserLevel> iPage= baseMapper.getPage(page, booksUserLevelVO.getSource());
        log.info("page list result{}",iPage.getRecords().toString());
        return iPage;
    }


    @Override
    public String add(BooksUserLevelVO booksUserLevelVO) {
        if(checkRepeat(booksUserLevelVO)){
            return ResultUtil.errorWithMessage(MessageConf.LEVEL_REPEAT);
        }
        BooksUserLevel booksUserLevel = new BooksUserLevel();
        BeanUtils.copyProperties(booksUserLevelVO, booksUserLevel);

        boolean isSuccess = booksUserLevel.insert();
        log.info("insert success:{},data:{}",isSuccess,booksUserLevel.toString());
        return ResultUtil.successWithMessage(MessageConf.INSERT_SUCCESS);
    }
    private boolean checkRepeat(BooksUserLevelVO booksUserLevelVO){
        QueryWrapper<BooksUserLevel> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("level_id",booksUserLevelVO.getLevelId());
        queryWrapper.eq("source",booksUserLevelVO.getSource());
        if(booksUserLevelVO.getUid()!=null){
            queryWrapper.ne("uid",booksUserLevelVO.getUid());
        }
        int count = baseMapper.selectCount(queryWrapper);
        return count>0;
    }

    @Override
    public String edit(BooksUserLevelVO booksUserLevelVO) {
        if(checkRepeat(booksUserLevelVO)){
            return ResultUtil.errorWithMessage(MessageConf.LEVEL_REPEAT);
        }
        BooksUserLevel booksUserLevel = new BooksUserLevel();
        BeanUtils.copyProperties(booksUserLevelVO, booksUserLevel);
        booksUserLevel.setUpdateTime(new Date());
        LambdaUpdateWrapper<BooksUserLevel> updateWrapper = new LambdaUpdateWrapper();
        updateWrapper.set(BooksUserLevel::getLevelId,booksUserLevel.getLevelId());
        updateWrapper.set(BooksUserLevel::getLevelName,booksUserLevel.getLevelName());
        updateWrapper.eq(BooksUserLevel::getUid,booksUserLevel.getUid());
        int rows =this.baseMapper.update(null,updateWrapper);
        log.info("update success:{},data:{}",rows>0,booksUserLevel.toString());
        return ResultUtil.successWithMessage(MessageConf.UPDATE_SUCCESS);
    }
    @Override
    public String delete(BooksUserLevelVO booksUserLevelVO) {
        //删除分类上下架妹子
        this.removeById(booksUserLevelVO.getUid());
        return ResultUtil.successWithMessage(MessageConf.UPDATE_SUCCESS);
    }

}
