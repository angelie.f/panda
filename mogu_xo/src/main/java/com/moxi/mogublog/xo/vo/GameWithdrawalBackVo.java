package com.moxi.mogublog.xo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

@ToString
@Data
public class GameWithdrawalBackVo extends BaseVO<GameWithdrawalBackVo> {
     private String remarks;
    /**
     * 用户id
     */
    private String openId;

    private String customerId;

    /**
     * 订单号
     */
    private String requestId;
    /**
     * 订单状态
     */
    private String orderStatus;

    private String remark;
}
