package com.moxi.mogublog.xo.vo;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * UserVO
 *
 * @author: 陌溪
 * @create: 2019-12-03-22:29
 */
@ToString
@Data
public class PandaUserVO extends BaseVO<PandaUserVO> {


    /**
     * 渠道
     */
    private Integer source;

    /**
     * 用户id
     */
    private String openId;

    private String customerId;

    private String isVip;

    /**
     * 创建时间-开始
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private String beginCreateTime;

    /**
     * 创建时间-结束
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private String endCreateTime;
    private boolean phoneHide;
    private Integer isBlack;
    private Integer isOneMember;

}
