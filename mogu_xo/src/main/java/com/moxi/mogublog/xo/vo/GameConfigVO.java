package com.moxi.mogublog.xo.vo;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class GameConfigVO {

    private Integer uid;
    private String customerId;
    private String thirdNo;

    private String name;

    private Integer sort;

    private String timestamp;
}
