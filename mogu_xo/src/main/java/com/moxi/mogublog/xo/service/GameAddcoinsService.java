package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.GameAddCoins;
import com.moxi.mogublog.xo.vo.GameAddCoinsVo;
import com.moxi.mougblog.base.service.SuperService;

public interface GameAddcoinsService extends SuperService<GameAddCoins> {

    IPage<GameAddCoins> getPageList(GameAddCoinsVo gameAddCoinsVo);

    String addConis(GameAddCoinsVo gameAddCoinsVo);
}
