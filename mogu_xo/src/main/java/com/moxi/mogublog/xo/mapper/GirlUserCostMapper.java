package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.GirlUserCost;
import com.moxi.mougblog.base.mapper.SuperMapper;

/**
 * 用户费用表 Mapper 接口
 *
 * @author Vergift
 * @since 2021-12-29
 */
public interface GirlUserCostMapper extends SuperMapper<GirlUserCost> {

}
