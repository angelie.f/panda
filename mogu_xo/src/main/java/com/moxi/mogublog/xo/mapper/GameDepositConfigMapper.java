package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.GameDepositConfig;
import com.moxi.mougblog.base.mapper.SuperMapper;

public interface GameDepositConfigMapper extends SuperMapper<GameDepositConfig> {

}
