package com.moxi.mogublog.xo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;
@Data
public class ActConfigVo extends BaseVO<ActConfigVo> {

    private String actTitle;
    private String actType;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date actBeginTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date actEndTime;
    private String actPreAmount;
    private int actPreAmount1;
    private int actPreAmount2;
    private int actPreAmount3;
    private int actPreAmount4;
    private int actPreAmount5;
    private int actPreAmount6;
    private int actPreAmount7;
    private int actPreAmount8;
    private int actPreAmount9;
    private int actPreAmount10;
    private int actPreAmount11;
    private int actPreAmount12;
    private int actPreAmount13;
    private int actPreAmount14;
    private int actPreAmount15;
    private int actPreAmount16;
    private int actPreAmount17;
    private int actPreAmount18;
    private int actPreAmount19;
    private int actPreAmount20;
    private int actPreAmount21;
    private int actPreAmount22;
    private int actPreAmount23;
    private int actPreAmount24;
    private int actPreAmount25;
    private int actPreAmount26;
    private int actPreAmount27;
    private int actPreAmount28;
    private int actPreAmount29;
    private int actPreAmount30;
    private int actPreAmount31;
    private int actPreAmount32;
    private int actPreAmount33;
    private int actPreAmount34;
    private int actPreAmount35;
    private int actPreAmount41;
    private int actPreAmount42;
    private int actPreAmount99;
    private int actPreAmount101;
    private int actPreAmount36;
    private int actPreAmount37;
    private int actPreAmount38;
    private int actPreAmount39;
    private int actPreAmount40;
    private int actPreAmount43;
    private int actPreAmount44;
    private int actPreAmount45;
    private int actPreAmount46;
    private int actPreAmount47;
    private int actPreAmount48;
    private String actDesc;
    private String actChannel;


    @Override
    public String toString() {
        return   actPreAmount1 +
                "," + actPreAmount2 +
                "," + actPreAmount3 +
                "," + actPreAmount4 +
                "," + actPreAmount5 +
                "," + actPreAmount6 +
                "," + actPreAmount7 +
                "," + actPreAmount8 +
                "," + actPreAmount9 +
                "," + actPreAmount10 +
                "," + actPreAmount11 +
                "," + actPreAmount12 +
                "," + actPreAmount13 +
                "," + actPreAmount14 +
                "," + actPreAmount15 +
                "," + actPreAmount16 +
                "," + actPreAmount17 +
                "," + actPreAmount18 +
                "," + actPreAmount19 +
                "," + actPreAmount20 +
                "," + actPreAmount21 +
                "," + actPreAmount22 +
                "," + actPreAmount23 +
                "," + actPreAmount24 +
                "," + actPreAmount25 +
                "," + actPreAmount26 +
                "," + actPreAmount27 +
                "," + actPreAmount28 +
                "," + actPreAmount29 +
                "," + actPreAmount30 +
                "," + actPreAmount31 +
                "," + actPreAmount32 +
                "," + actPreAmount33 +
                "," + actPreAmount34 +
                "," + actPreAmount35 +
                "," + actPreAmount41 +
                "," + actPreAmount42 +
                "," + actPreAmount99 +
                "," + actPreAmount101+
                "," + actPreAmount36+
                "," + actPreAmount37+
                "," + actPreAmount38+
                "," + actPreAmount39+
                "," + actPreAmount40+
                "," + actPreAmount43+
                "," + actPreAmount44+
                "," + actPreAmount45+
                "," + actPreAmount46+
                "," + actPreAmount47+
                "," + actPreAmount48;
    }
}
