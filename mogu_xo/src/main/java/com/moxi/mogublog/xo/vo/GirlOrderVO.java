package com.moxi.mogublog.xo.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.moxi.mougblog.base.validator.annotion.NotBlank;
import com.moxi.mougblog.base.validator.group.Insert;
import com.moxi.mougblog.base.validator.group.Update;
import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * GirlOrderVO
 *
 * @author Vergift
 * @since 2022-01-07
 */
@Data
public class GirlOrderVO extends BaseVO<GirlOrderVO> {

    /**
     * OrderBy排序字段（desc: 降序）
     */
    private String orderByDescColumn;

    /**
     * OrderBy排序字段（asc: 升序）
     */
    private String orderByAscColumn;

    /**
     * 订单ID
     */
    private String orderId;

    /**
     * 流水号
     */
    private String thirdOrderId;

    /**
     * 金额
     */
    private BigDecimal amount;

    /**
     * 用户ID
     */
    private String userId;

    private String customerId;

    /**
     * 用户昵称
     */
    private String userNick;

    /**
     * 妹子uid
     */
    private String girlUid;

    /**
     * 妹子名称
     */
    private String girlName;

    /**
     * 支付次数:1.首次;2.续费2;...;n.续费n
     */
    private Integer payCount;

    /**
     * 支付状态:0.待付款.已支付2.已过期
     */
    private Integer payStatus;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 创建时间-开始
     */
    private String beginCreateTime;

    /**
     * 创建时间-结束
     */
    private String endCreateTime;

    /**
     * 更新时间
     */
    private String updateTime;
    /**
     * 更新时间-开始
     */
    private String beginUpdateTime;

    /**
     * 更新时间-结束
     */
    private String endUpdateTime;


    /**
     * 渠道來源
     */
    private String source;

    /**
     * 會員等級
     */
    private Integer vipLevel;

    private String  payCount1;

    private String payCount2;

}
