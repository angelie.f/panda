package com.moxi.mogublog.xo.utils.sign;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

public class MD5Util {
    /**
     * 获得字符串的md5值.
     *
     * @param str 待加密的字符串
     * @return md5 加密后的字符串
     */
    public static String getMD5String(String str) throws UnsupportedEncodingException, NoSuchAlgorithmException {

        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        messageDigest.reset();
        messageDigest.update(str.getBytes("UTF-8"));
        byte[] byteArray = messageDigest.digest();

        StringBuffer md5StrBuff = new StringBuffer();

        //将加密后的byte数组转换为十六进制的字符串,否则的话生成的字符串会乱码
        for (int i = 0; i < byteArray.length; i++) {
            if (Integer.toHexString(0xFF & byteArray[i]).length() == 1) {
                md5StrBuff.append("0").append(Integer.toHexString(0xFF & byteArray[i]));
            } else {
                md5StrBuff.append(Integer.toHexString(0xFF & byteArray[i]));
            }
        }

        return md5StrBuff.toString();
    }

    /**
     * 获得字符串的md5大写值.
     *
     * @param str 待加密的字符串
     * @return md5 加密后的字符串
     */
    public static String getMD5UpperString(String str) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        return getMD5String(str).toUpperCase();
    }


    public static void main(String[] args) {
        String tt = "[{\n" +
                "\"100012\": \"icbc\",\n" +
                "\"100013\": \"abc\",\n" +
                "\"100014\": \"ccb\",\n" +
                "\"100015\": \"boco\",\n" +
                "\"100016\": \"cmb\",\n" +
                "\"100017\": \"bofc\",\n" +
                "\"100018\": \"msb\",\n" +
                "\"100019\": \"hxb\",\n" +
                "\"100020\": \"inb\",\n" +
                "\"100021\": \"shpdb\",\n" +
                "\"100022\": \"gddb\",\n" +
                "\"100023\": \"citic\",\n" +
                "\"100024\": \"ebb\",\n" +
                "\"100025\": \"psb\",\n" +
                "\"100026\": \"bjyh\",\n" +
                "\"100027\": \"tccb\",\n" +
                "\"100028\": \"bos\",\n" +
                "\"100029\": \"srcb\",\n" +
                "\"100030\": \"pab\",\n" +
                "\"100031\": \"bjrcb\"\n" +
                "}]";

        JSONArray objects = JSONObject.parseArray(tt);

        HashMap map = new HashMap();
        for (Object object : objects) {
            HashMap hashMap = JSONObject.parseObject(object.toString(), HashMap.class);
            map.putAll(hashMap);
        }
        System.out.println(map);

    }
}
