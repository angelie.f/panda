package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.Girl;
import com.moxi.mogublog.commons.entity.UsePower;
import com.moxi.mougblog.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * 妹子表 Mapper 接口
 *
 * @author Vergift
 * @since 2021-12-29
 */
public interface GirlMapper extends SuperMapper<Girl> {

    @Update("update t_girl set available =${available} where uid=${uid}")
    int updateAvailable(@Param("uid") int uid, @Param("available") int available);
}
