package com.moxi.mogublog.xo.service;

import com.moxi.mogublog.commons.entity.PictureSortItem;
import com.moxi.mougblog.base.service.SuperService;

/**
 * 上下架 服务类
 *
 * @author Vergift
 * @date 2021-12-03
 */
public interface PictureSortItemService extends SuperService<PictureSortItem> {

}
