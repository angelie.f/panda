package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.GameDepositRequest;
import com.moxi.mogublog.xo.vo.CallbackOrderVO;
import com.moxi.mogublog.xo.vo.GameDepositRequestVo;
import com.moxi.mogublog.xo.vo.GameDepositTotalVo;
import com.moxi.mogublog.xo.vo.GameDepositVO;
import com.moxi.mougblog.base.service.SuperService;

import java.util.Map;

public interface GameDepositRequestService extends SuperService<GameDepositRequest> {
    String createOrder(GameDepositVO gameDepositVO);

    Map<String, Object> callbackOrder(CallbackOrderVO callbackOrderVO);

    /**
     * 获取充值列表列表
     *
     * @param
     * @return
     */
    IPage<GameDepositRequest> getPageList(GameDepositRequestVo gameDepositRequestVo);

    IPage<GameDepositRequest> getOfficePageList(GameDepositRequestVo gameDepositRequestVo);

    GameDepositTotalVo getDepositTotal(GameDepositRequestVo gameDepositRequestVo);

    Integer getFirstDepositNum();


    Map getUserFirstDepositStatus(String customerId);
}