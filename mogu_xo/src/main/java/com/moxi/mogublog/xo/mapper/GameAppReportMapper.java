package com.moxi.mogublog.xo.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.BooksProject;
import com.moxi.mogublog.commons.entity.GameAppReport;
import com.moxi.mougblog.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface GameAppReportMapper extends SuperMapper<GameAppReport> {

    @Select("select gar.*," +
            "  (select name from t_panda_project where source =gar.source) as source_name" +
            " from t_game_app_report gar " +
            "${ew.customSqlSegment}")
    IPage<GameAppReport> getPage(Page<GameAppReport> page, @Param(Constants.WRAPPER) QueryWrapper queryWrapper);
}
