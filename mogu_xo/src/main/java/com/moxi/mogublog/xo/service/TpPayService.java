package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.TpPayLog;
import com.moxi.mogublog.xo.vo.LocalBooksVO;
import com.moxi.mogublog.xo.vo.TpPayLogVO;
import com.moxi.mougblog.base.service.SuperService;

public interface TpPayService extends SuperService<TpPayLog> {
    String pay(LocalBooksVO localBooksVO);

    /**
     * 获取漫画订单列表
     *
     * @param tpPayLogVO
     * @return
     */
    IPage<TpPayLog> getPageList(TpPayLogVO tpPayLogVO);
}

