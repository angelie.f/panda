package com.moxi.mogublog.xo.service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.BooksProject;
import com.moxi.mogublog.commons.entity.BooksUserCost;
import com.moxi.mogublog.commons.entity.BooksUserLevel;
import com.moxi.mogublog.xo.vo.BooksProjectVO;
import com.moxi.mogublog.xo.vo.BooksUserCostVO;
import com.moxi.mogublog.xo.vo.LocalBooksVO;
import com.moxi.mougblog.base.service.SuperService;
import com.moxi.mougblog.base.vo.BaseVO;

import java.util.List;


public interface BooksUserCostService extends SuperService<BooksUserCost> {

    /**
     * 获取用户费用列表
     *
     * @param localBooksVO
     * @return
     */

    String getList(LocalBooksVO localBooksVO);

    IPage<BooksUserCost> getPageList(BooksUserCostVO booksUserCostVO);

    List<BooksUserLevel> levels(BooksUserCostVO booksUserCostVO);

    String add(BooksUserCostVO booksUserCostVO);

    String edit(BooksUserCostVO booksUserCostVO);
}

