package com.moxi.mogublog.xo.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.BooksUserLevel;
import com.moxi.mogublog.commons.entity.Customers;
import com.moxi.mogublog.xo.vo.PandaUserVO;
import com.moxi.mougblog.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.math.BigDecimal;
import java.util.List;

/**
 * 第三方用户 Mapper 接口
 *
 * @author Vergift
 * @since 2021-12-07
 */
public interface CustomersMapper extends SuperMapper<Customers> {
    Customers queryBalanceByCustomerId(@Param("customerId") String customerId);

    IPage<Customers> queryCustList(IPage<Customers> page, @Param("pandaUserVO") PandaUserVO pandaUserVO);

    int queryDayOnlineNum(@Param("startTime") String startTime,@Param("endTime") String endTime,@Param("source") String source);

    int queryNewCusNum(@Param("startTime") String startTime,@Param("endTime") String endTime,@Param("source") String source);

    int queryTotalOrders(@Param("startTime") String startTime,@Param("endTime") String endTime,@Param("source") String source);

    int queryTotalOrdersSucess(@Param("startTime") String startTime, @Param("endTime") String endTime, @Param("source") String source, @Param("flags") List<String> flags);

    int queryFirstDepTotalOrders(@Param("startTime") String startTime, @Param("endTime") String endTime, @Param("source") String source, @Param("flags") List<String> flags,@Param("isFirstDep") String isFirstDep);

    int queryVipTotalOrders(@Param("startTime") String startTime,@Param("endTime") String endTime,@Param("source") String source,@Param("isVip") String isVip);

    BigDecimal queryTotalAmountOrdersSucess(@Param("startTime") String startTime, @Param("endTime") String endTime, @Param("source") String source, @Param("flags") List<String> flags);

    BigDecimal queryFirstDepTotalAmountOrders (@Param("startTime") String startTime, @Param("endTime") String endTime, @Param("source") String source, @Param("flags") List<String> flags,@Param("isFirstDep") String isFirstDep);

    BigDecimal queryWithdrawalTotalAmountOrders(@Param("startTime") String startTime, @Param("endTime") String endTime, @Param("source") String source, @Param("flags") List<String> flags);

    BigDecimal queryWithdrawalTotalTaxAmountOrders(@Param("startTime") String startTime, @Param("endTime") String endTime, @Param("source") String source, @Param("flags") List<String> flags);

    int queryDepTotalNum(@Param("startTime") String startTime,@Param("endTime") String endTime,@Param("source") String source, @Param("flags") List<String> flags);

    int queryWithdrawalTotalNum(@Param("startTime") String startTime,@Param("endTime") String endTime,@Param("source") String source, @Param("flags") List<String> flags);

    int queryDepALLNum(@Param("startTime") String startTime,@Param("endTime") String endTime,@Param("source") String source);

    @Select(" select vip_level from t_customers where customer_id = #{customerId} ")
    Integer queryVipLevel(@Param("customerId") String customerId);

    @Select(" select b.coins from t_customers c inner join t_game_customers_balance b on c.customer_id = b.customer_id where c.customer_id =#{customerId}  ")
    BigDecimal queryCoinsByCustomerId(@Param("customerId") String customerId);

    @Select(" select b.coins from t_customers c inner join t_game_customers_balance b on c.customer_id = b.customer_id where c.source =#{source} and c.open_id =#{userId}  ")
    BigDecimal queryCoinsByUserId(@Param("source") String source,@Param("userId") String userId);

    @Select(" select * from t_customers where customer_id =#{customerId} ")
    Customers queryPhoneCustomerId(@Param("customerId") String customerId);

    @Select(" select is_one_member as isOneMember,uid from t_customers where customer_id =#{customerId} ")
    Customers queryIsOneMemberCustomerId(@Param("customerId") String customerId);
}
