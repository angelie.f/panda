package com.moxi.mogublog.xo.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.Customers;
import com.moxi.mogublog.commons.entity.GameCustomersBalance;
import com.moxi.mogublog.commons.entity.GameWithdrawalRequest;
import com.moxi.mogublog.commons.entity.SystemConfig;
import com.moxi.mogublog.utils.RedisUtil;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.mapper.*;
import com.moxi.mogublog.xo.service.CustomersService;
import com.moxi.mogublog.xo.service.GameActRecordAsyncService;
import com.moxi.mogublog.xo.service.SystemConfigService;
import com.moxi.mogublog.xo.utils.HttpUtil;
import com.moxi.mogublog.xo.vo.*;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * 第三方用户 服务实现类
 *
 * @author Vergift
 * @since 2021-12-07
 */
@Slf4j
@Service
public class CustomersServiceImpl extends SuperServiceImpl<CustomersMapper, Customers> implements CustomersService {

    @Resource
    private CustomersMapper customersMapper;
    @Resource
    private GameCustomersBalanceMapper gameCustomersBalanceMapper;

    @Autowired
    private GameWithdrawalRequestMapper gameWithdrawalRequestMapper;

    @Autowired
    private GameDepositRequestMapper gameDepositRequestMapper;

    @Autowired
    private BlacklistMapper blacklistMapper;

    @Autowired
    private GameActRecordAsyncService gameActRecordAsyncService;

    @Autowired
    RedisUtil redisUtil;

    @Value("${web.url}")
    private String webUrl;

    @Autowired
    private Environment env;

    @Autowired
    private SystemConfigService systemConfigService;


    @Override
    public LoginGameResponse registerCustomer(Customers customers, QQCLoginRequest request) throws Exception {
        LoginGameResponse response = new LoginGameResponse();
        QueryWrapper<Customers> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("open_id",customers.getOpenId());
        queryWrapper.eq("source",customers.getSource());
        List<Customers> customers1 = customersMapper.selectList(queryWrapper);
        //判断用户是否存在，存在 -> 更新， 不存在 -> 新增
        customers.setLastLoginDate(new Date());
        String uid = null;
        if (null == customers1 || customers1.size()==0) {
            log.info("customer首次登陆; openId:{}", customers.getOpenId());
            customers.setCustomerId(UUID.randomUUID().toString());
            customersMapper.insert(customers);
            response.setUserId(String.valueOf(customers.getUid()));
            setURL(customers, response);
        } else {
            log.info("customer登陆; openId:{}, customerId:{}, loginName:{}", customers.getOpenId(), customers.getUid(), customers.getLoginName());
            customers1.get(0).setProductId(customers.getProductId());
            customers1.get(0).setLastLoginDate(new Date());
            customers1.get(0).setIsVip(customers.getIsVip());
            customersMapper.updateById(customers1.get(0));
            response.setUserId(customers1.get(0).getUid());
            setURL(customers, response);
        }
        List<Customers> customersResult = customersMapper.selectList(queryWrapper);
        response.setUrl(StringUtils.replace(response.getUrl(), "{1}", customersResult.get(0).getCustomerId()));
        String screen = StringUtils.hasText(request.getScreen())?request.getScreen():"";
        response.setUrl(StringUtils.replace(response.getUrl(), "{2}", screen));
        response.setUrl(StringUtils.replace(response.getUrl(), "{0}", customers.getProductId()));
        response.setUrl(StringUtils.replace(response.getUrl(), "{3}", customers.getOpenId()));
        response.setUrl(StringUtils.replace(response.getUrl(), "{4}", customers.getOpenCode()));
        String token = DigestUtils.md5DigestAsHex(request.getUserToken().getBytes());
        response.setUrl(StringUtils.replace(response.getUrl(), "{5}", token));
        response.setUrl(StringUtils.replace(response.getUrl(), "{6}", request.getUserToken()));
        response.setUrl(StringUtils.replace(response.getUrl(), "{7}", request.getPlatform()));
        response.setUrl(StringUtils.replace(response.getUrl(), "{8}", request.getGameId()));
        response.setUrl(StringUtils.replace(response.getUrl(), "{9}", request.getSource()+""));
        redisUtil.setEx("login_cookies_key&" + customers.getProductId() + customers.getOpenId() + customers.getOpenCode(), token, 7*24*60*60*1000L,TimeUnit.MILLISECONDS);

        response.setUserId(customersResult.get(0).getOpenId());
        response.setCustomersId(customersResult.get(0).getCustomerId());
        response.setSource(customersResult.get(0).getSource());
        response.setProductId(customersResult.get(0).getProductId());
        response.setPlatform(request.getPlatform());
        response.setAppToken(request.getUserToken());
        response.setToken(token);
        response.setOpenCode(customers.getOpenCode());
        response.setScreen(screen);
        response.setRoute("EggSmash");
        response.setIsVip(customersResult.get(0).getIsVip());
        response.setIsBindPhone(StringUtils.isEmpty(customersResult.get(0).getPhone())?0:1);
        int blackCount =blacklistMapper.countBlack(customersResult.get(0).getUid());
        response.setIsBlacklist(blackCount>0?1:0);
//        response.setIsH5(request.getIsH5());
        return response;
    }

    @Override
    public LoginGameResponse registerCustomerForGame(Customers customers, QQCLoginRequest request) throws Exception {
        LoginGameResponse response = new LoginGameResponse();
        QueryWrapper<Customers> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isEmpty(customers.getOpenId())){
            log.error("openId不能为空");
            throw new Exception("openId不能为空");
        }
        queryWrapper.eq("open_id",customers.getOpenId());
        queryWrapper.eq("source",customers.getSource());
        List<Customers> customers1 = customersMapper.selectList(queryWrapper);
        //判断用户是否存在，存在 -> 更新， 不存在 -> 新增
        customers.setLastLoginDate(new Date());
        String uid = null;
        if (null == customers1 || customers1.size()==0) {
            log.info("customer首次登陆; openId:{}", customers.getOpenId());
            customers.setCustomerId(UUID.randomUUID().toString());
            customersMapper.insert(customers);
            GameCustomersBalance balance = new GameCustomersBalance();
            balance.setCustomerId(customers.getCustomerId());
            balance.setCoins(new BigDecimal(0));
            balance.setFrozenCoins(new BigDecimal(0));
            gameCustomersBalanceMapper.insert(balance);
            response.setUserId(String.valueOf(customers.getUid()));
            setURL(customers, response);
        } else {
            //如果已经同步返回one登录地址
            if (customers1.get(0).getIsOneMember()!=null&&customers1.get(0).getIsOneMember()==1){

                SystemConfig systemConfig = systemConfigService.list().get(0);
                log.info("登陆one游戏; openId:{}, customerId:{}, loginName:{}", customers.getOpenId(), customers.getUid(), customers.getLoginName());
                response.setUrl(systemConfig.getOneLoginUrl());
                response.setIsOneMember(customers1.get(0).getIsOneMember());
                response.setOneSecret("Aa"+customers1.get(0).getPhone());
                response.setMobile(customers1.get(0).getPhone());
                response.setUserId(customers1.get(0).getUid());
                return response;
            }
            if (customers1.get(0).getIsOneMember()!=null&&customers1.get(0).getIsOneMember()==2){
                response.setIsOneMember(customers1.get(0).getIsOneMember());
                response.setUserId(customers1.get(0).getUid());
                return response;
            }
            log.info("customer登陆; openId:{}, customerId:{}, loginName:{}", customers.getOpenId(), customers.getUid(), customers.getLoginName());
            if (!StringUtils.isEmpty(customers.getPhone())) {
                customers1.get(0).setPhone(customers.getPhone());
            }
            customers1.get(0).setProductId(customers.getProductId());
            customers1.get(0).setLastLoginDate(new Date());
            customers1.get(0).setIsVip(customers.getIsVip());
            customers1.get(0).setLoginName(customers.getLoginName());
            customersMapper.updateById(customers1.get(0));
            QueryWrapper<GameCustomersBalance> query = new QueryWrapper<>();
            query.eq("customer_id",customers1.get(0).getCustomerId());
            GameCustomersBalance gameCustomersBalance = gameCustomersBalanceMapper.selectOne(query);
            if (gameCustomersBalance==null){
                GameCustomersBalance balance = new GameCustomersBalance();
                balance.setCustomerId(customers1.get(0).getCustomerId());
                balance.setCoins(new BigDecimal(0));
                balance.setFrozenCoins(new BigDecimal(0));
                gameCustomersBalanceMapper.insert(balance);
            }
            response.setUserId(customers1.get(0).getUid());
            setURL(customers, response);
        }
        log.info("更新用户信息");
        List<Customers> customersResult = customersMapper.selectList(queryWrapper);
        response.setUrl(StringUtils.replace(response.getUrl(), "{1}", customersResult.get(0).getCustomerId()));
        String screen = StringUtils.hasText(request.getScreen())?request.getScreen():"";
        response.setUrl(StringUtils.replace(response.getUrl(), "{2}", screen));
        response.setUrl(StringUtils.replace(response.getUrl(), "{0}", customers.getProductId()));
        response.setUrl(StringUtils.replace(response.getUrl(), "{3}", customers.getOpenId()));
        response.setUrl(StringUtils.replace(response.getUrl(), "{4}", customers.getOpenCode()));
        String token = DigestUtils.md5DigestAsHex(request.getUserToken().getBytes());
        response.setUrl(StringUtils.replace(response.getUrl(), "{5}", token));
        response.setUrl(StringUtils.replace(response.getUrl(), "{6}", request.getUserToken()));
        response.setUrl(StringUtils.replace(response.getUrl(), "{7}", request.getPlatform()));
        response.setUrl(StringUtils.replace(response.getUrl(), "{8}", request.getGameId()));
        response.setUrl(StringUtils.replace(response.getUrl(), "{9}", request.getSource()+""));
        redisUtil.setEx("login_cookies_key&" + customers.getProductId() + customers.getOpenId() + customers.getOpenCode(), token, 7*24*60*60*1000L,TimeUnit.MILLISECONDS);

        response.setUserId(customersResult.get(0).getOpenId());
        response.setCustomersId(customersResult.get(0).getCustomerId());
        response.setSource(customersResult.get(0).getSource());
        response.setProductId(customersResult.get(0).getProductId());
        response.setPlatform(request.getPlatform());
        response.setAppToken(request.getUserToken());
        response.setToken(token);
        response.setOpenCode(customers.getOpenCode());
        response.setScreen(screen);
        response.setRoute("EggSmash");
        response.setIsVip(customersResult.get(0).getIsVip());
        //response.setIsBindPhone(StringUtils.isEmpty(customersResult.get(0).getPhone())?0:1);
        response.setMobile(customersResult.get(0).getPhone());
//        Customers balanceCustomers = customersMapper.queryBalanceByCustomerId(customersResult.get(0).getCustomerId());
//        response.setCoins(balanceCustomers.getCoins());
        QueryWrapper<GameWithdrawalRequest> query = new QueryWrapper<>();
        query.eq("customer_id",customersResult.get(0).getCustomerId());
        query.eq("flag",2);
        List<GameWithdrawalRequest> list = gameWithdrawalRequestMapper.selectList(query);
        BigDecimal withdrawalCoins = new BigDecimal(0);
        if (list.size()==0) {
            response.setWithdrawalCoins(withdrawalCoins);
        }else {
            for (GameWithdrawalRequest gameWithdrawalRequest:list){
                withdrawalCoins = withdrawalCoins.add(gameWithdrawalRequest.getAmount());
            }
            response.setWithdrawalCoins(withdrawalCoins);
        }
        int blackCount =blacklistMapper.countBlack(customersResult.get(0).getUid());
        response.setIsBlacklist(blackCount>0?1:0);
//        response.setIsH5(request.getIsH5());
        //执行欢乐送彩金活动
        try{
            String key = "panda:promotion"+customersResult.get(0).getCustomerId();
            boolean isLock = redisUtil.setIfAbsent(key,"",7*1000,TimeUnit.MILLISECONDS);
            if(isLock){
                gameActRecordAsyncService.addActRecord(customersResult.get(0));
            }
        }catch(Exception e){
            log.error("执行欢乐送彩金活动异常:{}",e.getMessage());
            return response;
        }

        return response;
    }
    @Override
    public CoinsResponse queryCoins(UserRequest request) throws Exception {
        CoinsResponse response = new CoinsResponse();
        log.info("request:{}",request);
        log.info("request customerId:{}",request.getCustomerId());
        BigDecimal coins = customersMapper.queryCoinsByCustomerId(request.getCustomerId());
        log.info("queryCoins :{}",coins);
        response.setCoins(coins==null?BigDecimal.ZERO:coins);
        return response;
    }
    @Override
    public String queryCoinsByUserId(UserRequest request){
        if(StringUtils.isEmpty(request.getSource())){
            return ResultUtil.result(SysConf.ERROR, MessageConf.SOURCE_NULL);
        }
        if(StringUtils.isEmpty(request.getUserId())){
            return ResultUtil.result(SysConf.ERROR, MessageConf.USER_ID_NULL);
        }
        BigDecimal coins =  customersMapper.queryCoinsByUserId(request.getSource(),request.getUserId());
        if(coins==null){
            return ResultUtil.result(SysConf.ERROR, MessageConf.USER_NOT_EXIST);
        }
        CoinsResponse response = new CoinsResponse();
        log.info("queryCoinsByUserId :{}",coins);
        response.setCoins(coins);
        return ResultUtil.result(SysConf.SUCCESS, response);
    }
    @Override
    public BindPhoneResponse queryBindPhone(UserRequest request) throws Exception {
        log.info("request:{}",request);
        log.info("request customerId:{}",request.getCustomerId());
        BindPhoneResponse response = new BindPhoneResponse();
        Customers customers = customersMapper.queryPhoneCustomerId(request.getCustomerId());
        log.info("queryBindPhone customers:{}",customers);
        if(customers==null){
            response.setIsBindPhone(0);
        }
        else{
            response.setIsBindPhone(StringUtils.isEmpty(customers.getPhone())?0:1);
        }
        return response;
    }

    @Override
    public Customers queryIsOneMemberCustomerId(String customerId) {
        return customersMapper.queryIsOneMemberCustomerId(customerId);

    }

    @Override
    public Integer editOne(PandaUserVO pandaUserVO) {
        Customers customers=new Customers();
        customers.setUid(pandaUserVO.getUid());
        customers.setIsOneMember(pandaUserVO.getIsOneMember());
        if (pandaUserVO.getIsOneMember()==1){
            customers.setOneDate(new Date());
        }
        return customersMapper.updateById(customers);
    }

    private void setURL(Customers customers, LoginGameResponse response) {
        StringBuffer urlBuffer = new StringBuffer();
        urlBuffer.append(StringUtils.replace(webUrl,"{1}",customers.getUid()));
        response.setUrl(urlBuffer.toString());
    }

    @Override
    public LoginGameResponse refreshCustomer(CustomersRefreshVO customersRefreshVO){
        LoginGameResponse response = new LoginGameResponse();
        QueryWrapper<Customers> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("customer_id",customersRefreshVO.getCustomerId());
        Customers customers = customersMapper.selectOne(queryWrapper);

        try {
            String result = httpGetRefresh(customersRefreshVO.getPlatform(),customersRefreshVO.getAppToken(),customersRefreshVO.getSource());
            log.info("用户最新信息返回结果result: {}",result);
            if (result != null && !"".equals(result)){
                JSONObject resultJson = JSONObject.parseObject(result);
                if ("200".equals(String.valueOf(resultJson.get("code")))){
                    JSONObject dataJson = resultJson.getJSONObject("data");
                    JSONObject userJson = dataJson.getJSONObject("user");
                    if (userJson.getBooleanValue("is_vip")){
                        customers.setParam(result);
                        customers.setIsVip(1);
                        customers.setVipLevel(userJson.getInteger("vip_level"));
                        customers.setVipType(userJson.getString("vip_type"));
                    }else {
                        customers.setIsVip(0);
                    }
                    customers.updateById();
                }
            }
        }catch (Exception e){
            log.info("获取用户最新信息失败,{}",e.getMessage());
            e.printStackTrace();
        }

        response.setCustomersId(customers.getCustomerId());
        response.setIsVip(customers.getIsVip());
        return response;
    }

    @Override
    public IPage<Customers> getPageList(PandaUserVO pandaUserVO) {
        Page<Customers> page = new Page<>();
        page.setCurrent(pandaUserVO.getCurrentPage());
        page.setSize(pandaUserVO.getPageSize());
        IPage<Customers> customers = customersMapper.queryCustList(page,pandaUserVO);
        for(Customers c:customers.getRecords()){
            c.setOneSecret("Aa"+c.getPhone());
            if(c.getPhone()!=null&&pandaUserVO.isPhoneHide()){
                c.setPhone(com.moxi.mogublog.utils.StringUtils.mask(c.getPhone(),0,c.getPhone().length()-4,'*'));
            }
            BigDecimal sumAmount = gameDepositRequestMapper.querySumAmountByCustomerId(c.getCustomerId());
            c.setSumAmount(sumAmount==null?BigDecimal.ZERO:sumAmount);
        }

        return customers;
    }

    public String httpGetRefresh(String platform, String token,Integer source) {
        String app = "";
        String qqcUrl = "";
        if (source!=null) {
            qqcUrl = env.getProperty(source + ".url");
            if (source==1){
                app = "3";
            }else if (source==2){
                app = "1";
            }
        }else {
            qqcUrl = env.getProperty("1.url");
            app = "3";
        }
        String url = qqcUrl + "/api/user/read";
        String result = "";
        try {
            log.info("用户最新信息传参url:{},app:{},platform:{},token:{}",url,app,platform,token);
            result = HttpUtil.get(url, app, platform, token);
            log.info("用户最新信息返回结果result: {}",result);
        }catch (Exception e){
            log.info("获取用户最新信息失败,{}",e.getMessage());
            return result;
        }
        return result;
    }
}
