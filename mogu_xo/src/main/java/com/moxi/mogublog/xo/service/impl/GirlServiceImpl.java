package com.moxi.mogublog.xo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.config.security.SecurityUser;
import com.moxi.mogublog.commons.entity.*;
import com.moxi.mogublog.commons.feign.PictureFeignClient;
import com.moxi.mogublog.utils.DateUtils;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.mapper.GirlMapper;
import com.moxi.mogublog.xo.mapper.GirlUserCostMapper;
import com.moxi.mogublog.xo.service.*;
import com.moxi.mogublog.xo.utils.WebUtil;
import com.moxi.mogublog.xo.vo.GirlBySortVO;
import com.moxi.mogublog.xo.vo.GirlVO;
import com.moxi.mogublog.xo.vo.LocalGirlVO;
import com.moxi.mougblog.base.enums.EStatus;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

/**
 * 妹子表 服务实现类
 *
 * @author 陌溪
 * @since 2018-09-04
 */
@Service
@Slf4j
public class GirlServiceImpl extends SuperServiceImpl<GirlMapper, Girl> implements GirlService {

    @Autowired
    private WebUtil webUtil;

    @Resource
    private PictureFeignClient pictureFeignClient;

    @Autowired
    private GirlService girlService;

    @Autowired
    private GirlPictureService girlPictureService;

    @Autowired
    private GirlProjectService girlProjectService;

    @Autowired
    private GirlSortItemService girlSortItemService;

    @Autowired
    private GirlTagService girlTagService;

    @Autowired
    private GirlCityService girCityService;

    @Autowired
    private GirlUserCostMapper girlUserCostMapper;

    @Autowired
    private GirlOrderCommentService girlOrderCommentService;

    @Autowired
    private GirlOrderService girlOrderService;

    @Override
    public IPage<Girl> getGirlList(LocalGirlVO localGirlVO, List<Integer> girlUids) throws Exception{

        QueryWrapper<GirlProject> girlProjectQueryWrapper = new QueryWrapper<>();

        girlProjectQueryWrapper.eq(SQLConf.SOURCE, localGirlVO.getSource());
        List<GirlProject> girlProjectList = girlProjectService.list(girlProjectQueryWrapper);

        QueryWrapper<Girl> queryWrapper = new QueryWrapper<>();
        queryWrapper.in(SQLConf.UID, girlUids);
        if (StringUtils.isNotEmpty(localGirlVO.getCityUid()) && !StringUtils.isEmpty(localGirlVO.getCityUid())) {
            queryWrapper.eq(SQLConf.CITY_UID, localGirlVO.getCityUid().trim());
        }
        queryWrapper.orderByDesc(SQLConf.UPDATE_TIME);

        Page<Girl> page = new Page<>();
        page.setCurrent(localGirlVO.getCurrentPage()!=null?localGirlVO.getCurrentPage():1l);
        page.setSize(localGirlVO.getPageSize()!=null?localGirlVO.getPageSize():10);
        IPage<Girl> pageList = girlService.page(page, queryWrapper);

        List<Girl> list = pageList.getRecords();

        String pictureResult = null;
        Map<String, String> pictureMap = new HashMap<>();
        final StringBuffer fileUids = new StringBuffer();
        List<String> cityUids = new ArrayList<>();
        Map<String,Integer> isVideo = new HashMap<>();
        list.forEach(item -> {
            //当该图集没有封面时默认第一张图片为封面
            if (StringUtils.isEmpty(item.getVideoUid()) || "0".equals(item.getVideoUid())) {
                isVideo.put(item.getUid(),0);
            }else {
                isVideo.put(item.getUid(),1);
            }
            fileUids.append(item.getVideoUid() + SysConf.FILE_SEGMENTATION);
            QueryWrapper<GirlPicture> picQueryWrapper = new QueryWrapper<>();
                picQueryWrapper.eq(SQLConf.GIRL_UID, item.getUid());
                List<GirlPicture>  picList = girlPictureService.list(picQueryWrapper);
            List<String> picUids = new ArrayList<>();
            for (GirlPicture pic:picList) {
                fileUids.append(pic.getFileUid() + SysConf.FILE_SEGMENTATION);
                picUids.add(pic.getFileUid());
            }
            item.setPicUids(picUids);
            if (StringUtils.isNotEmpty(item.getCityUid())) {
                cityUids.add(item.getCityUid());
            }

        });
        if (fileUids != null) {
            pictureResult = this.pictureFeignClient.getPicture(fileUids.toString(), SysConf.FILE_SEGMENTATION);
        }
        List<Map<String, Object>> picList = webUtil.getPictureMap(pictureResult);

        picList.forEach(item -> {
            pictureMap.put(item.get(SysConf.UID).toString(), item.get(SysConf.URL).toString());
        });
        Collection<GirlCity> cityList = new ArrayList<>();
        if (cityUids.size() > 0) {
            cityList = girCityService.listByIds(cityUids);
        }
        Map<String, GirlCity> cityMap = new HashMap<>();

        cityList.forEach(item -> {
            cityMap.put(item.getUid(), item);
        });

        for (Girl item : list) {
            QueryWrapper<GirlPicture> girlPicQueryWrapper = new QueryWrapper<>();
            girlPicQueryWrapper.eq(SQLConf.GIRL_UID,item.getUid());
            if (StringUtils.isNotEmpty(item.getVideoUid())){
                item.setCount(girlPictureService.count(girlPicQueryWrapper) + 1);
            }else {
                item.setCount(girlPictureService.count(girlPicQueryWrapper));
            }
            //认证信息
            item.setAuthentication(girlProjectList.get(0).getAuthentication());

            //前端价格=妹子价格+服务费用
            QueryWrapper<GirlUserCost> ptUserCostQueryWrapper = new QueryWrapper<>();
            ptUserCostQueryWrapper.eq(SQLConf.PROJECT_UID,girlProjectList.get(0).getUid());
            ptUserCostQueryWrapper.eq(SQLConf.LEVEL_NAME,MessageConf.ORDINARY_VIP);
            GirlUserCost ptuserCost = girlUserCostMapper.selectOne(ptUserCostQueryWrapper);
            if (item.getPriceOne() == null){
                item.setPriceOne(ptuserCost.getCost());
            }else {
                item.setPriceOne(ptuserCost.getCost().add(item.getPriceOne()));
            }
            if (item.getPriceNight() == null){
                item.setPriceNight(ptuserCost.getCost());
            }else {
                item.setPriceNight(ptuserCost.getCost().add(item.getPriceNight()));
            }


            List<String> pictureListTemp = new ArrayList<>();
            //获取图片
            if (StringUtils.isNotEmpty(item.getVideoUid())) {
                List<String> pictureUidsTemp = StringUtils.changeStringToString(item.getVideoUid(), SysConf.FILE_SEGMENTATION);
                pictureUidsTemp.forEach(picture -> {
                    pictureListTemp.add(pictureMap.get(picture));
                });
            }
            if (item.getPicUids().size() > 0){
                for (String picUid: item.getPicUids() ) {
                    List<String> pictureUidsTemp = StringUtils.changeStringToString(picUid, SysConf.FILE_SEGMENTATION);
                    pictureUidsTemp.forEach(picture -> {
                        pictureListTemp.add(pictureMap.get(picture));
                    });
                }
            }
            item.setPhotoList(pictureListTemp);
            item.setIsVideo(isVideo.get(item.getUid()));

            //获取城市
            if (StringUtils.isNotEmpty(item.getCityUid())) {
                item.setCity(cityMap.get(item.getCityUid()));
            }
            Map<String,Object> map = girlOrderCommentService.getScoreByGirl(Integer.valueOf(item.getUid()));
            item.setScore((Float)map.get("score"));
            String lastServiceTime = girlOrderService.getRecentServiceTime(item.getUid());
            log.info("girlUid:{},lastServiceTime:{}",item.getUid(),lastServiceTime);
            item.setLastServiceTime(lastServiceTime);
            item.setDateable(dateable(item));
        }
        pageList.setRecords(list);
        return pageList;
    }
    private Integer dateable(Girl girl){

        if(isMoning()){
            return 3;
        }
        if(girl.getAvailable().equals(2)){
            return 2;
        }
        if(StringUtils.isNotEmpty(girl.getLastServiceTime()) && org.apache.commons.lang.StringUtils.isNumeric(girl.getLastServiceTime())){
            Date serviceTime = new Date(Long.parseLong(girl.getLastServiceTime()));
            Calendar rightNow = Calendar.getInstance();
            rightNow.setTime(new Date());
            rightNow.add(Calendar.HOUR, -2);
            if(rightNow.getTime().before(serviceTime)){
                return 2;
            }
        }
        return 1;
    }
    private boolean isMoning() {
        int hour = LocalDateTime.now().getHour();
        if(hour>=2&&hour<=13){
            return true;
        }
        return false;
    }

    @Override
    public IPage<Girl> getPageList(GirlVO girlVO) {
        QueryWrapper<Girl> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(girlVO.getGirlUid())){//分类uid获取图集
            QueryWrapper<GirlSortItem> sortItemQueryWrapper = new QueryWrapper<>();
            sortItemQueryWrapper.eq(SQLConf.GIRL_UID, girlVO.getGirlUid());

            Page<GirlSortItem> sortItemPage = new Page<>();
            sortItemPage.setCurrent(1);
            sortItemPage.setSize(10000);
            IPage<GirlSortItem> sortItemPageList =  girlSortItemService.page(sortItemPage,sortItemQueryWrapper);
            List<GirlSortItem> sortItemList = sortItemPageList.getRecords();
            List<Integer> girlUids = new ArrayList<>();
            for (GirlSortItem girlSortItem:sortItemList) {
                girlUids.add(girlSortItem.getGirlUid());
            }
            if (girlUids.size() >0 ){
                queryWrapper.in(SQLConf.UID,girlUids);
            }
        }
        if (StringUtils.isNotEmpty(girlVO.getCityUid())) {
            queryWrapper.eq(SQLConf.CITY_UID, girlVO.getCityUid().trim());
        }
        if (StringUtils.isNotEmpty(girlVO.getName()) && !StringUtils.isEmpty(girlVO.getName().trim())) {
            queryWrapper.like(SQLConf.NAME, girlVO.getName().trim());
        }
        if (StringUtils.isNotEmpty(girlVO.getUid()) && !StringUtils.isEmpty(girlVO.getUid().trim())) {
            queryWrapper.eq(SQLConf.UID, girlVO.getUid().trim());
        }
        if (!StringUtils.isEmpty(girlVO.getTagUid())) {
            queryWrapper.and(wrapper ->wrapper.eq(SQLConf.TAG_UID, girlVO.getTagUid())
                    .or().likeRight(SQLConf.TAG_UID, girlVO.getTagUid() + SysConf.FILE_SEGMENTATION)
                    .or().like(SQLConf.TAG_UID, SysConf.FILE_SEGMENTATION + girlVO.getTagUid() + SysConf.FILE_SEGMENTATION)
                    .or().likeLeft(SQLConf.TAG_UID, SysConf.FILE_SEGMENTATION + girlVO.getTagUid()));
        }
        if (!StringUtils.isEmpty(girlVO.getCreateTime())) {
            queryWrapper.like(SQLConf.CREATE_TIME, girlVO  .getCreateTime());
        }
        if (!StringUtils.isEmpty(girlVO.getUpdateTime())) {
            queryWrapper.like(SQLConf.UPDATE_TIME, girlVO.getUpdateTime());
        }
        Page<Girl> page = new Page<>();
        page.setCurrent(girlVO.getCurrentPage());
        page.setSize(girlVO.getPageSize());
        queryWrapper.orderByDesc(SQLConf.UPDATE_TIME);
        IPage<Girl> pageList = girlService.page(page, queryWrapper);
        List<Girl> list = pageList.getRecords();
        List<String> tagUids = new ArrayList<>();
        List<String> cityUids = new ArrayList<>();
        final StringBuffer fileUids = new StringBuffer();
        list.forEach(item -> {
            if (StringUtils.isNotEmpty(item.getVideoUid())) {
                fileUids.append(item.getVideoUid() + SysConf.FILE_SEGMENTATION);
            }
            if (StringUtils.isNotEmpty(item.getTagUid())) {
                List<String> tagUidsTemp = StringUtils.changeStringToString(item.getTagUid(), SysConf.FILE_SEGMENTATION);
                for (String itemTagUid : tagUidsTemp) {
                    tagUids.add(itemTagUid);
                }
            }
            if (StringUtils.isNotEmpty(item.getCityUid())) {
                cityUids.add(item.getCityUid());
            }
        });
        String pictureResult = null;
        Map<String, String> pictureMap = new HashMap<>();
        if (fileUids != null) {
            pictureResult = this.pictureFeignClient.getPicture(fileUids.toString(), SysConf.FILE_SEGMENTATION);
        }
        List<Map<String, Object>> picList = webUtil.getPictureMap(pictureResult);

        picList.forEach(item -> {
            pictureMap.put(item.get(SysConf.UID).toString(), item.get(SysConf.URL).toString());
        });

        Collection<GirlTag> tagList = new ArrayList<>();
        if (tagUids.size() > 0) {
            tagList = girlTagService.listByIds(tagUids);
        }

        Collection<GirlCity> cityList = new ArrayList<>();
        if (cityUids.size() > 0) {
            cityList = girCityService.listByIds(cityUids);
        }

        Map<String, GirlTag> tagMap = new HashMap<>();

        tagList.forEach(item -> {
            tagMap.put(item.getUid(), item);
        });

        Map<String, GirlCity> cityMap = new HashMap<>();

        cityList.forEach(item -> {
            cityMap.put(item.getUid(), item);
        });

        for (Girl item : list) {
            //获取图集下面图片数量
            QueryWrapper<GirlPicture> girlPicQueryWrapper = new QueryWrapper<>();
            girlPicQueryWrapper.eq(SQLConf.GIRL_UID,item.getUid());
            /*if (StringUtils.isEmpty(item.getVideoUid())){
                item.setCount(girlPictureService.count(girlPicQueryWrapper));
            }else {
                item.setCount(girlPictureService.count(girlPicQueryWrapper) + 1);
            }*/
            item.setCount(girlPictureService.count(girlPicQueryWrapper));
            //获取标签
            if (StringUtils.isNotEmpty(item.getTagUid())) {
                List<String> tagUidsTemp = StringUtils.changeStringToString(item.getTagUid(), SysConf.FILE_SEGMENTATION);
                List<GirlTag> tagListTemp = new ArrayList<>();

                tagUidsTemp.forEach(tag -> {
                    tagListTemp.add(tagMap.get(tag));
                });
                item.setTagList(tagListTemp);
            }

            //获取城市
            if (StringUtils.isNotEmpty(item.getCityUid())) {
                item.setCity(cityMap.get(item.getCityUid()));
            }

            //获取图片
            if (StringUtils.isNotEmpty(item.getVideoUid())) {
                List<String> pictureUidsTemp = StringUtils.changeStringToString(item.getVideoUid(), SysConf.FILE_SEGMENTATION);
                List<String> pictureListTemp = new ArrayList<>();
                pictureUidsTemp.forEach(picture -> {
                    pictureListTemp.add(pictureMap.get(picture));
                });
                item.setPhotoList(pictureListTemp);
            }
        }
        pageList.setRecords(list);
        return pageList;
    }

    @Override
    public String addGirl(GirlVO girlVO) {
        girlVO.setStatus(EStatus.ENABLE);
        Girl girl = new Girl();
        BeanUtils.copyProperties(girlVO, girl);
        SecurityUser securityUser = (SecurityUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        girl.setAdminUid(securityUser.getUid());
        girl.setUserName(securityUser.getUsername());
        girl.setOperatorTime(new Date());
        girl.insert();
//        girlVO.getPicList().stream().filter(v -> v.setGirlUid(Integer.valueOf(girl.getUid())));
        if (girlVO.getPicList() != null && girlVO.getPicList().size() > 0) {
            for (GirlPicture girlPicture :girlVO.getPicList()) {
                girlPicture.setGirlUid(Integer.valueOf(girl.getUid()));
            }
            girl.updateById();
        }
        girlPictureService.saveBatch(girlVO.getPicList());
        return ResultUtil.successWithMessage(MessageConf.INSERT_SUCCESS);
    }

    @Override
    public String editGirl(GirlVO girlVO) {
        Girl girl = new Girl();
        BeanUtils.copyProperties(girlVO, girl);
        SecurityUser securityUser = (SecurityUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        girl.setAdminUid(securityUser.getUid());
        girl.setUserName(securityUser.getUsername());
        girl.setOperatorTime(new Date());
        girl.setUpdateTime(new Date());
        //先删除原有的图片
        QueryWrapper<GirlPicture> girlPicQueryWrapper = new QueryWrapper<>();
        girlPicQueryWrapper.eq(SQLConf.GIRL_UID, girlVO.getUid());
        girlPictureService.remove(girlPicQueryWrapper);
        if (girlVO.getPicList() != null && girlVO.getPicList().size() > 0) {
            for (GirlPicture girlPicture :girlVO.getPicList()) {
                girlPicture.setGirlUid(Integer.valueOf(girl.getUid()));
            }
            //添加新的图片
            girlPictureService.saveBatch(girlVO.getPicList());
        }
        girl.updateById();
        return ResultUtil.successWithMessage(MessageConf.UPDATE_SUCCESS);
    }

    @Override
    public String deleteGirl(GirlVO girlVO) {
        //删除妹子的图片
        QueryWrapper<GirlPicture> girlPicQueryWrapper = new QueryWrapper<>();
        girlPicQueryWrapper.eq(SQLConf.GIRL_UID, girlVO.getUid());
        girlPictureService.remove(girlPicQueryWrapper);

        girlService.removeById(girlVO.getUid());
        return ResultUtil.successWithMessage(MessageConf.UPDATE_SUCCESS);
    }
    @Override
    public String changeAvailable(GirlVO girlVO) {
        //改变可约状态
        log.info("changeAvailable girl:{}",girlVO.toString());
        getBaseMapper().updateAvailable(Integer.parseInt(girlVO.getUid()),girlVO.getAvailable());
        return ResultUtil.successWithMessage(MessageConf.UPDATE_SUCCESS);
    }

    @Override
    public IPage<Girl> getGirlListBySort(GirlBySortVO girlBySortVO) {
        //获取上下架里面的妹子uid
        QueryWrapper<GirlSortItem> sortItemQueryWrapper = new QueryWrapper<>();
        sortItemQueryWrapper.eq(SQLConf.SORT_UID, girlBySortVO.getSortUid());
        sortItemQueryWrapper.eq(SQLConf.STATUS, girlBySortVO.getStatus());

        Page<GirlSortItem> sortItemPage = new Page<>();
        sortItemPage.setCurrent(1);
        sortItemPage.setSize(10000);
        IPage<GirlSortItem> sortItemPageList =  girlSortItemService.page(sortItemPage,sortItemQueryWrapper);
        List<GirlSortItem> sortItemList = sortItemPageList.getRecords();
        List<Integer> girlUids = new ArrayList<>();
        for (GirlSortItem pictureSortItem:sortItemList) {
            girlUids.add(pictureSortItem.getGirlUid());
        }

        //根据上下架里面的妹子uid获取妹子消息
        QueryWrapper<Girl> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(girlBySortVO.getName())){
            queryWrapper.like(SQLConf.NAME,girlBySortVO.getName());
        }
        if (StringUtils.isNotEmpty(girlBySortVO.getUid())){
            queryWrapper.like(SQLConf.UID,girlBySortVO.getUid());
        }
        if (girlUids.size() > 0){
            queryWrapper.in(SQLConf.UID,girlUids);
        }

        Page<Girl> page = new Page<>();
        page.setCurrent(girlBySortVO.getCurrentPage());
        page.setSize(girlBySortVO.getPageSize());
        queryWrapper.orderByDesc(SQLConf.CREATE_TIME);
        IPage<Girl> pageList = girlService.page(page, queryWrapper);
        List<Girl> list = pageList.getRecords();

        List<String> cityUids = new ArrayList<>();
        List<String> tagUids = new ArrayList<>();

        final StringBuffer fileUids = new StringBuffer();
        list.forEach(item -> {
            if (StringUtils.isNotEmpty(item.getVideoUid())) {
                fileUids.append(item.getVideoUid() + SysConf.FILE_SEGMENTATION);
            }
            if (StringUtils.isNotEmpty(item.getTagUid())) {
                List<String> tagUidsTemp = StringUtils.changeStringToString(item.getTagUid(), SysConf.FILE_SEGMENTATION);
                for (String itemTagUid : tagUidsTemp) {
                    tagUids.add(itemTagUid);
                }
            }
            if (StringUtils.isNotEmpty(item.getCityUid())) {
                cityUids.add(item.getCityUid());
            }
        });
        String pictureResult = null;
        Map<String, String> pictureMap = new HashMap<>();
        if (fileUids != null) {
            pictureResult = this.pictureFeignClient.getPicture(fileUids.toString(), SysConf.FILE_SEGMENTATION);
        }
        List<Map<String, Object>> picList = webUtil.getPictureMap(pictureResult);

        picList.forEach(item -> {
            pictureMap.put(item.get(SysConf.UID).toString(), item.get(SysConf.URL).toString());
        });

        Collection<GirlTag> tagList = new ArrayList<>();
        if (tagUids.size() > 0) {
            tagList = girlTagService.listByIds(tagUids);
        }

        Collection<GirlCity> cityList = new ArrayList<>();
        if (tagUids.size() > 0) {
            cityList = girCityService.listByIds(cityUids);
        }

        Map<String, GirlTag> tagMap = new HashMap<>();
        tagList.forEach(item -> {
            tagMap.put(item.getUid(), item);
        });


        Map<String, GirlCity> cityMap = new HashMap<>();
        cityList.forEach(item -> {
            cityMap.put(item.getUid(), item);
        });

        for (Girl item : list) {
            item.setStatus(girlBySortVO.getStatus());
            //获取妹子图片数量
            if (item.getCount() == null){
                QueryWrapper<GirlPicture> pictureQueryWrapper = new QueryWrapper<>();
                pictureQueryWrapper.eq(SQLConf.GIRL_UID,item.getUid());
                item.setCount(girlPictureService.count(pictureQueryWrapper));
                /*if (StringUtils.isEmpty(item.getVideoUid())){
                    item.setCount(girlPictureService.count(pictureQueryWrapper));
                }else {
                    item.setCount(girlPictureService.count(b) + 1);
                }*/
            }

            //获取城市
            if (StringUtils.isNotEmpty(item.getCityUid())) {
                item.setCity(cityMap.get(item.getCityUid()));
            }

            //获取标签
            if (StringUtils.isNotEmpty(item.getTagUid())) {
                List<String> tagUidsTemp = StringUtils.changeStringToString(item.getTagUid(), SysConf.FILE_SEGMENTATION);
                List<GirlTag> tagListTemp = new ArrayList<>();

                tagUidsTemp.forEach(tag -> {
                    tagListTemp.add(tagMap.get(tag));
                });
                item.setTagList(tagListTemp);
            }

            //获取妹子的封面图片
            if (StringUtils.isNotEmpty(item.getVideoUid())) {
                List<String> pictureUidsTemp = StringUtils.changeStringToString(item.getVideoUid(), SysConf.FILE_SEGMENTATION);
                List<String> pictureListTemp = new ArrayList<>();
                pictureUidsTemp.forEach(picture -> {
                    pictureListTemp.add(pictureMap.get(picture));
                });
                item.setPhotoList(pictureListTemp);

            }
        }
        pageList.setRecords(list);
        return pageList;
    }

    @Override
    public String upOrOut(List<GirlSortItem> list) {
        List<Integer> girlUid = new ArrayList<>();
        for (GirlSortItem girlSortItem:list) {
            girlUid.add(girlSortItem.getGirlUid());
        }
        QueryWrapper<GirlSortItem> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SQLConf.SORT_UID, list.get(0).getSortUid());
        queryWrapper.in(SQLConf.GIRL_UID,girlUid);
        List<GirlSortItem> sortItemList =  girlSortItemService.list(queryWrapper);

        for (GirlSortItem girlSortItem:sortItemList) {
            girlSortItem.setStatus(list.get(0).getStatus());
            girlSortItem.setUpdateTime(new Date());
        }
        girlSortItemService.updateBatchById(sortItemList);
        return ResultUtil.successWithMessage(MessageConf.OPERATION_SUCCESS);
    }
}
