package com.moxi.mogublog.xo.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.GameDepositRequest;
import com.moxi.mogublog.xo.vo.GameDepositRequestVo;
import com.moxi.mougblog.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

public interface GameDepositRequestMapper extends SuperMapper<GameDepositRequest> {

    IPage<GameDepositRequest> queryDepRequestList(IPage<GameDepositRequest> page, @Param("gameDepositRequestVo") GameDepositRequestVo gameDepositRequestVo);

    BigDecimal queryDepRequestSunAmout(@Param("gameDepositRequestVo") GameDepositRequestVo gameDepositRequestVo);

    int queryDepRequestTimesTotal(@Param("gameDepositRequestVo") GameDepositRequestVo gameDepositRequestVo);

    @Select(" select count(1) from t_game_deposit_request where status =1 and (flag =1 or flag=2) and open_id =#{openId} ")
    int isUserDeposit(@Param("openId") String openId);

    @Select("select count(1) from t_game_deposit_request where status =1 and (flag =1 or flag=2) and open_id =#{openId} and create_time <#{createTime}")
    int countDeposit(@Param("openId") String openId,@Param("createTime") String createTime);

    @Select("select sum(amount) from t_game_deposit_request where status =1 and (flag =1 or flag=2) and customer_id =#{customerId}")
    BigDecimal querySumAmountByCustomerId(@Param("customerId") String customerId);

    @Select("select count(1) from t_game_deposit_request where status =1 and (flag =1 or flag=2) and is_first_dep =1 and DATE(pay_time)=CURDATE()")
    int firstDepositToday();

    @Select(" select * from t_game_deposit_request where status =1 and (flag =1 or flag=2) and  is_first_dep =1  and customer_id =#{customerId} ")
    GameDepositRequest getFirstDeposit(@Param("customerId") String customerId);

    @Select("select count(1) from t_game_deposit_request where status =1 and (flag =1 or flag=2) and is_first_dep =1 and DATE(pay_time)=#{awardDate}")
    int countFirstDepositByDate(@Param("awardDate") LocalDate awardDate);

    @Select("select a.source, a.customer_id,c.uid as open_id  " +
            " from t_game_deposit_request a " +
            " left join t_customers c on a.customer_id = c.customer_id " +
            " where a.status =1 and (a.flag =1 or a.flag=2) " +
            " and a.is_first_dep =1 " +
            " and DATE(pay_time)=#{awardDate} ORDER BY RAND() limit 1")
    GameDepositRequest getFirstDepRandomWinner(@Param("awardDate") LocalDate awardDate);
}
