package com.moxi.mogublog.xo.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.GameActRecord;
import com.moxi.mogublog.xo.vo.GameActRecordVo;
import com.moxi.mougblog.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface GameActRecordMapper extends SuperMapper<GameActRecord> {
    IPage<GameActRecord> queryGameActRecord(IPage<GameActRecord> page, @Param("gameActRecordVo") GameActRecordVo gameActRecordVo);

    int queryGameActTypeNum(@Param("startTime") String startTime,@Param("endTime") String endTime,@Param("source") String source,@Param("actType") String actType);

    BigDecimal queryGameActTypeAmount(@Param("startTime") String startTime,@Param("endTime") String endTime,@Param("source") String source,@Param("actType") String actType);

    BigDecimal queryAddCoinsAmount(@Param("startTime") String startTime,@Param("endTime") String endTime,@Param("source") String source);

    int queryAllGameActTypeNum(@Param("startTime") String startTime,@Param("endTime") String endTime,@Param("source") String source,@Param("actTypes") List<String> actTypes);
}
