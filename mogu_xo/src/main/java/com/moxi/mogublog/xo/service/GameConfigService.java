package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.GameConfig;
import com.moxi.mogublog.xo.vo.GameConfigTrialVO;
import com.moxi.mogublog.xo.vo.GameConfigVO;
import com.moxi.mogublog.xo.vo.GameManagerVo;
import com.moxi.mougblog.base.service.SuperService;

import java.util.Map;

public interface GameConfigService extends SuperService<GameConfig> {

    Map<String,Object> getConfigAndType();

    String getGameTrail(GameConfigTrialVO gameConfigTrialVO);

    String enterGame(GameConfigVO gameConfigVO);

    String transferOut(GameConfigVO gameConfigVO);



    /**
     * 编辑
     *
     * @param gameManagerVo
     * @return
     */
    String editGameConfig(GameManagerVo gameManagerVo);

    String changeAvailable(GameConfigTrialVO gameConfigTrialVO);

    IPage<GameConfig> getPageList(GameManagerVo gameManagerVo);
}
