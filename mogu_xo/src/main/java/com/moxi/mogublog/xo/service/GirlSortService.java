package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.GirlSort;
import com.moxi.mogublog.xo.vo.GirlSortVO;
import com.moxi.mogublog.xo.vo.LocalGirlVO;
import com.moxi.mougblog.base.service.SuperService;

/**
 * 妹子分类 服务类
 *
 * @author Vergift
 * @date 2021-11-29
 */
public interface GirlSortService extends SuperService<GirlSort> {
    /**
     * 前端获取妹子分类列表
     *
     * @param localGirlVO
     * @return
     */
    String getGirlSortList(LocalGirlVO localGirlVO);

    /**
     * 获取妹子分类列表
     *
     * @param girlSortVO
     * @return
     */
    IPage<GirlSort> getPageList(GirlSortVO girlSortVO);

    /**
     * 新增妹子分类
     *
     * @param girlSortVO
     */
    String addGirlSort(GirlSortVO girlSortVO);

    /**
     * 编辑妹子分类
     *
     * @param girlSortVO
     */
    String editGirlSort(GirlSortVO girlSortVO);

    /**
     * 删除妹子分类
     *
     * @param girlSortVO
     */
    String deleteGirlSort(GirlSortVO girlSortVO);

}
