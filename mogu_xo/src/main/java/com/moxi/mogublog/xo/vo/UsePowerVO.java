package com.moxi.mogublog.xo.vo;

import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;
import lombok.ToString;

/**
 * 用户查询参数类
 *
 * @author Vergift
 * @date 2021年12月02日17:07:38
 */
@ToString
@Data
public class UsePowerVO extends BaseVO<UsePowerVO> {

    /**
     * 所属项目uid
     */
    private String projectUid;

    /**
     * 普通用户是否限制:0不限制；1限制
     */
    private int userAstrict;

    /**
     * 普通用户限制页数
     */
    private int userLimit;

    /**
     * vip否限制:0不限制；1限制
     */
    private int vipAstrict;

    /**
     * vip户限制页数
     */
    private int vipLimit;

}
