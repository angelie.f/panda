package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.GameDepositAward;
import com.moxi.mogublog.xo.vo.*;
import com.moxi.mougblog.base.service.SuperService;

import java.util.List;

public interface GameDepositAwardService extends SuperService<GameDepositAward> {

    IPage<GameDepositAward> getPageList(GameDepositAwardVo gameDepositAwardVo);

    GameDepositAward queryAward(GameDepositAwardVo gameDepositAwardVo);

    List<GameDepositAward> queryRecentAwards();
}