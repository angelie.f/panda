package com.moxi.mogublog.xo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.PictureAtlas;
import com.moxi.mogublog.commons.entity.PictureTag;
import com.moxi.mogublog.commons.entity.Project;
import com.moxi.mogublog.commons.entity.UsePower;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.mapper.UsePowerMapper;
import com.moxi.mogublog.xo.service.ProjectService;
import com.moxi.mogublog.xo.service.UsePowerService;
import com.moxi.mogublog.xo.vo.PictureTagVO;
import com.moxi.mogublog.xo.vo.UsePowerVO;
import com.moxi.mougblog.base.enums.EStatus;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 图片表 服务实现类
 *
 * @author vergift
 * @since 2021-12-04
 */
@Service
public class UsePowerServiceImpl extends SuperServiceImpl<UsePowerMapper, UsePower> implements UsePowerService {

    @Autowired
    private UsePowerService usePowerService;

    @Autowired
    private ProjectService projectService;

    @Override
    public UsePower getUsePower(UsePowerVO usePowerVO) {
        QueryWrapper<UsePower> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SQLConf.PROJECT_UID,usePowerVO.getProjectUid());//只取当前用户所属项目的图集分类
        return usePowerService.list(queryWrapper).get(0);
    }

    @Override
    public String addUsePower(UsePower usePower) {
        QueryWrapper<UsePower> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SQLConf.PROJECT_UID, usePower.getProjectUid());
        List<UsePower> usePowerList = usePowerService.list(queryWrapper);
        if (usePowerList.size() > 0){
            return ResultUtil.errorWithMessage(MessageConf.PROJECT_USE_POWER_ENTITY_EXIST);
        }
        usePower.setStatus(EStatus.ENABLE);
        usePower.insert();
        return ResultUtil.successWithMessage(MessageConf.INSERT_SUCCESS);
    }

    @Override
    public String editUsePower(UsePowerVO usePowerVO) {
        UsePower usePower = new UsePower();
        usePower.setUserAstrict(usePowerVO.getUserAstrict());
        usePower.setUserLimit(usePowerVO.getUserLimit());
        usePower.setVipAstrict(usePowerVO.getVipAstrict());
        usePower.setVipLimit(usePowerVO.getVipLimit());
        usePower.setUid(usePowerVO.getUid());
        usePower.setUpdateTime(new Date());
        usePower.updateById();
        return ResultUtil.successWithMessage(MessageConf.UPDATE_SUCCESS);
    }

    @Override
    public IPage<UsePower> getPageList(UsePowerVO usePowerVO) {
        QueryWrapper<UsePower> queryWrapper = new QueryWrapper<>();

        List<Project> projectList = projectService.list();
        Map<String,Project> projectMap = new HashMap<>();
        for (Project project:projectList) {
            projectMap.put(project.getUid(),project);
        }

        if (StringUtils.isNotEmpty(usePowerVO.getProjectUid())) {
            queryWrapper.eq(SQLConf.PROJECT_UID, usePowerVO.getProjectUid());
        }

        Page<UsePower> page = new Page<>();
        page.setCurrent(usePowerVO.getCurrentPage());
        page.setSize(usePowerVO.getPageSize());
        queryWrapper.orderByDesc(SQLConf.CREATE_TIME);

        IPage<UsePower> pageList = usePowerService.page(page, queryWrapper);
        List<UsePower> list = pageList.getRecords();
        for (UsePower item : list) {
            item.setProjectName(projectMap.get(item.getProjectUid()).getName());
        }
        pageList.setRecords(list);
        return pageList;
    }
}
