package com.moxi.mogublog.xo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;

@ToString
@Data
public class GameDepositRequestVo extends BaseVO<GameDepositRequestVo> {

    /**
     * 用户id
     */
    private String openId;

    private String customerId;

    /**
     * 订单号
     */
    private String requestId;
    /**
     * 订单状态
     */
    private String orderStatus;


    private Integer source;

    private Integer isFirstDep;

    private String isVip;

    /**
     * 创建时间-开始
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private String beginCreateTime;

    /**
     * 创建时间-结束
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private String endCreateTime;

}
