package com.moxi.mogublog.xo.vo;

import lombok.Data;
import lombok.ToString;

/**
 * 同城约妹参数类
 *
 * @author Vergift
 * @date 2021-12-29
 */
@ToString
@Data
public class LocalGirlVO {

    /**
     * 用户id
     */
    private String customerId;

    /**
     * 第三方用户id
     */
    private String productId;

    /**
     * 注册来源：1.青青草 2.小黄鸭
     */
    private String source;

    /**
     * 第三方code
     */
    private String openCode;

    /**
     * 妹子分类uid
     */
    private String girlSortUid;

    /**
     * 妹子uid
     */
    private String girlUid;

    private Integer girlId;

    private String orderId;

    private Float score;

    private Float score1;

    private Float score2;

    private String content;

    /**
     * 回复某条评论的uid
     */
    private Integer toUid;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 一级评论uid
     */
    private Integer firstCommentUid;

    /**
     * 妹子名称
     */
    private String girlName;

    /**
     * 城市uid
     */
    private String cityUid;

    private String receiveName;

    private String receiveNumber;

    private String receiveAddress;

    /**
     * 签名
     */
    private String sign;

    /**
     * 访问请求终端 H5
     */
    private String endPoint;

    /**
     * 当前页
     */
    private Long currentPage;

    /**
     * 页大小
     */
    private Long pageSize;

    /**
     * 服務地點 1:上門;2:租屋;3:酒店
     */
    private Integer serviceType;

    /**
     * 服務時間 yyyy-MM-dd HH:mm:ss
     */
    private String serviceTime;

    private String offPrice;

}
