package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.Customers;
import com.moxi.mogublog.commons.entity.GameDepositRequest;
import com.moxi.mogublog.commons.entity.GameWithdrawalRequest;
import com.moxi.mogublog.commons.entity.WebVisit;
import com.moxi.mogublog.xo.vo.CallbackWithdrawalVO;
import com.moxi.mogublog.xo.vo.GameAddCoinsVo;
import com.moxi.mogublog.xo.vo.GameConfigVO;
import com.moxi.mogublog.xo.vo.WebVisitVO;
import com.moxi.mougblog.base.service.SuperService;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public interface CoinsChangeService {

    void withdrawalCallbackOrderSuccess(GameWithdrawalRequest gameWithdrawalRequest, CallbackWithdrawalVO callbackWithdrawalVO);

    void withdrawalCallbackOrderFail(GameWithdrawalRequest gameWithdrawalRequest, CallbackWithdrawalVO callbackWithdrawalVO);

    void transferOut(String transferable, Customers customers, GameConfigVO gameConfigVO, String orderId);

    void depositCallbackOrder(GameDepositRequest gameDepositRequest);

    void withdrawOrder(GameWithdrawalRequest gameWithdrawalRequest,Customers customers);

    void addCoins(GameAddCoinsVo gameAddCoinsVo, Customers customers);
}
