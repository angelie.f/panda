package com.moxi.mogublog.xo.vo;

import lombok.Data;
import org.joda.time.DateTime;

@Data
public class DateTimeVo {
    private DateTime startTime;
    private DateTime endTime;

    public DateTimeVo(DateTime startTime, DateTime endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }
}
