package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.GameAppReport;
import com.moxi.mogublog.xo.vo.GameAppReportVo;
import com.moxi.mougblog.base.service.SuperService;

public interface GameAppReportService extends SuperService<GameAppReport> {
    IPage<GameAppReport> getPageList(GameAppReportVo gameAppReportVo);
}
