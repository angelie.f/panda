package com.moxi.mogublog.xo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.BooksProject;
import com.moxi.mogublog.commons.entity.PandaProject;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.mapper.PandaProjectMapper;
import com.moxi.mogublog.xo.service.PandaProjectService;
import com.moxi.mogublog.xo.vo.BooksProjectVO;
import com.moxi.mogublog.xo.vo.ProjectVO;
import com.moxi.mougblog.base.enums.EStatus;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;


@Service
public class PandaProjectServiceImpl extends SuperServiceImpl<PandaProjectMapper, PandaProject> implements PandaProjectService {


    @Override
    public IPage<PandaProject> getPageList(ProjectVO projectVO) {
        QueryWrapper<PandaProject> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(projectVO.getName()) && !StringUtils.isEmpty(projectVO.getName())) {
            queryWrapper.like(SQLConf.NAME, projectVO.getName().trim());
        }

        Page<PandaProject> page = new Page<>();
        page.setCurrent(projectVO.getCurrentPage());
        page.setSize(projectVO.getPageSize());
        queryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
        if(StringUtils.isNotEmpty(projectVO.getOrderByAscColumn())) {
            // 将驼峰转换成下划线
            String column = StringUtils.underLine(new StringBuffer(projectVO.getOrderByAscColumn())).toString();
            queryWrapper.orderByAsc(column);
        } else if(StringUtils.isNotEmpty(projectVO.getOrderByDescColumn())) {
            // 将驼峰转换成下划线
            String column = StringUtils.underLine(new StringBuffer(projectVO.getOrderByDescColumn())).toString();
            queryWrapper.orderByDesc(column);
        }
        IPage<PandaProject> pageList = page(page, queryWrapper);
        return pageList;
    }

    @Override
    public List<PandaProject> getList(ProjectVO projectVO) {
        QueryWrapper<PandaProject> queryWrapper = new QueryWrapper<>();

        if (projectVO.getSource() != null) {
            queryWrapper.eq(SQLConf.SOURCE, projectVO.getSource());
        }
        List<PandaProject> tagList = list(queryWrapper);
        return tagList;
    }

    @Override
    public String addTag(ProjectVO projectVO) {
        QueryWrapper<PandaProject> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SQLConf.NAME, projectVO.getName());
        if (getOne(queryWrapper) != null) {
            return ResultUtil.errorWithMessage(MessageConf.ENTITY_EXIST);
        }
        PandaProject project = new PandaProject();
        project.setName(projectVO.getName());
        project.setSource(Integer.toString(projectVO.getSource()));
        project.insert();
        return ResultUtil.successWithMessage(MessageConf.INSERT_SUCCESS);
    }

    @Override
    public String editTag(ProjectVO projectVO) {
        PandaProject project = getById(projectVO.getUid());

        if (project != null && !project.getName().equals(projectVO.getName())) {
            QueryWrapper<PandaProject> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq(SQLConf.NAME, projectVO.getName());
            queryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
            PandaProject tempTag = getOne(queryWrapper);
            if (tempTag != null) {
                return ResultUtil.errorWithMessage(MessageConf.ENTITY_EXIST);
            }
        }

        project.setName(projectVO.getName());
        project.setSource(Integer.toString(projectVO.getSource()));
        project.setStatus(EStatus.ENABLE);
        project.setUpdateTime(new Date());
        project.updateById();
        return ResultUtil.successWithMessage(MessageConf.UPDATE_SUCCESS);
    }

    @Override
    public String deleteBatchTag(List<ProjectVO> projectVOList) {
        if (projectVOList.size() <= 0) {
            return ResultUtil.errorWithMessage(MessageConf.PARAM_INCORRECT);
        }
        List<String> uids = new ArrayList<>();
        projectVOList.forEach(item -> {
            uids.add(item.getUid());
        });

        Collection<PandaProject> tagList = listByIds(uids);
        tagList.forEach(item -> {
            item.setUpdateTime(new Date());
            item.setStatus(EStatus.DISABLED);
        });

        Boolean save = updateBatchById(tagList);

        if (save) {
            return ResultUtil.successWithMessage(MessageConf.DELETE_SUCCESS);
        } else {
            return ResultUtil.errorWithMessage(MessageConf.DELETE_FAIL);
        }
    }
    @Override
    public IPage<PandaProject> sources(ProjectVO projectVO) {
        Page<PandaProject> page = new Page<>();
        page.setCurrent(projectVO.getCurrentPage());
        page.setSize(projectVO.getPageSize());
        QueryWrapper<PandaProject> queryWrapper = new QueryWrapper<>();
        queryWrapper.select(SQLConf.SOURCE,SQLConf.NAME);
        queryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
        queryWrapper.orderByAsc(SQLConf.SOURCE);
        IPage<PandaProject> iPage = baseMapper.selectPage(page, queryWrapper);
        return iPage;
    }
}
