package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.BooksSetting;
import com.moxi.mogublog.xo.vo.BooksSettingVO;
import com.moxi.mougblog.base.service.SuperService;

public interface BooksSettingService extends SuperService<BooksSetting> {
    /**
     * 获取漫画設定列表
     *
     * @param bookSettingVO
     * @return
     */
    IPage<BooksSetting> getPageList(BooksSettingVO bookSettingVO);

    /**
     * 编辑漫画設定
     *
     * @param bookSettingVO
     */
    String editSetting(BooksSettingVO bookSettingVO);

}
