package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.GameType;
import com.moxi.mogublog.xo.vo.GameTypeVO;
import com.moxi.mougblog.base.service.SuperService;

import java.util.List;


public interface GameTypeService extends SuperService<GameType> {
    IPage<GameType> getPageList(GameTypeVO gameTypeVO);
    List<GameType> getList(GameTypeVO gameTypeVO);
    String add(GameTypeVO gameTypeVO);
    String edit(GameTypeVO gameTypeVO);
    IPage<GameType> gameTypes(GameTypeVO gameTypeVO);
}
