package com.moxi.mogublog.xo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.GameAppReport;
import com.moxi.mogublog.commons.entity.GameWithdrawalRequest;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.mapper.GameAppReportMapper;
import com.moxi.mogublog.xo.service.GameAppReportService;
import com.moxi.mogublog.xo.vo.GameAppReportVo;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class GameAppReportServiceImpl extends SuperServiceImpl<GameAppReportMapper, GameAppReport> implements GameAppReportService {

    @Autowired
    private GameAppReportMapper gameAppReportMapper;

    @Override
    public IPage<GameAppReport> getPageList(GameAppReportVo gameAppReportVo) {
        QueryWrapper<GameAppReport> queryWrapper = new QueryWrapper<>();

        if (StringUtils.isNotBlank(gameAppReportVo.getSource())) {
            queryWrapper.eq(SQLConf.SOURCE, gameAppReportVo.getSource().trim());
        }
        if (StringUtils.isNotBlank(gameAppReportVo.getBeginCreateTime()) && gameAppReportVo.getBeginCreateTime().equals(gameAppReportVo.getEndCreateTime())){
            queryWrapper.like(SQLConf.PERIOD, gameAppReportVo.getEndCreateTime().trim());
        }else {
            if (StringUtils.isNotEmpty(gameAppReportVo.getBeginCreateTime())) {
                queryWrapper.ge(SQLConf.PERIOD, gameAppReportVo.getBeginCreateTime().trim());
            }
            if (StringUtils.isNotEmpty(gameAppReportVo.getEndCreateTime())) {
                queryWrapper.le(SQLConf.PERIOD, gameAppReportVo.getEndCreateTime().trim());
            }
        }
        Page<GameAppReport> page = new Page<>();
        page.setCurrent(gameAppReportVo.getCurrentPage());
        page.setSize(gameAppReportVo.getPageSize());
        queryWrapper.orderByDesc(SQLConf.UID);
        IPage<GameAppReport> pageList = gameAppReportMapper.getPage(page, queryWrapper);
        return pageList;
    }
}
