package com.moxi.mogublog.xo;

import com.moxi.mogublog.xo.vo.CallbackWithdrawalVO;
import lombok.Data;

@Data
public class CallbackWithdrawalRequest {

    private CallbackWithdrawalVO param;
}
