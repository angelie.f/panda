package com.moxi.mogublog.xo.service.impl;


import cn.hutool.core.lang.Assert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.Banner;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.mapper.BannerMapper;
import com.moxi.mogublog.xo.service.BannerService;
import com.moxi.mogublog.xo.vo.BannerVo;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Slf4j
@Service
public class BannerServiceImpl extends SuperServiceImpl<BannerMapper, Banner> implements BannerService {

    @Autowired
    private BannerService bannerService;
    private final static String regex = "^,*|,*$";

    @Override
    public String addBanner(BannerVo bannerVo) {
            bannerVo.setShowSource(bannerVo.getShowSource().replaceAll(regex,""));
            Banner banner = new Banner();
            BeanUtils.copyProperties(bannerVo,banner);
            bannerService.save(banner);
            return ResultUtil.successWithMessage(MessageConf.INSERT_SUCCESS);
    }

    @Override
    public String editBanner(BannerVo bannerVo) {
            if(StringUtils.isNotEmpty(bannerVo.getShowSource())){
                bannerVo.setShowSource(bannerVo.getShowSource().replaceAll(regex,""));
            }
            Banner banner = bannerService.getById(bannerVo.getUid());
            Assert.notNull(banner, MessageConf.PARAM_INCORRECT);
            banner.setImageUrl(bannerVo.getImageUrl());
            banner.setTitle(bannerVo.getTitle());
            banner.setFileUid(bannerVo.getFileUid());
            banner.setJumpUrl(bannerVo.getJumpUrl());
            banner.setOpenFlag(bannerVo.getOpenFlag());
            banner.setSort(bannerVo.getSort());
            banner.setStatus(bannerVo.getStatus());
            banner.setShowSource(bannerVo.getShowSource());
            banner.setBannerType(bannerVo.getBannerType());
            banner.updateById();
            return ResultUtil.successWithMessage(MessageConf.OPERATION_SUCCESS);
    }

    @Override
    public IPage<Banner> getPageList(BannerVo bannerVo) {
        QueryWrapper<Banner> queryWrapper = new QueryWrapper<>();
        if(bannerVo.getBannerType()!=null){
            queryWrapper.eq("banner_type",bannerVo.getBannerType());
        }
        Page<Banner> page = new Page<>();
        page.setCurrent(bannerVo.getCurrentPage());
        page.setSize(bannerVo.getPageSize());
        //queryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
        queryWrapper.orderByDesc(SQLConf.CREATE_TIME);
        IPage<Banner> pageList = bannerService.page(page, queryWrapper);
        return pageList;
    }
}
