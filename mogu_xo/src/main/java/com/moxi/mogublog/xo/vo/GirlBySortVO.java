package com.moxi.mogublog.xo.vo;

import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;

/**
 * GirlBySortVO
 *
 * @author Vergift
 * @since 2022-01-12
 */
@Data
public class GirlBySortVO extends BaseVO<GirlBySortVO> {

    /**
     * 分类uid
     */
    private String sortUid;

    /**
     * 姓名
     */
    private String name;
}
