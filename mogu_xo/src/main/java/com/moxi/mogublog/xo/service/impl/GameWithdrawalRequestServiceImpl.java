package com.moxi.mogublog.xo.service.impl;

import cn.hutool.core.lang.Assert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.*;
import com.moxi.mogublog.utils.RedisUtil;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.mapper.CustomersMapper;
import com.moxi.mogublog.xo.mapper.GameCustomersBalanceMapper;
import com.moxi.mogublog.xo.mapper.GameWithdrawalRequestMapper;
import com.moxi.mogublog.xo.service.CoinsChangeService;
import com.moxi.mogublog.xo.service.CustomersService;
import com.moxi.mogublog.xo.service.GameCreditLogsService;
import com.moxi.mogublog.xo.service.GameWithdrawalRequestService;
import com.moxi.mogublog.xo.vo.*;
import com.moxi.mougblog.base.global.BaseMessageConf;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class GameWithdrawalRequestServiceImpl extends SuperServiceImpl<GameWithdrawalRequestMapper, GameWithdrawalRequest> implements GameWithdrawalRequestService {
    @Autowired
    private GameCustomersBalanceMapper gameCustomersBalanceMapper;
    @Autowired
    private CustomersMapper customersMapper;
    @Autowired
    private GameWithdrawalRequestMapper gameWithdrawalRequestMapper;
    @Autowired
    private GameCreditLogsService gameCreditLogsService;

    @Autowired
    private CoinsChangeService coinsChangeService;
    @Autowired
    RedisUtil redisUtil;

    @Override
    public IPage<GameWithdrawalRequest> getPageList(GameWithdrawalRequestVo gameWithdrawalRequestVo) {
        QueryWrapper<GameWithdrawalRequest> queryWrapper = new QueryWrapper<>();

        if (StringUtils.isNotBlank(gameWithdrawalRequestVo.getCustomerId())) {
            queryWrapper.eq(SQLConf.CUSTOMER_ID, gameWithdrawalRequestVo.getCustomerId().trim());
        }
        if (StringUtils.isNotBlank(gameWithdrawalRequestVo.getOpenId())) {
            queryWrapper.eq(SQLConf.OPEN_ID, gameWithdrawalRequestVo.getOpenId().trim());
        }

        if (StringUtils.isNotBlank(gameWithdrawalRequestVo.getRequestId())) {
            queryWrapper.eq(SQLConf.REQUEST_ID, gameWithdrawalRequestVo.getRequestId().trim());
        }

        if (gameWithdrawalRequestVo.getOrderStatus() !=null) {
            queryWrapper.eq(SQLConf.FLAG, gameWithdrawalRequestVo.getOrderStatus());
        }

        if (StringUtils.isNotBlank(gameWithdrawalRequestVo.getBeginCreateTime()) && gameWithdrawalRequestVo.getBeginCreateTime().equals(gameWithdrawalRequestVo.getEndCreateTime())){
            queryWrapper.like(SQLConf.CREATE_TIME, gameWithdrawalRequestVo.getEndCreateTime().trim());
        }else {
            if (StringUtils.isNotEmpty(gameWithdrawalRequestVo.getBeginCreateTime())) {
                queryWrapper.ge(SQLConf.CREATE_TIME, gameWithdrawalRequestVo.getBeginCreateTime().trim());
            }
            if (StringUtils.isNotEmpty(gameWithdrawalRequestVo.getEndCreateTime())) {
                queryWrapper.le(SQLConf.CREATE_TIME, gameWithdrawalRequestVo.getEndCreateTime().trim());
            }
        }

        Page<GameWithdrawalRequest> page = new Page<>();
        page.setCurrent(gameWithdrawalRequestVo.getCurrentPage());
        page.setSize(gameWithdrawalRequestVo.getPageSize());
        //queryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
        queryWrapper.orderByDesc(SQLConf.CREATE_TIME);
        IPage<GameWithdrawalRequest> pageList = this.page(page, queryWrapper);
        return pageList;
    }

    @Transactional(rollbackFor = { Exception.class })
    @Override
    public String createOrder(GameWithdrawalVO gameWithdrawalVO) {
        try {
            QueryWrapper<GameCustomersBalance> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("customer_id",gameWithdrawalVO.getCustomerId());
            GameCustomersBalance gameCustomersBalance = gameCustomersBalanceMapper.selectOne(queryWrapper);
            if (gameCustomersBalance.getCoins().compareTo(gameWithdrawalVO.getAmount())==-1){
                return ResultUtil.result(SysConf.ERROR, "用户额度不足，无法提款");
            }
            QueryWrapper<Customers> customersQueryWrapper = new QueryWrapper<>();
            customersQueryWrapper.eq("customer_id", gameWithdrawalVO.getCustomerId());
            Customers customers = customersMapper.selectOne(customersQueryWrapper);
            String url = "/withdrawal?userId="+customers.getUid()+"&amount="+gameWithdrawalVO.getDisAmount()+"&gameId=G2044";
            return url;
        }catch (Exception e){
            log.error("申请提现报错，{}",e);
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return ResultUtil.result(SysConf.ERROR, MessageConf.OPERATION_FAIL);
        }
    }


    @Override
    public Map<String, Object> callbackOrder(CallbackWithdrawalVO callbackWithdrawalVO) {
        Map<String, Object> resutMap = new HashMap<>();
        Map<String, Object> resutData = new HashMap<>();
        String requestId = callbackWithdrawalVO.getRequestId();
        QueryWrapper<GameWithdrawalRequest> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("third_order_no", requestId);
        queryWrapper.eq("flag",0);
        GameWithdrawalRequest gameWithdrawalRequest = gameWithdrawalRequestMapper.selectOne(queryWrapper);
        if (gameWithdrawalRequest!=null) {
            if ("1".equals(callbackWithdrawalVO.getStatus())) { //提款回调成功

                String key = "gameBalance_"+gameWithdrawalRequest.getCustomerId();
                boolean isLock = false;
                Integer tryTimes=0;
                while(!isLock){
                    isLock = redisUtil.setIfAbsent(key,"",30*1000, TimeUnit.MILLISECONDS);
                    if (isLock) {
                        try {
                            coinsChangeService.withdrawalCallbackOrderSuccess(gameWithdrawalRequest,callbackWithdrawalVO);
                            resutMap.put("code", "200");
                            resutMap.put("msg", "成功");
                            resutMap.put("data", resutData);
                        } finally {
                            if (redisUtil.hasKey(key))
                                redisUtil.delete(key);
                        }
                    }else {
                        try {
                            Thread.sleep(500);
                            tryTimes+=1;
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    if (tryTimes>10){
                        resutMap.put("code", "500");
                        resutMap.put("msg", "系统繁忙");
                        resutMap.put("data", resutData);
                    }
                }
            } else if ("-1".equals(callbackWithdrawalVO.getStatus())) { //提款回调拒绝

                String key = "gameBalance_"+gameWithdrawalRequest.getCustomerId();
                boolean isLock = false;
                Integer tryTimes=0;
                while(!isLock){
                    isLock = redisUtil.setIfAbsent(key,"",30*1000, TimeUnit.MILLISECONDS);
                    if (isLock) {
                        try {
                            coinsChangeService.withdrawalCallbackOrderFail(gameWithdrawalRequest,callbackWithdrawalVO);
                            resutMap.put("code", "200");
                            resutMap.put("msg", "成功");
                            resutMap.put("data", resutData);
                        } finally {
                            if (redisUtil.hasKey(key))
                                redisUtil.delete(key);
                        }
                    }else {
                        try {
                            Thread.sleep(500);
                            tryTimes+=1;
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    if (tryTimes>10){
                        resutMap.put("code", "500");
                        resutMap.put("msg", "系统繁忙");
                        resutMap.put("data", resutData);
                    }
                }
            }
        }else {
            resutMap.put("code", "500");
            resutMap.put("msg", "查无此提案");
            resutMap.put("data", resutData);
        }
        return resutMap;
    }

    @Override
    public IPage<GameWithdrawalRequest> queryWithdrawalList(GameWithdrawalRequestVo gameWithdrawalRequestVo) {
        Page<GameWithdrawalRequest> page = new Page<>();
        page.setCurrent(gameWithdrawalRequestVo.getCurrentPage());
        page.setSize(gameWithdrawalRequestVo.getPageSize());
        IPage<GameWithdrawalRequest> pageList = gameWithdrawalRequestMapper.queryWithdrawalList(page, gameWithdrawalRequestVo);
        return pageList;
    }

    @Override
    public GameDepositTotalVo getWithdrawalTotal(GameWithdrawalRequestVo gameWithdrawalRequestVo) {
        GameDepositTotalVo gameDepositTotalVo = new GameDepositTotalVo();
        BigDecimal sumAmount = gameWithdrawalRequestMapper.queryWithdrawalSunAmout(gameWithdrawalRequestVo);
        int totalTimes = gameWithdrawalRequestMapper.queryWithdrawalTimesTotal(gameWithdrawalRequestVo);
        gameDepositTotalVo.setSumAmount(sumAmount);
        gameDepositTotalVo.setTotalTimes(totalTimes);
        return gameDepositTotalVo;
}

    @Override
    public String editWithdrawalInfo(GameWithdrawalBackVo gameWithdrawalBackVo) {
        GameWithdrawalRequest gameWithdrawalRequest = gameWithdrawalRequestMapper.selectById(gameWithdrawalBackVo.getUid());
        Assert.notNull(gameWithdrawalRequest, MessageConf.PARAM_INCORRECT);
        gameWithdrawalRequest.setRemarks(gameWithdrawalBackVo.getRemarks());
        gameWithdrawalRequestMapper.updateById(gameWithdrawalRequest);
        return ResultUtil.result(SysConf.SUCCESS, MessageConf.OPERATION_SUCCESS);
    }

}
