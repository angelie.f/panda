package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.GameType;
import com.moxi.mougblog.base.mapper.SuperMapper;


public interface GameTypeMapper extends SuperMapper<GameType> {

}
