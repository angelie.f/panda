package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.GirlProject;
import com.moxi.mougblog.base.mapper.SuperMapper;

/**
 * 妹子项目表 Mapper 接口
 *
 * @author Vergift
 * @since 2021-12-29
 */
public interface GirlProjectMapper extends SuperMapper<GirlProject> {

}
