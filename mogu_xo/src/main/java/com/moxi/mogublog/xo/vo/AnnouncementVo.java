package com.moxi.mogublog.xo.vo;

import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;

@Data
public class AnnouncementVo extends BaseVO<AnnouncementVo> {
    private String content;
    private String title;
}
