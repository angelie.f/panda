package com.moxi.mogublog.xo.vo;

import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;
@ToString
@Data
public class GameDepositConfigVo extends BaseVO<GameDepositConfigVo> {

    private BigDecimal price;
    private Integer coins;
    private Integer promotionCoins;
    private Integer depositType;
    private String description;
}
