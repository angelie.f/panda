package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.GirlConstant;
import com.moxi.mogublog.commons.entity.GirlGift;
import com.moxi.mogublog.commons.entity.GirlOrder;
import com.moxi.mogublog.xo.vo.GirlGiftVO;
import com.moxi.mogublog.xo.vo.GirlOrderVO;
import com.moxi.mougblog.base.service.SuperService;


/**
 * 妹子常量表 服务类
 *
 * @author 陌溪
 * @date 2021-12-29
 */
public interface GirlGiftService extends SuperService<GirlGift> {
    IPage<GirlGift> getPageList(GirlGiftVO girlGiftVO);
}
