package com.moxi.mogublog.xo.vo;

import com.moxi.mougblog.base.validator.annotion.IntegerNotNull;
import com.moxi.mougblog.base.validator.group.Insert;
import com.moxi.mougblog.base.validator.group.Update;
import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;
import lombok.ToString;

/**
 * 图集实体查询参数类
 *
 * @author 陌溪
 * @date 2018年9月17日16:10:38
 */
@ToString
@Data
public class PictureAtlasBySortVO extends BaseVO<PictureAtlasBySortVO> {

    /**
     * 图集名
     */
    private String name;

    /**
     * 分类Uid
     */
    private String sortUid;

}
