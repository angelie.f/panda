package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.GameCustomersBalance;
import com.moxi.mougblog.base.mapper.SuperMapper;

public interface GameCustomersBalanceMapper extends SuperMapper<GameCustomersBalance> {
}
