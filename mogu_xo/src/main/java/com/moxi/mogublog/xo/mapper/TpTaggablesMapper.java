package com.moxi.mogublog.xo.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.moxi.mogublog.commons.entity.TpBooks;
import com.moxi.mogublog.commons.entity.TpTaggables;
import com.moxi.mougblog.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;


public interface TpTaggablesMapper extends SuperMapper<TpTaggables> {
    @Select(" select taggable_id from tp_taggables where  taggable_type ='Book' and taggable_id !=#{bookId} and tag_id in " +
            " (select tag_id from tp_taggables where taggable_type ='Book' and taggable_id =#{bookId}) ")
    List<Integer> getRelatedBookIds(@Param("bookId") Integer bookId);

    @Select(" select ttg.taggable_id from tp_taggables ttg " +
            " left join tp_tags tt on ttg.tag_id =tt.id " +
            " ${ew.customSqlSegment}")
    List<Integer> queryBookIdsByTagName(@Param(Constants.WRAPPER) QueryWrapper queryWrapper);

    @Select(" select distinct ttg.taggable_id from tp_taggables ttg " +
            " left join tp_tags tt on ttg.tag_id =tt.id " +
            " left join tp_topics ttp on tt.name =ttp.tag " +
            " ${ew.customSqlSegment}")
    List<Integer> queryBookIdsByTopics(@Param(Constants.WRAPPER) QueryWrapper queryWrapper);

    @Insert("<script>" +
            " INSERT INTO tp_taggables" +
            "  (tag_id, taggable_type, taggable_id) " +
            " VALUES " +
            " <foreach collection='list' item='taggables' separator=','>" +
            " (" +
            "  #{taggables.tagId},'Book',#{taggables.taggableId}" +
            " )" +
            "</foreach> " +
            " ON DUPLICATE KEY UPDATE " +
            " tag_id=VALUES(tag_id),taggable_id=VALUES(taggable_id) " +
            "</script>"
    )
    int insertOrUpdateBatch(@Param("list") List<TpTaggables> list);

}
