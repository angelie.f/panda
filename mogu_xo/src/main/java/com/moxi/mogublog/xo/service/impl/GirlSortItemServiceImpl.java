package com.moxi.mogublog.xo.service.impl;

import com.moxi.mogublog.commons.entity.GirlSortItem;
import com.moxi.mogublog.xo.mapper.GirlSortItemMapper;
import com.moxi.mogublog.xo.service.GirlSortItemService;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 妹子上下架表 服务实现类
 *
 * @author Vergift
 * @date 2021-12-29
 */
@Service
public class GirlSortItemServiceImpl extends SuperServiceImpl<GirlSortItemMapper, GirlSortItem> implements GirlSortItemService {

}
