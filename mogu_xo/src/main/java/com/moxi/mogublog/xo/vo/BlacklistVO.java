package com.moxi.mogublog.xo.vo;

import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;

@Data
public class BlacklistVO extends BaseVO<BlacklistVO> {

    private Integer valiUid;

    private String reason;

    private Integer isBlack;

    private String phone;


}
