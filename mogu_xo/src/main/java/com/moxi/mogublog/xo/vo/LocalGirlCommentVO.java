package com.moxi.mogublog.xo.vo;

import lombok.Data;
import lombok.ToString;

/**
 * 同城约妹参数类
 *
 * @author Vergift
 * @date 2021-12-29
 */
@ToString
@Data
public class LocalGirlCommentVO {

    private Integer uid;
    /**
     * 用户id
     */
    private String customerId;

    /**
     * 第三方用户id
     */
    private String productId;

    /**
     * 注册来源：1.青青草 2.小黄鸭
     */
    private String source;

    /**
     * 第三方code
     */
    private String openCode;

    private Integer girlId;

    private Float score;

    private Float score1;

    private Float score2;

    /**
     * 订单ID
     */
    private String orderId;

    /**
     * 评论内容
     */
    private String content;

    /**
     * 回复某条评论的uid
     */
    private Integer toUid;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 一级评论uid
     */
    private Integer firstCommentUid;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 更新时间
     */
    private String updateTime;

    /**
     * 签名
     */
    private String sign;

    /**
     * 访问请求终端 H5
     */
    private String endPoint;

    /**
     * 当前页
     */
    private Long currentPage;

    /**
     * 页大小
     */
    private Long pageSize;

}
