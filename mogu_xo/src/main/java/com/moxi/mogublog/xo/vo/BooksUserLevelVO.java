package com.moxi.mogublog.xo.vo;

import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;

/**
 * BooksUserLevelVO
 * @author Sid
 * @since 2022-05-31
 */
@Data
public class BooksUserLevelVO extends BaseVO<BooksUserLevelVO> {



    /**
     * OrderBy排序字段（desc: 降序）
     */
    private String orderByDescColumn;

    /**
     * OrderBy排序字段（asc: 升序）
     */
    private String orderByAscColumn;

    /**
     * 会员等级ID
     */
    private Integer levelId;

    /**
     * 会员等级名称
     */
    private String levelName;

    /**
     * 漫画项目source
     */
    private Integer source;

}
