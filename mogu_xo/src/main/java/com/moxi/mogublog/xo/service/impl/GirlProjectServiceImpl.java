package com.moxi.mogublog.xo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.Admin;
import com.moxi.mogublog.commons.entity.GirlProject;
import com.moxi.mogublog.commons.entity.GirlUserCost;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.mapper.GirlProjectMapper;
import com.moxi.mogublog.xo.service.AdminService;
import com.moxi.mogublog.xo.service.GirlProjectService;
import com.moxi.mogublog.xo.vo.GirlProjectVO;
import com.moxi.mougblog.base.enums.EStatus;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 妹子项目表 服务实现类
 *
 * @author Vergift
 * @date 2021-11-29
 */
@Service
public class GirlProjectServiceImpl extends SuperServiceImpl<GirlProjectMapper, GirlProject> implements GirlProjectService {

    @Autowired
    private GirlProjectService girlProjectService;

    @Autowired
    private AdminService adminService;

    @Override
    public IPage<GirlProject> getPageList(GirlProjectVO girlProjectVO) {
        QueryWrapper<GirlProject> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(girlProjectVO.getName()) && !StringUtils.isEmpty(girlProjectVO.getName())) {
            queryWrapper.like(SQLConf.NAME, girlProjectVO.getName().trim());
        }

        Page<GirlProject> page = new Page<>();
        page.setCurrent(girlProjectVO.getCurrentPage());
        page.setSize(girlProjectVO.getPageSize());
        queryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
        if(StringUtils.isNotEmpty(girlProjectVO.getOrderByAscColumn())) {
            // 将驼峰转换成下划线
            String column = StringUtils.underLine(new StringBuffer(girlProjectVO.getOrderByAscColumn())).toString();
            queryWrapper.orderByAsc(column);
        } else if(StringUtils.isNotEmpty(girlProjectVO.getOrderByDescColumn())) {
            // 将驼峰转换成下划线
            String column = StringUtils.underLine(new StringBuffer(girlProjectVO.getOrderByDescColumn())).toString();
            queryWrapper.orderByDesc(column);
        }
        IPage<GirlProject> pageList = girlProjectService.page(page, queryWrapper);
        return pageList;
    }

    @Override
    public List<GirlProject> getList(GirlProjectVO girlProjectVO) {
        QueryWrapper<GirlProject> queryWrapper = new QueryWrapper<>();

        if (girlProjectVO.getSource() != null) {
            queryWrapper.eq(SQLConf.SOURCE, girlProjectVO.getSource());
        }
        List<GirlProject> tagList = girlProjectService.list(queryWrapper);
        return tagList;
    }

    @Override
    public String addGirlProject(GirlProjectVO girlProjectVO) {
        QueryWrapper<GirlProject> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SQLConf.NAME, girlProjectVO.getName()).or().eq(SQLConf.SOURCE, girlProjectVO.getSource());
        if (girlProjectService.getOne(queryWrapper) != null) {
            return ResultUtil.errorWithMessage(MessageConf.PROJECT_EXIST);
        }
        GirlProject project = new GirlProject();
        project.setName(girlProjectVO.getName());
        project.setProKey(girlProjectVO.getProKey());
        project.setSource(girlProjectVO.getSource());
        project.setIsVip(girlProjectVO.getIsVip());
        project.setStatus(EStatus.ENABLE);
        project.setOnOff(girlProjectVO.getOnOff());
        project.setAuthentication(girlProjectVO.getAuthentication());
        project.insert();
        //新增项目是默认添加一条普通用户的费用
        GirlUserCost girlUserCost = new GirlUserCost();
        girlUserCost.setProjectUid(Integer.valueOf(project.getUid()));
        girlUserCost.setCost(new BigDecimal(500));
        girlUserCost.setLevelName(MessageConf.ORDINARY_VIP);
        girlUserCost.insert();
        return ResultUtil.successWithMessage(MessageConf.INSERT_SUCCESS);
    }

    @Override
    public String editGirlProject(GirlProjectVO girlProjectVO) {
        GirlProject project = girlProjectService.getById(girlProjectVO.getUid());

        if (project != null) {
            if (project.getName().equals(girlProjectVO.getName()) && !project.getSource().equals(girlProjectVO.getSource())){
                QueryWrapper<GirlProject> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq(SQLConf.SOURCE, girlProjectVO.getSource());
                List<GirlProject> projectList = girlProjectService.list(queryWrapper);
                if (projectList.size() != 0) {
                    return ResultUtil.errorWithMessage(MessageConf.PROJECT_SOURCE_EXIST);
                }
            } else if(project.getSource().equals(girlProjectVO.getSource()) && !project.getName().equals(girlProjectVO.getName())) {
                QueryWrapper<GirlProject> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq(SQLConf.NAME, girlProjectVO.getName());
                List<GirlProject> projectList = girlProjectService.list(queryWrapper);
                if (projectList.size() != 0) {
                    return ResultUtil.errorWithMessage(MessageConf.PROJECT_NAME_EXIST);
                }
            }else if(!project.getSource().equals(girlProjectVO.getSource()) && !project.getName().equals(girlProjectVO.getName())) {
                QueryWrapper<GirlProject> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq(SQLConf.NAME, girlProjectVO.getName()).or().eq(SQLConf.SOURCE, girlProjectVO.getSource());
                List<GirlProject> projectList = girlProjectService.list(queryWrapper);
                if (projectList.size() != 0) {
                    return ResultUtil.errorWithMessage(MessageConf.PROJECT_EXIST);
                }
            }
        }
        project.setSource(girlProjectVO.getSource());
        project.setName(girlProjectVO.getName());
        project.setIsVip(girlProjectVO.getIsVip());
        project.setOnOff(girlProjectVO.getOnOff());
        project.setAuthentication(girlProjectVO.getAuthentication());
        project.setUpdateTime(new Date());
        project.updateById();
        return ResultUtil.successWithMessage(MessageConf.UPDATE_SUCCESS);
    }

    //获取当前用户账号和所属项目
    @Override
    public GirlProject initPorject(){
        ServletRequestAttributes attribute = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attribute.getRequest();
        // 解析出请求者的ID和用户名
        String adminUid = request.getAttribute(SysConf.ADMIN_UID).toString();
        Admin admin = adminService.getById(adminUid);
        return girlProjectService.getById(admin.getProjectUid());
    }
}
