package com.moxi.mogublog.xo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.*;
import com.moxi.mogublog.utils.*;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.mapper.*;
import com.moxi.mogublog.xo.service.CoinsChangeService;
import com.moxi.mogublog.xo.service.GameCreditLogsService;
import com.moxi.mogublog.xo.service.GameDepositRequestService;
import com.moxi.mogublog.xo.vo.CallbackOrderVO;
import com.moxi.mogublog.xo.vo.GameDepositRequestVo;
import com.moxi.mogublog.xo.vo.GameDepositTotalVo;
import com.moxi.mogublog.xo.vo.GameDepositVO;
import com.moxi.mougblog.base.enums.EStatus;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class GameDepositRequestServiceImpl extends SuperServiceImpl<GameDepositRequestMapper, GameDepositRequest> implements GameDepositRequestService {

    @Autowired
    private GameDepositRequestMapper gameDepositRequestMapper;
    @Autowired GameDepositRequestService gameDepositRequestService;
    @Autowired
    private CustomersMapper customersMapper;
    @Autowired
    private GameCustomersBalanceMapper gameCustomersBalanceMapper;
    @Autowired
    private GameCreditLogsService gameCreditLogsService;
    @Autowired
    private GameWithdrawalRequestMapper gameWithdrawalRequestMapper;

    @Autowired
    private GameDepositAwardMapper gameDepositAwardMapper;

    @Autowired
    private CoinsChangeService coinsChangeService;
    @Autowired
    RedisUtil redisUtil;
    @Override
    public String createOrder(GameDepositVO gameDepositVO) {
        QueryWrapper<Customers> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("customer_id", gameDepositVO.getCustomerId());
        Customers customers = customersMapper.selectOne(queryWrapper);
        GameDepositRequest gameDepositRequest = new GameDepositRequest();
        gameDepositRequest.setCustomerId(gameDepositVO.getCustomerId());
        gameDepositRequest.setLoginName(customers.getLoginName());
        gameDepositRequest.setOpenId(gameDepositVO.getOpenId());
        gameDepositRequest.setSource(gameDepositVO.getSource());
        gameDepositRequest.setAmount(gameDepositVO.getPrice());
        gameDepositRequest.setCoins(gameDepositVO.getCoins());
        gameDepositRequest.setFlag(0);
        gameDepositRequest.setRequestId("D"+StringUtils.getSnowflakeId());
        gameDepositRequestMapper.insert(gameDepositRequest);
        String url = "/newPay?name="+customers.getLoginName()+"&userId="+customers.getUid()+"&price="+gameDepositVO.getPrice()+"&desc=熊猫游戏"+"&gameId=G2044&orderId="+gameDepositRequest.getRequestId()+"&userAgent=h5";
        return url;
    }


    @Override
    public Map<String, Object> callbackOrder(CallbackOrderVO callbackOrderVO) {
        Map<String, Object> resutMap = new HashMap<>();
        Map<String, Object> resutData = new HashMap<>();

        if (callbackOrderVO.getOpType()==0) {
            QueryWrapper<GameDepositRequest> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("request_id", callbackOrderVO.getReferenceId());
            queryWrapper.eq("flag", EStatus.DISABLED);
            GameDepositRequest gameDepositRequest = gameDepositRequestMapper.selectOne(queryWrapper);
            if (gameDepositRequest != null) {
                if (StringUtils.isEmpty(gameDepositRequest.getCustomerId().trim())) {
                    log.error("订单的用户id为空！");
                    resutMap.put("code", "500");
                    resutMap.put("msg", "订单的用户id为空");
                    return resutMap;
                }
                String key = "gameBalance_"+gameDepositRequest.getCustomerId();
                boolean isLock = redisUtil.setIfAbsent(key,"",30*1000, TimeUnit.MILLISECONDS);
                if (isLock) {
                    try {
                        coinsChangeService.depositCallbackOrder(gameDepositRequest);
                        resutMap.put("code", "200");
                        resutData.put("customerId", gameDepositRequest.getOpenId());
                        resutData.put("balance", gameDepositRequest.getAmount());
                        resutMap.put("msg", "成功");
                        resutMap.put("data", resutData);
                    }finally {
                        if (redisUtil.hasKey(key))
                            redisUtil.delete(key);
                    }
                }else {
                    resutMap.put("code", "500");
                    resutMap.put("msg", "系统繁忙");
                    resutMap.put("data", resutData);
                }
            } else {
                resutMap.put("code", "200");
                resutMap.put("msg", "查无此单或已经上分成功");
            }
            return resutMap;
        }else {
//            QueryWrapper<GameWithdrawalRequest>query = new QueryWrapper<>();
//            query.eq("request_id",callbackOrderVO.getReferenceId());
//            GameWithdrawalRequest gameWithdrawalRequest = gameWithdrawalRequestMapper.selectOne(query);
//            if (gameWithdrawalRequest==null){
//                log.error("没有此条取款提案！");
//                resutMap.put("code", "500");
//                resutMap.put("msg", "没有此条取款提案");
//                return resutMap;
//            }
            QueryWrapper<Customers> customersQueryWrapper = new QueryWrapper<>();
            customersQueryWrapper.eq("uid", callbackOrderVO.getCustomerId());
            Customers customers = customersMapper.selectOne(customersQueryWrapper);
            GameWithdrawalRequest gameWithdrawalRequest = new GameWithdrawalRequest();
            gameWithdrawalRequest.setCustomerId(customers.getCustomerId());
            gameWithdrawalRequest.setOpenId(customers.getOpenId());
            gameWithdrawalRequest.setSource(customers.getSource());
            if (callbackOrderVO.getAmount().compareTo(new BigDecimal(475))==1){
                gameWithdrawalRequest.setAmount(callbackOrderVO.getAmount().divide(new BigDecimal(0.97),0, BigDecimal.ROUND_HALF_UP));
            }else {
                gameWithdrawalRequest.setAmount(callbackOrderVO.getAmount().divide(new BigDecimal(0.95),0, BigDecimal.ROUND_HALF_UP));
            }
            gameWithdrawalRequest.setLoginName(customers.getLoginName());
            gameWithdrawalRequest.setRemarks("");
            gameWithdrawalRequest.setFlag(0);
            gameWithdrawalRequest.setRequestId("W"+StringUtils.getSnowflakeId());
            gameWithdrawalRequest.setThirdOrderNo(callbackOrderVO.getReferenceId());
            Customers balanceCustomers = customersMapper.queryBalanceByCustomerId(customers.getCustomerId());
            if (balanceCustomers.getCoins().compareTo(gameWithdrawalRequest.getAmount())==-1){
                log.error("用户额度不足！，当前额度{},用户uid{},取款额度{}",balanceCustomers.getCoins(),customers.getUid(),gameWithdrawalRequest.getAmount());
                resutMap.put("code", "500");
                resutMap.put("msg", "用户额度不足");
                return resutMap;
            }
            String key = "gameBalance_"+gameWithdrawalRequest.getCustomerId();
            boolean isLock = redisUtil.setIfAbsent(key,"",30*1000, TimeUnit.MILLISECONDS);
            if (isLock) {
                try {
                    coinsChangeService.withdrawOrder(gameWithdrawalRequest,customers);
                    resutMap.put("code", "200");
                    resutMap.put("msg", "成功");
                } finally {
                if (redisUtil.hasKey(key))
                    redisUtil.delete(key);
                }
            }else {
                resutMap.put("code", "500");
                resutMap.put("msg", "系统繁忙");
            }
            return resutMap;
        }
    }

    @Override
    public IPage<GameDepositRequest> getOfficePageList(GameDepositRequestVo gameDepositRequestVo) {
        Page<GameDepositRequest> page = new Page<>();
        page.setCurrent(gameDepositRequestVo.getCurrentPage());
        page.setSize(gameDepositRequestVo.getPageSize());
//        if(!ObjectUtils.isEmpty(gameDepositRequestVo.getIsFirstDep())){
//            gameDepositRequestVo.setOrderStatus(null);
//       }
        IPage<GameDepositRequest> pageList = gameDepositRequestMapper.queryDepRequestList(page, gameDepositRequestVo);
        for (GameDepositRequest item:pageList.getRecords()) {
            if(item.getIsFirstDep()==1||item.getIsFirstDep()==2){
                item.setIsFirstDepShow(item.getIsFirstDep());
            }
            else{
                String createTime = DateUtils.formateDate(item.getCreateTime(),"yyyy-MM-dd HH:mm:ss");
                int count= gameDepositRequestMapper.countDeposit(item.getOpenId(),createTime);
                log.info("openId:{},createTime:{},isUserDeposit:{}",item.getOpenId(),createTime,count);
                item.setIsFirstDepShow(count>0?2:1);
            }
        }
        return pageList;
    }

    @Override
    public IPage<GameDepositRequest> getPageList(GameDepositRequestVo gameDepositRequestVo) {
        QueryWrapper<GameDepositRequest> queryWrapper = new QueryWrapper<>();

        if (StringUtils.isNotBlank(gameDepositRequestVo.getCustomerId())) {
            queryWrapper.eq(SQLConf.CUSTOMER_ID, gameDepositRequestVo.getCustomerId().trim());
        }
        if (StringUtils.isNotBlank(gameDepositRequestVo.getOpenId())) {
            queryWrapper.eq(SQLConf.OPEN_ID, gameDepositRequestVo.getOpenId().trim());
        }

        if (StringUtils.isNotBlank(gameDepositRequestVo.getRequestId())) {
            queryWrapper.eq(SQLConf.REQUEST_ID, gameDepositRequestVo.getRequestId().trim());
        }

        if (gameDepositRequestVo.getOrderStatus() !=null) {
            queryWrapper.eq(SQLConf.FLAG, gameDepositRequestVo.getOrderStatus());
        }

        if (StringUtils.isNotBlank(gameDepositRequestVo.getBeginCreateTime()) && gameDepositRequestVo.getBeginCreateTime().equals(gameDepositRequestVo.getEndCreateTime())){
            queryWrapper.like(SQLConf.CREATE_TIME, gameDepositRequestVo.getEndCreateTime().trim());
        }else {
            if (StringUtils.isNotEmpty(gameDepositRequestVo.getBeginCreateTime())) {
                queryWrapper.ge(SQLConf.CREATE_TIME, gameDepositRequestVo.getBeginCreateTime().trim());
            }
            if (StringUtils.isNotEmpty(gameDepositRequestVo.getEndCreateTime())) {
                queryWrapper.le(SQLConf.CREATE_TIME, gameDepositRequestVo.getEndCreateTime().trim());
            }
        }

        Page<GameDepositRequest> page = new Page<>();
        page.setCurrent(gameDepositRequestVo.getCurrentPage());
        page.setSize(gameDepositRequestVo.getPageSize());
        //queryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
        queryWrapper.orderByDesc(SQLConf.CREATE_TIME);
        IPage<GameDepositRequest> pageList = gameDepositRequestService.page(page, queryWrapper);
        return pageList;
    }

    @Override
    public GameDepositTotalVo getDepositTotal(GameDepositRequestVo gameDepositRequestVo) {
        GameDepositTotalVo gameDepositTotalVo = new GameDepositTotalVo();
//        if(!ObjectUtils.isEmpty(gameDepositRequestVo.getIsFirstDep())){
//            gameDepositRequestVo.setOrderStatus(null);
//        }
        BigDecimal sumAmount = gameDepositRequestMapper.queryDepRequestSunAmout(gameDepositRequestVo);
        int totalTimes = gameDepositRequestMapper.queryDepRequestTimesTotal(gameDepositRequestVo);
        gameDepositTotalVo.setSumAmount(sumAmount);
        gameDepositTotalVo.setTotalTimes(totalTimes);
        return gameDepositTotalVo;
    }
    @Override
    public Integer getFirstDepositNum() {
        return gameDepositRequestMapper.firstDepositToday();
    }

    @Override
    public Map getUserFirstDepositStatus(String customerId) {
        Map<String,String> map = new HashMap<>();
        map.put("customerId",customerId);
        GameDepositRequest gameDepositRequest=gameDepositRequestMapper.getFirstDeposit(customerId);
        if(gameDepositRequest==null){
            map.put("status","a");
        }
        else{
            if(DateUtils.isToday(gameDepositRequest.getPayTime())){
                map.put("status","c");
            }
            else{
                int isAwardBefore=gameDepositAwardMapper.isAwardBefore(customerId);
                //当日首存已开奖已中奖：中奖已发
                if(isAwardBefore>0){
                    map.put("status","d");
                }
                else{
                    map.put("status","b");
                }
            }
        }
        return map;
    }
}
