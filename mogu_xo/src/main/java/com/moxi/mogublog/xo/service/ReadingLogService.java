package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.ReadingLog;
import com.moxi.mogublog.xo.vo.LocalBooksVO;
import com.moxi.mogublog.xo.vo.ReadingLogVO;
import com.moxi.mougblog.base.service.SuperService;
import java.util.Map;

public interface ReadingLogService extends SuperService<ReadingLog> {

    Map getReadingLogInfo(LocalBooksVO localBooksVO);

    ReadingLog getReadingLog(LocalBooksVO localBooksVO);

    boolean updateReadingLog(LocalBooksVO localBooksVO);

    /**
     * 获取漫画阅读记录列表
     *
     * @param readingLogVO
     * @return
     */
    IPage<ReadingLog> getPageList(ReadingLogVO readingLogVO);

    String logDailyAvailable(LocalBooksVO localBooksVO);

    String getDailyAvailable(LocalBooksVO localBooksVO);
}

