package com.moxi.mogublog.xo.vo;

import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;
import lombok.ToString;

/**
 * 分类实体参数类
 *
 * @author Vergift
 * @date 2022-01-12
 */
@ToString
@Data
public class GirlSortVO extends BaseVO<GirlSortVO> {

    /**
     * 图集创建时间
     */
    private String createTime;

    /**
     * 更新时间
     */
    private String updateTime;

    /**
     * 妹子分类名称
     */
    private String name;

    /**
     * 分类介绍
     */
    private String introduce;

    /**
     * 分类图标图片uid
     */
    private String fileUid;

    /**
     * 排序字段，越大越靠前
     */
    private Integer sort;

    /**
     * 妹子标签uid:1,2,...
     */
    private String tagUid;

    /**
     * 妹子分类所属项目uid
     */
    private String projectUid;

}
