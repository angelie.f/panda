package com.moxi.mogublog.xo.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.moxi.mogublog.commons.entity.Announcement;
import com.moxi.mogublog.utils.ResultUtil;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.MessageConf;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.global.SysConf;
import com.moxi.mogublog.xo.mapper.AnnouncementMapper;
import com.moxi.mogublog.xo.service.AnnouncementService;
import com.moxi.mogublog.xo.vo.AnnouncementVo;
import com.moxi.mougblog.base.enums.EStatus;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Slf4j
@Service
public class AnnouncementServiceImpl extends SuperServiceImpl<AnnouncementMapper, Announcement> implements AnnouncementService {

    @Autowired
    private AnnouncementMapper announcementMapper;

    @Override
    public Announcement getAdvConfig() {
        QueryWrapper<Announcement> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(SQLConf.STATUS, EStatus.ENABLE);
        queryWrapper.last(SysConf.LIMIT_ONE);
        Announcement announcement =  announcementMapper.selectOne(queryWrapper);
        announcement.setContent(announcement.getContent().replaceAll(";","\n"));
        return announcement;
    }

    @Override
    public String editAdvConfig(AnnouncementVo announcementVo) {
        if(StringUtils.isNotBlank(announcementVo.getUid())){
            Announcement acvo = announcementMapper.selectById(announcementVo.getUid());
            acvo.setTitle(announcementVo.getTitle());
            acvo.setContent(announcementVo.getContent().replaceAll("\n",";"));
            acvo.updateById();
            return ResultUtil.successWithMessage(MessageConf.UPDATE_SUCCESS);
        }else{
            Announcement announcement = new Announcement();
            announcement.setStatus(1);
            announcement.setTitle(announcementVo.getTitle());
            announcement.setContent(announcementVo.getContent().replaceAll("\n",";"));
            announcementMapper.insert(announcement);
            return ResultUtil.successWithMessage(MessageConf.INSERT_SUCCESS);
        }
    }
}
