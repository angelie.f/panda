package com.moxi.mogublog.xo.service;

import com.moxi.mogublog.commons.entity.Customers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class GameActRecordAsyncService {
    @Autowired
    private GameActRecordService gameActRecordService;


    @Async("actLoginThreadPoolTaskExecutor")
    public Boolean addActRecord(Customers customersResult) {
        gameActRecordService.addLoginActRecord(customersResult);
        gameActRecordService.addVipActRecord(customersResult);
        gameActRecordService.addBindActRecord(customersResult);
        return true;
    }
}
