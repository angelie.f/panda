package com.moxi.mogublog.xo.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.GirlOrder;
import com.moxi.mougblog.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * 妹子订单表 Mapper 接口
 *
 * @author Vergift
 * @since 2021-12-29
 */
public interface GirlOrderMapper extends SuperMapper<GirlOrder> {

    @Select("SELECT " +
            " g.uid,g.order_id,g.third_order_id,g.amount,g.user_id, " +
            " g.user_nick,g.girl_uid,g.girl_name,g.pay_count,g.pay_status, " +
            " g.customer_id,g.pay_time, " +
            " CONVERT_TZ(FROM_UNIXTIME(g.service_time/1000,'%Y-%m-%d %H:%i:%s'), 'SYSTEM','+08:00')  as service_time, " +
            " g.service_type,g.status,g.off_price, " +
            " g.create_time,g.update_time,c.vip_level,c.vip_type,c.source as source, " +
            " (select name from t_girl_project where source =c.source) as source_name " +
            "FROM t_girl_order g " +
            "left join t_customers c on g.customer_id=c.customer_id  ${ew.customSqlSegment}")
    IPage<GirlOrder> getPage(Page<GirlOrder> page, @Param(Constants.WRAPPER) QueryWrapper queryWrapper);

    @Select("SELECT " +
            " count(1) as count ,sum(g.amount) as sum " +
            "FROM t_girl_order g " +
            "left join t_customers c on g.customer_id=c.customer_id  ${ew.customSqlSegment}")
    Map<String, Object> getGirlOrderCountSum(@Param(Constants.WRAPPER) QueryWrapper queryWrapper);
}
