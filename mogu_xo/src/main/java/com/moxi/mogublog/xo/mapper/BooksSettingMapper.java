package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.BooksSetting;
import com.moxi.mougblog.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;


public interface BooksSettingMapper extends SuperMapper<BooksSetting> {
    @Select(" select value from t_books_setting where app_key =#{appKey}  and status =1")
    Integer queryFee(@Param("appKey") String appKey);
}
