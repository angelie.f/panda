package com.moxi.mogublog.xo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.moxi.mogublog.commons.entity.GameCreditLogs;
import com.moxi.mogublog.xo.vo.GameCreditLogsVO;
import com.moxi.mougblog.base.service.SuperService;

import java.util.List;


public interface GameCreditLogsService extends SuperService<GameCreditLogs> {
    IPage<GameCreditLogs> getPageList(GameCreditLogsVO gameCreditLogsVO);
}
