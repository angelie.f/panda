package com.moxi.mogublog.xo.vo;

import lombok.Data;
import lombok.ToString;

/**
 * 刷新用户信息参数
 *
 * @author Vergift
 * @date 2021年12月22日18:01:38
 */
@ToString
@Data
public class CustomersRefreshVO {

    /**
     * 用户id
     */
    private String customerId;

    private String platform;

    /**
     * 注册接口接收到的token
     */
    private String token;

    /**
     * 注册接口接收到的appToken
     */
    private String appToken;

    /**
     * 签名
     */
    private String sign;

    private Integer source;
}
