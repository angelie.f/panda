package com.moxi.mogublog.xo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.Comment;
import com.moxi.mogublog.commons.entity.Customers;
import com.moxi.mogublog.commons.entity.GirlGift;
import com.moxi.mogublog.commons.entity.GirlOrder;
import com.moxi.mogublog.utils.StringUtils;
import com.moxi.mogublog.xo.global.SQLConf;
import com.moxi.mogublog.xo.mapper.CustomersMapper;
import com.moxi.mogublog.xo.mapper.GirlGiftMapper;
import com.moxi.mogublog.xo.service.GirlGiftService;
import com.moxi.mogublog.xo.vo.GirlGiftVO;
import com.moxi.mougblog.base.serviceImpl.SuperServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 妹子订单评论表 服务实现类
 *
 * @author 陌溪
 * @since 2021-12-29
 */
@Service
@Slf4j
public class GirlGiftServiceImpl extends SuperServiceImpl<GirlGiftMapper, GirlGift> implements GirlGiftService {
    @Autowired
    private GirlGiftService girlGiftService;

    @Resource
    private CustomersMapper customersMapper;

    @Override
    public IPage<GirlGift> getPageList(GirlGiftVO girlGiftVO) {
        log.info("girlGift params:{}",girlGiftVO.toString());
        QueryWrapper<GirlGift> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(girlGiftVO.getUserId())) {
            QueryWrapper<Customers> customersQueryWrapper = new QueryWrapper<>();
            customersQueryWrapper.eq(SQLConf.UID,girlGiftVO.getUserId());
            Customers customers = customersMapper.selectOne(customersQueryWrapper);
            if(customers!=null){
                log.info("girlGift customers:{}",customers.toString());
                queryWrapper.eq(SQLConf.CUSTOMER_ID,customers.getCustomerId());
            }
        }
        if (StringUtils.isNotEmpty(girlGiftVO.getReceiveName()) && !StringUtils.isEmpty(girlGiftVO.getReceiveName())) {
            queryWrapper.like("receive_name", girlGiftVO.getReceiveName().trim());
        }
        if (StringUtils.isNotEmpty(girlGiftVO.getBeginCreateTime()) && !StringUtils.isEmpty(girlGiftVO.getBeginCreateTime())) {
            queryWrapper.ge(SQLConf.CREATE_TIME, girlGiftVO.getBeginCreateTime().trim());
        }
        if (StringUtils.isNotEmpty(girlGiftVO.getEndCreateTime()) && !StringUtils.isEmpty(girlGiftVO.getEndCreateTime())){
            queryWrapper.le(SQLConf.CREATE_TIME, girlGiftVO.getEndCreateTime().trim());
        }
        queryWrapper.orderByDesc(SQLConf.UPDATE_TIME);

        Page<GirlGift> page = new Page<>();
        page.setCurrent(girlGiftVO.getCurrentPage());
        page.setSize(girlGiftVO.getPageSize());
        if(StringUtils.isNotEmpty(girlGiftVO.getOrderByAscColumn())) {
            // 将驼峰转换成下划线
            String column = StringUtils.underLine(new StringBuffer(girlGiftVO.getOrderByAscColumn())).toString();
            queryWrapper.orderByAsc(column);
        } else if(StringUtils.isNotEmpty(girlGiftVO.getOrderByDescColumn())) {
            // 将驼峰转换成下划线
            String column = StringUtils.underLine(new StringBuffer(girlGiftVO.getOrderByDescColumn())).toString();
            queryWrapper.orderByDesc(column);
        }
        IPage<GirlGift> pageList = girlGiftService.page(page, queryWrapper);
        List<GirlGift> list = pageList.getRecords();
        list.forEach(item -> {
            QueryWrapper<Customers> customersQueryWrapper = new QueryWrapper<>();
            customersQueryWrapper.eq(SQLConf.CUSTOMER_ID,item.getCustomerId());
            Customers customers = customersMapper.selectOne(customersQueryWrapper);
            if(customers!=null){
                item.setUserId(customers.getUid());
            }
            if(item.getReceiveNumber()!=null&&girlGiftVO.isPhoneHide()) {
                item.setReceiveNumber(StringUtils.mask(item.getReceiveNumber(), 0, item.getReceiveNumber().length() - 4, '*'));
            }
            log.info("list GirlGift item:{}", item.toString());
        });
        return pageList;
    }
}
