package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.GirlSortItem;
import com.moxi.mougblog.base.mapper.SuperMapper;

/**
 * 妹子上下架表 Mapper 接口
 *
 * @author Vergift
 * @since 2021-12-29
 */
public interface GirlSortItemMapper extends SuperMapper<GirlSortItem> {

}
