package com.moxi.mogublog.xo.vo;

import lombok.Data;
import lombok.ToString;

/**
 * 用户查询参数类
 *
 * @author Vergift
 * @date 2021年12月02日17:07:38
 */
@ToString
@Data
public class CustomersVO {

    /**
     * 用户id
     */
    private String customerId;

    /**
     * 第三方用户id
     */
    private String productId;

    /**
     * 注册来源：1.青青草 2.小黄鸭
     */
    private Integer source;

    /**
     * 第三方code
     */
    private String openCode;

    /**
     * 分类uid
     */
    private String sortUid;

    /**
     * 图集uid
     */
    private String atlasUid;

    /**
     * 标签uid
     */
    private String tagUid;

    /**
     * 签名
     */
    private String sign;

    /**
     * 访问请求终端 H5
     */
    private String endPoint;

    /**
     * 当前页
     */
    private Long currentPage;

    /**
     * 页大小
     */
    private Long pageSize;

}
