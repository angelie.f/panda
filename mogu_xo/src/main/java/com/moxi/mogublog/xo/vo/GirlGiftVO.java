package com.moxi.mogublog.xo.vo;

import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;

import java.math.BigDecimal;

/**
 * GirlOrderVO
 *
 * @author Vergift
 * @since 2022-01-07
 */
@Data
public class GirlGiftVO extends BaseVO<GirlGiftVO> {

    /**
     * OrderBy排序字段（desc: 降序）
     */
    private String orderByDescColumn;

    /**
     * OrderBy排序字段（asc: 升序）
     */
    private String orderByAscColumn;

    private String orderId;

    private String customerId;

    private String userId;

    private String receiveName;

    private String receiveNumber;

    private String receiveAddress;



    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 创建时间-开始
     */
    private String beginCreateTime;

    /**
     * 创建时间-结束
     */
    private String endCreateTime;

    /**
     * 更新时间
     */
    private String updateTime;
    /**
     * 更新时间-开始
     */
    private String beginUpdateTime;

    /**
     * 更新时间-结束
     */
    private String endUpdateTime;

    private boolean phoneHide;
}
