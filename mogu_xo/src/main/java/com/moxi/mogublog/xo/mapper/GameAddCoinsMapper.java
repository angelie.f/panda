package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.GameAddCoins;
import com.moxi.mougblog.base.mapper.SuperMapper;

public interface GameAddCoinsMapper extends SuperMapper<GameAddCoins> {

}
