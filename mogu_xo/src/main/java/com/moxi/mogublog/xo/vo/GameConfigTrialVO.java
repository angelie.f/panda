package com.moxi.mogublog.xo.vo;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class GameConfigTrialVO {

    private String source;

    private Integer status;

}
