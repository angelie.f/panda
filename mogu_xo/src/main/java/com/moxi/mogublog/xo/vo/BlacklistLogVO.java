package com.moxi.mogublog.xo.vo;

import com.moxi.mougblog.base.vo.BaseVO;
import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class BlacklistLogVO extends BaseVO<BlacklistLogVO> {

    private Integer valiUid;
    private String userName;
    private String operation;

}
