package com.moxi.mogublog.xo.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.moxi.mogublog.commons.entity.GameDepositAward;
import com.moxi.mougblog.base.mapper.SuperMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface GameDepositAwardMapper extends SuperMapper<GameDepositAward> {

    @Select("select a.period,a.customer_id,a.award,c.uid as wuid,c.open_id,a.first_deposit_num,a.source, " +
            "  (select name from t_panda_project where source =a.source) as source_name " +
            "   from t_game_deposit_award a " +
            " left join t_customers c on a.customer_id =c.customer_id " +
            " ${ew.customSqlSegment}")
    IPage<GameDepositAward> getPage(Page<GameDepositAward> page, @Param(Constants.WRAPPER) QueryWrapper queryWrapper);

    @Select("select a.period,a.customer_id,a.award,c.uid as wuid,c.open_id from t_game_deposit_award a " +
            "  left join t_customers c on a.customer_id =c.customer_id " +
            " where c.status =1 and a.period =#{period}")
    GameDepositAward queryAward(@Param("period") String period);


    @Select("select a.period,a.customer_id,a.award,c.uid as wuid,c.open_id from t_game_deposit_award a" +
            " left join t_customers c on a.customer_id =c.customer_id " +
            " where c.status =1 order by a.period desc limit 7 ")
    List<GameDepositAward> queryRecent7Award();

    @Select("select count(1) from t_game_deposit_award " +
            " where customer_id=#{customerId}" )
    Integer isAwardBefore(@Param("customerId") String customerId);
}
