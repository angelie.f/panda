package com.moxi.mogublog.xo.mapper;

import com.moxi.mogublog.commons.entity.PictureAtlas;
import com.moxi.mougblog.base.mapper.SuperMapper;

/**
 * 图集表 Mapper 接口
 *
 * @author 陌溪
 * @since 2018年9月17日16:16:35
 */
public interface PictureAtlasMapper extends SuperMapper<PictureAtlas> {

}
